/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;

    export interface IProjectEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents {
        setOnProjectCreate($handler : IProjectEventsHandler) : void;

        setOnProjectLoad($handler : IProjectEventsHandler) : void;

        setOnProjectSettingsChange($handler : ISettingsPanelEventsHandler) : void;

        setOnPathRequest($handler : IEventsHandler) : void;

        setOnProjectExport($handler : IEventsHandler) : void;
    }
}
