/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;

    /**
     * ProjectEventArgs class provides args connected with change of project settings.
     */
    export class ProjectEventArgs extends EventArgs {
        private descriptor : IProjectSettingsValue;

        /**
         * @param {IProjectSettingsValue} [$value] Set value representing project descriptor.
         * @return {IProjectSettingsValue} Returns value representing project descriptor.
         */
        public Descriptor($value? : IProjectSettingsValue) : IProjectSettingsValue {
            if (ObjectValidator.IsSet($value)) {
                this.descriptor = $value;
            }
            return this.descriptor;
        }
    }
}
