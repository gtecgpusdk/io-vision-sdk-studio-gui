/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import LayerSelectorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.LayerSelectorPanel;
    import LayerSelectorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.LayerSelectorPanelViewerArgs;

    export class LayerSelectorPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : LayerSelectorPanelViewerArgs {
            const args : LayerSelectorPanelViewerArgs = new LayerSelectorPanelViewerArgs();
            args.PropertyViewSelectorText("Property view");
            args.HierarchyViewSelectorText("Hierarchy view");
            args.MixedViewSelectorText("Mixed view");
            return args;
        }

        /* dev:end */

        constructor($args? : LayerSelectorPanelViewerArgs) {
            super($args);
            this.setInstance(new LayerSelectorPanel());
        }

        public getInstance() : LayerSelectorPanel {
            return <LayerSelectorPanel>super.getInstance();
        }

        public ViewerArgs($args? : LayerSelectorPanelViewerArgs) : LayerSelectorPanelViewerArgs {
            return <LayerSelectorPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : LayerSelectorPanel, $args : LayerSelectorPanelViewerArgs) : void {
            $instance.propertyViewSelector.Title().Text($args.PropertyViewSelectorText());
            $instance.hierarchyViewSelector.Title().Text($args.HierarchyViewSelectorText());
            $instance.mixedViewSelector.Title().Text($args.MixedViewSelectorText());
        }

        /* dev:start */
        protected testImplementation($instance : LayerSelectorPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(450);
            $instance.Height(34);
            return "<style>.TestCss .PropertiesLayerSelectorPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
