/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class LayerSelectorPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private propertyViewSelectorText : string;
        private hierarchyViewSelectorText : string;
        private mixedViewSelectorText : string;

        constructor() {
            super();
            this.propertyViewSelectorText = "";
            this.hierarchyViewSelectorText = "";
            this.mixedViewSelectorText = "";
        }

        public PropertyViewSelectorText($value? : string) : string {
            return this.propertyViewSelectorText = Property.String(this.propertyViewSelectorText, $value);
        }

        public HierarchyViewSelectorText($value? : string) : string {
            return this.hierarchyViewSelectorText = Property.String(this.hierarchyViewSelectorText, $value);
        }

        public MixedViewSelectorText($value? : string) : string {
            return this.mixedViewSelectorText = Property.String(this.mixedViewSelectorText, $value);
        }
    }
}
