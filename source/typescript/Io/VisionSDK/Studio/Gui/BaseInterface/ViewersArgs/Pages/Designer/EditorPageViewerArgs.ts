/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import AppWizardPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.AppWizardPanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DirectoryBrowserPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.DirectoryBrowserPanelViewerArgs;
    import KernelNodeHelpPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.KernelNodeHelpPanelViewerArgs;
    import ProjectPickerPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectPickerPanelViewerArgs;
    import ExportPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ExportPanelViewerArgs;
    import SettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.SettingsPanelViewerArgs;
    import ConnectionSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ConnectionSettingsPanelViewerArgs;
    import ProjectCreationPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectCreationPanelViewerArgs;

    export class EditorPageViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private editorPanelViewerArgs : EditorPanelViewerArgs;
        private appWizardPanelViewerArgs : AppWizardPanelViewerArgs;
        private directoryBrowserDialogArgs : DirectoryBrowserPanelViewerArgs;
        private kernelNodeHelpDialogArgs : KernelNodeHelpPanelViewerArgs;
        private connectionSettingsDialogArgs : ConnectionSettingsPanelViewerArgs;
        private projectPickerDialogArgs : ProjectPickerPanelViewerArgs;
        private settingsPanelArgs : SettingsPanelViewerArgs;
        private exportDialogArgs : ExportPanelViewerArgs;
        private projectCreationDialogArgs : ProjectCreationPanelViewerArgs;

        constructor() {
            super();

            this.editorPanelViewerArgs = new EditorPanelViewerArgs();
            this.appWizardPanelViewerArgs = new AppWizardPanelViewerArgs();
            this.directoryBrowserDialogArgs = new DirectoryBrowserPanelViewerArgs();
            this.kernelNodeHelpDialogArgs = new KernelNodeHelpPanelViewerArgs();
            this.connectionSettingsDialogArgs = new ConnectionSettingsPanelViewerArgs();
            this.projectPickerDialogArgs = new ProjectPickerPanelViewerArgs();
            this.settingsPanelArgs = new SettingsPanelViewerArgs();
            this.exportDialogArgs = new ExportPanelViewerArgs();
            this.projectCreationDialogArgs = new ProjectCreationPanelViewerArgs();
        }

        public EditorPanelViewerArgs($value? : EditorPanelViewerArgs) : EditorPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(EditorPanelViewerArgs)) {
                this.editorPanelViewerArgs = $value;
            }
            return this.editorPanelViewerArgs;
        }

        public AppWizardPanelViewerArgs($value? : AppWizardPanelViewerArgs) : AppWizardPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(AppWizardPanelViewerArgs)) {
                this.appWizardPanelViewerArgs = $value;
            }
            return this.appWizardPanelViewerArgs;
        }

        public DirectoryBrowserDialogArgs($value? : DirectoryBrowserPanelViewerArgs) : DirectoryBrowserPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(DirectoryBrowserPanelViewerArgs)) {
                this.directoryBrowserDialogArgs = $value;
            }
            return this.directoryBrowserDialogArgs;
        }

        public KernelNodeHelpDialogArgs($value? : KernelNodeHelpPanelViewerArgs) : KernelNodeHelpPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(KernelNodeHelpPanelViewerArgs)) {
                this.kernelNodeHelpDialogArgs = $value;
            }
            return this.kernelNodeHelpDialogArgs;
        }

        public ProjectPickerDialogArgs($value? : ProjectPickerPanelViewerArgs) : ProjectPickerPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ProjectPickerPanelViewerArgs)) {
                this.projectPickerDialogArgs = $value;
            }
            return this.projectPickerDialogArgs;
        }

        public ProjectCreationDialogArgs($value? : ProjectCreationPanelViewerArgs) : ProjectCreationPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ProjectCreationPanelViewerArgs)) {
                this.projectCreationDialogArgs = $value;
            }
            return this.projectCreationDialogArgs;
        }

        public SettingsPanelArgs($value? : SettingsPanelViewerArgs) : SettingsPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(SettingsPanelViewerArgs)) {
                this.settingsPanelArgs = $value;
            }
            return this.settingsPanelArgs;
        }

        public ExportDialogArgs($value? : ExportPanelViewerArgs) : ExportPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ExportPanelViewerArgs)) {
                this.exportDialogArgs = $value;
            }
            return this.exportDialogArgs;
        }
    }
}
