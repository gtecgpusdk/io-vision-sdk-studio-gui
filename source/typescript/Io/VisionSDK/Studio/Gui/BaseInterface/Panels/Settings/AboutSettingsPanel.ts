/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import AboutSettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.AboutSettingsPanelViewerArgs;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;

    export class AboutSettingsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerText : string;
        public versionLabel : Label;
        public versionContents : Label;
        public textContents : Label;
        public panelDescription : string;
        private readonly textPanel : BasePanel;

        constructor($id? : string) {
            super($id);

            this.headerText = "";
            this.versionLabel = new Label();
            this.versionContents = new Label();
            this.textPanel = new BasePanel();
            this.textContents = new Label();
            this.panelDescription = "";
            this.textPanel.AddChild(this.textContents);
        }

        public Value($value? : AboutSettingsPanelViewerArgs) : AboutSettingsPanelViewerArgs {
            return <AboutSettingsPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.textPanel.Scrollable(true);
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .FitToParent(FitToParent.FULL)
                .Alignment(Alignment.CENTER_PROPAGATED)
                .Add(this.addRow().HeightOfRow(120, UnitType.PX)
                    .Add(this.addColumn())
                    .Add(this.versionLabel)
                    .Add(this.versionContents))
                .Add(this.addRow()
                    .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT)
                        .Add(this.textPanel)));
        }
    }
}
