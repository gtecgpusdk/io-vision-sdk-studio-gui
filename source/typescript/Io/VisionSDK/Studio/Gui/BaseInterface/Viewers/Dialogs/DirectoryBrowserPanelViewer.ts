/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DirectoryBrowserPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.DirectoryBrowserPanelViewerArgs;
    import DirectoryBrowserPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.DirectoryBrowserPanel;
    import WuiBuilderConnector = Com.Wui.Framework.Commons.Connectors.WuiBuilderConnector;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;

    export class DirectoryBrowserPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DirectoryBrowserPanelViewerArgs {
            const args : DirectoryBrowserPanelViewerArgs = new DirectoryBrowserPanelViewerArgs();
            args.SaveButtonText("OK");
            args.CloseButtonText("Storno");
            args.PathValue("C:/");

            return args;
        }

        /* dev:end */

        constructor($args? : DirectoryBrowserPanelViewerArgs) {
            super($args);
            this.setInstance(new DirectoryBrowserPanel());
        }

        public getInstance() : DirectoryBrowserPanel {
            return <DirectoryBrowserPanel>super.getInstance();
        }

        public ViewerArgs($args? : DirectoryBrowserPanelViewerArgs) : DirectoryBrowserPanelViewerArgs {
            return <DirectoryBrowserPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DirectoryBrowserPanel, $args : DirectoryBrowserPanelViewerArgs) : void {
            $instance.saveButton.Text($args.SaveButtonText());
            $instance.closeButton.Text($args.CloseButtonText());
            $instance.pathValue.Value($args.PathValue());
            $instance.browser.Value($args.PathValue());
        }

        /* dev:start */
        protected testImplementation($instance : DirectoryBrowserPanel) : string {
            $instance.StyleClassName("TestCss");

            const getDefaultStrucuture : any = () : IFileSystemItemProtocol[] => {
                return <IFileSystemItemProtocol[]>[
                    {
                        map : <IFileSystemItemProtocol[]>[
                            {
                                attributes: [],
                                map       : [],
                                name      : "C:/Program Files/NXP/GPU_SDK/CodeGenerator",
                                type      : FileSystemItemType.DIRECTORY,
                                value     : "C:/Program Files/NXP/GPU_SDK/CodeGenerator"
                            }
                        ],
                        name: "Recent",
                        type: FileSystemItemType.RECENT
                    }
                ];
            };
            $instance.browser.setStructure(getDefaultStrucuture());

            const connector : WuiBuilderConnector = WuiBuilderConnector.Connect();
            $instance.browser.getEvents().setOnPathRequest(() : void => {
                connector.Send("FileSystem", {
                    type : "getPath",
                    value: $instance.browser.Value()
                }, ($data : any) : void => {
                    const structure : any = getDefaultStrucuture();
                    JSON.parse(ObjectDecoder.Base64($data.data)).forEach(($group : any) : void => {
                        $group.forEach(($item : any) : void => {
                            structure.push($item);
                        });
                    });
                    $instance.browser.setStructure(structure);
                });
            });

            $instance.browser.getEvents().setOnDirectoryRequest(() : void => {
                connector.Send("FileSystem", {
                    type : "getDirectory",
                    value: $instance.browser.Value()
                }, ($data : any) : void => {
                    $data = JSON.parse(ObjectDecoder.Base64($data.data));
                    $instance.browser.setStructure($data[0], $instance.browser.Value());
                });
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .DirectoryBrowserPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
