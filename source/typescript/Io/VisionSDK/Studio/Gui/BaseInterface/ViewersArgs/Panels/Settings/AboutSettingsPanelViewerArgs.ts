/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class AboutSettingsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private versionText : string;
        private versionContentsText : string;
        private textContentsText : string;
        private panelDescription : string;

        constructor() {
            super();
            this.headerText = "";
            this.versionText = "";
            this.versionContentsText = "";
            this.textContentsText = "";
            this.panelDescription = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public VersionText($value? : string) : string {
            return this.versionText = Property.String(this.versionText, $value);
        }

        public VersionContentsText($value? : string) : string {
            return this.versionContentsText = Property.String(this.versionContentsText, $value);
        }

        public TextContentsText($value? : string) : string {
            return this.textContentsText = Property.String(this.textContentsText, $value);
        }

        public PanelDescription($value? : string) : string {
            return this.panelDescription = ObjectValidator.IsSet($value) ? $value : this.panelDescription;
        }
    }
}
