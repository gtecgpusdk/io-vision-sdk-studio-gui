/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IGraphChange = Io.VisionSDK.Studio.Gui.Interfaces.IGraphChange;
    import INodeChange = Io.VisionSDK.Studio.Gui.Interfaces.INodeChange;
    import IAttributeChange = Io.VisionSDK.Studio.Gui.Interfaces.IAttributeChange;
    import IConnectionChange = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionChange;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import ConnectionManager = Io.VisionSDK.UserControls.Utils.ConnectionManager;
    import GridManager = Io.VisionSDK.UserControls.Utils.GridManager;
    import IConnectionStack = Io.VisionSDK.UserControls.Interfaces.IConnectionStack;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import StateEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.StateEventArgs;
    import IIndexChange = Io.VisionSDK.Studio.Gui.Interfaces.IIndexChange;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import KernelNodeRegistry = Io.VisionSDK.UserControls.Utils.KernelNodeRegistry;
    import StateUtils = Io.VisionSDK.UserControls.Utils.StateUtils;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import DragNodeType = Io.VisionSDK.UserControls.Enums.DragNodeType;
    import GraphChangeType = Io.VisionSDK.Studio.Gui.Enums.GraphChangeType;
    import IHistoryCache = Io.VisionSDK.Studio.Gui.Interfaces.IHistoryCache;
    import InterfaceDataType = Io.VisionSDK.UserControls.Enums.InterfaceDataType;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import Size = Com.Wui.Framework.Gui.Structures.Size;

    export class HistoryManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static readonly resizeAnimMs : number = 300;
        private historyCache : IHistoryCache;
        private readonly maxCount : number;
        private gridManager : GridManager;
        private connectionManager : ConnectionManager;
        private readonly rootSwapHandler : ($rootName : string, $callback : any) => void;
        private nodeRegistry : KernelNodeRegistry;
        private queuedChangeHandler : () => void;
        private onStateChangeHandler : () => void;
        private isComplete : boolean;

        constructor($gridManager : GridManager, $connectionManager : ConnectionManager, $nodeRegistry : KernelNodeRegistry,
                    $rootSwapHandler : ($rootName : string, $callback : any) => void) {
            super();
            this.maxCount = 1000;
            this.historyCache = new IHistoryCache();
            this.gridManager = $gridManager;
            this.connectionManager = $connectionManager;
            this.nodeRegistry = $nodeRegistry;
            this.rootSwapHandler = $rootSwapHandler;
            this.queuedChangeHandler = null;
            this.isComplete = true;
            this.onStateChangeHandler = () : void => {
                // default handler
            };
        }

        public setOnStateChange($value : () => void) : void {
            this.onStateChangeHandler = $value;
        }

        public HistoryCache($value? : IHistoryCache) : IHistoryCache {
            if (ObjectValidator.IsSet($value)) {
                this.historyCache = $value;
            }
            return this.historyCache;
        }

        public IsUndoable() : boolean {
            return !(ObjectValidator.IsEmptyOrNull(this.historyCache) || ObjectValidator.IsEmptyOrNull(this.historyCache.graphChanges) ||
                this.historyCache.index === -1);
        }

        public IsRedoable() : boolean {
            return !(ObjectValidator.IsEmptyOrNull(this.historyCache) || ObjectValidator.IsEmptyOrNull(this.historyCache.graphChanges) ||
                this.historyCache.index === this.historyCache.graphChanges.length - 1);
        }

        public IsComplete() : boolean {
            return this.isComplete;
        }

        public IsForceable() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.queuedChangeHandler);
        }

        public getCurrentRoot() : string {
            return ObjectValidator.IsEmptyOrNull(this.getAppliedChange()) ? null : this.getAppliedChange().root;
        }

        public getCurrentSelectedNodesIds() : string[] {
            return ObjectValidator.IsEmptyOrNull(this.getAppliedChange()) ? null : this.getAppliedChange().selectedNodesIds;
        }

        public ForceChangeApply() : void {
            if (ObjectValidator.IsFunction(this.queuedChangeHandler)) {
                this.queuedChangeHandler();
            }
        }

        public Push($args : StateEventArgs) : void {
            const change : IGraphChange = this.getChange($args);

            const nextIndex : number = this.historyCache.index + 1;
            const appliedChange : IGraphChange = this.getAppliedChange();
            if (ObjectValidator.IsEmptyOrNull(appliedChange) || JSON.stringify(appliedChange) !== JSON.stringify(change)) {
                const overflow : number = this.historyCache.graphChanges.length - nextIndex;
                if (overflow > 0) {
                    this.historyCache.graphChanges
                        = this.historyCache.graphChanges.slice(0, nextIndex);
                }
                this.historyCache.graphChanges.push(change);
                this.historyCache.index = this.historyCache.graphChanges.length - 1;

                const lengthOverflow : number = this.historyCache.graphChanges.length - this.maxCount;
                if (lengthOverflow > 0) {
                    this.historyCache.graphChanges
                        = this.historyCache.graphChanges.slice(lengthOverflow);
                    this.historyCache.index = this.maxCount - 1;
                }
            }
        }

        public Undo() : void {
            if (this.IsComplete() && this.IsUndoable()) {
                const change : IGraphChange = JSON.parse(JSON.stringify(this.historyCache.graphChanges[this.historyCache.index]));
                this.invertChangeType(change);
                if (this.isChangeApplicable(change)) {
                    this.invertChange(change);
                    this.historyCache.index -= 1;
                } else {
                    change.type = GraphChangeType.SELECT;
                    change.nodeChanges = [];
                }
                this.applyChange(change);
            }
        }

        public Redo() : void {
            if (this.IsComplete() && this.IsRedoable()) {
                const targetIndex : number = this.historyCache.index + 1;
                const change : IGraphChange = JSON.parse(JSON.stringify(this.historyCache.graphChanges[targetIndex]));
                if (this.isChangeApplicable(change)) {
                    this.historyCache.index = targetIndex;
                } else {
                    change.type = GraphChangeType.SELECT;
                    change.nodeChanges = [];
                }
                this.applyChange(change);
            }
        }

        private isChangeApplicable($change : IGraphChange) : boolean {
            const selectedNodesIds : string[] = this.gridManager.getSelectedNodesIds();
            const selectedConnectionsIds : IConnectionChange[]
                = StateUtils.getConnectionChanges(this.connectionManager.getSelectedConnections()).ToArray();
            const rootId : string = this.gridManager.Root().UniqueId();
            const selectedConsIdentity : string = JSON.stringify(selectedConnectionsIds);
            return $change.root === rootId &&
                ($change.type === GraphChangeType.CREATE ||
                    $change.type === GraphChangeType.ROOT_INTERFACE_CREATE ||
                    $change.type === GraphChangeType.ROOT_INTERFACE_REMOVE ||
                    $change.type === GraphChangeType.RESIZE ||
                    JSON.stringify(selectedNodesIds) === JSON.stringify($change.selectedNodesIds)
                    && (selectedConsIdentity === JSON.stringify($change.connectionChanges)
                        || $change.type === GraphChangeType.MOVE
                        || $change.type === GraphChangeType.ATTRIBUTE_UPDATE
                        || $change.type === GraphChangeType.CONNECTION_CREATE
                        || $change.type === GraphChangeType.CONNECTION_MOVE
                        && (selectedConsIdentity === JSON.stringify([$change.connectionChanges[0]])
                            || selectedConsIdentity === JSON.stringify([$change.connectionChanges[1]]))));
        }

        private getAppliedChange() : IGraphChange {
            if (!ObjectValidator.IsEmptyOrNull(this.historyCache.graphChanges) &&
                this.historyCache.graphChanges.indexOf(<any>this.historyCache.index) !== -1) {
                return this.historyCache.graphChanges[this.historyCache.index];
            }
            return null;
        }

        private getChange($args : StateEventArgs) : IGraphChange {
            const changeType : GraphChangeType = $args.getChangeType();
            const change : IGraphChange = new IGraphChange();
            change.type = changeType;
            change.root = this.gridManager.Root().UniqueId();
            const selectedNodesIdsList : ArrayList<string> = ArrayList.ToArrayList(changeType === GraphChangeType.REMOVE ?
                $args.getSelectedNodesIds() : this.gridManager.getSelectedNodesIds());
            change.selectedNodesIds = selectedNodesIdsList.ToArray();
            change.nodeChanges = changeType === GraphChangeType.MOVE ?
                [] : StateUtils.mergeNodeChanges($args.getNodeChanges(), StateUtils.getNodeChanges(this.gridManager.getSelectedNodes(),
                    changeType === GraphChangeType.CREATE || changeType === GraphChangeType.REMOVE,
                    changeType === GraphChangeType.ATTRIBUTE_UPDATE || changeType === GraphChangeType.CONNECTION_CREATE)).ToArray();
            change.positionChange = $args.getPositionChange();
            if (ObjectValidator.IsEmptyOrNull(change.positionChange)) {
                change.positionChange = undefined;
            }
            change.sizeChange = $args.getSizeChange();
            if (ObjectValidator.IsEmptyOrNull(change.sizeChange)) {
                change.sizeChange = undefined;
            }

            let connectionChanges : ArrayList<IConnectionChange> = new ArrayList<IConnectionChange>();
            switch (changeType) {
            case GraphChangeType.CREATE:
                connectionChanges = StateUtils.getNodeConnectionChanges(this.gridManager.getSelectedNodes());
                break;
            case GraphChangeType.CONNECTION_CREATE:
                connectionChanges = StateUtils.getConnectionChanges(this.connectionManager.getSelectedConnections());
                break;
            case GraphChangeType.CONNECTION_MOVE:
            case GraphChangeType.REMOVE:
                connectionChanges = $args.getConnectionChanges();
                break;
            case GraphChangeType.ROOT_INTERFACE_CREATE:
            case GraphChangeType.ROOT_INTERFACE_REMOVE:
                change.rootChange = $args.getIndexChange();
                const root : IKernelNode = this.gridManager.Root();
                if (change.rootChange.interfaceType === KernelNodeInterfaceType.OUTPUT) {
                    const $input : Input = root.getInput(change.rootChange.toIndex);
                    $input.innerOutput.inputs.foreach(($input : Input) : void => {
                        const change : IConnectionChange = StateUtils.getConnectionChange($input, $input.output);
                        connectionChanges.Add(change, JSON.stringify(change));
                    });
                } else if (change.rootChange.interfaceType === KernelNodeInterfaceType.INPUT) {
                    const $output : Output = root.getOutput(change.rootChange.toIndex);
                    if (!ObjectValidator.IsEmptyOrNull($output.innerInput.output)) {
                        const change : IConnectionChange = StateUtils.getConnectionChange($output.innerInput, $output.innerInput.output);
                        connectionChanges.Add(change, JSON.stringify(change));
                    }
                }
                break;
            }
            change.connectionChanges = connectionChanges.ToArray();
            return change;
        }

        private invertChangeType($change : IGraphChange) : void {
            if ($change.type === GraphChangeType.CREATE) {
                $change.type = GraphChangeType.REMOVE;
            } else if ($change.type === GraphChangeType.REMOVE) {
                $change.type = GraphChangeType.CREATE;
            } else if ($change.type === GraphChangeType.ROOT_INTERFACE_CREATE) {
                $change.type = GraphChangeType.ROOT_INTERFACE_REMOVE;
            } else if ($change.type === GraphChangeType.ROOT_INTERFACE_REMOVE) {
                $change.type = GraphChangeType.ROOT_INTERFACE_CREATE;
            } else if ($change.type === GraphChangeType.CONNECTION_CREATE) {
                $change.type = GraphChangeType.CONNECTION_REMOVE;
            }
        }

        private invertChange($change : IGraphChange) : void {
            const swapChange : any = ($change : INodeChange) : void => {
                $change.attributeChanges.forEach(($change : IAttributeChange) : void => {
                    const tmp : any = $change.to;
                    $change.to = $change.from;
                    $change.from = tmp;
                });
                const tmp : any = $change.nameChange.to;
                $change.nameChange.to = $change.nameChange.from;
                $change.nameChange.from = tmp;
                $change.subnodeChanges.forEach(swapChange);
            };
            $change.nodeChanges.forEach(swapChange);
            if (!ObjectValidator.IsEmptyOrNull($change.positionChange)) {
                $change.positionChange.offsetLeft = -$change.positionChange.offsetLeft;
                $change.positionChange.offsetTop = -$change.positionChange.offsetTop;
            }
            if (!ObjectValidator.IsEmptyOrNull($change.rootChange)) {
                const tmp : any = $change.rootChange.toIndex;
                $change.rootChange.toIndex = $change.rootChange.fromIndex;
                $change.rootChange.fromIndex = tmp;
            }
            if (!ObjectValidator.IsEmptyOrNull($change.sizeChange)) {
                $change.sizeChange.directionHorizontal = -$change.sizeChange.directionHorizontal;
                $change.sizeChange.directionVertical = -$change.sizeChange.directionVertical;
            }
        }

        private getNodesFromChanges($changes : INodeChange[]) : IKernelNode[] {
            const nodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();

            $changes.forEach(($change : INodeChange) : void => {
                const node : IKernelNode = this.gridManager.getNodeFromId($change.id);
                if (ObjectValidator.IsEmptyOrNull(node)) {
                    const createdNode : IKernelNode = KernelNode.TypeToNodeMapper()($change.type);
                    createdNode.Name($change.nameChange.to);
                    createdNode.UniqueId($change.id);
                    createdNode.Row($change.row);
                    createdNode.Column($change.column);
                    createdNode.ChildNodes(new ArrayList<IKernelNode>());
                    if (!ObjectValidator.IsEmptyOrNull($change.attributeChanges)) {
                        $change.attributeChanges.forEach(($change : IAttributeChange) : void => {
                            if (!ObjectValidator.IsEmptyOrNull(createdNode.getAttributes().getItem($change.name))) {
                                createdNode.setAttributeValue($change.name, $change.to);
                            }
                        });
                    }
                    this.getNodesFromChanges($change.subnodeChanges).forEach(($childNode : IKernelNode) : void => {
                        createdNode.ChildNodes().Add($childNode);
                        $childNode.ParentNode(createdNode);
                    });
                    nodes.Add(createdNode);
                }
            });
            this.nodeRegistry.AssignIdentity(nodes);
            return nodes.ToArray();
        }

        private applyChange($change : IGraphChange) : void {
            this.isComplete = false;
            const handleConnectionChange : any = ($connectionChange : IConnectionChange) : void => {
                const inputNode : IKernelNode = this.nodeRegistry.getNodeByUniqueId($connectionChange.inputNodeId);
                const outputNode : IKernelNode = this.nodeRegistry.getNodeByUniqueId($connectionChange.outputNodeId);

                let isRemoved : boolean = false;
                if (!ObjectValidator.IsEmptyOrNull(inputNode) && !ObjectValidator.IsEmptyOrNull(outputNode)) {
                    const input : Input = inputNode === outputNode.ParentNode() ?
                        inputNode.getOutput($connectionChange.inputNodeIndex).innerInput :
                        inputNode.getInput($connectionChange.inputNodeIndex);
                    const output : Output = outputNode === inputNode.ParentNode() ?
                        outputNode.getInput($connectionChange.outputNodeIndex).innerOutput :
                        outputNode.getOutput($connectionChange.outputNodeIndex);

                    if (ObjectValidator.IsEmptyOrNull(input.output)) {
                        input.output = output;
                        output.inputs.Add(input);
                        if (input.node.IsLoaded() || output.node.IsLoaded()) {
                            this.connectionManager.CreateConnection(input);
                        }
                    } else {
                        this.connectionManager.getConnections().ToArray().forEach(($connection : IConnection) : boolean => {
                            if ($connection.input.node === input.node && $connection.input.position === input.position) {
                                this.connectionManager.RemoveConnection($connection.id, true);
                                isRemoved = true;
                                return false;
                            }
                        });
                        if (!isRemoved) {
                            this.connectionManager.getConnectionStacks().ToArray().forEach(($stack : IConnectionStack) : boolean => {
                                $stack.connections.foreach(($connection : IConnection) : boolean => {
                                    if ($connection.input.node === input.node && $connection.input.position === input.position) {
                                        this.connectionManager.RemoveConnection($connection.id, true);
                                        isRemoved = true;
                                        return false;
                                    }
                                });
                                if (isRemoved) {
                                    return false;
                                }
                            });
                        }
                    }
                }
            };

            const handleChange : any = () : void => {
                const root : IKernelNode = this.gridManager.Root();

                const onUpdateHandler : any = () : void => {
                    this.gridManager.DeselectAll();
                    this.connectionManager.DeselectAll();
                    $change.connectionChanges.forEach(($connectionChange : IConnectionChange) : void => {
                        if ($change.type !== GraphChangeType.SELECT) {
                            handleConnectionChange($connectionChange);
                        }
                        if ($change.type === GraphChangeType.CREATE || $change.type === GraphChangeType.SELECT
                            || $change.type === GraphChangeType.CONNECTION_CREATE || $change.type === GraphChangeType.CONNECTION_MOVE) {
                            const inputNode : IKernelNode = this.nodeRegistry.getNodeByUniqueId($connectionChange.inputNodeId);
                            const input : Input = inputNode === root ? inputNode.getOutput($connectionChange.inputNodeIndex).innerInput :
                                inputNode.getInput($connectionChange.inputNodeIndex);
                            if (!ObjectValidator.IsEmptyOrNull(input.connection)) {
                                this.connectionManager.SelectConnection(input.connection);
                                if (!ObjectValidator.IsEmptyOrNull(input.connection.stack)) {
                                    this.connectionManager.UnstackConnections(input.connection.stack);
                                }
                            }
                        }
                    });
                    this.gridManager.SelectNodesByIds($change.selectedNodesIds,
                        ($change.type === GraphChangeType.CREATE || $change.type === GraphChangeType.MOVE)
                        && ObjectValidator.IsEmptyOrNull($change.sizeChange) || $change.type === GraphChangeType.SELECT);
                    if (ObjectValidator.IsEmptyOrNull($change.connectionChanges)) {
                        this.gridManager.getSelectedNodes().foreach(($node : IKernelNode) : void => {
                            this.connectionManager.SelectNodeConnections($node);
                        });
                    }
                    this.isComplete = true;
                    this.onStateChangeHandler();
                };

                const animateResize : any = ($onResize : () => void, $scrollToSelected : boolean = false,
                                             $nodes? : ArrayList<IKernelNode>) : void => {
                    if (ObjectValidator.IsSet($change.sizeChange)) {
                        const direction : ElementOffset
                            = new ElementOffset($change.sizeChange.directionVertical, $change.sizeChange.directionHorizontal);
                        const anchor : ElementOffset
                            = new ElementOffset($change.sizeChange.anchorVertical, $change.sizeChange.anchorHorizontal);
                        if (direction.Left() === 0) {
                            anchor.Left(0);
                        }
                        if (direction.Top() === 0) {
                            anchor.Top(0);
                        }

                        if (!$scrollToSelected) {
                            // todo : integrate into area manager
                            this.gridManager.ToggleScroll(() : IVector => {
                                const signedDirection : ElementOffset = new ElementOffset();
                                signedDirection.Top(direction.Top() * anchor.Top());
                                signedDirection.Left(direction.Left() * anchor.Left());

                                const points : IVector[] = [];
                                if (!ObjectValidator.IsEmptyOrNull($nodes)) {
                                    $nodes.foreach(($node : IKernelNode) : void => {
                                        const position : IVector
                                            = this.gridManager.getEnvironment().getNodePosition($node.Row(), $node.Column());
                                        if ($change.type !== GraphChangeType.MOVE) {
                                            position.x = position.x + Math.min(0, -signedDirection.Left());
                                            position.y = position.y + Math.min(0, -signedDirection.Top());
                                        }
                                        points.push(position);
                                    });
                                } else {
                                    const point : IVector = {x: 0, y: 0};
                                    const areaSize : Size = this.gridManager.getAreaSize();
                                    const areaWidth : number = areaSize.Width();
                                    const areaHeight : number = areaSize.Height();

                                    if (anchor.Left() > 0) {
                                        point.x = areaWidth + signedDirection.Left();
                                    } else if (anchor.Left() < 0) {
                                        point.x = -signedDirection.Left();
                                    }
                                    if (anchor.Top() > 0) {
                                        point.y = areaHeight + signedDirection.Top();
                                    } else if (anchor.Top() < 0) {
                                        point.y = -signedDirection.Top();
                                    }

                                    if (anchor.Left() === 0) {
                                        point.x = this.gridManager.getMinOverflowPoint([
                                            {x: 0, y: point.y},
                                            {x: areaWidth, y: point.y}
                                        ]).x;
                                        anchor.Left(point.x > 0 ? 1 : -1);
                                    } else if (anchor.Top() === 0) {
                                        point.y = this.gridManager.getMinOverflowPoint([
                                            {x: point.x, y: 0},
                                            {x: point.x, y: areaHeight}
                                        ]).y;
                                        anchor.Top(point.y > 0 ? 1 : -1);
                                    }
                                    points.push(point);
                                }
                                return this.gridManager.getPointsScrollVector(points, false,
                                    100, undefined,
                                    ObjectValidator.IsEmptyOrNull($nodes) ? 160 : undefined);
                            });
                        }

                        this.gridManager.IndicateAreaResize(direction, anchor);
                        this.gridManager.UpdateDimensionsIndicator(anchor);
                        const handle : number = EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                            this.queuedChangeHandler();
                        }, false, direction.Left() === 0 && direction.Top() === 0 ? 0 : HistoryManager.resizeAnimMs);
                        this.queuedChangeHandler = () : void => {
                            clearTimeout(handle);
                            this.queuedChangeHandler = null;
                            this.gridManager.ApplyAreaResize();
                            if ($scrollToSelected) {
                                this.gridManager.SelectNodesByIds($change.selectedNodesIds);
                            }
                            direction.Left(0);
                            direction.Top(0);
                            $onResize();
                        };
                        this.onStateChangeHandler();
                    } else {
                        $onResize();
                    }
                };

                const rootChange : IIndexChange = $change.rootChange;
                switch ($change.type) {
                case GraphChangeType.SELECT:
                    onUpdateHandler();
                    break;
                case GraphChangeType.CREATE:
                    if (ObjectValidator.IsEmptyOrNull($change.nodeChanges)) {
                        onUpdateHandler();
                    } else {
                        const nodesToDrag : ArrayList<IKernelNode> = ArrayList.ToArrayList(this.getNodesFromChanges($change.nodeChanges));
                        animateResize(() : void => {
                            this.gridManager.DragNodes(nodesToDrag, nodesToDrag.getFirst(), DragNodeType.PASTE, () : void => {
                                onUpdateHandler();
                            });
                        }, false, nodesToDrag);
                    }
                    break;
                case GraphChangeType.REMOVE:
                    $change.nodeChanges.forEach(($change : INodeChange) : void => {
                        const node : IKernelNode = this.gridManager.getNodeFromId($change.id);
                        this.gridManager.RemoveNode(node);
                    });
                    if (!ObjectValidator.IsEmptyOrNull($change.sizeChange)) {
                        animateResize(onUpdateHandler);
                    } else {
                        onUpdateHandler();
                    }
                    break;
                case GraphChangeType.RESIZE:
                    animateResize(onUpdateHandler);
                    break;
                case GraphChangeType.MOVE:
                    const nodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();
                    $change.selectedNodesIds.forEach(($nodeId : string) : void => {
                        const node : IKernelNode = this.gridManager.getNodeFromId($nodeId);
                        nodes.Add(node);
                        this.gridManager.ApplyMove(node, new ElementOffset(node.Row() + $change.positionChange.offsetTop,
                            node.Column() + $change.positionChange.offsetLeft));
                    });
                    animateResize(onUpdateHandler, false, nodes);
                    break;
                case GraphChangeType.CONNECTION_CREATE:
                case GraphChangeType.CONNECTION_REMOVE:
                case GraphChangeType.CONNECTION_MOVE:
                case GraphChangeType.ATTRIBUTE_UPDATE:
                    $change.nodeChanges.forEach(($nodeChange : INodeChange) : void => {
                        const node : IKernelNode = this.gridManager.getNodeFromId($nodeChange.id);
                        if ($nodeChange.nameChange.to !== node.Name()) {
                            this.nodeRegistry.UnassignName(node.Name());
                            node.Name($nodeChange.nameChange.to);
                            this.nodeRegistry.AssignName(node);
                        }
                        $nodeChange.attributeChanges.forEach(($change : IAttributeChange) : void => {
                            if (!ObjectValidator.IsEmptyOrNull(node.getAttributes().getItem($change.name))) {
                                node.setAttributeValue($change.name, $change.to);
                            }
                        });
                    });
                    onUpdateHandler();
                    break;
                case GraphChangeType.ROOT_INTERFACE_MOVE:
                    root.MoveInterface(rootChange.fromIndex, rootChange.toIndex,
                        rootChange.interfaceType === KernelNodeInterfaceType.OUTPUT ?
                            KernelNodeInterfaceType.INPUT : KernelNodeInterfaceType.OUTPUT);
                    onUpdateHandler();
                    break;
                case GraphChangeType.ROOT_INTERFACE_CREATE:
                    let curPos : number = -1;
                    if (rootChange.interfaceType === KernelNodeInterfaceType.OUTPUT) {
                        root.AddInput(InterfaceDataType.DELEGATED);
                        const input : Input = root.getInputs().getLast();
                        input.innerOutput = new Output();
                        input.innerOutput.node = root;
                        input.innerOutput.position = input.position;
                        input.innerOutput.delegator = input;
                        input.innerOutput.dataType = InterfaceDataType.DELEGATED;
                        curPos = input.position;
                    } else if (rootChange.interfaceType === KernelNodeInterfaceType.INPUT) {
                        root.AddOutput(InterfaceDataType.DELEGATED);
                        const output : Output = root.getOutputs().getLast();
                        output.innerInput = new Input();
                        output.innerInput.node = root;
                        output.innerInput.position = output.position;
                        output.innerInput.delegator = output;
                        output.innerInput.dataType = InterfaceDataType.DELEGATED;
                        curPos = output.position;
                    }
                    if (curPos !== rootChange.toIndex) {
                        root.MoveInterface(curPos, rootChange.toIndex, rootChange.interfaceType === KernelNodeInterfaceType.OUTPUT ?
                            KernelNodeInterfaceType.INPUT : KernelNodeInterfaceType.OUTPUT);
                    }
                    onUpdateHandler();
                    break;
                case GraphChangeType.ROOT_INTERFACE_REMOVE:
                    if (rootChange.interfaceType === KernelNodeInterfaceType.OUTPUT) {
                        root.getInput(rootChange.fromIndex).innerOutput.inputs.ToArray().forEach(($input : Input) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                                this.connectionManager.RemoveConnection($input.connection.id);
                            }
                        });
                        root.RemoveInterface(rootChange.fromIndex, KernelNodeInterfaceType.INPUT);
                    } else if (rootChange.interfaceType === KernelNodeInterfaceType.INPUT) {
                        const innerInput : Input = root.getOutput(rootChange.fromIndex).innerInput;
                        if (!ObjectValidator.IsEmptyOrNull(innerInput) && !ObjectValidator.IsEmptyOrNull(innerInput.connection)) {
                            this.connectionManager.RemoveConnection(innerInput.connection.id);
                        }
                        root.RemoveInterface(rootChange.fromIndex, KernelNodeInterfaceType.OUTPUT);
                    }
                    break;
                default:
                    break;
                }
            };

            if (this.gridManager.Root().UniqueId() !== $change.root) {
                this.rootSwapHandler($change.root, handleChange);
            } else {
                handleChange();
            }
        }
    }
}
