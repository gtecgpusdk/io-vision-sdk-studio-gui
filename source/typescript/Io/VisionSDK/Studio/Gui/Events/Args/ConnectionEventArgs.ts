/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * ConnectionEventArgs class provides args connected with selection of connections.
     */
    export class ConnectionEventArgs extends EventArgs {
        private connection : IConnection;

        /**
         * @param {KernelNode} [$value] Set value representing active kernel node.
         * @return {KernelNode} Returns value representing active kernel node.
         */
        public Connection($value? : IConnection) : IConnection {
            if (ObjectValidator.IsSet($value)) {
                this.connection = $value;
            }
            return this.connection;
        }
    }
}
