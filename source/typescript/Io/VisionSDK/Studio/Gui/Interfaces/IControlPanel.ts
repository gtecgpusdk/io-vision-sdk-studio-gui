/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces {
    "use strict";

    export abstract class IControlPanelResizeConfiguration {
        public getControlMenuWidth : () => string | string[];
        public getBreadcrumbButtonWidth : ($index : number) => string | string[];
        public isControlButtonsVisible : () => boolean;
    }

    export abstract class IControlPanelZoomConfiguration {
        public minValue : number;
        public maxValue : number;
        public defaultValue : number;
        public valueStep : number;
    }

    export abstract class IControlPanelType {
        public type : any;
        public childTypes : IControlPanelChildTypes;
    }

    export abstract class IControlPanelChildTypes {
        public minusButtonType : any;
        public plusButtonType : any;
        public pickerType : any;
        public resetButtonType : any;
    }
}
