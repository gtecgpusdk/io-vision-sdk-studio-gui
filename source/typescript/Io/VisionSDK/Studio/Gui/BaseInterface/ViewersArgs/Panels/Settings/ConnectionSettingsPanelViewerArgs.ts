/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IConnectionSettings = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettings;

    export class ConnectionSettingsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private nameText : string;
        private addressText : string;
        private usernameText : string;
        private passwordText : string;
        private platformText : string;
        private agentNameText : string;
        private panelDescription : string;
        private value : IConnectionSettings;

        constructor() {
            super();
            this.headerText = "";
            this.nameText = "";
            this.addressText = "";
            this.usernameText = "";
            this.passwordText = "";
            this.platformText = "";
            this.agentNameText = "";
            this.panelDescription = "";
            this.value = null;
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public AddressText($value? : string) : string {
            return this.addressText = Property.String(this.addressText, $value);
        }

        public UsernameText($value? : string) : string {
            return this.usernameText = Property.String(this.usernameText, $value);
        }

        public PasswordText($value? : string) : string {
            return this.passwordText = Property.String(this.passwordText, $value);
        }

        public PlatformText($value? : string) : string {
            return this.platformText = Property.String(this.platformText, $value);
        }

        public AgentNameText($value? : string) : string {
            return this.agentNameText = Property.String(this.agentNameText, $value);
        }

        public PanelDescription($value? : string) : string {
            return this.panelDescription = ObjectValidator.IsSet($value) ? $value : this.panelDescription;
        }

        public Value($value? : IConnectionSettings) : IConnectionSettings {
            return this.value = ObjectValidator.IsSet($value) ? $value : this.value;
        }
    }
}
