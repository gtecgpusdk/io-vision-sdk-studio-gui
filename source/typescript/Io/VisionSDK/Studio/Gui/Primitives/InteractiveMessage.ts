/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class InteractiveMessage extends BaseObject {

        private parts : ArrayList<any>;

        public static ToHtmlElement($message : InteractiveMessage) : HTMLElement {
            const message : HTMLElement = document.createElement("span");
            const id : string = new Date().getMilliseconds().toString();
            message.id = "InteractiveMessage_" + id;
            message.className = "InteractiveMessage";
            let prevSpan : HTMLElement = null;
            $message.parts.foreach(($part : any) : void => {
                let element : HTMLElement;
                if (ObjectValidator.IsSet($part.onClickHandler)) {
                    element = document.createElement("a");
                    element.onmouseenter = $part.onHoverHandler;
                    element.onmouseup = $part.onClickHandler;
                    element.innerText = $part.text;
                    if (prevSpan !== null) {
                        message.appendChild(prevSpan);
                        prevSpan = null;
                    }
                    message.appendChild(element);
                } else {
                    if (prevSpan === null) {
                        element = document.createElement("span");
                        prevSpan = element;
                    }
                    prevSpan.innerText = prevSpan.innerText + $part.text;
                    if ($part === $message.parts.getLast()) {
                        message.appendChild(prevSpan);
                        prevSpan = null;
                    }
                }
            });
            return message;
        }

        public constructor() {
            super();
            this.parts = new ArrayList<any>();
        }
        public Append($message : string, $onClickHandler? : () => void, $onHoverHandler? : () => void) : InteractiveMessage {
            const part : any = {};
            part.text = $message;
            if (ObjectValidator.IsSet($onClickHandler) || ObjectValidator.IsSet($onHoverHandler)) {
                if (ObjectValidator.IsSet($onClickHandler)) {
                    part.onClickHandler = $onClickHandler;
                } else {
                    part.onClickHandler = () => {
                        // empty handler
                    };
                }
                if (ObjectValidator.IsSet($onHoverHandler)) {
                    part.onHoverHandler = $onHoverHandler;
                } else {
                    part.onHoverHandler = () => {
                        // empty handler
                    };
                }
            }
            this.parts.Add(part);
            return this;
        }

        public ToString() : string {
            let message : string = "";
            this.parts.foreach(($part : any) : void => {
                message += $part.text;
            });
            return message;
        }
    }
}
