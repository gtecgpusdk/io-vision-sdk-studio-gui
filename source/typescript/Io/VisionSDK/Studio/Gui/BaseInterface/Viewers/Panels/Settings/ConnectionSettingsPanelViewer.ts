/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings {
    "use strict";
    import ConnectionSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ConnectionSettingsPanelViewerArgs;
    import ConnectionSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ConnectionSettingsPanel;

    export class ConnectionSettingsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ConnectionSettingsPanelViewerArgs {
            const args : ConnectionSettingsPanelViewerArgs = new ConnectionSettingsPanelViewerArgs();
            args.HeaderText("Build connection settings");
            args.NameText("Connection name");
            args.AddressText("Connection address");
            args.UsernameText("Username");
            args.PasswordText("Password");
            args.PlatformText("Platform");
            args.AgentNameText("Agent");
            args.PanelDescription("Connections settings test description");

            args.Value({
                /* tslint:disable: object-literal-sort-keys */
                options: [
                    {
                        name    : "Test ssh connection",
                        platform: "i.MX",
                        address : "Test address",
                        username: "Test username",
                        password: "Test password"
                    },
                    {
                        name     : "Test cloud connection",
                        platform : "WinGCC",
                        address  : "Test address",
                        agentName: "Win-agent"
                    }
                ]/* tslint:enable */
            });

            return args;
        }

        /* dev:end */

        constructor($args? : ConnectionSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new ConnectionSettingsPanel());
        }

        public getInstance() : ConnectionSettingsPanel {
            return <ConnectionSettingsPanel>super.getInstance();
        }

        public ViewerArgs($args? : ConnectionSettingsPanelViewerArgs) : ConnectionSettingsPanelViewerArgs {
            return <ConnectionSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ConnectionSettingsPanel, $args : ConnectionSettingsPanelViewerArgs) : void {
            $instance.headerText = $args.HeaderText();
            $instance.nameLabel.Text($args.NameText());
            $instance.addressLabel.Text($args.AddressText());
            $instance.usernameLabel.Text($args.UsernameText());
            $instance.passwordLabel.Text($args.PasswordText());
            $instance.platformLabel.Text($args.PlatformText());
            $instance.agentNameLabel.Text($args.AgentNameText());
            $instance.panelDescription = $args.PanelDescription();
            $instance.ApplySettings($args.Value());
        }

        /* dev:start */
        protected testImplementation($instance : ConnectionSettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ConnectionSettingsPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
