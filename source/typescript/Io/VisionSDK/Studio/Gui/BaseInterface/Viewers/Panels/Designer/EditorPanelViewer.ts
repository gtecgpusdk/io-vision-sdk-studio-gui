/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EditorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPanelViewerArgs;
    import EditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.EditorPanel;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;

    export class EditorPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : EditorPanelViewerArgs {
            const args : EditorPanelViewerArgs = new EditorPanelViewerArgs();
            args.ToolbarArgs((<any>ToolbarPanelViewer).getTestViewerArgs());
            args.NodesFilterArgs((<any>NodesFilterPanelViewer).getTestViewerArgs());
            args.DetailsPanelArgs((<any>DetailsPanelViewer).getTestViewerArgs());
            args.PropertiesLayerSelectorArgs((<any>LayerSelectorPanelViewer).getTestViewerArgs());
            args.KernelNodePickerPanelArgs((<any>KernelNodesPickerPanelViewer).getTestViewerArgs());
            args.VisualGraphPanelArgs((<any>VisualGraphPanelViewer).getTestViewerArgs());

            return args;
        }

        /* dev:end */

        constructor($args? : EditorPanelViewerArgs) {
            super($args);
            this.setInstance(new EditorPanel());
        }

        public getInstance() : EditorPanel {
            return <EditorPanel>super.getInstance();
        }

        public ViewerArgs($args? : EditorPanelViewerArgs) : EditorPanelViewerArgs {
            return <EditorPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : EditorPanel, $args : EditorPanelViewerArgs) : void {
            $instance.toolbar.Value($args.ToolbarArgs());
            $instance.statusLabel.Text($args.StatusText());
            $instance.kernelNodePickerPanel.nodesFilter.Value($args.NodesFilterArgs());
            $instance.kernelNodePickerPanel.Value($args.KernelNodePickerPanelArgs());
            $instance.visualGraphPanel.Value($args.VisualGraphPanelArgs());
            if ($instance.IsCompleted()) {
                $instance.detailsPanel.accordion.getPanel(PropertiesPanel).Value($args.DetailsPanelArgs().PropertiesPanelArgs());
            } else {
                $instance.getEvents().setOnComplete(() : void => {
                    $instance.detailsPanel.accordion.getPanel(PropertiesPanel).Value($args.DetailsPanelArgs().PropertiesPanelArgs());
                });
            }
        }

        protected beforeLoad($instance : EditorPanel) : void {
            const borderOffset : number = ElementManager.getCssIntegerValue("Browser", "border-width") * 2;
            const resizeHandler : any = () : void => {
                const browserSize : Size = new Size("Browser", true);
                browserSize.Height(browserSize.Height() === 0 ? WindowManager.getSize().Height() : browserSize.Height());
                const topOffset : number = ElementManager.getAbsoluteOffset(
                    ObjectValidator.IsEmptyOrNull(parent) ? $instance : $instance.Parent()).Top();
                const width = browserSize.Width() - borderOffset;
                const height = browserSize.Height() - topOffset - borderOffset;
                if ($instance.Width() !== width || $instance.Height() !== height) {
                    $instance.Width(width);
                    $instance.Height(height);
                    const parent : BasePanel = <BasePanel>$instance.Parent();
                    if (!ObjectValidator.IsEmptyOrNull(parent)) {
                        parent.Width(width);
                        parent.Height(height);
                    }
                }
            };

            $instance.getEvents().FireAsynchronousMethod(resizeHandler);

            this.getEventsManager().setEvent("Browser", EventType.ON_RESIZE_COMPLETE, () : void => {
                resizeHandler();
            });
        }

        /* dev:start */
        protected testImplementation($instance : EditorPanel) : string {
            $instance.StyleClassName("TestCss");
            const sizeHandler : any = () : void => {
                ElementManager.setCssProperty("Browser", "height", WindowManager.getSize().Height() - 60);
                ElementManager.setCssProperty("Browser", "width", WindowManager.getSize().Width() - 60);
                const eventArgs : ResizeEventArgs = new ResizeEventArgs();
                eventArgs.Owner("Browser");
                this.getEventsManager().FireEvent("Browser", EventType.ON_RESIZE_COMPLETE, eventArgs);
            };
            WindowManager.getEvents().setOnResize(() : void => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });
            $instance.getEvents().FireAsynchronousMethod(sizeHandler);
            return "<style>" +
                "body {overflow: hidden!important;} </style>";
        }

        /* dev:end */
    }
}
