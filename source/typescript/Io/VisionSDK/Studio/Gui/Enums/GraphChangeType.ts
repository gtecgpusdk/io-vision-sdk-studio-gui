/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums {
    "use strict";

    export class GraphChangeType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly RESIZE : string = "Resize";
        public static readonly CREATE : string = "Create";
        public static readonly REMOVE : string = "Remove";
        public static readonly MOVE : string = "Move";
        public static readonly ATTRIBUTE_UPDATE : string = "AttributeUpdate";
        public static readonly CONNECTION_CREATE : string = "ConnectionCreate";
        public static readonly CONNECTION_REMOVE : string = "ConnectionRemove";
        public static readonly CONNECTION_MOVE : string = "ConnectionMove";
        public static readonly SELECT : string = "Select";
        public static readonly ROOT_INTERFACE_CREATE : string = "RootInterfaceCreate";
        public static readonly ROOT_INTERFACE_REMOVE : string = "RootInterfaceRemove";
        public static readonly ROOT_INTERFACE_MOVE : string = "RootInterfaceMove";
    }
}
