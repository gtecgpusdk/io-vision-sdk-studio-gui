/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums {
    "use strict";
    import BaseViewerLayerType = Com.Wui.Framework.Gui.Enums.BaseViewerLayerType;

    export class ProjectSettingsLayerType extends BaseViewerLayerType {
        public static readonly NEW_PROJECT : string = "NewProject";
        public static readonly PROJECT_SETTINGS : string = "ProjectSettings";
    }
}
