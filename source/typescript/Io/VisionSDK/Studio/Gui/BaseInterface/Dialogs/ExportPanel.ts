/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import IProjectEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IProjectEvents;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import ExportPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ExportPanelViewerArgs;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;

    export class ExportPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerLabel : Label;
        public platformLabel : Label;
        public platformType : DropDownList;
        public exportPathLabel : Label;
        public exportPath : TextField;
        public closeButton : Button;
        public exportButton : Button;

        constructor($id? : string) {
            super($id);
            this.headerLabel = new Label();
            this.platformLabel = new Label();
            this.platformType = new DropDownList(DropDownListType.SETTING);
            this.exportPathLabel = new Label();
            this.exportPath = new TextField(TextFieldType.SETTING);
            this.closeButton = new Button(ButtonType.GRAY_MEDIUM);
            this.exportButton = new Button(ButtonType.GREEN_MEDIUM);
        }

        public Value($value? : ExportPanelViewerArgs) : ExportPanelViewerArgs {
            return <ExportPanelViewerArgs>super.Value($value);
        }

        public getEvents() : IProjectEvents {
            return <IProjectEvents>super.getEvents();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);
            this.exportPath.ReadOnly(true);
            this.exportPath.IsPersistent(false);
            this.exportPath.getEvents().setOnClick(() : void => {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this.exportPath);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, eventArgs);
            });

            this.exportButton.getEvents().setOnComplete(() : void => {
                this.getEventsManager().FireEvent(this, EventType.ON_COMPLETE);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow().FitToParent(FitToParent.NONE)
                    .HeightOfRow(80, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT, true)
                        .StyleClassName("Header")
                        .Add(this.headerLabel))
                )
                .Add(this.addRow()
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                    .Add(this.addColumn().WidthOfColumn(150, UnitType.PX).HeightOfRow(65, UnitType.PX, true)
                        .Add(this.platformLabel)
                        .Add(this.exportPathLabel)
                    )
                    .Add(this.addColumn().HeightOfRow(65, UnitType.PX, true)
                        .Add(this.platformType)
                        .Add(this.exportPath)
                    )
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT)))
                .Add(this.addRow()
                    .HeightOfRow(80, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT)
                        .Add(this.addRow()
                            .Add(this.exportButton)
                            .Add(this.addColumn().WidthOfColumn(25, UnitType.PX))
                            .Add(this.closeButton)
                        )
                    )
                );
        }
    }
}
