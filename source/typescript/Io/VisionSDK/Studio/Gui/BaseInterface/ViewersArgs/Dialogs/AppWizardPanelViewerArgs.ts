/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class AppWizardPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private descriptionText : string;

        constructor() {
            super();
            this.headerText = "";
            this.descriptionText = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }
    }
}
