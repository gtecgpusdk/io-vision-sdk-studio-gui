/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelNodeHelpPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.KernelNodeHelpPanelViewerArgs;
    import KernelNodeHelpPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.KernelNodeHelpPanel;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeHelp = Io.VisionSDK.UserControls.Interfaces.IKernelNodeHelp;

    export class KernelNodeHelpPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : KernelNodeHelpPanelViewerArgs {
            const args : KernelNodeHelpPanelViewerArgs = new KernelNodeHelpPanelViewerArgs();
            args.HeaderText("NODE");
            args.DescriptionText("PURPOSE");
            args.LinkText("Looking for more information?");

            const node : IKernelNode = new KernelNode(KernelNodeType.ABSOLUTE_DIFFERENCE, -1, -1);
            node.Type(KernelNodeType.ABSOLUTE_DIFFERENCE);
            node.Help({
                description: "Test description text",
                inputs     : [],
                link       : "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/index.html",
                outputs    : [],
                tooltip    : "Test tooltip text"
            });
            args.KernelNode(node);

            return args;
        }

        /* dev:end */

        constructor($args? : KernelNodeHelpPanelViewerArgs) {
            super($args);
            this.setInstance(new KernelNodeHelpPanel());
        }

        public getInstance() : KernelNodeHelpPanel {
            return <KernelNodeHelpPanel>super.getInstance();
        }

        public ViewerArgs($args? : KernelNodeHelpPanelViewerArgs) : KernelNodeHelpPanelViewerArgs {
            return <KernelNodeHelpPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : KernelNodeHelpPanel, $args : KernelNodeHelpPanelViewerArgs) : void {
            $instance.header.Text($args.HeaderText());
            $instance.description.Text($args.DescriptionText());
            $instance.link.Text($args.LinkText());

            const node : IKernelNode = $args.KernelNode();
            if (!ObjectValidator.IsEmptyOrNull(node)) {
                const help : IKernelNodeHelp = node.Help();
                $instance.nodeName.Text(ObjectValidator.IsEmptyOrNull(help.name) ? node.Type() : help.name);
                $instance.nodeInfo.Text(ObjectValidator.IsEmptyOrNull(help.description) ?
                    help.tooltip : StringUtils.Replace(help.description, "  ", StringUtils.Space()));
                if (!ObjectValidator.IsEmptyOrNull(help.link)) {
                    $instance.link.Visible(true);
                    $instance.NavigateTo(help.link);
                } else {
                    $instance.link.Visible(false);
                }
            }

            if ($args.ResetView()) {
                $args.ResetView(false);
                $instance.ResetView();
            }
            $instance.SizeLimit($args.SizeLimit());
        }

        /* dev:start */
        protected testImplementation($instance : KernelNodeHelpPanel) : string {
            $instance.StyleClassName("TestCss");

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .KernelNodeHelpPanel{border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
