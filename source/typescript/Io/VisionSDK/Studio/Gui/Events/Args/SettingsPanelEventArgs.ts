/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;

    /**
     * SettingsPanelEventArgs class provides args connected with modification of project settings.
     */
    export class SettingsPanelEventArgs extends EventArgs {
        private settings : ISettings;

        public Settings($args? : ISettings) : ISettings {
            if (ObjectValidator.IsSet($args)) {
                this.settings = $args;
            }
            return this.settings;
        }
    }
}
