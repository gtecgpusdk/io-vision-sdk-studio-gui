/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IBuildSettingsEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnSettingsRequest",
                "setOnBuildPlatformChange"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IBuildSettingsEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IConnectionEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IControlPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnZoomChange",
                "setOnReset"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IProjectEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnProjectCreate",
                "setOnProjectLoad",
                "setOnProjectSettingsChange",
                "setOnPathRequest",
                "setOnProjectExport"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IEditorPageEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnWizardForward",
                "setOnWizardBack",
                "setOnWizardClose",
                "setOnBuildPlatformChange"
            ], IProjectEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IEditorPageEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IEditorPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnPropertyChange",
                "setOnStateChange"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IKernelNodesPickerPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnNodeSelect",
                "setOnNodesPicked"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IProjectEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IPropertiesPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnPropertyChange",
                "setOnPropertySelect"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IPropertyEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let ISettingsPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnSettingsUpdate",
                "setOnPathRequest",
                "setOnValidationRequest"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let ISettingsPanelEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IStateEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IVisualGraphEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    export let IVisualGraphPanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnNodeSelect",
                "setOnNodeDrag",
                "setOnRootChangeStart",
                "setOnRootChangeComplete",
                "setOnZoomChange",
                "setOnStateChange",
                "setOnDragActionStart",
                "setOnDragActionComplete"
            ], IControlPanelEvents);
        }();
}

/* tslint:enable */
