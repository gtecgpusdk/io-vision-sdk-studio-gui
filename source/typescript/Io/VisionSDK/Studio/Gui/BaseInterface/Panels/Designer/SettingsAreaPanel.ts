/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;
    import CropBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.CropBoxType;
    import AreaPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.AreaPanelType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ConnectionSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ConnectionSettingsPanelViewer;
    import ConnectionSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ConnectionSettingsPanel;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;
    import ToolchainSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ToolchainSettingsPanel;
    import ToolchainSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ToolchainSettingsPanelViewer;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProjectSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ProjectSettingsPanel;
    import ProjectSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ProjectSettingsPanelViewer;
    import ControlPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.ControlPanelType;

    export class SettingsAreaPanel extends AreaPanel {
        public projectSettingsPanel : ProjectSettingsPanel;
        public connectionSettingsPanel : ConnectionSettingsPanel;
        public toolchainSettingsPanel : ToolchainSettingsPanel;

        constructor($args : AreaPanelViewerArgs, $id? : string) {
            super($args, $id);
            this.projectSettingsPanel = <ProjectSettingsPanel>this.addChildPanel(ProjectSettingsPanelViewer, null);
            this.connectionSettingsPanel = <ConnectionSettingsPanel>this.addChildPanel(ConnectionSettingsPanelViewer, null);
            this.toolchainSettingsPanel = <ToolchainSettingsPanel>this.addChildPanel(ToolchainSettingsPanelViewer, null);
            this.connectionSettingsPanel.Visible(false);
            this.toolchainSettingsPanel.Visible(false);
            this.IsAreaSelectEnabled(false);
            this.getAreaStrategy().setAreaConstraints(480, 640);
            this.controlPanel.setBreadcrumbButtons(this.getMenu());

            const controlPanelType : any = this.controlPanel.GuiType();
            controlPanelType.type = ControlPanelType.GRAY;
            this.GuiType({
                childTypes: {
                    controlPanelType: this.controlPanel.GuiType(),
                    cropBoxType     : CropBoxType.SKY_BLUE
                },
                type      : AreaPanelType.GRAY
            });
        }

        protected onResizeCompleteHandler() : void {
            super.onResizeCompleteHandler();
            const areaSize : Size = this.getAreaStrategy().getAreaSize();
            [this.projectSettingsPanel, this.connectionSettingsPanel, this.toolchainSettingsPanel]
                .forEach(($panel : any) : void => {
                    $panel.Width(areaSize.Width());
                    $panel.Height(areaSize.Height());
                });
        }

        protected getAreaElement() : IGuiElement {
            return this.addElement()
                .Add(this.projectSettingsPanel)
                .Add(this.connectionSettingsPanel)
                .Add(this.toolchainSettingsPanel);
        }

        protected IsHovered() : boolean {
            const guiManager : GuiObjectManager = this.getGuiManager();
            return !guiManager.IsHovered(ScrollBar);
        }

        protected cssContainerName() : string {
            return SettingsAreaPanel.ClassNameWithoutNamespace() + " " + AreaPanel.ClassNameWithoutNamespace();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnComplete(() : void => {
                this.getAreaStrategy().setAreaSize(480, 640);
                const areaSize : Size = this.getAreaStrategy().getAreaSize();
                [this.projectSettingsPanel, this.connectionSettingsPanel, this.toolchainSettingsPanel]
                    .forEach(($panel : any) : void => {
                        $panel.Width(areaSize.Width());
                        $panel.Height(areaSize.Height());
                    });
            });

            return super.innerCode();
        }

        private getMenu() : IDropDownButtonArgs[] {
            return [
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback: () : void => {
                                this.projectSettingsPanel.Visible(true);
                                this.toolchainSettingsPanel.Visible(false);
                                this.connectionSettingsPanel.Visible(false);
                            },
                            text    : "Project settings"
                        }
                    ]
                },
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback: () : void => {
                                this.projectSettingsPanel.Visible(false);
                                this.toolchainSettingsPanel.Visible(false);
                                this.connectionSettingsPanel.Visible(true);
                            },
                            text    : "Connection settings"
                        }
                    ]
                },
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback: () : void => {
                                this.projectSettingsPanel.Visible(false);
                                this.connectionSettingsPanel.Visible(false);
                                this.toolchainSettingsPanel.Visible(true);
                            },
                            text    : "Toolchain settings"
                        }
                    ]
                }
            ];
        }
    }
}
