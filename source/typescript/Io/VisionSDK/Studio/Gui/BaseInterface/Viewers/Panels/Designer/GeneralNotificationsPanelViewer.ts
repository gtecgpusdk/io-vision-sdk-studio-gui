/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import GeneralNotificationsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.GeneralNotificationsPanel;

    export class GeneralNotificationsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new GeneralNotificationsPanel());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.GeneralNotificationsPanelTest);
            /* dev:end */
        }

        public getInstance() : GeneralNotificationsPanel {
            return <GeneralNotificationsPanel>super.getInstance();
        }
    }
}
