/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Enums {
    "use strict";

    export class AreaPanelType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly ORANGE : string = "Orange";
        public static readonly SKY_BLUE : string = "SkyBlue";
        public static readonly GREEN : string = "Green";
        public static readonly GREEN_DARK : string = "GreenDark";
        public static readonly GRAY : string = "Gray";
    }
}
