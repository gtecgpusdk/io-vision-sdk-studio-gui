/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import DetailsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.DetailsPanel;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;

    export class DetailsPanelTest extends ViewerTestRunner<DetailsPanel> {

        public __IgnoretestHierarchyClick() : void {
            const detail : DetailsPanel = this.getInstance();
            const visualGraph : KernelNode = new KernelNode(KernelNodeType.VISUAL_GRAPH, 11, 11);
            const vxContext : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, 9, 9);
            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
            vxGraph.ChildNodes().Add(file);
            file.ParentNode(vxGraph);
            visualGraph.ChildNodes().Add(vxContext);
            vxContext.ParentNode(visualGraph);
            vxContext.ChildNodes().Add(vxGraph);
            vxGraph.ParentNode(vxContext);
            detail.setProjectStructure(vxGraph);
            const hierarchy : DirectoryBrowser = detail.accordion.getPanel(DirectoryBrowser);
            this.assertEquals((<any>hierarchy).visibleItems.getItem(0).Text(), "VisualGraph");
            this.assertEquals((<any>hierarchy).visibleItems.getItem(1).Text(), "VxContext0");
            this.assertEquals((<any>hierarchy).visibleItems.getItem(2).Text(), "VxGraph0");
        }

        public testSelectedNode() : void {
            const detail : DetailsPanel = this.getInstance();
            const visualGraph : KernelNode = new KernelNode(KernelNodeType.VISUAL_GRAPH, 11, 11);
            const vxContext : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, 9, 9);
            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
            vxGraph.ChildNodes().Add(file);
            file.ParentNode(vxGraph);
            visualGraph.ChildNodes().Add(vxContext);
            vxContext.ParentNode(visualGraph);
            vxContext.ChildNodes().Add(vxGraph);
            vxGraph.ParentNode(vxContext);
            detail.UpdateNodeInfo(file);
            detail.SelectNode(file);
            this.assertEquals(detail.getSelectedNode(), file);
        }

        protected before() : string {
            const instance : DetailsPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.Width(300);
            instance.Height(900);
            return "<style>.TestCss .DetailsPanel {border: 1px solid #E7E7E8;}</style>";
        }
    }
}
/* dev:end */
