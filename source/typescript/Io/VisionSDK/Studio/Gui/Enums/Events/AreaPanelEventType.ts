/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class AreaPanelEventType extends EventType {
        public static readonly ON_AREA_RESIZE_START : string = "OnAreaResizeStart";
        public static readonly BEFORE_AREA_RESIZE_COMPLETE : string = "BeforeAreaResizeComplete";
        public static readonly ON_AREA_RESIZE_COMPLETE : string = "OnAreaResizeComplete";
        public static readonly ON_AREA_MOUSE_MOVE : string = "OnAreaMouseMove";
        public static readonly ON_AREA_SELECT_CHANGE : string = "OnAreaSelectChange";
        public static readonly ON_AREA_SELECT_COMPLETE : string = "OnAreaSelectComplete";
        public static readonly ON_AREA_MOUSE_DOWN : string = "OnAreaMouseDown";
        public static readonly ON_AREA_MOUSE_UP : string = "OnAreaMouseUp";
        public static readonly ON_AREA_KEY_DOWN : string = "OnAreaKeyDown";
        public static readonly ON_AREA_KEY_UP : string = "OnAreaKeyUp";
        public static readonly ON_AREA_ZOOM_CHANGE : string = "OnAreaZoomChange";
    }
}
