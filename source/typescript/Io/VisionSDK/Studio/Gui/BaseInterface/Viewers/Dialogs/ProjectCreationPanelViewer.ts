/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import ProjectCreationPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ProjectCreationPanel;
    import ProjectCreationPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectCreationPanelViewerArgs;
    import ProjectSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ProjectSettingsPanelViewer;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;

    export class ProjectCreationPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectCreationPanelViewerArgs {
            const args : ProjectCreationPanelViewerArgs = new ProjectCreationPanelViewerArgs();
            args.CreateText("Create");
            args.SettingsArgs((<any>ProjectSettingsPanelViewer).getTestViewerArgs());
            return args;
        }

        /* dev:end */

        constructor($args? : ProjectCreationPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectCreationPanel());
        }

        public getInstance() : ProjectCreationPanel {
            return <ProjectCreationPanel>super.getInstance();
        }

        public ViewerArgs($args? : ProjectCreationPanelViewerArgs) : ProjectCreationPanelViewerArgs {
            return <ProjectCreationPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectCreationPanel, $args : ProjectCreationPanelViewerArgs) : void {
            const settingsArgs : ProjectSettingsPanelViewerArgs = $args.SettingsArgs();
            $instance.headerLabel.Text(settingsArgs.HeaderText());
            $instance.descriptionLabel.Text(settingsArgs.DescriptionText());
            $instance.nameLabel.Text(settingsArgs.NameText());
            $instance.pathLabel.Text(settingsArgs.PathText());
            $instance.vxVersionLabel.Text(settingsArgs.VxVersionText());
            $instance.vxVersionOption.Clear();
            $instance.vxVersionOption.Height(-1);
            settingsArgs.getVxVersions().foreach(($version : string) : void => {
                $instance.vxVersionOption.Add($version);
            });
            $instance.vxVersionOption.Select(0);
            $instance.ApplySettings(settingsArgs.Value());

            $instance.createButton.Text($args.CreateText());
        }

        /* dev:start */
        protected testImplementation($instance : ProjectCreationPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ProjectCreationPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
