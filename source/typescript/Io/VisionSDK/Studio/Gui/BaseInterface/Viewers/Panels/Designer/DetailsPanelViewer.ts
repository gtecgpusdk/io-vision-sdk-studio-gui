/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import DetailsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.DetailsPanel;
    import DetailsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DetailsPanelViewerArgs;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;

    export class DetailsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : DetailsPanelViewerArgs {
            const args : DetailsPanelViewerArgs = new DetailsPanelViewerArgs();
            args.PropertiesPanelArgs((<any>PropertiesPanelViewer).getTestViewerArgs());
            return args;
        }

        /* dev:end */

        constructor($args? : DetailsPanelViewerArgs) {
            super($args);
            this.setInstance(new DetailsPanel());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.DetailsPanelTest);
            /* dev:end */
        }

        public getInstance() : DetailsPanel {
            return <DetailsPanel>super.getInstance();
        }

        public ViewerArgs($args? : DetailsPanelViewerArgs) : DetailsPanelViewerArgs {
            return <DetailsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : DetailsPanel, $args : DetailsPanelViewerArgs) : void {
            $instance.accordion.getPanel(PropertiesPanel).Value($args.PropertiesPanelArgs());
        }
    }
}
