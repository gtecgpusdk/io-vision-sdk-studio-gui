/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;

    export interface IControlPanelEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents {
        setOnZoomChange($handler : IEventsHandler) : void;

        setOnReset($handler : IEventsHandler) : void;
    }
}
