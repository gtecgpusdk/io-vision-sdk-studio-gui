/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import ProjectDescriptionPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ProjectDescriptionPanel;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;

    export class ProjectDescriptionPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectDescriptionPanel());
        }

        public getInstance() : ProjectDescriptionPanel {
            return <ProjectDescriptionPanel>super.getInstance();
        }

        /* dev:start */
        protected testImplementation($instance : ProjectDescriptionPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(500);
            $instance.Height(300);

            this.addTestButton("set default", () : void => {
                $instance.ApplyDefaultPage();
            });

            this.addTestButton("apply descriptor", () : void => {
                $instance.ApplyDescriptor({
                    description: "This example will show how to manage graph IO.",
                    name       : "File read and write",
                    path       : "...",
                    vxVersion  : "1.1.0"
                });
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ProjectDescriptionPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
