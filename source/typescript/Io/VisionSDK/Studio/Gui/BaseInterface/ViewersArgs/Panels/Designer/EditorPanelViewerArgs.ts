/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class EditorPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private toolbarArgs : ToolbarPanelViewerArgs;
        private statusText : string;
        private propertiesPanelArgs : PropertiesPanelViewerArgs;
        private nodesFilterArgs : NodesFilterPanelViewerArgs;
        private kernelNodePickerPanelArgs : KernelNodesPickerPanelViewerArgs;
        private visualGraphPanelArgs : VisualGraphPanelViewerArgs;
        private propertiesLayerArgs : LayerSelectorPanelViewerArgs;
        private detailsPanelArgs : DetailsPanelViewerArgs;

        constructor() {
            super();
            this.toolbarArgs = new ToolbarPanelViewerArgs();
            this.statusText = "";
            this.propertiesPanelArgs = new PropertiesPanelViewerArgs();
            this.nodesFilterArgs = new NodesFilterPanelViewerArgs();
            this.kernelNodePickerPanelArgs = new KernelNodesPickerPanelViewerArgs();
            this.visualGraphPanelArgs = new VisualGraphPanelViewerArgs();
            this.propertiesLayerArgs = new LayerSelectorPanelViewerArgs();
            this.detailsPanelArgs = new DetailsPanelViewerArgs();
        }

        public ToolbarArgs($value? : ToolbarPanelViewerArgs) : ToolbarPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ToolbarPanelViewerArgs)) {
                this.toolbarArgs = $value;
            }
            return this.toolbarArgs;
        }

        public StatusText($value? : string) : string {
            return this.statusText = Property.String(this.statusText, $value);
        }

        public NodesFilterArgs($value? : NodesFilterPanelViewerArgs) : NodesFilterPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(NodesFilterPanelViewerArgs)) {
                this.nodesFilterArgs = $value;
            }
            return this.nodesFilterArgs;
        }

        public KernelNodePickerPanelArgs($value? : KernelNodesPickerPanelViewerArgs) : KernelNodesPickerPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(KernelNodesPickerPanelViewerArgs)) {
                this.kernelNodePickerPanelArgs = $value;
            }
            return this.kernelNodePickerPanelArgs;
        }

        public VisualGraphPanelArgs($value? : VisualGraphPanelViewerArgs) : VisualGraphPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(VisualGraphPanelViewerArgs)) {
                this.visualGraphPanelArgs = $value;
            }
            return this.visualGraphPanelArgs;
        }

        public PropertiesLayerSelectorArgs($value? : LayerSelectorPanelViewerArgs) : LayerSelectorPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(LayerSelectorPanelViewerArgs)) {
                this.propertiesLayerArgs = $value;
            }
            return this.propertiesLayerArgs;
        }

        public DetailsPanelArgs($value? : DetailsPanelViewerArgs) : DetailsPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(DetailsPanelViewerArgs)) {
                this.detailsPanelArgs = $value;
            }
            return this.detailsPanelArgs;
        }
    }
}
