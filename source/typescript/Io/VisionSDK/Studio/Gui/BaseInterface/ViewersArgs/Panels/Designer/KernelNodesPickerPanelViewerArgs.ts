/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    export class KernelNodesPickerPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private readonly nodes : ArrayList<IKernelNode>;

        constructor() {
            super();

            this.nodes = new ArrayList<IKernelNode>();
        }

        public getKernelNodes() : ArrayList<IKernelNode> {
            return this.nodes;
        }

        public AddKernelNode($value : IKernelNode) : IKernelNode {
            this.nodes.Add($value);
            return $value;
        }
    }
}
