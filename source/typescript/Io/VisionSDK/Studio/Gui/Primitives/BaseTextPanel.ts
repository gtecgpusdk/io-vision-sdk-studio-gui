/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;

    export class BaseTextPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        private text : Label;

        constructor($id? : string) {
            super($id);
            this.text = new Label();
        }

        public Text($value? : string) : string {
            return this.text.Text($value);
        }

        public Value($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
            return <BasePanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.text);
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }
}
