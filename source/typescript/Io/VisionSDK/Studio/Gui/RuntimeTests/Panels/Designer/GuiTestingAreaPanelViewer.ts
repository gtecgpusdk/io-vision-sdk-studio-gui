/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;

    export class GuiTestingAreaPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : AreaPanelViewerArgs) {
            super();
            this.setInstance(new GuiTestingAreaPanel($args));
        }

        public getInstance() : GuiTestingAreaPanel {
            return <GuiTestingAreaPanel>super.getInstance();
        }

        public ViewerArgs($args? : AreaPanelViewerArgs) : AreaPanelViewerArgs {
            return <AreaPanelViewerArgs>super.ViewerArgs($args);
        }

        protected testImplementation($instance : GuiTestingAreaPanel) : string {
            const sizeHandler : any = () : void => {
                $instance.Width(Math.max(640, WindowManager.getSize().Width() - 60));
                $instance.Height(Math.max(480, WindowManager.getSize().Height() - 160));
            };

            WindowManager.getEvents().setOnResize(() => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            $instance.getEvents().setOnComplete(() : void => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(768);
            return "<style>.TestCss .ComWuiFrameworkUserControlsPrimitivesBasePanel {border:0; margin:0}</style>";
        }
    }
}
/* dev:end */
