/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import ConnectionSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ConnectionSettingsPanelViewerArgs;
    import ToolchainSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ToolchainSettingsPanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;

    export class SettingsAreaPanelViewerArgs extends AreaPanelViewerArgs {
        private projectSettingsPanelArgs : ProjectSettingsPanelViewerArgs;
        private connectionSettingsPanelArgs : ConnectionSettingsPanelViewerArgs;
        private toolchainSettingsPanelArgs : ToolchainSettingsPanelViewerArgs;

        public ProjectSettingsPanelArgs($value? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ProjectSettingsPanelViewerArgs)) {
                this.projectSettingsPanelArgs = $value;
            }
            return this.projectSettingsPanelArgs;
        }

        public ConnectionSettingsPanelArgs($value? : ConnectionSettingsPanelViewerArgs) : ConnectionSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ConnectionSettingsPanelViewerArgs)) {
                this.connectionSettingsPanelArgs = $value;
            }
            return this.connectionSettingsPanelArgs;
        }

        public ToolchainSettingsPanelArgs($value? : ToolchainSettingsPanelViewerArgs) : ToolchainSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ToolchainSettingsPanelViewerArgs)) {
                this.toolchainSettingsPanelArgs = $value;
            }
            return this.toolchainSettingsPanelArgs;
        }
    }
}
