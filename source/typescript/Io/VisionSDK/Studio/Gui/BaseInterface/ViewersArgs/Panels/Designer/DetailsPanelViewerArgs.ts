/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class DetailsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private propertiesPanelArgs : PropertiesPanelViewerArgs;

        constructor() {
            super();
            this.propertiesPanelArgs = new PropertiesPanelViewerArgs();
        }

        public PropertiesPanelArgs($value? : PropertiesPanelViewerArgs) : PropertiesPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(PropertiesPanelViewerArgs)) {
                this.propertiesPanelArgs = $value;
            }
            return this.propertiesPanelArgs;
        }
    }
}
