/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import PropertiesPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.PropertiesPanelViewerArgs;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import PropertyFormArgs = Io.VisionSDK.UserControls.Structures.PropertyFormArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import BaseFormInputArgs = Com.Wui.Framework.UserControls.Structures.BaseFormInputArgs;
    import PropertyListFormArgs = Io.VisionSDK.UserControls.Structures.PropertyListFormArgs;
    import IPropertiesPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IPropertiesPanelEvents;
    import PropertyEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.PropertyEventArgs;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import PropertiesPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.PropertiesPanelEventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import PropertyPickerFormArgs = Io.VisionSDK.UserControls.Structures.PropertyPickerFormArgs;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Image = Io.VisionSDK.UserControls.BaseInterface.UserControls.Image;
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import IKernelNodeEnumAttributeItem = Io.VisionSDK.UserControls.Interfaces.IKernelNodeEnumAttributeItem;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;

    export class PropertiesPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public nameForm : FormInput;
        public typeForm : FormInput;
        private properties : ArrayList<FormInput>;
        private inputImage : Image;
        private outputImage : Image;
        private displayOutput : Image;
        private workingDirectory : string;
        private node : IKernelNode;
        private currentGraphRoot : IKernelNode;
        private nodesByName : ArrayList<IKernelNode>;
        private childMarginRight : number;
        private renameEnabled : boolean;

        private static resizeHandler($element : PropertiesPanel, $size : number) : void {
            $element.getChildElements().foreach(($child : FormInput) : void => {
                if ($child.IsTypeOf(FormInput) && $child.Visible()) {
                    $child.Configuration().ForceSetValue(true);
                    $child.Configuration().Width($size);
                    $child.Configuration($child.Configuration());
                }
            });
            if (!ObjectValidator.IsEmptyOrNull($element.inputImage) && $element.inputImage.Visible()) {
                PropertiesPanel.fitImage($element.inputImage, $size);
            }
            if (!ObjectValidator.IsEmptyOrNull($element.outputImage) && $element.outputImage.Visible()) {
                PropertiesPanel.fitImage($element.outputImage, $size);
            }
            if (!ObjectValidator.IsEmptyOrNull($element.displayOutput) && $element.displayOutput.Visible()) {
                PropertiesPanel.fitImage($element.displayOutput, $size);
            }
        }

        private static fitImage($image : Image, $size : number) : void {
            if (!ObjectValidator.IsEmptyOrNull($image)) {
                if ($image.Width() !== $size) {
                    $image.setSize($size, $size);
                    $image.OutputArgs($image.OutputArgs());
                }
            }
        }

        constructor($id? : string) {
            super($id);

            this.properties = new ArrayList<FormInput>();
            this.nameForm = new FormInput(new PropertyFormArgs());
            this.typeForm = new FormInput(new PropertyFormArgs());
            this.workingDirectory = "..";
            this.node = null;
            this.nodesByName = new ArrayList<IKernelNode>();
            this.currentGraphRoot = null;
            this.renameEnabled = true;
            this.childMarginRight = 6;
        }

        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                PropertiesPanel.resizeHandler(this, $value - this.childMarginRight);
            }
            return super.Width($value);
        }

        public getEvents() : IPropertiesPanelEvents {
            return <IPropertiesPanelEvents>super.getEvents();
        }

        public Value($value? : PropertiesPanelViewerArgs) : PropertiesPanelViewerArgs {
            return <PropertiesPanelViewerArgs>super.Value($value);
        }

        public getCurrentNode() : IKernelNode {
            return this.node;
        }

        public WorkingDirectory($value? : string) : string {
            return this.workingDirectory = Property.String(this.workingDirectory, $value);
        }

        public MarkCorrections(attrNames : string[]) : void {
            attrNames.forEach(($attrName : string) => {
                const propertyName : string = this.node.Type() + "." + $attrName;
                // todo : mark corrected value
            });
        }

        public setNameValidity($value : boolean) : void {
            if ($value) {
                this.nameForm.Error(false);
            } else {
                this.nameForm.Error(true);
            }
        }

        public ShowProperties($node : IKernelNode, $isRenameEnabled : boolean = true) : void {
            this.node = $node;
            this.renameEnabled = $isRenameEnabled;
            if (this.Visible()) {
                const attributes : string[] = [];
                const propertyWidth : number = Math.ceil(this.Width() - this.childMarginRight);
                const forceSetValue : any = ($property : FormInput, $value : any, $attributeType? : string, name? : string) : void => {
                    if (ObjectValidator.IsArray($value)) {
                        $value = JSON.stringify($value);
                    }
                    const config : BaseFormInputArgs = $property.Configuration();
                    $property.setWidth(propertyWidth);
                    let updated : boolean = false;
                    if (!ObjectValidator.IsEmptyOrNull(name) && config.Name() !== name) {
                        config.Name(name);
                        updated = true;
                    }
                    if (!this.getGuiManager().IsActive($property.getInput()) && $property.Value() !== $value) {
                        config.Value($value);
                        config.ForceSetValue(true);
                        updated = true;
                    }
                    if (config.Value() !== $value) {
                        config.Value($value);
                    }

                    if (updated) {
                        $property.Configuration(config);
                    }
                };
                if (!this.nameForm.Visible()) {
                    this.nameForm.Visible(true);
                    this.typeForm.Visible(true);
                }
                forceSetValue(this.nameForm, $node.Name());
                forceSetValue(this.typeForm, $node.Type());

                (<TextField>this.nameForm.getInput()).ReadOnly(!$isRenameEnabled);

                const keyFilter : any = ($type : string, $eventArgs : EventArgs) : void => {
                    const events : KeyboardEvent = <KeyboardEvent>$eventArgs.NativeEventArgs();
                    const key : any = events.key;
                    if ((($type === "Directory" ||
                        $type === "ImageFile" ||
                        $type === "VideoFile" ||
                        $type === "DataFile" ||
                        $type === "JsonpFile") && key.match(/[a-zA-Z0-9_\-\s:\/\.\\]/g) === null) ||
                        $type === "Name" && key.match(/[a-zA-Z0-9_]/g) === null) {
                        $eventArgs.PreventDefault();
                    }
                };
                (<TextField>this.nameForm.getInput()).getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : EventArgs) : void => {
                    keyFilter("Name", $eventArgs);
                });

                $node.getAttributes().foreach(($attribute : IKernelNodeAttribute) : void => {
                    let property : FormInput;
                    const propertyName : string = $node.Type() + "." + $attribute.name;
                    if (this.properties.KeyExists(propertyName)) {
                        property = this.properties.getItem(propertyName);
                        const visible : boolean = !(ObjectValidator.IsSet($attribute.format) && $attribute.format.hidden);
                        property.Visible(visible);
                        property.Configuration().Visible(visible);
                    } else if (!ObjectValidator.IsSet($attribute.format) ||
                        ObjectValidator.IsSet($attribute.format) && !$attribute.format.hidden) {
                        let args : BaseFormInputArgs = new PropertyFormArgs();
                        if ($attribute.type !== "Array" &&
                            ObjectValidator.IsSet($attribute.format) &&
                            (ObjectValidator.IsSet($attribute.format.min) || ObjectValidator.IsSet($attribute.format.max))) {
                            args = new PropertyPickerFormArgs();
                            (<PropertyPickerFormArgs>args).RangeStart($attribute.format.min);
                            (<PropertyPickerFormArgs>args).RangeEnd($attribute.format.max);
                            (<PropertyPickerFormArgs>args).Step($attribute.format.step);
                            (<PropertyPickerFormArgs>args).Size($attribute.format.decimalPlaces);
                            (<PropertyPickerFormArgs>args).Value($attribute.value);
                        } else if ($attribute.type === "Integer") {
                            (<PropertyFormArgs>args).IntegerOnly(true);
                        } else if ($attribute.type === "Float") {
                            (<PropertyFormArgs>args).FloatOnly(true);
                        } else if ($attribute.type === "Enum") {
                            args = new PropertyListFormArgs();
                            $attribute.items.forEach(($item : string | IKernelNodeEnumAttributeItem) : void => {
                                if (ObjectValidator.IsString($item)) {
                                    (<PropertyListFormArgs>args).AddItem(<string>$item);
                                } else {
                                    (<PropertyListFormArgs>args).AddItem(
                                        (<IKernelNodeEnumAttributeItem>$item).text,
                                        (<IKernelNodeEnumAttributeItem>$item).value);
                                }
                            });
                        }
                        if (args.IsTypeOf(PropertyFormArgs) &&
                            (ObjectValidator.IsSet($attribute.format) && $attribute.format.readonly) ||
                            $attribute.type === "Directory" ||
                            $attribute.type === "ImageFile" ||
                            $attribute.type === "VideoFile" ||
                            $attribute.type === "DataFile" ||
                            $attribute.type === "JsonpFile") {
                            (<PropertyFormArgs>args).ReadOnly(true);
                        }
                        property = new FormInput(args);
                        property.Visible(true);
                        property.getInput().IsPersistent(false);

                        const firePropertyEvent : any = ($eventType : PropertiesPanelEventType) : void => {
                            const eventArgs : PropertyEventArgs = new PropertyEventArgs();
                            eventArgs.Owner(this.node);
                            eventArgs.Name($attribute.name);
                            eventArgs.PropertyType($attribute.type);
                            if ($attribute.type === "Array") {
                                try {
                                    eventArgs.Value(JSON.parse(<string>property.Value()));
                                    this.getEventsManager().FireEvent(this, <string>$eventType, eventArgs);
                                    this.getEventsManager().FireEvent(this.getClassName(), <string>$eventType, eventArgs);
                                } catch (ex) {
                                    // dont fire events if array is invalid
                                }
                            } else {
                                eventArgs.Value(property.Value());
                                this.getEventsManager().FireEvent(this, <string>$eventType, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), <string>$eventType, eventArgs);
                            }
                        };
                        property.getInput().getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                            if ($manager.IsActive(property.getInput())) {
                                firePropertyEvent(PropertiesPanelEventType.ON_PROPERTY_CHANGE);
                            }
                        });
                        if (args.IsTypeOf(PropertyFormArgs)) {
                            property.getInput().getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : EventArgs) : void => {
                                keyFilter($attribute.type, $eventArgs);
                            });
                        }
                        property.getEvents().setOnClick(() : void => {
                            ToolTip.HideGlobal(true);
                            firePropertyEvent(PropertiesPanelEventType.ON_PROPERTY_SELECT);
                        });

                        property.getInput().getEvents().setOnClick(() : void => {
                            ToolTip.HideGlobal(true);
                        });

                        this.AddChild(property);
                        this.properties.Add(property, propertyName);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(property)) {
                        attributes.push(propertyName);
                        forceSetValue(property, $attribute.value, $attribute.type, $attribute.text);
                    }
                });

                this.properties.foreach(($property : FormInput, $name : string) : void => {
                    if (attributes.indexOf($name) === -1) {
                        if ($property.Visible()) {
                            $property.Visible(false);
                        }
                    }
                });

                if (!ObjectValidator.IsEmptyOrNull(this.inputImage)) {
                    this.inputImage.Visible(false);
                }
                if (!ObjectValidator.IsEmptyOrNull(this.outputImage)) {
                    this.outputImage.Visible(false);
                }
                if (!ObjectValidator.IsEmptyOrNull(this.displayOutput)) {
                    this.displayOutput.Visible(false);
                }

                if ($node.Type() === "IMAGE_INPUT" ||
                    $node.Type() === "IMAGE_OUTPUT" ||
                    $node.Type() === "VIDEO_OUTPUT" ||
                    $node.Type() === "DISPLAY") {
                    let imageSource : string;
                    if ($node.Type() === "VIDEO_OUTPUT" ||
                        $node.Type() === "DISPLAY") {
                        if ($node.getAttributes().KeyExists("stream")) {
                            imageSource = $node.getAttributes().getItem("stream").value;
                        }
                    } else {
                        imageSource = $node.getAttributes().getItem("path").value;
                        imageSource = StringUtils.Replace(imageSource, "\\", "/");
                        imageSource = StringUtils.Replace(imageSource, "//", "/");
                    }

                    if (!ObjectValidator.IsEmptyOrNull(imageSource) && imageSource !== "unspecified") {
                        if (!StringUtils.Contains(imageSource, ":/")) {
                            let cwd : string = this.workingDirectory;
                            if (!StringUtils.Contains(cwd, ":/")) {
                                cwd = "../" + cwd;
                            }
                            imageSource = cwd + "/" + imageSource;
                        } else if (!StringUtils.StartsWith(imageSource, "http://") &&
                            !StringUtils.StartsWith(imageSource, "https://") &&
                            !StringUtils.StartsWith(imageSource, "file://")) {
                            imageSource = "file:///" + imageSource;
                        }

                        const createImage : any = () : Image => {
                            const image : Image = new Image(imageSource);
                            image.loadingSpinner.IconType(IconType.SPINNER_TRANSPARENT_BIG);
                            image.loadingText.Visible(false);
                            const args : ImageOutputArgs = new ImageOutputArgs();
                            if ($node.Type() === "VIDEO_OUTPUT" ||
                                $node.Type() === "DISPLAY") {
                                args.CanvasEnabled(false);
                            } else {
                                args.FrontEndCacheEnabled(false);
                            }
                            args.FillEnabled(false);
                            args.setSize(this.Width(), this.Width());
                            image.OutputArgs(args);
                            this.AddChild(image);
                            return image;
                        };
                        if ($node.Type() === "IMAGE_INPUT") {
                            if (ObjectValidator.IsEmptyOrNull(this.inputImage)) {
                                this.inputImage = createImage();
                            } else {
                                this.inputImage.Visible(true);
                                this.inputImage.Source(imageSource);
                            }
                            const colorType : string = $node.getAttributes().getItem("outputType").value;
                            const imageArgs : ImageOutputArgs = this.inputImage.OutputArgs();
                            imageArgs.GrayscaleEnabled(colorType !== "VX_DF_IMAGE_RGB" && colorType !== "VX_DF_IMAGE_RGBX");
                            PropertiesPanel.fitImage(this.inputImage, propertyWidth);
                        }
                        if ($node.Type() === "IMAGE_OUTPUT") {
                            if (ObjectValidator.IsEmptyOrNull(this.outputImage)) {
                                this.outputImage = createImage();
                            } else {
                                this.outputImage.Visible(true);
                                this.outputImage.Source("/");
                                this.outputImage.Source(imageSource);
                            }
                            PropertiesPanel.fitImage(this.outputImage, propertyWidth);
                        }
                        if ($node.Type() === "VIDEO_OUTPUT" ||
                            $node.Type() === "DISPLAY") {
                            if (ObjectValidator.IsEmptyOrNull(this.displayOutput)) {
                                this.displayOutput = createImage();
                            } else {
                                this.displayOutput.Visible(true);
                                this.displayOutput.Source(imageSource);
                            }
                            this.displayOutput.alternateText.Text(" ");
                            this.displayOutput.alternateText.Visible(true);
                            PropertiesPanel.fitImage(this.displayOutput, propertyWidth);
                        }
                    }
                }
                BasePanel.scrollBarVisibilityHandler(this);
            }
        }

        public getInputFileIOSize($callback : ($size : Size) => void) : void {
            const size : Size = new Size();
            if (!ObjectValidator.IsEmptyOrNull(this.inputImage)) {
                const reader : HTMLImageElement = document.createElement("img");
                reader.onload = () : void => {
                    try {
                        size.Width(reader.naturalWidth);
                        size.Height(reader.naturalHeight);
                        $callback(size);
                    } catch (ex) {
                        LogIt.Error("Unable to get FileIO size", ex);
                    }
                };
                reader.onerror = () : void => {
                    $callback(size);
                };
                reader.src = this.inputImage.Source();
            } else {
                $callback(size);
            }
        }

        protected innerCode() : IGuiElement {
            this.nameForm.Visible(false);
            this.typeForm.Visible(false);
            this.Scrollable(true);
            this.getEvents().setOnLoad(() : void => {
                this.getChildElements().foreach(($child : FormInput) : void => {
                    if ($child.IsTypeOf(FormInput)) {
                        $child.getInput().IsPersistent(false);
                    }
                });
            });

            this.getEvents().setBeforeResize(() : void => {
                this.Scrollable(false);
            });

            this.getEvents().setOnShow(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.node)) {
                    this.ShowProperties(this.node, this.renameEnabled);
                }
            });

            this.getEvents().setOnResize(($eventArgs : ResizeEventArgs) : void => {
                if ($eventArgs.ScrollBarChanged()) {
                    // PropertiesPanel.resizeHandler(this, (ElementManager.IsVisible(this.Id() + "_Vertical_ScrollBar") ?
                    //     $eventArgs.AvailableWidth() : $eventArgs.Width()) - this.childMarginRight);
                } else if (!this.Scrollable()) {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.Scrollable(true);
                        BasePanel.scrollBarVisibilityHandler(this);
                    }, true, 300);
                }
            });

            (<TextField>this.nameForm.getInput()).getEvents().setEvent(EventType.ON_KEY_UP, () : void => {
                const eventArgs : PropertyEventArgs = new PropertyEventArgs();
                eventArgs.Owner(this.node);
                eventArgs.Name("name");
                eventArgs.Value(this.nameForm.Value());
                this.getEventsManager().FireEvent(this, PropertiesPanelEventType.ON_PROPERTY_CHANGE, eventArgs);
                this.getEventsManager().FireEvent(this.getClassName(), PropertiesPanelEventType.ON_PROPERTY_CHANGE, eventArgs);
            });
            (<PropertyFormArgs>this.typeForm.Configuration()).ReadOnly(true);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.nameForm)
                .Add(this.typeForm);
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }
}
