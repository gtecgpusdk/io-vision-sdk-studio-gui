/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelNodesPickerPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.KernelNodesPickerPanelViewerArgs;
    import KernelNodesPickerPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.KernelNodesPickerPanel;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeHelp = Io.VisionSDK.UserControls.Interfaces.IKernelNodeHelp;

    export class KernelNodesPickerPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : KernelNodesPickerPanelViewerArgs {
            const args : KernelNodesPickerPanelViewerArgs = new KernelNodesPickerPanelViewerArgs();
            const groupNum : number = Math.ceil(Math.random() * 10);
            KernelNodeType.getProperties().forEach(($type : string) : void => {
                $type = KernelNodeType[$type];
                if (KernelNodeType.Contains($type) &&
                    !StringUtils.ContainsIgnoreCase($type, KernelNodeType.DROP_AREA, KernelNodeType.VISUAL_GRAPH)) {
                    const node : IKernelNode = new KernelNode($type, -1, -1,
                        "Group" + Math.ceil(groupNum * Math.random()));
                    node.Type($type);
                    node.Name($type);
                    if (node.Type() !== KernelNodeType.VXCONTEXT && node.Type() !== KernelNodeType.VXGRAPH) {
                        if (Math.random() > 0.5) {
                            node.AddInput("DATATYPE1");
                            node.AddInput("DATATYPE2");
                            node.AddInput("DATATYPE2");
                            node.Help(<IKernelNodeHelp>{
                                description: "TEST INPUT NODE.",
                                inputs     : ["index0 DATATYPE1", "index1 DATATYPE2", "index2 DATATYPE2"],
                                link       : "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/index.html#" + $type,
                                tooltip    : "Test input node"
                            });
                        } else {
                            node.AddOutput("DATATYPE1");
                            node.AddOutput("DATATYPE2");
                            node.AddOutput("DATATYPE2");
                            node.Help(<IKernelNodeHelp>{
                                description: "TEST OUTPUT NODE.",
                                link       : "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/index.html",
                                outputs    : ["index0 DATATYPE1", "index1 DATATYPE2", "index2 DATATYPE2"],
                                tooltip    : "Test output node"
                            });
                        }
                    }
                    args.AddKernelNode(node);
                }
            });
            return args;
        }

        /* dev:end */

        constructor($args? : KernelNodesPickerPanelViewerArgs) {
            super($args);
            this.setInstance(new KernelNodesPickerPanel());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.KernelNodesPickerPanelTest);
            /* dev:end */
        }

        public getInstance() : KernelNodesPickerPanel {
            return <KernelNodesPickerPanel>super.getInstance();
        }

        public ViewerArgs($args? : KernelNodesPickerPanelViewerArgs) : KernelNodesPickerPanelViewerArgs {
            return <KernelNodesPickerPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : KernelNodesPickerPanel, $args : KernelNodesPickerPanelViewerArgs) : void {
            $instance.setNodes($args.getKernelNodes());
        }
    }
}
