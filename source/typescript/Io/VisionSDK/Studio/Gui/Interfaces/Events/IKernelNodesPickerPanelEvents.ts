/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";

    export interface IKernelNodesPickerPanelEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents {
        setOnNodeSelect($handler : IVisualGraphEventsHandler) : void;

        setOnNodesPicked($handler : IVisualGraphEventsHandler) : void;
    }
}
