/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ToolchainSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ToolchainSettingsPanelViewerArgs;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ISettingsPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEvents;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IToolchainSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettingsValue;
    import IToolchainSettings = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettings;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    export class ToolchainSettingsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerText : string;
        public nameLabel : Label;
        public cmakeLabel : Label;
        public cppLabel : Label;
        public linkerLabel : Label;
        public toolchainOption : DropDownList;
        public cmake : TextField;
        public cpp : TextField;
        public linker : TextField;
        public validateButton : Button;
        public panelDescription : string;
        public values : ArrayList<IToolchainSettingsValue>;
        private readonly selected : number;
        private isEnabled : boolean;
        private fieldSizeHandler : () => (string | string[]);

        constructor($id? : string) {
            super($id);
            this.headerText = "";

            this.validateButton = new Button(ButtonType.GREEN_GRAY_MEDIUM);
            this.nameLabel = new Label();
            this.cmakeLabel = new Label();
            this.cppLabel = new Label();
            this.linkerLabel = new Label();
            this.toolchainOption = new DropDownList(DropDownListType.SETTING);
            this.cmake = new TextField(TextFieldType.SETTING);
            this.cpp = new TextField(TextFieldType.SETTING);
            this.linker = new TextField(TextFieldType.SETTING);
            this.panelDescription = "";
            this.values = new ArrayList<IToolchainSettingsValue>();
            this.selected = 0;
            this.isEnabled = true;
            this.fieldSizeHandler = () : string => {
                return undefined;
            };
        }

        public Enabled($value? : boolean) : boolean {
            this.isEnabled = Property.Boolean(this.isEnabled, $value);
            [this.toolchainOption, this.cmake, this.cpp, this.linker].forEach(($commons : GuiCommons) : void => {
                $commons.Enabled(this.isEnabled);
            });
            return this.isEnabled;
        }

        public Value($value? : ToolchainSettingsPanelViewerArgs) : ToolchainSettingsPanelViewerArgs {
            return <ToolchainSettingsPanelViewerArgs>super.Value($value);
        }

        public getSettings() : IToolchainSettings {
            const settings : IToolchainSettings = {
                options: JSON.parse(JSON.stringify(this.values.ToArray()))
            };
            const selected : IToolchainSettingsValue = settings.options[this.selected];
            if (!ObjectValidator.IsEmptyOrNull(selected)) {
                selected.name = this.toolchainOption.Value();
                selected.compiler = this.cpp.Value();
                selected.cmake = this.cmake.Value();
                selected.linker = this.linker.Value();
            }
            return settings;
        }

        public ApplySettings($value? : IToolchainSettings) : void {
            const replacer : any = (key : any, val : any) : void => {
                if (key !== "selected") {
                    return val;
                }
            };
            const apply : any = ($settings : IToolchainSettings) : void => {
                const value : IToolchainSettingsValue = $settings.options[this.selected];
                this.linker.Value(value.linker);
                this.cpp.Value(value.compiler);
                this.cmake.Value(value.cmake);

                this.values.Clear();
                this.toolchainOption.Clear();
                $settings.options.forEach(($option : IToolchainSettingsValue) : void => {
                    this.values.Add($option, $option.name);
                    this.toolchainOption.Add($option.name);
                });
                this.toolchainOption.Select(value.name);

                const args : ToolchainSettingsPanelViewerArgs = this.Value();
                if (!ObjectValidator.IsEmptyOrNull(args)) {
                    args.Value($settings);
                }
            };
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (JSON.stringify(this.getSettings(), replacer) !== JSON.stringify($value, replacer)) {
                    apply($value);
                }
            } else {
                apply(this.getSettings());
            }
        }

        public setFieldSizeHandler($value : () => string | string[]) : void {
            this.fieldSizeHandler = $value;
        }

        public getEvents() : ISettingsPanelEvents {
            return <ISettingsPanelEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.toolchainOption.MaxVisibleItemsCount(3);

            this.validateButton.Visible(false);
            this.validateButton.Enabled(true);

            [this.toolchainOption, this.cmake, this.cpp, this.linker].forEach(($element : any) : void => {
                $element.getEvents().setOnChange(() : void => {
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                });
            });

            [this.cmake, this.cpp].forEach(($field : TextField) : void => {
                $field.ReadOnly(true);
                $field.getEvents().setOnClick(() : void => {
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner($field);
                    this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, eventArgs);
                });
            });

            this.toolchainOption.getEvents().setOnSelect(() : void => {
                const selected : IToolchainSettingsValue = this.values.getItem(this.toolchainOption.Value());
                this.cmake.Value(selected.cmake);
                this.cpp.Value(selected.compiler);
                this.linker.Value(selected.linker);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                .Add(this.addRow()
                    .HeightOfRow(() : PropagableNumber => {
                        return new PropagableNumber(this.validateButton.Visible() ? "70px" : "1px");
                    }))
                .Add(this.addRow()
                    .Add(this.addColumn()
                        .WidthOfColumn(10, UnitType.PCT))
                    .Add(this.addColumn()
                        .WidthOfColumn(180, UnitType.PX)
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.nameLabel)
                        .Add(this.cmakeLabel)
                        .Add(this.cppLabel)
                        .Add(this.linkerLabel))
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            return new PropagableNumber(this.fieldSizeHandler());
                        })
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.toolchainOption)
                        .Add(this.cmake)
                        .Add(this.cpp)
                        .Add(this.linker))
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                ).Add(this.addRow()
                    .HeightOfRow(80, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn("5%"))
                    .Add(this.addColumn().Add(this.validateButton))
                    .Add(this.addColumn().WidthOfColumn("5%"))
                );
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }
}
