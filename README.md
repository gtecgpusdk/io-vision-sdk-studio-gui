# io-vision-sdk-studio-gui v2019.1.0

> WUI Framework library focused on GUI with design for Vision SDK Studio.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## Documentation

This project provides automatically generated documentation in [TypeDoc](http://typedoc.org/) from TypeScript source by running the `docs`
command from {projectRoot}/bin/batch folder.

> NOTE: Documentation can be accessible also from {projectRoot}/build/target/docs/index.html file after successful creation.

## History

### v2019.1.0
WUI Core update. Added support for OVX 1.2 and OVX selection. Stability bug fixes. Responsive design enhancements.
### v2019.0.2
Fixed double event emit for project load. Update of WUI Core.
### v2019.0.1
Stability bug fixes. Settings page update.
### v2019.0.0
Integration of settings page. Refactoring of namespaces. Added better UI management. Stability bug fixes. Enable to add console links.
Added ability to resize grid.
### v2018.3.0
WUI Core update. Added ability to create nodes groups.
### v2018.2.0
Added Hierarchy view. Properties and NodePicker panels integrated into Accordions together with Hierarchy view. 
Stability and performance bug fixes. Added support for groups at NodePicker. Wizard update and bug fixes. 
Update of toolbar and added button suitable for manual update. Convert docs to TypeDoc.
### v2018.1.3
WUI Core update.
### v2018.1.2
VisualGraph API update reflecting new history manager requirements. Added dialog suitable for project export. 
Stability bug fixes and clean up.
### v2018.1.1
Cumulative GUI stability bug fixes. Usage of PointerIcon in Wizard dialog.
### v2018.1.0
Added Context and Graph viewer. Integration of global tooltip. Added zoom feature. Console window stability bug fixes.
### v2018.0.5
Grid for drop area changed to unlimited size. Added ability to select/manipulate with multiple nodes. 
Added ability to drop/copy/pase multiple nodes. Added support for Video IOComs. Removed property change feedback loops.
Debug mode allows to modify paths without dialog interaction. Added filter for passing chars to text properties. Removed run-in-loop button.
Added ability to represent single plain images in grayscale mode.
### v2018.0.4
Added Dialogs focused on project management and examples pool. Bug fix for graph edit when console is shown. 
Propagated API required by custom context menu.
### v2018.0.3
Added build bar with integrated progress bar and target platform selection. Update of validation feedback. 
Fixed stability of nodes help dialog. Added connections settings dialog. Bug fix for loading of input images.
### v2018.0.2
Stability bug fixes for properties tab. Added notifications panel.
### v2018.0.1
Updated toolbar and redesign of AppWizard. Fixed bluer effect for AppWizard at ChromiumRE. 
Stability bug fixes for graph load and properties edit.
### v2018.0.0
Added AppWizard and Console dialogs. Structure refactoring. Toolbar improvements. Added support for nodes name aliasing. 
Added filter bar for nodes picker. Added support for connection stacks.
### v1.2.2
Help dialog stability bug fixes. Add support for connection error messages. Add new toolbar.
### v1.2.1
Stability bug fixes and rendering performance increase.
### v1.2.0
Added responsive design and DirectoryBrowser dialog. Stability bug fixes. Added unlimited matrix sizing. 
Refactoring of KernelNodes and help propagation from templates. Update of SCR and change of history ordering.
Added simple connection validator. Added appPath symbol resolver.
### v1.1.2
Added connections point reflecting VXData outputs/inputs. Bug fixes for drag-and-drop stabilization. 
Added dynamic properties tab and basic support for help metadata. Added static probing hook. Updated temp toolbar.
### v1.1.1
Usage of new icons and toolbar update. Update of dependencies.
### v1.1.0
Added parameters tab and temporary toolbar for debugging in desktop RE. Added fixed version for dependencies.
### v1.0.1
Added basic drag-and-drop feature.
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
