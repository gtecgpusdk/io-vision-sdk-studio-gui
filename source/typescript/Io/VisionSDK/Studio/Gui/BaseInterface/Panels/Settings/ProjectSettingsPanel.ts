/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import TextArea = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextArea;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BaseField = Com.Wui.Framework.UserControls.Primitives.BaseField;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import TextAreaType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextAreaType;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ISettingsPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEvents;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    export class ProjectSettingsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerText : string;
        public nameLabel : Label;
        public vxVersionLabel : Label;
        public descriptionLabel : Label;
        public pathLabel : Label;
        public name : TextField;
        public vxVersionOption : DropDownList;
        public description : TextArea;
        public path : TextField;
        public panelDescription : string;
        private fieldSizeHandler : () => (string | string[]);

        constructor($id? : string) {
            super($id);
            this.headerText = "";
            this.nameLabel = new Label();
            this.vxVersionLabel = new Label();
            this.descriptionLabel = new Label();
            this.pathLabel = new Label();
            this.name = new TextField(TextFieldType.SETTING);
            this.vxVersionOption = new DropDownList(DropDownListType.SETTING);
            this.description = new TextArea(TextAreaType.SETTING);
            this.path = new TextField(TextFieldType.SETTING);
            this.panelDescription = "";
            this.fieldSizeHandler = () : string => {
                return undefined;
            };
        }

        public Value($value? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            return <ProjectSettingsPanelViewerArgs>super.Value($value);
        }

        public getSettings() : IProjectSettingsValue {
            /* tslint:disable: object-literal-sort-keys */
            return {
                name       : this.name.Value(),
                description: this.description.Value(),
                path       : this.path.Value(),
                vxVersion  : this.vxVersionOption.Value()
            };
            /* tslint:enable */
        }

        public ApplySettings($value? : IProjectSettingsValue) : void {
            const replacer : any = (key : any, val : any) : void => {
                if (key !== "selected") {
                    return val;
                }
            };
            const apply : any = ($settings : IProjectSettingsValue) : void => {
                this.name.Value($settings.name);
                this.description.Value($settings.description);
                this.path.Value($settings.path);
                this.vxVersionOption.Value($settings.vxVersion);

                const args : ProjectSettingsPanelViewerArgs = this.Value();
                if (!ObjectValidator.IsEmptyOrNull(args)) {
                    args.Value($settings);
                }
            };
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (JSON.stringify(this.getSettings(), replacer) !== JSON.stringify($value, replacer)) {
                    apply($value);
                }
            } else {
                apply(this.getSettings());
            }
        }

        public getEvents() : ISettingsPanelEvents {
            return <ISettingsPanelEvents>super.getEvents();
        }

        public setFieldSizeHandler($value : () => string | string[]) : void {
            this.fieldSizeHandler = $value;
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            [this.name, this.vxVersionOption, this.description, this.path].forEach(($element : any) : void => {
                $element.getEvents().setOnChange(() : void => {
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                });
            });

            this.vxVersionOption.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.description.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.path.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.vxVersionOption.Enabled(false);
            this.path.ReadOnly(true);

            let isValid : boolean = false;
            const validate : any = ($field : BaseField) : void => {
                if (ObjectValidator.IsEmptyOrNull($field.Value())) {
                    $field.Error(true);
                    isValid = false;
                } else if ($field.Error()) {
                    $field.Error(false);
                }
            };

            this.name.getEvents().setOnChange(() : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    validate(this.name);
                }, 50);
            });

            this.path.getEvents().setOnClick(() : void => {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this.path);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, eventArgs);
            });

            this.getEvents().setOnComplete(() : void => {
                this.name.Error(false);
                this.description.Error(false);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                .Add(this.addRow()
                    .Add(this.addColumn()
                        .WidthOfColumn(10, UnitType.PCT))
                    .Add(this.addColumn()
                        .WidthOfColumn(180, UnitType.PX)
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.nameLabel)
                        .Add(this.vxVersionLabel)
                        .Add(this.descriptionLabel)
                        .Add(this.pathLabel))
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            return new PropagableNumber(this.fieldSizeHandler());
                        })
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.name)
                        .Add(this.vxVersionOption)
                        .Add(this.description)
                        .Add(this.path)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(10, UnitType.PCT))
                );
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }
}
