/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;
    import CropBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.CropBoxType;
    import AreaPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.AreaPanelType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ControlPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.ControlPanelType;
    import UserControlsTestPanelResponsive = Com.Wui.Framework.UserControls.RuntimeTests.UserControlsTestPanelResponsive;
    import UserControlsTestPanelResponsiveSimple = Com.Wui.Framework.UserControls.RuntimeTests.UserControlsTestPanelResponsiveSimple;
    import UserControlsTestPanelResponsiveSimpleViewer
        = Com.Wui.Framework.UserControls.RuntimeTests.UserControlsTestPanelResponsiveSimpleViewer;
    import UserControlsTestPanelResponsiveViewer = Com.Wui.Framework.UserControls.RuntimeTests.UserControlsTestPanelResponsiveViewer;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import AreaPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.AreaPanel;
    import SettingsAreaPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.SettingsAreaPanel;

    export class GuiTestingAreaPanel extends AreaPanel {
        public responsiveTestPanel : UserControlsTestPanelResponsive;
        public collapseTestPanel : UserControlsTestPanelResponsiveSimple;

        constructor($args : AreaPanelViewerArgs, $id? : string) {
            super($args, $id);
            this.responsiveTestPanel
                = <UserControlsTestPanelResponsive>this.addChildPanel(UserControlsTestPanelResponsiveViewer, null);
            this.collapseTestPanel
                = <UserControlsTestPanelResponsiveSimple>this.addChildPanel(UserControlsTestPanelResponsiveSimpleViewer, null);
            this.responsiveTestPanel.Visible(false);
            this.collapseTestPanel.Visible(false);

            const controlPanelType : any = this.controlPanel.GuiType();
            controlPanelType.type = ControlPanelType.GRAY;
            this.GuiType({
                childTypes: {
                    controlPanelType: this.controlPanel.GuiType(),
                    cropBoxType     : CropBoxType.SKY_BLUE
                },
                type      : AreaPanelType.GRAY
            });
        }

        protected onResizeCompleteHandler() : void {
            super.onResizeCompleteHandler();
            const areaSize : Size = this.getAreaStrategy().getAreaSize();
            [this.responsiveTestPanel, this.collapseTestPanel]
                .forEach(($panel : any) : void => {
                    $panel.Width(areaSize.Width());
                    $panel.Height(areaSize.Height());
                });
        }

        protected getAreaElement() : IGuiElement {
            return this.addElement()
                .Add(this.responsiveTestPanel)
                .Add(this.collapseTestPanel);
        }

        protected IsHovered() : boolean {
            const guiManager : GuiObjectManager = this.getGuiManager();
            return !guiManager.IsHovered(ScrollBar);
        }

        protected cssContainerName() : string {
            return SettingsAreaPanel.ClassNameWithoutNamespace() + " " + AreaPanel.ClassNameWithoutNamespace();
        }

        protected innerCode() : IGuiElement {
            this.IsAreaSelectEnabled(false);
            this.getAreaStrategy().setAreaConstraints(480, 640);
            this.responsiveTestPanel.getEvents().setOnResize(() : void => {
                this.getAreaStrategy().setAreaSize(this.responsiveTestPanel.Width(), this.responsiveTestPanel.Height());
            });

            this.collapseTestPanel.getEvents().setOnResize(() : void => {
                this.getAreaStrategy().setAreaSize(this.collapseTestPanel.Width(), this.collapseTestPanel.Height());
            });

            this.controlPanel.setBreadcrumbButtons(this.getMenu());
            this.controlPanel.ResizeConfiguration({
                getBreadcrumbButtonWidth: ($index : number) : string | string[] => {
                    if (this.Width() > 800) {
                        const controlMenuWidth : number
                            = new PropagableNumber(this.controlPanel.ResizeConfiguration().getControlMenuWidth())
                            .Normalize(this.Width(), UnitType.PX);
                        return (800 - controlMenuWidth) * .334 + "px";
                    } else {
                        return undefined;
                    }
                },
                getControlMenuWidth     : () : string | string[] => {
                    if (this.Width() > 700) {
                        return "250px";
                    } else {
                        return "150px";
                    }
                },
                isControlButtonsVisible : () : boolean => {
                    return this.Width() > 700;
                }
            });

            this.getEvents().setOnComplete(() : void => {
                this.getAreaStrategy().setAreaSize(480, 640);
                const areaSize : Size = this.getAreaStrategy().getAreaSize();
                this.collapseTestPanel.Visible(true);
                [this.responsiveTestPanel, this.collapseTestPanel]
                    .forEach(($panel : any) : void => {
                        $panel.Width(areaSize.Width());
                        $panel.Height(areaSize.Height());
                    });
            });

            return super.innerCode();
        }

        private getMenu() : IDropDownButtonArgs[] {
            return [
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback: () : void => {
                                this.responsiveTestPanel.Visible(true);
                                this.collapseTestPanel.Visible(false);
                            },
                            text    : "Resize test"
                        }
                    ]
                },
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback: () : void => {
                                this.responsiveTestPanel.Visible(false);
                                this.collapseTestPanel.Visible(true);
                            },
                            text    : "Collapse test"
                        }
                    ]
                }
            ];
        }
    }
}
/* dev:end */
