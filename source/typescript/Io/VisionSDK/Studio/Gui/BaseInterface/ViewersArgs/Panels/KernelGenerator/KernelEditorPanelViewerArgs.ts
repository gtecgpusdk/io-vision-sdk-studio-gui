/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.KernelGenerator {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class KernelEditorPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private projectPathText : string;
        private projectPathValue : string;
        private projectNameText : string;
        private readonly projects : ArrayList<string>;
        private projectNamespaceText : string;
        private projectNamespaceValue : string;
        private kernelNamespaceText : string;
        private kernelNamespaceValue : string;
        private kernelNameText : string;
        private kernelNameValue : string;
        private descriptionText : string;
        private descriptionValue : string;
        private parametersText : string;
        private parameterDirectionText : string;
        private parameterNameText : string;
        private parameterTypeText : string;
        private parameterDescriptionText : string;
        private readonly parameterDirections : ArrayList<string>;
        private readonly parameterTypes : ArrayList<string>;
        private generateButtonText : string;

        constructor() {
            super();
            this.headerText = "";

            this.projectPathText = "";
            this.projectPathValue = "";
            this.projectNameText = "";
            this.projects = new ArrayList<string>();
            this.projectNamespaceText = "";
            this.projectNamespaceValue = "";
            this.kernelNamespaceText = "";
            this.kernelNamespaceValue = "";
            this.kernelNameText = "";
            this.kernelNameValue = "";
            this.descriptionText = "";
            this.descriptionValue = "";
            this.parametersText = "";
            this.parameterDirectionText = "";
            this.parameterNameText = "";
            this.parameterTypeText = "";
            this.parameterDescriptionText = "";
            this.parameterDirections = new ArrayList<string>();
            this.parameterTypes = new ArrayList<string>();

            this.generateButtonText = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public ProjectPathText($value? : string) : string {
            return this.projectPathText = Property.String(this.projectPathText, $value);
        }

        public ProjectPathValue($value? : string) : string {
            return this.projectPathValue = Property.String(this.projectPathValue, $value);
        }

        public ProjectNameText($value? : string) : string {
            return this.projectNameText = Property.String(this.projectNameText, $value);
        }

        public AddProject($value : string) : void {
            if (!this.projects.Contains($value)) {
                this.projects.Add($value);
            }
        }

        public getProjects() : ArrayList<string> {
            return this.projects;
        }

        public ProjectNamespaceText($value? : string) : string {
            return this.projectNamespaceText = Property.String(this.projectNamespaceText, $value);
        }

        public ProjectNamespaceValue($value? : string) : string {
            return this.projectNamespaceValue = Property.String(this.projectNamespaceValue, $value);
        }

        public KernelNamespaceText($value? : string) : string {
            return this.kernelNamespaceText = Property.String(this.kernelNamespaceText, $value);
        }

        public KernelNamespaceValue($value? : string) : string {
            return this.kernelNamespaceValue = Property.String(this.kernelNamespaceValue, $value);
        }

        public KernelNameText($value? : string) : string {
            return this.kernelNameText = Property.String(this.kernelNameText, $value);
        }

        public KernelNameValue($value? : string) : string {
            return this.kernelNameValue = Property.String(this.kernelNameValue, $value);
        }

        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }

        public DescriptionValue($value? : string) : string {
            return this.descriptionValue = Property.String(this.descriptionValue, $value);
        }

        public ParametersText($value? : string) : string {
            return this.parametersText = Property.String(this.parametersText, $value);
        }

        public ParameterDirectionText($value? : string) : string {
            return this.parameterDirectionText = Property.String(this.parameterDirectionText, $value);
        }

        public ParameterNameText($value? : string) : string {
            return this.parameterNameText = Property.String(this.parameterNameText, $value);
        }

        public ParameterTypeText($value? : string) : string {
            return this.parameterTypeText = Property.String(this.parameterTypeText, $value);
        }

        public ParameterDescriptionText($value? : string) : string {
            return this.parameterDescriptionText = Property.String(this.parameterDescriptionText, $value);
        }

        public AddParameterDirection($value : string) : void {
            if (!this.parameterDirections.Contains($value)) {
                this.parameterDirections.Add($value);
            }
        }

        public getParameterDirections() : ArrayList<string> {
            return this.parameterDirections;
        }

        public AddParameterType($value : string) : void {
            if (!this.parameterTypes.Contains($value)) {
                this.parameterTypes.Add($value);
            }
        }

        public getParameterTypes() : ArrayList<string> {
            return this.parameterTypes;
        }

        public GenerateButtonText($value? : string) : string {
            return this.generateButtonText = Property.String(this.generateButtonText, $value);
        }
    }
}
