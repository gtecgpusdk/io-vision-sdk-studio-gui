/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import PanelViewers = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels;
    import PageViewers = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Pages;
    import DialogViewers = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>Vision SDK Studio GUI Library</h1>" +
                "<h3>WUI Framework library focused on GUI with design for Vision SDK Studio.</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Pages</H3>" +
                "<a href=\"" + PageViewers.Designer.EditorPageViewer.CallbackLink(true) + "\">Editor page</a>" +
                StringUtils.NewLine() +

                "<H3>Panels</H3>" +
                "<a href=\"" + PanelViewers.Designer.ToolbarPanelViewer.CallbackLink(true) + "\">Toolbar panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.GeneralNotificationsPanelViewer.CallbackLink(true) + "\">" +
                "General notifications panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.NodesFilterPanelViewer.CallbackLink(true) + "\">" +
                "Kernel nodes filter panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.KernelNodesPickerPanelViewer.CallbackLink(true) + "\">" +
                "Kernel nodes picker panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.PropertiesPanelViewer.CallbackLink(true) + "\">Properties panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.VisualGraphPanelViewer.CallbackLink(true) + "\">Visual graph panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.VisualGraphControlPanelViewer.CallbackLink(true) + "\">" +
                "Visual graph control panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.DetailsPanelViewer.CallbackLink(true) + "\">Details panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.LayerSelectorPanelViewer.CallbackLink(true) + "\">Layer selector panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.EditorPanelViewer.CallbackLink(true) + "\">Editor panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.KernelGenerator.KernelEditorPanelViewer.CallbackLink(true) + "\">Kernel editor panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.ControlPanelViewer.CallbackLink(true) + "\">Control panel</a>" +
                StringUtils.NewLine() +

                "<H3>Settings</H3>" +
                "<a href=\"" + PanelViewers.Settings.SettingsPanelViewer.CallbackLink(true) + "\">Settings Panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Settings.ProjectSettingsPanelViewer.CallbackLink(true) + "\">Project settings panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Settings.ConnectionSettingsPanelViewer.CallbackLink(true) + "\">Connection settings panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Settings.ToolchainSettingsPanelViewer.CallbackLink(true) + "\">Toolchain Panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Settings.AboutSettingsPanelViewer.CallbackLink(true) + "\">About Panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + PanelViewers.Designer.SettingsAreaPanelViewer.CallbackLink(true) + "\">Settings area panel</a>" +

                "<H3>Dialogs</H3>" +
                "<a href=\"" + DialogViewers.DirectoryBrowserPanelViewer.CallbackLink(true) + "\">Directory browser panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.KernelNodeHelpPanelViewer.CallbackLink(true) + "\">KernelNode Help panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.AppWizardPanelViewer.CallbackLink(true) + "\">Wizard panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.ConsolePanelViewer.CallbackLink(true) + "\">Console panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.ProjectCreationPanelViewer.CallbackLink(true) + "\">Project creation panel</a>" +
                StringUtils.NewLine() +

                "<a href=\"" + DialogViewers.ProjectDescriptionPanelViewer.CallbackLink(true) + "\">Project description panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.ProjectPickerPanelViewer.CallbackLink(true) + "\">Project picker panel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + DialogViewers.ExportPanelViewer.CallbackLink(true) + "\">Project export panel</a>" +
                StringUtils.NewLine();

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("Vision SDK Studio - GUI Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
