/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import VisualGraphControlPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.VisualGraphControlPanel;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class VisualGraphControlsPanelTest extends ViewerTestRunner<VisualGraphControlPanel> {

        public testZoom() : void {
            const visualGraphPanel : VisualGraphControlPanel = this.getInstance();
            visualGraphPanel.Zoom(120);
            this.assertEquals(visualGraphPanel.Zoom(), 120);
            visualGraphPanel.Zoom(80);
            this.assertEquals(visualGraphPanel.Zoom(), 80);
            visualGraphPanel.Zoom(100);
        }

        public testzoomEvent() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const visualGraphPanel : VisualGraphControlPanel = this.getInstance();
                const manager : GuiObjectManager = new GuiObjectManager();
                manager.setActive(visualGraphPanel, true);
                visualGraphPanel.getEvents().setOnZoomChange(($eventArgs : EventArgs) : void => {
                    visualGraphPanel.Zoom(115);
                    this.assertEquals(visualGraphPanel.Zoom(), 115);
                    $done();
                });
                this.emulateEvent(visualGraphPanel, EventType.ON_CLICK, <MouseEvent>{clientX: 0, clientY: 0});
            };
        }

        protected before() : string {
            const instance : VisualGraphControlPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.Width(1024);
            instance.Height(300);
            return "<style>.TestCss .BreadcrumbPanel {border: 1px solid #E7E7E8;}</style>";
        }
    }
}
/* dev:end */
