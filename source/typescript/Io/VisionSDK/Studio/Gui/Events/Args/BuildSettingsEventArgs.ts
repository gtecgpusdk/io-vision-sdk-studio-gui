/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IBuildPlatform = Io.VisionSDK.Studio.Gui.Interfaces.IBuildPlatform;

    /**
     * BuildSettingsEventArgs class provides args connected with change of build environment settings.
     */
    export class BuildSettingsEventArgs extends EventArgs {
        private platform : IBuildPlatform;

        /**
         * @param {IBuildPlatform} [$value] Set value representing selected build platform.
         * @return {IBuildPlatform} Returns value representing selected build platform.
         */
        public BuildPlatform($value? : IBuildPlatform) : IBuildPlatform {
            if (ObjectValidator.IsSet($value)) {
                this.platform = $value;
            }
            return this.platform;
        }
    }
}
