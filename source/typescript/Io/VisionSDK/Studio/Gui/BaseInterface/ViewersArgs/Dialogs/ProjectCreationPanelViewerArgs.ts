/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ProjectCreationPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private createText : string;
        private settingsArgs : ProjectSettingsPanelViewerArgs;

        constructor() {
            super();

            this.createText = "";
            this.settingsArgs = new ProjectSettingsPanelViewerArgs();
        }

        public SettingsArgs($value? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            return this.settingsArgs = ObjectValidator.IsSet($value) ? $value : this.settingsArgs;
        }

        public CreateText($value? : string) : string {
            return this.createText = Property.String(this.createText, $value);
        }
    }
}
