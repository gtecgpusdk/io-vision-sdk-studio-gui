/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import ConsolePanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ConsolePanel;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class ConsolePanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new ConsolePanel());
        }

        public getInstance() : ConsolePanel {
            return <ConsolePanel>super.getInstance();
        }

        /* dev:start */
        protected testImplementation($instance : ConsolePanel) : string {
            $instance.StyleClassName("TestCss");

            Echo.setOnPrint(($message : string) : void => {
                $instance.PrintMessage($message);
            });

            this.addTestButton("Echo", () : void => {
                Echo.Println("Some random text some random text some random text some random text some random text some random");
            });

            this.addTestButton("Clear", () : void => {
                $instance.Clear();
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ConsolePanel{background-color: rgba(0,0,0,0.5); width: 640px; height: 468px;}</style>";
        }

        /* dev:end */
    }
}
