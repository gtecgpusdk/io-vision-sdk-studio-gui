/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ToolbarPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.ToolbarPanel;
    import ToolbarPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ToolbarPanelViewerArgs;

    export class ToolbarPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ToolbarPanelViewerArgs {
            const args : ToolbarPanelViewerArgs = new ToolbarPanelViewerArgs();
            args.ProjectMenuText("Project");
            args.SaveButtonText("Save");
            args.UndoButtonText("Undo");
            args.RedoButtonText("Redo");
            args.RunButtonText("Run once");
            args.OpenButtonText("Open MS Visual Studio");
            args.ConsoleButtonText("Console output");
            args.HelpButtonText("Open application wizard");
            args.ConfigureBuildText("Configure build connection");
            args.UpdateButtonText("Application update");
            args.UpdateButtonHint("Update is available. Press button to start this process.");
            args.BuildPlatforms([
                {text: "remote - i.MX", value: "IMX"},
                {text: "local - WinGCC", value: "WinGCC"}
            ]);
            return args;
        }

        /* dev:end */

        constructor($args? : ToolbarPanelViewerArgs) {
            super($args);
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.ToolbarPanelTest);
            /* dev:end */
            this.setInstance(new ToolbarPanel());
        }

        public getInstance() : ToolbarPanel {
            return <ToolbarPanel>super.getInstance();
        }

        public ViewerArgs($args? : ToolbarPanelViewerArgs) : ToolbarPanelViewerArgs {
            return <ToolbarPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ToolbarPanel, $args : ToolbarPanelViewerArgs) : void {
            $instance.fileMenu.Title().Text($args.ProjectMenuText());
            $instance.saveButton.Title().Text($args.SaveButtonText());
            $instance.undoButton.Title().Text($args.UndoButtonText());
            $instance.redoButton.Title().Text($args.RedoButtonText());
            $instance.runButton.Title().Text($args.RunButtonText());
            $instance.openProjectFolderButton.Title().Text($args.OpenButtonText());
            $instance.consoleButton.Title().Text($args.ConsoleButtonText());
            $instance.helpButton.Title().Text($args.HelpButtonText());
            $instance.updateButton.Text($args.UpdateButtonText());
            $instance.updateButton.Title().Text($args.UpdateButtonHint());
            $instance.configureBuildText = $args.ConfigureBuildText();
            $instance.setPlatforms($args.BuildPlatforms());
        }
    }
}
