/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class ProjectEventType extends EventType {
        public static readonly ON_PROJECT_CREATE : string = "OnProjectCreate";
        public static readonly ON_PROJECT_LOAD : string = "OnProjectLoad";
        public static readonly ON_PATH_REQUEST : string = "OnPathRequest";
        public static readonly ON_PROJECT_SETTINGS_UPDATE : string = "OnProjectSettingsUpdate";
        public static readonly ON_PROJECT_EXPORT : string = "OnProjectExport";
    }
}
