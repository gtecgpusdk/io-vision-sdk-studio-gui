/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IConnectionChange = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionChange;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import INodeChange = Io.VisionSDK.Studio.Gui.Interfaces.INodeChange;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IAttributeChange = Io.VisionSDK.Studio.Gui.Interfaces.IAttributeChange;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;

    export class StateUtils extends BaseObject {

        public static getNodeChanges($nodes : ArrayList<IKernelNode>, $parseSubnodes? : boolean,
                                     $includeVicinity? : boolean) : ArrayList<INodeChange> {
            const getChanges : any = ($nodes : IKernelNode[], $parseVicinity : boolean = $includeVicinity) : INodeChange[] => {
                const returnVal : INodeChange[] = [];
                $nodes.forEach(($node : IKernelNode) : void => {
                    returnVal.push(<INodeChange>{
                        attributeChanges: StateUtils.getAttributeChanges($node),
                        column          : $node.Column(),
                        id              : $node.UniqueId(),
                        nameChange      : {from: $node.Name(), to: $node.Name()},
                        row             : $node.Row(),
                        subnodeChanges  : $parseSubnodes ? getChanges($node.ChildNodes().ToArray(), false) : [],
                        type            : $node.Type()
                    });
                    if ($parseVicinity) {
                        const nodes : IKernelNode[] = [];
                        $node.getOutputs().foreach(($output : Output) : void => {
                            $output.inputs.foreach(($input : Input) : void => {
                                nodes.push($input.node);
                            });
                        });

                        getChanges(nodes, false).forEach(($change : INodeChange) : void => {
                            returnVal.push($change);
                        });
                    }
                });
                return returnVal;
            };

            return ArrayList.ToArrayList(getChanges($nodes.ToArray()));
        }

        public static getConnectionChanges($connections : ArrayList<IConnection>) : ArrayList<IConnectionChange> {
            const returnVal : ArrayList<IConnectionChange> = new ArrayList<IConnectionChange>();
            $connections.foreach(($connection : IConnection) : void => {
                const change : IConnectionChange = StateUtils.getConnectionChange($connection.input, $connection.input.output);
                returnVal.Add(change, JSON.stringify(change));
            });
            returnVal.SortByKeyUp();
            return returnVal;
        }

        public static getNodeConnectionChanges($nodes : ArrayList<IKernelNode>,
                                               $parseSubnodes? : boolean) : ArrayList<IConnectionChange> {
            const returnVal : ArrayList<IConnectionChange> = new ArrayList<IConnectionChange>();
            const getChanges : any = ($node : IKernelNode) : void => {
                $node.getInputs().foreach(($input : Input) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($input.output)) {
                        const change : IConnectionChange = StateUtils.getConnectionChange($input, $input.output);
                        returnVal.Add(change, JSON.stringify(change));
                    }
                });
                $node.getOutputs().foreach(($output : Output) => {
                    $output.inputs.foreach(($input : Input) : void => {
                        const change : IConnectionChange = StateUtils.getConnectionChange($input, $input.output);
                        returnVal.Add(change, JSON.stringify(change));
                    });
                });
                if ($parseSubnodes) {
                    $node.ChildNodes().foreach(($node : IKernelNode) : void => {
                        getChanges($node);
                    });
                }
            };
            $nodes.foreach(getChanges);
            returnVal.SortByKeyUp();
            return returnVal;
        }

        public static getConnectionChange($input : Input, $output : Output) : IConnectionChange {
            return {
                inputNodeId    : $input.node.UniqueId(),
                inputNodeIndex : $input.position,
                outputNodeId   : $output.node.UniqueId(),
                outputNodeIndex: $output.position
            };
        }

        public static getAttributeChange($name : string, $value : any) : IAttributeChange {
            return {
                from: $value,
                name: $name,
                to  : $value
            };
        }

        public static getAttributeChanges($node : IKernelNode) : IAttributeChange[] {
            const returnVal : IAttributeChange[] = [];
            const addChange : any = ($name : string, $value : any) : void => {
                const attributeChange : IAttributeChange = StateUtils.getAttributeChange($name, $value);
                returnVal.push(attributeChange);
            };
            const parseAttribute : any = ($node : IKernelNode) : void => {
                $node.getAttributes().foreach(($attribute : IKernelNodeAttribute) : void => {
                    addChange($attribute.name, $attribute.value, $node.UniqueId());
                });
            };
            parseAttribute($node);
            return returnVal;
        }

        public static mergeNodeChanges($before : ArrayList<INodeChange>,
                                       $after : ArrayList<INodeChange>) : ArrayList<INodeChange> {
            const merged : ArrayList<INodeChange> = new ArrayList<INodeChange>();
            const beforeCopy : INodeChange[] = JSON.parse(JSON.stringify($before.ToArray()));
            const afterCopy : INodeChange[] = JSON.parse(JSON.stringify($after.ToArray()));
            const beforeChanges : ArrayList<INodeChange> = new ArrayList<INodeChange>();
            const afterChanges : ArrayList<INodeChange> = new ArrayList<INodeChange>();

            const getChanges : any = ($changes : INodeChange[], $targetList : ArrayList<INodeChange>) : void => {
                $changes.forEach(($change : INodeChange) : void => {
                    $targetList.Add($change, $change.id);
                    getChanges($change.subnodeChanges, $targetList);
                });
            };
            getChanges(beforeCopy, beforeChanges);
            getChanges(afterCopy, afterChanges);

            beforeChanges.foreach(($change : INodeChange) : void => {
                merged.Add($change, $change.id);
            });
            afterChanges.foreach(($change : INodeChange) : void => {
                if (ObjectValidator.IsEmptyOrNull(merged.getItem($change.id))) {
                    merged.Add($change, $change.id);
                } else {
                    const beforeChange : INodeChange = merged.getItem($change.id);
                    beforeChange.nameChange.to = $change.nameChange.to;
                    beforeChange.row = $change.row;
                    beforeChange.column = $change.column;
                    beforeChange.attributeChanges
                        = StateUtils.mergeAttributeChanges(beforeChange.attributeChanges, $change.attributeChanges);
                }
            });

            const returnVal : ArrayList<INodeChange> = new ArrayList<INodeChange>();
            beforeCopy.forEach(($change : INodeChange) : void => {
                returnVal.Add(merged.getItem($change.id), $change.id);
            });
            afterCopy.forEach(($change : INodeChange) : void => {
                returnVal.Add(merged.getItem($change.id), $change.id);
            });
            return returnVal;
        }

        public static mergeAttributeChanges($before : IAttributeChange[], $after : IAttributeChange[]) : IAttributeChange[] {
            const returnVal : IAttributeChange[] = [];
            for (let i : number = 0; i < $before.length; i++) {
                const merged : IAttributeChange = StateUtils.getAttributeChange($before[i].name, $before[i].from);
                merged.to = $after[i].to;
                returnVal.push(merged);
            }
            return returnVal;
        }
    }
}
