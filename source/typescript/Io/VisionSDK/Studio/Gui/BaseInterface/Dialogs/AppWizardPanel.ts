/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import AppWizardPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.AppWizardPanelViewerArgs;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import IEditorPageEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IEditorPageEvents;
    import EditorPageEventType = Io.VisionSDK.Studio.Gui.Enums.Events.EditorPageEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import PointerIcon = Io.VisionSDK.UserControls.BaseInterface.UserControls.PointerIcon;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class AppWizardPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public description : Label;
        private wizardLogo : Icon;
        private backButton : ImageButton;
        private forwardButton : ImageButton;
        private readonly mouseEmulator : PointerIcon;

        constructor($id? : string) {
            super($id);
            this.description = new Label();

            this.backButton = new ImageButton(ImageButtonType.ARROW_LEFT);
            this.forwardButton = new ImageButton(ImageButtonType.ARROW_RIGHT);
            this.wizardLogo = new Icon(IconType.WIZARD_LOGO);
            this.mouseEmulator = new PointerIcon();
        }

        public HeaderText($value? : string) : string {
            const parent : Dialog = <Dialog>this.Parent();
            if (ObjectValidator.IsSet($value) && !ObjectValidator.IsEmptyOrNull(parent) && parent.IsMemberOf(Dialog)) {
                return parent.headerText.Text($value);
            }
            return "";
        }

        public Value($value? : AppWizardPanelViewerArgs) : AppWizardPanelViewerArgs {
            return <AppWizardPanelViewerArgs>super.Value($value);
        }

        public MouseClick($x : number, $y : number, $tooltip? : string, $callback? : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($callback)) {
                this.mouseEmulator.getEvents().setOnClick(() : void => {
                    $callback();
                });
            }
            this.showMouse($x, $y, this.mouseEmulator.Click, $tooltip);
        }

        public MouseDoubleClick($x : number, $y : number, $tooltip? : string, $callback? : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($callback)) {
                this.mouseEmulator.getEvents().setOnDoubleClick(() : void => {
                    $callback();
                });
            }
            this.showMouse($x, $y, this.mouseEmulator.DoubleClick, $tooltip);
        }

        public MouseDrag($x : number, $y : number, $tooltip? : string, $callback? : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($callback)) {
                this.mouseEmulator.getEvents().setOnDrag(() : void => {
                    $callback();
                });
            }
            this.showMouse($x, $y, this.mouseEmulator.Drag, $tooltip);
        }

        public MouseMoveTo($x : number, $y : number,
                           $animationMath? : ($current : number, $end? : number, $dx? : number, $dy? : number) => number,
                           $callback? : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($callback)) {
                this.mouseEmulator.getEvents().setOnMove(() : void => {
                    $callback();
                });
            }
            ElementManager.Show(this.Id() + "_EmulatorEnvelop");
            this.mouseEmulator.MoveTo($x, $y, $animationMath);
        }

        public MouseHide() : void {
            ElementManager.Hide(this.Id() + "_EmulatorEnvelop");
        }

        public ReleaseEvents() : void {
            this.mouseEmulator.ReleaseEvents();
        }

        public getEvents() : IEditorPageEvents {
            return <IEditorPageEvents>super.getEvents();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.backButton.StyleClassName("Back");
            this.backButton.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                $eventArgs.StopAllPropagation();
                this.getEventsManager().FireEvent(this, EditorPageEventType.ON_WIZARD_BACK);
            });

            this.forwardButton.StyleClassName("Forward");
            this.forwardButton.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                $eventArgs.StopAllPropagation();
                this.getEventsManager().FireEvent(this, EditorPageEventType.ON_WIZARD_FORWARD);
            });

            this.wizardLogo.StyleClassName("WizardLogo");

            this.getEvents().setOnComplete(() : void => {
                this.forwardButton.getEvents().Subscribe(this.forwardButton.Id());
                this.backButton.getEvents().Subscribe(this.backButton.Id());
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement(this.Id() + "_InfoEnvelop")
                    .StyleClassName("InfoEnvelop")
                    .Add(this.addElement().StyleClassName("Navigation")
                        .Add(this.wizardLogo)
                        .Add(this.forwardButton)
                        .Add(this.backButton)
                    )
                    .Add(this.description)
                    .Add(this.addElement(this.Id() + "_EmulatorEnvelop").StyleClassName("EmulatorEnvelop")
                        .Add(this.mouseEmulator)
                    )
                );
        }

        protected showMouse($x : number, $y : number, $method : ($text : string) => void, $tooltip? : string) : void {
            let tooltip : string = "";
            if (!ObjectValidator.IsEmptyOrNull($tooltip)) {
                tooltip = $tooltip;
            }
            ElementManager.Show(this.Id() + "_EmulatorEnvelop");
            if ($x >= 0 && $y >= 0) {
                this.mouseEmulator.setPosition($y, $x);
            }
            $method.apply(this.mouseEmulator, [tooltip]);
        }
    }
}
