/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import VisualGraphPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.VisualGraphPanelViewerArgs;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import KernelNodeEventArgs = Io.VisionSDK.UserControls.Events.Args.KernelNodeEventArgs;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import IVisualGraphPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IVisualGraphPanelEvents;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import KernelNodeConnector = Io.VisionSDK.UserControls.BaseInterface.Components.KernelNodeConnector;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import VisualGraphPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.VisualGraphPanelEventType;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import ConnectionManager = Io.VisionSDK.UserControls.Utils.ConnectionManager;
    import ConnectorHolder = Io.VisionSDK.UserControls.BaseInterface.UserControls.ConnectorHolder;
    import GridManager = Io.VisionSDK.UserControls.Utils.GridManager;
    import StateEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.StateEventArgs;
    import ConnectorHolderEventArgs = Io.VisionSDK.UserControls.Events.Args.ConnectorHolderEventArgs;
    import IIndexChange = Io.VisionSDK.Studio.Gui.Interfaces.IIndexChange;
    import KernelNodeUtils = Io.VisionSDK.UserControls.Utils.KernelNodeUtils;
    import KernelNodeRegistry = Io.VisionSDK.UserControls.Utils.KernelNodeRegistry;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IValidator = Io.VisionSDK.UserControls.Interfaces.IValidator;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IConnectorVisibilityArgs = Io.VisionSDK.UserControls.Interfaces.IConnectorVisibilityArgs;
    import DragNodeType = Io.VisionSDK.UserControls.Enums.DragNodeType;
    import ConnectorHolderStatus = Io.VisionSDK.UserControls.Enums.ConnectorHolderStatus;
    import InterfaceDataType = Io.VisionSDK.UserControls.Enums.InterfaceDataType;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import INodeIntersection = Io.VisionSDK.UserControls.Interfaces.INodeIntersection;
    import ConnectionStatus = Io.VisionSDK.UserControls.Enums.ConnectionStatus;
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;
    import IAreaStrategy = Io.VisionSDK.UserControls.Interfaces.IAreaStrategy;
    import VisualGraphControlPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.VisualGraphControlPanelViewer;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import IBounds = Io.VisionSDK.UserControls.Interfaces.IBounds;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import AreaPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.AreaPanelType;
    import IAreaPanelType = Io.VisionSDK.Studio.Gui.Interfaces.IAreaPanelType;

    export class VisualGraphPanel extends AreaPanel {
        public readonly gridManager : GridManager;
        public readonly connectionManager : ConnectionManager;
        public readonly nodeRegistry : KernelNodeRegistry;
        public readonly nodeUtils : KernelNodeUtils;
        public readonly clipboardNodes : ArrayList<IKernelNode>;
        private readonly joinArea : HTMLElement;
        private dropArea : IGuiElement;
        private graphRoot : IKernelNode;
        private activeNode : IKernelNode;
        private innerOutputsHolder : ConnectorHolder;
        private innerInputsHolder : ConnectorHolder;
        private isGroupingEnabled : boolean;
        private isRootChangeComplete : boolean;
        private rootQueueHandler : () => void;
        private prevHoveredRow : number;
        private prevHoveredCol : number;

        constructor($id? : string) {
            super(new AreaPanelViewerArgs(), $id);
            this.joinArea = document.createElement("svg");
            this.joinArea.id = this.Id() + "_JoinArea";
            this.dropArea = this.addElement(this.Id() + "_DropArea");

            this.gridManager = new GridManager(this, {
                dragHolderId: this.Id() + "_NodeGroupEnvelop",
                dropAreaId  : this.dropArea.getId()
            }, {
                /* tslint:disable: object-literal-sort-keys */
                getNodePosition     : KernelNode.getGridPosition,
                maxScrollSpeed      : 8,
                nodeSize            : KernelNode.envelopSize,
                nodeRadius          : KernelNode.radius,
                scrollAreaSize      : 80,
                selectScrollAreaSize: 160,
                minGridSize         : 4
                /* tslint:enable: object-literal-sort-keys */
            }, super.getAreaStrategy());

            this.connectionManager = new ConnectionManager(this.joinArea.id, this.gridManager);
            this.nodeRegistry = new KernelNodeRegistry("../../target");
            this.nodeUtils = new KernelNodeUtils(this.gridManager, this.connectionManager, this.nodeRegistry);
            this.graphRoot = null;
            this.clipboardNodes = new ArrayList<IKernelNode>();
            this.innerOutputsHolder = new ConnectorHolder(KernelNodeInterfaceType.OUTPUT, this.connectionManager);
            this.innerInputsHolder = new ConnectorHolder(KernelNodeInterfaceType.INPUT, this.connectionManager);
            this.innerOutputsHolder.Visible(false);
            this.innerInputsHolder.Visible(false);

            this.isGroupingEnabled = true;
            this.isRootChangeComplete = false;
        }

        public Value($value? : VisualGraphPanelViewerArgs) : VisualGraphPanelViewerArgs {
            return <VisualGraphPanelViewerArgs>super.Value($value);
        }

        public GraphRoot($value? : IKernelNode) : IKernelNode {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(KernelNode) && this.graphRoot !== $value) {
                this.nodeRegistry.ClearState();
                this.graphRoot = $value;
                this.graphRoot.Visible(false);
                this.nodeRegistry.AssignIdentity(new ArrayList(this.graphRoot));
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
            }
            return this.graphRoot;
        }

        public SearchRootNode($rootId? : string) : IKernelNode {
            let rootNode : IKernelNode = this.GraphRoot();
            if (!ObjectValidator.IsEmptyOrNull($rootId)) {
                const getRootNode : any = ($currRoot : IKernelNode) : IKernelNode => {
                    let rootNode : IKernelNode = null;
                    if ($currRoot.Type() === KernelNodeType.VISUAL_GRAPH || $currRoot.Type() === KernelNodeType.VXCONTEXT
                        || $currRoot.Type() === KernelNodeType.VXGRAPH || $currRoot.Type() === KernelNodeType.VXGROUP) {
                        if ($currRoot.UniqueId() === $rootId) {
                            rootNode = $currRoot;
                        } else {
                            $currRoot.ChildNodes().foreach(($node : IKernelNode) : boolean => {
                                const foundRoot : IKernelNode = getRootNode($node);
                                if (!ObjectValidator.IsEmptyOrNull(foundRoot)) {
                                    rootNode = foundRoot;
                                    return false;
                                }
                            });
                        }
                    }
                    return rootNode;
                };
                rootNode = getRootNode(this.GraphRoot());
            } else {
                while (rootNode.ChildNodes().Length() === 1 && rootNode.Type() !== KernelNodeType.VXGRAPH) {
                    rootNode = rootNode.ChildNodes().getFirst();
                }
            }
            return ObjectValidator.IsEmptyOrNull(rootNode) ? this.GraphRoot() : rootNode;
        }

        public RootNode($value? : IKernelNode, $asyncHandler? : () => void) : IKernelNode {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(KernelNode)) {
                if (this.gridManager.Root() === $value) {
                    if (ObjectValidator.IsFunction($asyncHandler)) {
                        $asyncHandler();
                    }
                } else {
                    if (this.isRootChangeComplete) {
                        this.rootQueueHandler = () : void => {
                            // empty handler
                        };
                        this.isRootChangeComplete = false;
                        this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_ROOT_CHANGE_START, false);
                        this.gridManager.Root($value);
                        this.clipboardNodes.Clear();
                        const nodes : ArrayList<IKernelNode> = $value.ChildNodes();
                        $value.ChildNodes(new ArrayList<IKernelNode>());
                        this.KernelNodes(this.nodeUtils.Copy(nodes, false, false), () : void => {
                            this.gridManager.setGridSize(this.gridManager.Root().GridSize());
                            this.gridManager.ResetAreaPosition();
                            ConnectorHolder.Update(true, false);
                            const targetColor : string = this.translateGuiType($value.Type());
                            const guiType : IAreaPanelType = this.GuiType();
                            guiType.type = targetColor;
                            guiType.childTypes.cropBoxType = targetColor;
                            guiType.childTypes.controlPanelType.type = targetColor;
                            this.GuiType(guiType);
                            this.UpdateBreadcrumb();
                            if (ObjectValidator.IsSet($asyncHandler)) {
                                $asyncHandler();
                            }
                            this.isRootChangeComplete = true;
                            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_ROOT_CHANGE_COMPLETE);
                            this.rootQueueHandler();
                        });
                    } else {
                        this.rootQueueHandler = () : void => {
                            this.RootNode($value, $asyncHandler);
                        };
                    }
                }
            }
            return this.gridManager.Root();
        }

        public KernelNodes($value? : ArrayList<IKernelNode>, $asyncHandler? : () => void) : ArrayList<IKernelNode> {
            if (ObjectValidator.IsSet($value)) {
                const prevNodes : ArrayList<IKernelNode> = ArrayList.ToArrayList(this.gridManager.getNodes());
                const prevConnections : ArrayList<IConnection> = ArrayList.ToArrayList(this.connectionManager.getConnections());
                this.RemoveReferences();
                const removePrevNodes : any = () : void => {
                    prevNodes.foreach(($node : IKernelNode) : void => {
                        this.gridManager.RemoveNode($node, false, false);
                    });
                    prevConnections.foreach(($connection : IConnection) : void => {
                        this.connectionManager.RemoveConnection($connection.id, false);
                    });
                };

                if (ObjectValidator.IsEmptyOrNull($value)) {
                    removePrevNodes();
                    if (ObjectValidator.IsSet($asyncHandler)) {
                        $asyncHandler();
                    }
                } else {
                    const createNodes : any = () : void => {
                        this.DragNodes($value, $value.getFirst(), DragNodeType.PASTE, () : void => {
                            this.DeselectAll(false);
                            removePrevNodes();
                            this.innerOutputsHolder.CreateConnections();
                            if (ObjectValidator.IsSet($asyncHandler)) {
                                $asyncHandler();
                            }
                        });
                    };

                    if (this.IsCompleted()) {
                        createNodes();
                    } else {
                        this.getEvents().setOnComplete(() : void => {
                            createNodes();
                        });
                    }
                }
            }
            return this.gridManager.getNodes();
        }

        public DragNodes($nodes : IKernelNode | ArrayList<IKernelNode>, $anchorType : IKernelNode | KernelNodeType, $type? : DragNodeType,
                         $callback? : () => void) : ArrayList<IKernelNode> {
            if ($type !== DragNodeType.PASTE) {
                this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_START, false);
            }
            this.connectionManager.DeselectAll();
            return this.gridManager.DragNodes($nodes, $anchorType, $type, () : void => {
                ArrayList.ToArrayList($nodes).foreach(($node : IKernelNode) : void => {
                    this.connectionManager.CreateNodeConnections($node);
                    this.connectionManager.SelectNodeConnections($node);
                });
                if (ObjectValidator.IsSet($callback)) {
                    $callback();
                }
            });
        }

        public DragConnection($sourceNode : IKernelNode, $index : number, $useInnerInterface? : boolean) : void {
            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_START, false);
            this.connectionManager.DragConnection($sourceNode, $index, $useInnerInterface);
        }

        public ApplicationDirectory($value? : string) : string {
            return this.nodeRegistry.ApplicationDirectory($value);
        }

        public EnableGrouping($value? : boolean) : boolean {
            return this.isGroupingEnabled = ObjectValidator.IsSet($value) ? $value : this.isGroupingEnabled;
        }

        public Validator($value? : IValidator) : IValidator {
            return this.connectionManager.Validator($value);
        }

        public SelectNodes($nodes : ArrayList<IKernelNode>, $status : boolean = true, $fireEvent : boolean = false) : void {
            this.gridManager.SelectNodes($nodes, $status);
            if ($fireEvent) {
                this.onGridSelectChangeHandler();
            }
        }

        public SelectNode($node : IKernelNode, $status : boolean = true, $fireEvent : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($node)) {
                this.gridManager.SelectNode($node, $status);
                if ($fireEvent) {
                    this.onGridSelectChangeHandler();
                }
            }
        }

        public SelectByBounds($value : IBounds) : void {
            this.gridManager.SelectByViewBounds($value);
            this.connectionManager.SelectByViewBounds($value);
        }

        public getSelectedNodes() : ArrayList<IKernelNode> {
            return this.gridManager.getSelectedNodes();
        }

        public getSelectedNode() : IKernelNode {
            return this.gridManager.getSelectedNode();
        }

        public SelectNodeById($id : string) : void {
            this.gridManager.SelectNodesByIds([$id]);
            this.onGridSelectChangeHandler();
        }

        public SelectNodesByIds($ids : string[]) : void {
            if ($ids.length === 1) {
                this.SelectNodeById($ids[0]);
            } else {
                this.gridManager.SelectNodesByIds($ids);
                this.onGridSelectChangeHandler();
            }
        }

        public SelectAll($fireEvent : boolean = true) : void {
            this.gridManager.SelectAll();
            this.connectionManager.SelectAll();

            if ($fireEvent) {
                this.onGridSelectChangeHandler();
            }
        }

        public DeselectAll($fireEvent : boolean = true) : void {
            this.gridManager.DeselectAll();
            this.connectionManager.DeselectAll();

            if ($fireEvent) {
                this.onGridSelectChangeHandler();
            }
        }

        public RemoveSelected() : void {
            if (!this.gridManager.getSelectedNodes().IsEmpty() || !this.connectionManager.getSelectedConnections().IsEmpty()) {
                const args : StateEventArgs = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                this.gridManager.RemoveSelected();
                this.connectionManager.RemoveSelected();

                ConnectorHolder.Update(false, true);
                ToolTip.HideGlobal(true);
                this.onChangeHandler();
                this.persistGraphChange(args);
            }
        }

        public RemoveAll() : void {
            if (!this.gridManager.getNodes().IsEmpty()) {
                this.SelectAll(false);
                const args : StateEventArgs = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                this.gridManager.RemoveAll();

                this.onChangeHandler();
                this.persistGraphChange(args);
            }
        }

        public RemoveReferences() : void {
            if (this.IsCompleted()) {
                this.gridManager.ClearReferences();
                this.connectionManager.getSelectedConnections().Clear();
            }
        }

        public UpdateBreadcrumb() : void {
            (<VisualGraphControlPanel>this.controlPanel).setBreadcrumb(this.RootNode());
        }

        public getEvents() : IVisualGraphPanelEvents {
            return <IVisualGraphPanelEvents>super.getEvents();
        }

        public Copy() : void {
            if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                this.clipboardNodes.Clear();
                this.nodeUtils.Copy(this.gridManager.getSelectedNodes(), true, true, false)
                    .foreach(($node : IKernelNode) : void => {
                        this.clipboardNodes.Add($node);
                    });
            }
        }

        public Cut() : void {
            if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                const args : StateEventArgs = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                this.Copy();
                this.gridManager.RemoveSelected();

                this.onChangeHandler();
                this.persistGraphChange(args);
            }
        }

        public Paste() : void {
            if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                let draggedAnchor : IKernelNode = null;
                const nodes = this.nodeUtils.Copy(this.clipboardNodes, true, false, true);
                nodes.foreach(($node : IKernelNode) : void => {
                    if (ObjectValidator.IsEmptyOrNull(draggedAnchor) ||
                        $node.Column() < draggedAnchor.Column() ||
                        $node.Column() === draggedAnchor.Column() && $node.Row() < draggedAnchor.Row()) {
                        draggedAnchor = $node;
                    }
                });
                this.DragNodes(nodes, draggedAnchor, DragNodeType.RELATIVE);
            }
        }

        public Zoom($value? : number) : number {
            return this.controlPanel.Zoom($value);
        }

        protected innerCode() : IGuiElement {
            KernelNode.RegisterEventsHandler(($node : KernelNode) : void => {
                this.subscribeNodeEvents($node);
            });
            this.getEvents().setBeforeResize(() : void => {
                ConnectorHolder.Update();
            });
            this.joinArea.className = "JoinArea";
            this.dropArea.StyleClassName("DropArea");

            this.gridManager.DragValidator(() : boolean => {
                if (this.gridManager.IsDragActive() && this.gridManager.IsDragLoaded()) {
                    const invalidNodes : ArrayList<IKernelNode>
                        = this.gridManager.getInvalidDraggedNodes();
                    const intersections : ArrayList<INodeIntersection>
                        = this.connectionManager.getNodesIntersections(this.gridManager.getNodes());

                    this.gridManager.getSelectedNodes().foreach(($node : IKernelNode) : void => {
                        if ($node.Error()) {
                            $node.Error(false);
                            $node.Selected(true);
                        }
                    });
                    this.connectionManager
                        .setNodeConnectionStatus(this.gridManager.getSelectedNodes(), ConnectionStatus.ACTIVE, true);

                    if (invalidNodes.IsEmpty() && intersections.IsEmpty()) {
                        this.gridManager.setDragStatus(GeneralCssNames.ACTIVE);
                        return true;
                    } else {
                        this.gridManager.setDragStatus(GeneralCssNames.ERROR);
                        invalidNodes.foreach(($node : IKernelNode) : void => {
                            $node.Error(true);
                        });
                        intersections.foreach(($intersection : INodeIntersection) : void => {
                            if ($intersection.node.Selected()) {
                                $intersection.node.Error(true);
                            }
                            if ($intersection.connection.input.node.Selected() || $intersection.connection.output.node.Selected()) {
                                this.connectionManager.setConnectionStatus($intersection.connection.id, ConnectionStatus.ERROR);
                            }
                        });
                        return false;
                    }
                } else {
                    return false;
                }
            });

            this.connectionManager.DragValidator(() : boolean => {
                let isValid : boolean = true;
                const hoveredNode : IKernelNode = this.gridManager.IsDropPointHovered() ? this.gridManager.getHoveredNode() : null;
                // todo: error returns true -- fix
                if (!ObjectValidator.IsEmptyOrNull(hoveredNode) && !ObjectValidator.IsEmptyOrNull((<any>hoveredNode).errorMessage)) {
                    isValid = false;
                } else {
                    const intersections : ArrayList<INodeIntersection>
                        = this.connectionManager.getDragIntersections(this.gridManager.getNodes(), this.gridManager.getHoveredNode());
                    isValid = intersections.IsEmpty();
                }
                this.connectionManager.setDragStatus(isValid);
                const connectors : ArrayList<KernelNodeConnector>
                    = <any>GuiObjectManager.getInstanceSingleton().getActive(KernelNodeConnector);
                return isValid && connectors.Length() === 1 && connectors.getFirst().GuiType() === KernelNodeInterfaceType.INPUT;
            });

            this.connectionManager.DragSetupHandler(($node : KernelNode, $sourceIndex : number) : void => {
                if (this.IsEventsEnabled() && !this.connectionManager.IsDragActive()) {
                    this.DeselectAll();
                    this.connectionManager.Enabled(false);

                    if ($node !== this.RootNode()) {
                        this.SelectNode($node, true, false);
                        KernelNode.TurnOn($node);
                    } else {
                        this.innerOutputsHolder.UpdateConnectorStatus($sourceIndex, ConnectorHolderStatus.ACTIVE, true);
                    }
                    this.DragConnection($node, $sourceIndex, this.RootNode() === $node);
                    this.gridManager.HighlightNodes(true, $node);

                    const delegatedOutput : Output = KernelNode.getTypedInterface(
                        $node === this.RootNode() ?
                            $node.getInput($sourceIndex).innerOutput :
                            $node.getOutput($sourceIndex));
                    const delegatedInput : Input = KernelNode.getTypedInterface(
                        $node === this.RootNode() ?
                            $node.getInput($sourceIndex).innerOutput.inputs.getFirst() :
                            $node.getOutput($sourceIndex).inputs.getFirst());

                    const dataType : string = delegatedOutput.dataType === InterfaceDataType.DELEGATED &&
                    !ObjectValidator.IsEmptyOrNull(delegatedInput) ?
                        delegatedInput.dataType : delegatedOutput.dataType;

                    const validator : IValidator = this.Validator();

                    this.gridManager.getNodes().foreach(($targetNode : KernelNode) : void => {
                        if ($node !== $targetNode) {
                            $targetNode.Error(false);
                            $targetNode.ShowInputConnectors({
                                dataType,
                                delegatedOutput,
                                scale          : this.gridManager.getScale(),
                                sourceConnector: $sourceIndex,
                                sourceNode     : $node,
                                validator
                            });
                            if ($targetNode.IsConnectorsVisible()) {
                                this.gridManager.HighlightNodes(true, $targetNode);
                            } else {
                                $targetNode.Error(true);
                            }
                        }
                    });

                    if (this.RootNode().Type() === KernelNodeType.VXGROUP) {
                        this.RootNode().getOutputs().foreach(($output : Output) : void => {
                            const targetInput : Input = KernelNode.getTypedInterface($output.innerInput);
                            const isConnected : boolean = !ObjectValidator.IsEmptyOrNull($output.innerInput.output);

                            const status : boolean = !isConnected &&
                                (!ObjectValidator.IsEmptyOrNull(validator) ?
                                    validator(delegatedOutput.node, targetInput.node,
                                        delegatedOutput.position, targetInput.position, true, false).status
                                    : dataType === targetInput.dataType ||
                                    dataType === InterfaceDataType.DELEGATED ||
                                    targetInput.dataType === InterfaceDataType.DELEGATED);

                            this.innerInputsHolder.UpdateConnectorStatus($output.position, status ?
                                ConnectorHolderStatus.ACTIVE : ConnectorHolderStatus.OFF, true);
                        });
                    }
                }
            });

            [this.innerOutputsHolder, this.innerInputsHolder].forEach(($holder : ConnectorHolder) : void => {
                $holder.getEvents().setOnConnectorMouseDown(($eventArgs : KernelNodeEventArgs) : void => {
                    if ($eventArgs.ConnectorType() === KernelNodeInterfaceType.OUTPUT) {
                        this.connectionManager.DragSetupHandler()($eventArgs.Owner(), $eventArgs.Index());
                    }
                });
                $holder.getEvents().setOnConnectorUpdate(($eventArgs : ConnectorHolderEventArgs) : void => {
                    this.persistGraphChange(StateEventArgs.getOnRootInterfaceUpdateArgs(<IIndexChange>{
                        fromIndex    : $eventArgs.FromIndex(),
                        interfaceType: $eventArgs.InterfaceType(),
                        toIndex      : $eventArgs.ToIndex()
                    }));
                });
            });

            this.getEvents().setOnClick(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                if (this.IsEventsEnabled() && this.IsMouseDown() && !this.gridManager.IsDragActive()
                    && !this.connectionManager.IsDragActive() && ObjectValidator.IsEmptyOrNull(this.activeNode)
                    && !this.IsSelectorActive() && !$manager.IsHovered(KernelNode)
                    && !(!ObjectValidator.IsEmptyOrNull(this.connectionManager.getHoveredLine())
                        && $eventArgs.NativeEventArgs().ctrlKey)) {
                    this.DeselectAll();
                }
            });

            (<VisualGraphControlPanel>this.controlPanel).getEvents()
                .setOnRootChangeComplete(($eventArgs : VisualGraphEventArgs) : void => {
                    this.RootNode($eventArgs.ActiveNode(), () : void => {
                        this.onGridSelectChangeHandler();
                    });
                });

            this.getEvents().setOnComplete(() : void => {
                ElementManager.Hide(this.Id() + "_NodeGroupEnvelop");
                this.isRootChangeComplete = true;
                if (!ObjectValidator.IsEmptyOrNull(this.rootQueueHandler)) {
                    this.rootQueueHandler();
                }
            });
            return super.innerCode();
        }

        protected onMouseMoveCompleteHandler($move : IVector) : void {
            if (this.IsCompleted() && this.IsEventsEnabled()) {
                if ($move.x !== 0 || $move.y !== 0) {
                    const hoveredRow : number = this.gridManager.getHoveredRow();
                    const hoveredCol : number = this.gridManager.getHoveredColumn();
                    if (this.gridManager.IsInsideGrid(this.prevHoveredRow, this.prevHoveredCol)) {
                        const prevHoveredNode : IKernelNode = this.gridManager.getNodeAtPosition(this.prevHoveredRow, this.prevHoveredCol);
                        if (!ObjectValidator.IsEmptyOrNull(prevHoveredNode) &&
                            prevHoveredNode !== this.gridManager.getNodeAtPosition(hoveredRow, hoveredCol)) {
                            if (!this.gridManager.getHighlightedNodes().Contains(prevHoveredNode)) {
                                prevHoveredNode.HideConnectors();
                                KernelNode.TurnOff(<KernelNode>prevHoveredNode);
                            }
                        }
                    }
                    this.prevHoveredRow = hoveredRow;
                    this.prevHoveredCol = hoveredCol;
                    if (this.gridManager.IsDragActive()) {
                        const handler : any = () : void => {
                            this.gridManager.PositionDraggedNodes(new ElementOffset($move.y, $move.x));
                            this.gridManager.DragValidator()();
                            this.gridManager.IndicateGridDragResize();
                        };
                        if (this.IsAreaScrollEnabled()) {
                            let positions : IVector[] = [];
                            this.gridManager.getSelectedNodes().foreach(($node : IKernelNode) : void => {
                                positions.push($node.getAbsolutePosition());
                            });
                            if (!this.gridManager.IsPointsBoundableByView(positions)) {
                                positions = [this.gridManager.getMouseAreaPosition()];
                            }
                            const scrollVector : IVector = this.gridManager.getPointsScrollVector(positions, false);
                            this.gridManager.ToggleScroll(() : IVector => {
                                if (!this.gridManager.IsDragSnapped()) {
                                    return scrollVector;
                                } else {
                                    return {x: 0, y: 0};
                                }
                            }, () : void => {
                                handler();
                            }, true);
                        } else {
                            handler();
                        }
                    } else if (this.connectionManager.IsDragActive()) {
                        const handler : any = () : void => {
                            this.connectionManager.PositionDraggedConnection();
                            this.connectionManager.DragValidator()();
                        };
                        if (this.IsAreaScrollEnabled()) {
                            const scrollVector : IVector = this.gridManager.getPointScrollVector();
                            this.gridManager.ToggleScroll(() : IVector => {
                                return scrollVector;
                            }, () : void => {
                                handler();
                            }, true);
                        } else {
                            handler();
                        }
                    } else if (!ObjectValidator.IsEmptyOrNull(this.activeNode)) {
                        if (!this.gridManager.getSelectedNodes().Contains(this.activeNode)) {
                            this.DeselectAll(false);
                        }
                        this.gridManager.SelectNode(this.activeNode);
                        if (this.IsCtrlDown()) {
                            this.DragNodes(this.nodeUtils.Copy(this.gridManager.getSelectedNodes()), this.activeNode,
                                DragNodeType.RELATIVE);
                        } else {
                            this.DragNodes(this.gridManager.getSelectedNodes(), this.activeNode,
                                DragNodeType.RETAIN_ON_CANCEL_RELATIVE);
                        }
                    } else if (ObjectValidator.IsEmptyOrNull(this.connectionManager.getHoveredLine())) {
                        super.onMouseMoveCompleteHandler($move);
                    }
                    const hoveredNode : KernelNode = <KernelNode>this.gridManager.getNodeAtPosition(hoveredRow, hoveredCol);
                    if (this.gridManager.IsViewHovered() && !ObjectValidator.IsEmptyOrNull(hoveredNode)
                        && GuiObjectManager.getInstanceSingleton().IsHovered(hoveredNode)
                        && !this.connectionManager.IsDragActive() && !this.gridManager.IsDragActive()) {
                        hoveredNode.ShowOutputConnectors(<IConnectorVisibilityArgs>{scale: this.gridManager.getScale()});
                        KernelNode.TurnOn(hoveredNode);
                    }
                }
            }
        }

        protected onMouseDownHandler($eventArgs : MouseEventArgs) : void {
            this.gridManager.ToggleScroll();
            super.onMouseDownHandler($eventArgs);
        }

        protected onMouseUpHandler($eventArgs : MouseEventArgs) {
            if (this.IsEventsEnabled()) {
                if (this.gridManager.IsDragActive()) {
                    this.gridManager.IndicateGridDragResize();
                    const args : StateEventArgs = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                    this.gridManager.ApplyDrag(($success : boolean) : void => {
                        if ($success) {
                            this.gridManager.ApplyAreaResize();
                            if (ObjectValidator.IsEmptyOrNull(args.getPositionChange())
                                || args.getPositionChange().offsetTop !== 0 || args.getPositionChange().offsetLeft !== 0) {
                                this.onGridSelectChangeHandler();
                                this.onChangeHandler();
                                this.persistGraphChange(args);
                            }
                        } else {
                            this.gridManager.CancelDrag();
                            this.gridManager.CancelResize();
                        }
                        this.connectionManager.ValidateOutputs(this.gridManager.getNodes());
                        this.gridManager.ToggleScroll();
                        this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_COMPLETE);
                    });
                } else if (this.connectionManager.IsDragActive()) {
                    const dragSourceNode : KernelNode = <KernelNode>this.connectionManager.getDragSourceOutput().node;
                    const defaultDragInput : Input = this.connectionManager.getDefaultDragInput();
                    const currentDragInput : Input = this.connectionManager.getDragTargetInput();
                    this.onConnectionDragComplete(dragSourceNode);

                    if (this.connectionManager.IsDragValid() && (ObjectValidator.IsEmptyOrNull(defaultDragInput) ||
                        defaultDragInput.node !== currentDragInput.node || defaultDragInput.position !== currentDragInput.position)) {
                        const args : StateEventArgs = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                        this.connectionManager.ApplyDrag();
                        this.persistGraphChange(args);
                    } else {
                        this.connectionManager.CancelDrag();
                    }
                    ConnectorHolder.Update();
                    this.gridManager.ToggleScroll();
                    this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_COMPLETE);
                } else {
                    super.onMouseUpHandler($eventArgs);
                }
                this.gridManager.GridVisible(false);
                this.connectionManager.Enabled(true);
                this.activeNode = null;
            }
        }

        protected onKeyDownHandler($eventArgs : KeyEventArgs) : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (this.IsEventsEnabled() && !manager.IsActive(FormInput) && !manager.IsActive(TextField)) {
                switch ($eventArgs.getKeyCode()) {
                case 65: // a
                    if ($eventArgs.NativeEventArgs().ctrlKey && !manager.IsActive(TextField)) {
                        $eventArgs.PreventDefault();
                    }
                    break;
                case 71: // g
                    if (this.isGroupingEnabled) {
                        if ($eventArgs.NativeEventArgs().ctrlKey) {
                            $eventArgs.PreventDefault();
                        }
                    }
                    break;
                default:
                    super.onKeyDownHandler($eventArgs);
                }
            }
        }

        protected onKeyUpHandler($eventArgs : KeyEventArgs) : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (this.IsEventsEnabled() && !manager.IsActive(FormInput) && !manager.IsActive(TextField)) {
                const completeRemove : any = ($isActionPending : boolean) : void => {
                    if ($isActionPending) {
                        this.gridManager.GridVisible(false);
                        this.gridManager.ToggleScroll();
                        this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_COMPLETE, false);
                    }
                    this.activeNode = null;
                };

                switch ($eventArgs.getKeyCode()) {
                case 65: // a
                    let step : number = 6;
                    const handler : any = () : void => {
                        this.Parent().Width(step * 50);
                        if (step < 30) {
                            step++;
                            this.getEvents().FireAsynchronousMethod(() : void => {
                                handler();
                            });
                        }
                    };
                    handler();
                    if ($eventArgs.NativeEventArgs().ctrlKey && !manager.IsActive(TextField)) {
                        this.SelectAll();
                        this.onGridSelectChangeHandler();
                    }
                    break;
                case 67: // c
                    if ($eventArgs.NativeEventArgs().ctrlKey) {
                        this.Copy();
                    }
                    break;
                case 71: // g
                    if (this.isGroupingEnabled) {
                        if ($eventArgs.NativeEventArgs().ctrlKey) {
                            const selectedNode : IKernelNode = this.gridManager.getSelectedNode();
                            if (!ObjectValidator.IsEmptyOrNull(selectedNode) && selectedNode.Type() === KernelNodeType.VXGROUP) {
                                this.nodeUtils.UnGroupNodes();
                            } else {
                                this.nodeUtils.GroupNodes();
                            }
                        }
                    }
                    break;
                case 88: // x
                    if ($eventArgs.NativeEventArgs().ctrlKey) {
                        this.Cut();
                    }
                    break;
                case 86: // v
                    if ($eventArgs.NativeEventArgs().ctrlKey) {
                        this.Paste();
                    }
                    break;
                case KeyMap.ESC:
                    let isCancellable : boolean = true;
                    if (this.gridManager.IsDragActive()) {
                        this.gridManager.CancelDrag();
                        this.gridManager.CancelResize();
                    } else if (this.connectionManager.IsDragActive()) {
                        const dragSourceNode : KernelNode = <KernelNode>this.connectionManager.getDragSourceOutput().node;
                        this.connectionManager.CancelDrag();
                        this.onConnectionDragComplete(dragSourceNode);
                    } else if (this.gridManager.IsResizeActive()) {
                        this.gridManager.CancelResize();
                    } else {
                        this.DeselectAll();
                        this.onGridSelectChangeHandler();
                        isCancellable = false;
                    }
                    completeRemove(isCancellable);
                    break;
                case KeyMap.DELETE:
                    let isDragPending : boolean = true;
                    if (this.gridManager.IsDragActive()) {
                        this.gridManager.CancelDrag();
                        this.gridManager.CancelResize();
                        if (!ObjectValidator.IsEmptyOrNull(this.gridManager.getSelectedNodes())) {
                            const args : StateEventArgs
                                = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                            this.gridManager.RemoveSelected();
                            this.persistGraphChange(args);
                        }
                    } else if (this.connectionManager.IsDragActive()) {
                        const dragSourceNode : KernelNode = <KernelNode>this.connectionManager.getDragSourceOutput().node;
                        const pushState : boolean = !ObjectValidator.IsEmptyOrNull(this.connectionManager.getDefaultDragInput());
                        this.connectionManager.CancelDrag();
                        this.onConnectionDragComplete(dragSourceNode);
                        if (pushState) {
                            this.gridManager.DeselectAll();
                        }
                        const args : StateEventArgs
                            = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
                        this.connectionManager.RemoveSelected();
                        if (pushState) {
                            this.SelectNode(dragSourceNode);
                            this.persistGraphChange(args);
                        }
                    } else {
                        isDragPending = false;
                        this.RemoveSelected();
                    }
                    completeRemove(isDragPending);
                    break;
                default:
                    super.onKeyUpHandler($eventArgs);
                }
            }
        }

        protected onSelectChangeHandler($bounds : IBounds) : void {
            this.SelectByBounds($bounds);
            super.onSelectChangeHandler($bounds);
        }

        protected onZoomChangeHandler($zoom : number) : void {
            ToolTip.HideGlobal(true);
            ConnectorHolder.Update(false, false);
            if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                this.gridManager.ToggleScroll();
            }
            this.gridManager.getNodes().foreach(($node : IKernelNode) : void => {
                $node.ScaleVisibleConnectors(this.gridManager.getScale());
            });
            if (!ObjectValidator.IsEmptyOrNull(this.getSelectionBounds())) {
                this.SelectByBounds(this.getSelectionBounds());
            }
            super.onZoomChangeHandler($zoom);
        }

        protected onResizeStartHandler() : void {
            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_START, false);
            super.onResizeStartHandler();
        }

        protected onResizeCompleteHandler() : void {
            const resizeArgs : StateEventArgs
                = StateEventArgs.getBeforeNodeUpdateArgs(this.gridManager, this.connectionManager);
            if (!ObjectValidator.IsEmptyOrNull(resizeArgs) && !ObjectValidator.IsEmptyOrNull(resizeArgs.getSizeChange()) &&
                (resizeArgs.getSizeChange().directionVertical !== 0 || resizeArgs.getSizeChange().directionHorizontal !== 0)) {
                this.persistGraphChange(resizeArgs);
                super.onResizeCompleteHandler();
            }
            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_DRAG_ACTION_COMPLETE, false);
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected getAreaElement() : IGuiElement {
            return this.addElement()
                .Add(this.joinArea)
                .Add(this.dropArea)
                .Add(this.innerOutputsHolder)
                .Add(this.innerInputsHolder)
                .Add(this.addElement(this.Id() + "_NodeGroupEnvelop").StyleClassName(GeneralCssNames.ACTIVE)
                    .Add(this.addElement(this.Id() + "_NodeGroup").StyleClassName("NodeGroup")));
        }

        protected getAreaStrategy() : IAreaStrategy {
            return this.gridManager;
        }

        protected getControlPanelViewerClass() : any {
            return VisualGraphControlPanelViewer;
        }

        protected IsHovered() : boolean {
            const guiManager : GuiObjectManager = this.getGuiManager();
            return guiManager.IsHovered(VisualGraphPanel) || guiManager.IsHovered(KernelNode);
        }

        protected cssContainerName() : string {
            return VisualGraphPanel.ClassNameWithoutNamespace() + " " + AreaPanel.ClassNameWithoutNamespace();
        }

        private onConnectionDragComplete($dragSourceNode : KernelNode) : void {
            this.SelectNode($dragSourceNode);
            this.gridManager.getNodes().foreach(($node : KernelNode) : void => {
                if ($node !== $dragSourceNode) {
                    $node.Error(false);
                    $node.Selected(false);
                }
            });
            const hoveredNode : KernelNode = <KernelNode>this.gridManager.getHoveredNode();
            this.gridManager.getHighlightedNodes().foreach(($node : KernelNode) : void => {
                if ($node === hoveredNode) {
                    $node.HideConnectors();
                    $node.ShowOutputConnectors(<IConnectorVisibilityArgs>{scale: this.gridManager.getScale()});
                } else {
                    $node.HideConnectors();
                    KernelNode.TurnOff($node);
                }
            });
            this.gridManager.HighlightNodes(false);
        }

        private subscribeNodeEvents($node : KernelNode) : void {
            $node.getEvents().setBeforeRemove(($eventArgs : KernelNodeEventArgs) : void => {
                this.connectionManager.RemoveNodeConnections($node, $eventArgs.Status());
                if ($eventArgs.Status()) {
                    this.nodeRegistry.UnAssignIdentity($node);
                }
            });

            $node.getEvents().setOnMove(() : void => {
                this.connectionManager.MoveNodeConnections($node, $node.getAbsolutePosition());
                this.connectionManager.StackConnections();
            });

            $node.getEvents().setOnConnectorMouseDown(($eventArgs : KernelNodeEventArgs) : void => {
                DropDownList.Blur();
                if ($eventArgs.ConnectorType() === KernelNodeInterfaceType.OUTPUT) {
                    this.connectionManager.DragSetupHandler()($eventArgs.Owner(), $eventArgs.Index());
                }
            });

            $node.getEvents().setOnMouseEnter(() : void => {
                if (this.IsEventsEnabled()) {
                    if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                        this.connectionManager.setNodeConnectionStatus($node, ConnectionStatus.ACTIVE);
                    }
                }
            });

            $node.getEvents().setOnMouseLeave(() : void => {
                if (this.IsEventsEnabled()) {
                    if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                        this.connectionManager.setNodeConnectionStatus($node, ConnectionStatus.INACTIVE);
                    }
                }
            });

            $node.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                if (this.IsEventsEnabled() && !this.gridManager.IsDragActive()) {
                    if ($eventArgs.NativeEventArgs().which === 1) {
                        $eventArgs.StopAllPropagation();
                        DropDownList.Blur();
                        ToolTip.HideGlobal(true);
                        this.connectionManager.StackConnections();
                        this.connectionManager.setNodeConnectionStatus($node, ConnectionStatus.INACTIVE);
                        this.connectionManager.Enabled(false);
                        this.getEvents().FireAsynchronousMethod(() : void => {
                            if (!this.gridManager.IsDragActive() && !this.connectionManager.IsDragActive()) {
                                this.activeNode = $node;
                            }
                        });
                    }
                }
            });

            $node.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs) : void => {
                if (this.IsEventsEnabled() && !this.gridManager.IsDragActive()) {
                    if ($eventArgs.NativeEventArgs().which === 1) {
                        if ($node === this.activeNode) {
                            if ($eventArgs.NativeEventArgs().ctrlKey) {
                                this.SelectNode($node, !$node.Selected());
                            } else {
                                this.DeselectAll(false);
                                this.SelectNode($node);
                            }
                        }
                    }
                }
            });

            $node.getEvents().setOnTooltipChange(($eventArgs : KernelNodeEventArgs, $manager : GuiObjectManager) : void => {
                if (this.IsEventsEnabled() && !this.gridManager.IsDragActive()) {
                    ToolTip.ShowGlobal($eventArgs.TooltipText(), ObjectValidator.IsEmptyOrNull($eventArgs.Index()) ? 500 : 200,
                        () : boolean => {
                            return ObjectValidator.IsEmptyOrNull($eventArgs.Index()) ? $manager.IsHovered($node)
                                : $manager.IsActive(KernelNodeConnector);
                        });
                }
            });

            $node.getEvents().setOnDoubleClick(() : void => {
                if (this.IsEventsEnabled() && !this.gridManager.IsDragActive() &&
                    ($node.Type() === KernelNodeType.VXCONTEXT || $node.Type() === KernelNodeType.VXGRAPH ||
                        $node.Type() === KernelNodeType.VXGROUP)) {
                    this.RootNode($node, () : void => {
                        this.onGridSelectChangeHandler();
                    });
                }
            });

            $node.getEvents().Subscribe(true);
        }

        private translateGuiType($value : string) : string {
            switch ($value) {
            case KernelNodeType.VISUAL_GRAPH:
                return AreaPanelType.ORANGE;
            case KernelNodeType.VXCONTEXT:
                return AreaPanelType.SKY_BLUE;
            case KernelNodeType.VXGRAPH:
                return AreaPanelType.GREEN;
            case KernelNodeType.VXGROUP:
                return AreaPanelType.GREEN_DARK;
            default :
                return AreaPanelType.ORANGE;
            }
        }

        private onGridSelectChangeHandler() : void {
            const eventArgs : VisualGraphEventArgs = new VisualGraphEventArgs();
            eventArgs.Owner(this);
            eventArgs.SelectedNodes(this.gridManager.getSelectedNodes());
            eventArgs.SelectedConnections(this.connectionManager.getSelectedConnections());
            this.getEventsManager().FireEvent(this, EventType.ON_SELECT, eventArgs);
        }

        private onChangeHandler() : void {
            this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
        }

        private persistGraphChange($args : StateEventArgs) : void {
            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_STATE_CHANGE, $args, false);
        }
    }
}
