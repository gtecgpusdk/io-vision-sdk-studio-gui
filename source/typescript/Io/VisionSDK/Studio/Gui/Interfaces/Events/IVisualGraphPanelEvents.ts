/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";

    export interface IVisualGraphPanelEvents extends IControlPanelEvents {
        setOnNodeSelect($handler : IVisualGraphEventsHandler) : void;

        setOnNodeDrag($handler : IVisualGraphEventsHandler) : void;

        setOnRootChangeStart($handler : IVisualGraphEventsHandler) : void;

        setOnRootChangeComplete($handler : IVisualGraphEventsHandler) : void;

        setOnZoomChange($handler : IVisualGraphEventsHandler) : void;

        setOnStateChange($handler : IStateEventsHandler) : void;

        setOnDragActionStart($handler : IStateEventsHandler) : void;

        setOnDragActionComplete($handler : IStateEventsHandler) : void;
    }
}
