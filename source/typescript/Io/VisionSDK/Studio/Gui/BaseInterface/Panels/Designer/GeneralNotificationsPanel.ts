/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import NotificationMessage = Io.VisionSDK.UserControls.BaseInterface.UserControls.NotificationMessage;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;

    /**
     * GeneralNotificationsPanel class is providing holder for ability to display general notifications.
     */
    export class GeneralNotificationsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        private static defaultHeight : number = 35;
        private static notificationIndex : number = 1;
        private static maxNotificationCount : number = 6;
        private notificationsRegistration : ArrayList<NotificationMessage>;
        private notifications : ArrayList<NotificationMessage>;

        private static resizeContent($eventArgs : EventArgs) : void {
            let element : GeneralNotificationsPanel = <GeneralNotificationsPanel>$eventArgs.Owner();
            if (!element.IsTypeOf(GeneralNotificationsPanel)) {
                element = <GeneralNotificationsPanel>element.Parent();
            }
            const width : number = element.Width()
                - ElementManager.getElement(element.Id() + "_MiddleLeft").offsetWidth
                - ElementManager.getElement(element.Id() + "_MiddleRight").offsetWidth;

            ElementManager.setWidth(element.Id() + "_TopCenter", width);
            ElementManager.setWidth(element.Id() + "_MiddleCenter", width);
            ElementManager.setWidth(element.Id() + "_BottomCenter", width);

            let height : number = 0;
            if (!element.notifications.IsEmpty()) {
                const lastMessage : NotificationMessage = element.notifications.getLast();
                ElementManager.Hide(lastMessage.Id() + "_Splitter");
                element.notifications.foreach(($message : NotificationMessage) : void => {
                    if (ElementManager.Exists($message.Id())) {
                        height += ElementManager.getElement($message.Id()).offsetHeight
                            + ElementManager.getCssIntegerValue($message.Id(), "margin-top")
                            + ElementManager.getCssIntegerValue($message.Id(), "margin-bottom");
                        if ($message !== lastMessage) {
                            ElementManager.Show($message.Id() + "_Splitter");
                            height += ElementManager.getElement($message.Id() + "_Splitter").offsetHeight;
                        }
                    }
                });
            }

            if (height === 0) {
                height = element.Height();
            }
            height -=
                ElementManager.getCssIntegerValue(element.Id() + "_Content", "margin-top")
                - ElementManager.getCssIntegerValue(element.Id() + "_Content", "margin-bottom");

            ElementManager.setSize(element.Id() + "_Content",
                element.Width()
                - ElementManager.getCssIntegerValue(element.Id() + "_Content", "margin-left")
                - ElementManager.getCssIntegerValue(element.Id() + "_Content", "margin-right"),
                height);
            element.Height(height);

            height -= (
                ElementManager.getElement(element.Id() + "_TopCenter").offsetHeight
                + ElementManager.getElement(element.Id() + "_BottomCenter").offsetHeight
            );

            ElementManager.setHeight(element.Id() + "_MiddleLeft", height);
            ElementManager.setHeight(element.Id() + "_MiddleCenter", height);
            ElementManager.setHeight(element.Id() + "_MiddleRight", height);

            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(element.Id());
            if (!element.notifications.IsEmpty()) {
                manipulatorArgs.Owner(element);
                manipulatorArgs.RangeStart(ElementManager.getElement(element.Id()).offsetHeight);
                manipulatorArgs.RangeEnd(element.Height());
                manipulatorArgs.CurrentValue(manipulatorArgs.RangeStart());
                manipulatorArgs.ProgressType(ProgressType.LINEAR);
                manipulatorArgs.Step(3);
                manipulatorArgs.DirectionType(DirectionType.UP);
                manipulatorArgs.ChangeEventType(element.Id() + "_Show_" + EventType.ON_CHANGE);
                manipulatorArgs.CompleteEventType(element.Id() + "_Show_" + EventType.ON_COMPLETE);

                element.getEventsManager().setEvent(element, element.Id() + "_Show_" + EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const element : GeneralNotificationsPanel = <GeneralNotificationsPanel>$eventArgs.Owner();
                        const newHeight : number = $eventArgs.CurrentValue();
                        ElementManager.setCssProperty(element.Id() + "_Background", "top",
                            (-1) * (element.Height() - newHeight));
                        ElementManager.setCssProperty(element.Id() + "_Content", "top", 0);

                        ElementManager.setHeight(element.Id() + "_PanelContent", newHeight);
                        ElementManager.setHeight(element.Id() + "_PanelContentEnvelop", newHeight);
                        ElementManager.setHeight(element.Id(), newHeight);
                    });
                ValueProgressManager.Execute(manipulatorArgs);
            } else {
                element.Height(0);
                ElementManager.setHeight(element.Id() + "_PanelContent", 0);
                ElementManager.setHeight(element.Id() + "_PanelContentEnvelop", 0);
                ElementManager.setHeight(element.Id(), 0);
                ValueProgressManager.Remove(manipulatorArgs.OwnerId());
                element.Visible(false);
            }
        }

        /**
         * @param {string} [$id] Force to set element's id, instead of generated one.
         */
        constructor($id? : string) {
            super($id);

            this.notificationsRegistration = new ArrayList<NotificationMessage>();
            this.notifications = new ArrayList<NotificationMessage>();

            this.Width(450);
            this.Height(GeneralNotificationsPanel.defaultHeight);
        }

        /**
         * @param {string} $text Specify text value, which should be displayed as notification content with INFO look and feel.
         * @return {void}
         */
        public AddInfo($text : string) : void {
            this.AddNotification($text, NotificationMessageType.GENERAL_INFO);
        }

        /**
         * @param {string} $text Specify text value, which should be displayed as notification content with WARNING look and feel.
         * @param {string} [$prefix] Set text value, which should be displayed as message prefix.
         * @return {void}
         */
        public AddWarning($text : string, $prefix? : string) : void {
            this.AddNotification($text, NotificationMessageType.GENERAL_WARNING, $prefix);
        }

        /**
         * @param {string} $text Specify text value, which should be displayed as notification content with SUCCESS look and feel.
         * @return {void}
         */
        public AddSuccess($text : string) : void {
            this.AddNotification($text, NotificationMessageType.GENERAL_SUCCESS);
        }

        /**
         * @param {string} $text Specify text value, which should be displayed as notification content with ERROR look and feel.
         * @param {string} [$prefix] Set text value, which should be displayed as message prefix.
         * @return {void}
         */
        public AddError($text : string, $prefix? : string) : void {
            this.AddNotification($text, NotificationMessageType.GENERAL_ERROR, $prefix);
        }

        /**
         * @param {string} $text Specify text value, which should be displayed as notification content.
         * @param {NotificationMessageType} $type Specify notification type.
         * @param {string} [$prefix] Set text value, which should be displayed as message prefix.
         * @return {void}
         */
        public AddNotification($text : string, $type : NotificationMessageType, $prefix? : string) : void {
            let message : NotificationMessage;
            const target : HTMLElement = ElementManager.getElement(this.Id() + "_Content");
            if (this.IsCompleted() && ElementManager.Exists(target)) {
                message = new NotificationMessage($text, $type,
                    this.Id() + "_Message" + GeneralNotificationsPanel.notificationIndex);
                GeneralNotificationsPanel.notificationIndex++;

                message.Parent(this);
                message.InstanceOwner(this.InstanceOwner());
                message.Width(this.Width());
                if (!ObjectValidator.IsEmptyOrNull($prefix)) {
                    message.Prefix($prefix);
                }

                const handleNotificationRemoval : any = ($eventArgs : EventArgs) : void => {
                    const element : NotificationMessage = <NotificationMessage>$eventArgs.Owner();
                    const parent : GeneralNotificationsPanel = <GeneralNotificationsPanel>element.Parent();
                    if (!ObjectValidator.IsEmptyOrNull(parent)) {
                        parent.removeNotificationContent(element.Id());
                        parent.notifications.RemoveAt(parent.notifications.IndexOf(element));

                        $eventArgs.Owner(parent);
                        GeneralNotificationsPanel.resizeContent($eventArgs);
                    }
                };

                message.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                    let isOverflow : boolean = false;
                    while (this.notifications.Length() > GeneralNotificationsPanel.maxNotificationCount) {
                        this.removeNotificationContent(this.notifications.getFirst().Id());
                        this.notifications.RemoveAt(this.notifications.IndexOf(this.notifications.getFirst()));
                        isOverflow = true;
                    }
                    if (!isOverflow) {
                        GeneralNotificationsPanel.resizeContent($eventArgs);
                    }
                });
                message.getEvents().setOnHide(handleNotificationRemoval);
                message.getEvents().setOnDoubleClick(handleNotificationRemoval);

                this.notifications.Add(message, message.Id());
                if (!ObjectValidator.IsSet(this.outputEndOfLine)) {
                    this.outputEndOfLine = StringUtils.NewLine(false);
                }
                const EOL : string = this.outputEndOfLine;
                const guiElement : IGuiElement = this.addElement(message.Id() + "_HtmlAppender")
                    .Add(this.addElement(message.Id() + "_Splitter").StyleClassName("Splitter"))
                    .Add(message);
                const element : HTMLElement = guiElement.ToDOMElement(EOL);
                target.appendChild(element);
                message.Visible(true);

                let timeout : number = 0;
                switch ($type) {
                case NotificationMessageType.GENERAL_INFO:
                    timeout = 5000;
                    break;
                case NotificationMessageType.GENERAL_WARNING:
                    timeout = 5000;
                    break;
                case NotificationMessageType.GENERAL_SUCCESS:
                    timeout = 5000;
                    break;
                case NotificationMessageType.GENERAL_ERROR:
                    timeout = 10000;
                    break;
                default :
                    timeout = 0;
                    break;
                }
                if (timeout > 0) {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        if (this.notifications.IndexOf(message) !== -1) {
                            this.removeNotificationContent(message.Id());
                            this.notifications.RemoveAt(this.notifications.IndexOf(message));
                            const eventArgs : EventArgs = new EventArgs();
                            eventArgs.Owner(this);
                            GeneralNotificationsPanel.resizeContent(eventArgs);
                        }
                    }, false, timeout);
                }
            } else {
                message = new NotificationMessage($text, $type);
                if (ObjectValidator.IsSet($prefix)) {
                    message.Prefix($prefix);
                }
                this.notificationsRegistration.Add(message);
            }
            this.Visible(true);
        }

        /**
         * Close all currently displayed notification.
         * @return {void}
         */
        public CloseAll() : void {
            if (!this.notifications.IsEmpty()) {
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.Id());
                manipulatorArgs.Owner(this);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.RangeEnd(this.Height());
                manipulatorArgs.CurrentValue(this.Height());
                manipulatorArgs.ProgressType(ProgressType.FAST_END);
                manipulatorArgs.Step(6);
                manipulatorArgs.DirectionType(DirectionType.DOWN);
                manipulatorArgs.ChangeEventType(this.Id() + "_Hide_" + EventType.ON_CHANGE);
                manipulatorArgs.CompleteEventType(this.Id() + "_Hide_" + EventType.ON_COMPLETE);

                this.getEventsManager().setEvent(this, this.Id() + "_Hide_" + EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const element : GeneralNotificationsPanel = <GeneralNotificationsPanel>$eventArgs.Owner();
                        const newHeight : number = $eventArgs.CurrentValue();
                        ElementManager.setHeight(element.Id() + "_PanelContent", newHeight);
                        ElementManager.setHeight(element.Id() + "_PanelContentEnvelop", newHeight);
                        ElementManager.setHeight(element.Id(), newHeight);
                    });

                this.getEventsManager().setEvent(this, this.Id() + "_Hide_" + EventType.ON_COMPLETE,
                    ($eventArgs : EventArgs) : void => {
                        const element : GeneralNotificationsPanel = <GeneralNotificationsPanel>$eventArgs.Owner();
                        element.notifications.foreach(($message : NotificationMessage) : void => {
                            this.removeNotificationContent($message.Id());
                        });
                        element.notifications.Clear();
                        ElementManager.setHeight(element.Id() + "_PanelContent", 0);
                        ElementManager.setHeight(element.Id() + "_PanelContentEnvelop", 0);
                        ElementManager.setHeight(element.Id(), 0);
                        this.Height(0);
                        this.Visible(false);
                    });
                ValueProgressManager.Execute(manipulatorArgs);
            } else {
                ElementManager.setHeight(this.Id() + "_PanelContent", 0);
                ElementManager.setHeight(this.Id() + "_PanelContentEnvelop", 0);
                ElementManager.setHeight(this.Id(), 0);
                this.Height(0);
                this.Visible(false);
            }
        }

        protected innerCode() : IGuiElement {
            this.Height(0);

            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                const element : GeneralNotificationsPanel = <GeneralNotificationsPanel>$eventArgs.Owner();
                if (!element.notificationsRegistration.IsEmpty()) {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        element.notificationsRegistration.foreach(($message : NotificationMessage) : void => {
                            element.AddNotification($message.Text(), $message.GuiType(), $message.Prefix());
                        });
                        element.notificationsRegistration.Clear();
                    });
                }
                element.getEvents().Subscribe(element.Id() + "_Content");
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Background").StyleClassName(GeneralCssNames.BACKGROUND).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_Top")
                    .StyleClassName(GeneralCssNames.TOP)
                    .Add(this.addElement(this.Id() + "_TopLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_TopCenter").StyleClassName(GeneralCssNames.CENTER))
                    .Add(this.addElement(this.Id() + "_TopRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_Middle")
                    .StyleClassName(GeneralCssNames.MIDDLE)
                    .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_MiddleCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Add(this.addElement(this.Id() + "_Content").StyleClassName("Content"))
                    )
                    .Add(this.addElement(this.Id() + "_MiddleRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_Bottom")
                    .StyleClassName(GeneralCssNames.BOTTOM)
                    .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_BottomCenter").StyleClassName(GeneralCssNames.CENTER))
                    .Add(this.addElement(this.Id() + "_BottomRight").StyleClassName(GeneralCssNames.RIGHT))
                );
        }

        protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : any) => void) : void {
            this.notifications.foreach(($message : NotificationMessage) : void => {
                this.removeNotificationContent($message.Id());
            });
            this.notifications.Clear();
            ElementManager.setHeight(this.Id() + "_PanelContent", 0);
            ElementManager.setHeight(this.Id() + "_PanelContentEnvelop", 0);
            ElementManager.setHeight(this.Id(), 0);
            this.Height(0);
            this.Visible(false);
        }

        private removeNotificationContent($id : string) : void {
            if (this.notifications.KeyExists($id)) {
                const message : NotificationMessage = this.notifications.getItem($id);
                GuiObjectManager.getInstanceSingleton().Clear(message);
                this.getEventsManager().Clear(message.Id());
                const htmlElement : Node = ElementManager.getElement(message.Id() + "_HtmlAppender");
                if (!ObjectValidator.IsEmptyOrNull(htmlElement)) {
                    htmlElement.parentNode.removeChild(htmlElement);
                }
            }
        }
    }
}
