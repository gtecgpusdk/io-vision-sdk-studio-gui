/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import PropertyEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.PropertyEventArgs;
    import PropertiesPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.PropertiesPanelEventType;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;

    export class PropertiesPanelTest extends ViewerTestRunner<PropertiesPanel> {

        public testPropertiesClick() : void {
            const panel : PropertiesPanel = this.getInstance();
            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            vxGraph.Name("TestGraph");
            panel.ShowProperties(vxGraph);
            this.assertEquals(vxGraph.Name(), panel.nameForm.Value());
        }

        public testPropertiesChangeParameter() : void {
            const panel : PropertiesPanel = this.getInstance();
            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            const vxGraph1 : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 6, 6);
            vxGraph.Name("TestGraph");
            panel.ShowProperties(vxGraph);
            this.assertEquals(vxGraph.Name(), panel.nameForm.Value());
            vxGraph1.Name("Test");
            panel.ShowProperties(vxGraph1);
            this.assertEquals(vxGraph1.Name(), panel.nameForm.Value());
        }

        public testPropertiesChangeEvent() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const panel : PropertiesPanel = this.getInstance();
                const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
                const vxGraph1 : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 6, 6);
                vxGraph.Name("TestGraph");
                panel.ShowProperties(vxGraph);
                this.assertEquals(vxGraph.Name(), panel.nameForm.Value());

                panel.getEvents().setOnPropertyChange(($eventArgs : PropertyEventArgs) => {
                    panel.getEvents().FireAsynchronousMethod(() : void => {
                        vxGraph.Name("ChangeName");
                        this.assertEquals(vxGraph.Name(), "ChangeName");
                        $done();
                    }, 400);
                });
                this.emulateEvent(panel, PropertiesPanelEventType.ON_PROPERTY_CHANGE);
            };
        }

        public testPropertiesSelectEvent() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const panel : PropertiesPanel = this.getInstance();
                const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
                vxGraph.Name("TestGraph");
                const vxGraph1 : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 6, 6);
                vxGraph1.Name("Test");
                panel.ShowProperties(vxGraph);

                panel.getEvents().setOnPropertySelect(($eventArgs : PropertyEventArgs) => {
                    panel.getEvents().FireAsynchronousMethod(() : void => {
                        panel.ShowProperties(vxGraph1);
                        this.assertEquals(panel.getCurrentNode(), vxGraph1);
                        this.assertEquals(vxGraph1.Name(), "Test");
                        $done();
                    }, 400);
                });
                this.emulateEvent(panel, PropertiesPanelEventType.ON_PROPERTY_SELECT);
            };
        }

        protected before() : string {
            const instance : PropertiesPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.Width(400);
            instance.Height(790);

            // $instance.getEvents().setOnPropertyChange(($eventArgs : PropertyEventArgs) : void => {
            //     Echo.Printf("Changed {0} {1}", $eventArgs.Name(), $eventArgs.Value());
            // });
            // const node : KernelNode = new KernelNode(KernelNodeType.ACCUMULATE_SQUARED, 0, 0);
            // this.addTestButton("Show node", () : void => {
            //     node.setAttribute({
            //         format: {
            //             decimalPlaces: 4,
            //             max          : 100,
            //             min          : 0,
            //             step         : 0.5
            //         },
            //         name  : "test",
            //         text  : "Test",
            //         type  : "Float",
            //         value : 0.5354
            //     });
            //     node.setAttribute({
            //         items: [
            //             "VX_DF_IMAGE_U8",
            //             "VX_DF_IMAGE_S16"
            //         ],
            //         name : "inputType",
            //         text : "Input type",
            //         type : "Enum",
            //         value: "VX_DF_IMAGE_U8"
            //     });
            //     node.setAttribute({
            //         items: [
            //             "VX_DF_IMAGE_U8",
            //             "VX_DF_IMAGE_S16"
            //         ],
            //         name : "outputType",
            //         text : "Output type",
            //         type : "Enum",
            //         value: "VX_DF_IMAGE_U8"
            //     });
            //     $instance.ShowProperties(node);
            // });
            //
            // this.addTestButton("Update dropdown", () : void => {
            //     node.setAttribute({
            //         items: [
            //             "VX_DF_IMAGE_U8",
            //             "VX_DF_IMAGE_S16",
            //             "VX_DF_IMAGE_RGB"
            //         ],
            //         name : "outputType",
            //         text : "Output type",
            //         type : "Enum",
            //         value: "VX_DF_IMAGE_U8"
            //     });
            //     $instance.ShowProperties(node);
            // });
            //
            // let curWidth : number = 300;
            // const minWidth : number = 300;
            // const step : number = 100;
            // this.addTestButton("Resize+", () : void => {
            //     curWidth += step;
            //     $instance.Width(Math.max(minWidth, curWidth));
            // });
            //
            // this.addTestButton("Resize-", () : void => {
            //     curWidth -= step;
            //     $instance.Width(Math.max(minWidth, curWidth));
            // });

            return "<style>.TestCss .PropertiesPanel {border: 1px solid #E7E7E8;}</style>";
        }
    }
}
/* dev:end */
