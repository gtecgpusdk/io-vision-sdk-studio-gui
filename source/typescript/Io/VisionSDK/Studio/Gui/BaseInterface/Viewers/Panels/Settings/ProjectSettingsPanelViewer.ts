/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings {
    "use strict";
    import ProjectSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ProjectSettingsPanelViewerArgs;
    import ProjectSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ProjectSettingsPanel;

    export class ProjectSettingsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectSettingsPanelViewerArgs {
            const args : ProjectSettingsPanelViewerArgs = new ProjectSettingsPanelViewerArgs();
            args.HeaderText("Project settings");
            args.DescriptionText("Description");
            args.NameText("Project name");
            args.PathText("Project path");
            args.VxVersionText("OpenVX version");
            args.AddVxVersion("1.1.0");
            args.AddVxVersion("1.0.1");
            args.PanelDescription("Project settings test description");

            args.Value({
                /* tslint:disable: object-literal-sort-keys */
                name       : "Test name",
                description: "Test description",
                path       : "Test path",
                vxVersion  : args.getVxVersions().getFirst()
                /* tslint:enable */
            });

            return args;
        }

        /* dev:end */

        constructor($args? : ProjectSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectSettingsPanel());
        }

        public getInstance() : ProjectSettingsPanel {
            return <ProjectSettingsPanel>super.getInstance();
        }

        public ViewerArgs($args? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            return <ProjectSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectSettingsPanel, $args : ProjectSettingsPanelViewerArgs) : void {
            $instance.headerText = $args.HeaderText();
            $instance.descriptionLabel.Text($args.DescriptionText());
            $instance.nameLabel.Text($args.NameText());
            $instance.pathLabel.Text($args.PathText());
            $instance.vxVersionLabel.Text($args.VxVersionText());
            $instance.vxVersionOption.Clear();
            $instance.vxVersionOption.Height(-1);
            $args.getVxVersions().foreach(($version : string) : void => {
                $instance.vxVersionOption.Add($version);
            });
            $instance.vxVersionOption.Select(0);
            $instance.panelDescription = $args.PanelDescription();
            $instance.ApplySettings($args.Value());
        }

        /* dev:start */
        protected testImplementation($instance : ProjectSettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ProjectSettingsPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
