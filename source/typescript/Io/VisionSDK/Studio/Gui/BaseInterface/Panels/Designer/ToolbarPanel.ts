/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ToolbarPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ToolbarPanelViewerArgs;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import ProgressBar = Io.VisionSDK.UserControls.BaseInterface.UserControls.ProgressBar;
    import ProgressBarType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ProgressBarType;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IBuildSettingsEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IBuildSettingsEvents;
    import BuildSettingsEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.BuildSettingsEventArgs;
    import BuildSettingsEventType = Io.VisionSDK.Studio.Gui.Enums.Events.BuildSettingsEventType;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import DropDownButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownButton;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import IBuildPlatform = Io.VisionSDK.Studio.Gui.Interfaces.IBuildPlatform;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import IDropDownButtonItem = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonItem;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;

    export class ToolbarPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public saveButton : ImageButton;
        public runButton : ImageButton;
        public openProjectFolderButton : ImageButton;
        public consoleButton : ImageButton;
        public undoButton : ImageButton;
        public redoButton : ImageButton;
        public helpButton : ImageButton;
        public updateButton : Button;
        public statusBar : ProgressBar;
        public platformList : DropDownButton;
        public configureBuildText : string;
        public fileMenu : DropDownList;
        private buildPlatform : IBuildPlatform;
        private menuHandlers : ArrayList<any>;
        private platforms : IBuildPlatform[];
        private selectedPlatform : string;

        constructor($id? : string) {
            super($id);

            this.fileMenu = new DropDownList();
            this.saveButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.runButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.openProjectFolderButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.consoleButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.undoButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.redoButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.helpButton = new ImageButton(ImageButtonType.TOOLBAR_ITEM);
            this.updateButton = new Button(ButtonType.GREEN_MEDIUM);
            this.statusBar = new ProgressBar();
            this.platformList = new DropDownButton();
            this.menuHandlers = new ArrayList<any>();
            this.buildPlatform = null;
            this.configureBuildText = "";
            this.platforms = [];
            this.selectedPlatform = "";
        }

        public Value($value? : ToolbarPanelViewerArgs) : ToolbarPanelViewerArgs {
            return <ToolbarPanelViewerArgs>super.Value($value);
        }

        public AddFileMenuItem($itemName : string, $handler : ($eventArgs : EventArgs, $manager : GuiObjectManager) => void,
                               $splitter : boolean = false) {
            if (!$splitter) {
                this.fileMenu.Add($itemName);
            } else {
                this.fileMenu.Add($itemName, $itemName, "WithSplitter");
            }
            this.menuHandlers.Add($handler, $itemName);
        }

        public setPlatforms($platforms : IBuildPlatform[], $selectedPlatform? : string, $enableConfigure? : boolean) : void {
            if (!ObjectValidator.IsEmptyOrNull($platforms)) {
                if (!ObjectValidator.IsEmptyOrNull($selectedPlatform)) {
                    this.selectedPlatform = $selectedPlatform;
                }
                if (ObjectValidator.IsEmptyOrNull(this.selectedPlatform)) {
                    this.selectedPlatform = $platforms[0].value;
                }
                const platformsIdentity : string = JSON.stringify($platforms) + JSON.stringify(this.selectedPlatform);
                const prevPlatformsIdentity : string = ObjectValidator.IsEmptyOrNull(this.buildPlatform) ?
                    "" : JSON.stringify(this.platforms) + JSON.stringify(this.buildPlatform.value);
                if (platformsIdentity !== prevPlatformsIdentity) {
                    this.platforms = $platforms;
                    const platformItems : IDropDownButtonItem[] = [];
                    if ($enableConfigure) {
                        platformItems.push({
                            callback      : () : void => {
                                const eventArgs : EventArgs = new EventArgs();
                                eventArgs.Owner(this);
                                this.getEventsManager().FireEvent(this, BuildSettingsEventType.ON_SETTINGS_REQUEST, eventArgs);
                            },
                            forceVisible  : true,
                            styleClassName: "Config",
                            text          : this.configureBuildText
                        });
                    }

                    let selected : number = null;
                    $platforms.forEach(($platform : IBuildPlatform, $index : number) : void => {
                        platformItems.push({
                            callback: () : void => {
                                const eventArgs : BuildSettingsEventArgs = new BuildSettingsEventArgs();
                                eventArgs.BuildPlatform($platform);
                                eventArgs.Owner(this);

                                this.setPlatforms($platforms, $platform.value);
                                this.getEventsManager().FireEvent(this, BuildSettingsEventType.ON_BUILD_PLATFORM_CHANGE, eventArgs);
                            },
                            text    : $platform.text
                        });
                        if ($platform.value === this.selectedPlatform) {
                            this.buildPlatform = $platform;
                            selected = $index;
                        }
                    });
                    this.platformList.Value({values: platformItems, activeIndex: selected, type: DropDownButtonType.GRAY_LIST});
                }
            }
        }

        public getPlatforms() : IBuildPlatform[] {
            return this.platforms;
        }

        public getEvents() : IBuildSettingsEvents {
            return <IBuildSettingsEvents>super.getEvents();
        }

        public EnableMouseEvents($value : boolean) {
            if ($value) {
                ElementManager.ClearCssProperty(this, "pointer-events");
            } else {
                ElementManager.setCssProperty(this, "pointer-events", "none");
            }
        }

        protected innerCode() : IGuiElement {
            this.fileMenu.ShowButton(true);
            this.fileMenu.ShowField(false);
            this.fileMenu.Height(-1);
            this.fileMenu.Width(125);
            this.fileMenu.StyleClassName("FileMenu");

            this.fileMenu.getEvents().setOnSelect(($eventArgs : EventArgs, $manager : GuiObjectManager) => {
                const handler : any = this.menuHandlers.getItem(this.fileMenu.Value());
                if (ObjectValidator.IsFunction(handler)) {
                    handler($eventArgs, $manager);
                }
            });

            this.saveButton.IconName(IconType.SAVE);
            this.runButton.IconName(IconType.RUN_ONCE);
            this.openProjectFolderButton.IconName(IconType.PROJECT_FOLDER);
            this.consoleButton.IconName(IconType.TERMINAL);
            this.undoButton.IconName(IconType.UNDO);
            this.redoButton.IconName(IconType.REDO);
            this.helpButton.IconName(IconType.HELP);

            this.platformList.Width(188);
            this.platformList.Enabled(true);

            this.updateButton.Visible(false);
            this.updateButton.Width(150);

            this.statusBar.Visible(false);
            this.statusBar.Width(this.platformList.Width());
            this.statusBar.GuiType(ProgressBarType.MARQUEE);

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement().StyleClassName("Buttons")
                    .Add(this.fileMenu)
                    .Add(this.saveButton)
                    .Add(this.undoButton)
                    .Add(this.redoButton)
                    .Add(this.openProjectFolderButton)
                    .Add(this.helpButton)
                )
                .Add(this.addElement().StyleClassName("BuildBar")
                    .Add(this.consoleButton)
                    .Add(this.addElement().StyleClassName("PlatformBar")
                        .Add(this.platformList)
                        .Add(this.statusBar)
                    )
                    .Add(this.addElement().StyleClassName("Last")
                        .Add(this.runButton))
                )
                .Add(this.addElement()
                    .Add(this.updateButton)
                );
        }
    }
}
