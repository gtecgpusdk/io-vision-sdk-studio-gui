/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import NodesFilterPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.NodesFilterPanel;
    import NodesFilterPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.NodesFilterPanelViewerArgs;

    export class NodesFilterPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : NodesFilterPanelViewerArgs {
            const args : NodesFilterPanelViewerArgs = new NodesFilterPanelViewerArgs();
            args.FilterFieldHint("Filter by node or method name ...");
            return args;
        }

        /* dev:end */

        constructor($args? : NodesFilterPanelViewerArgs) {
            super($args);
            this.setInstance(new NodesFilterPanel());
        }

        public getInstance() : NodesFilterPanel {
            return <NodesFilterPanel>super.getInstance();
        }

        public ViewerArgs($args? : NodesFilterPanelViewerArgs) : NodesFilterPanelViewerArgs {
            return <NodesFilterPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : NodesFilterPanel, $args : NodesFilterPanelViewerArgs) : void {
            $instance.filterField.Hint($args.FilterFieldHint());
        }

        /* dev:start */
        protected testImplementation($instance : NodesFilterPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(450);
            $instance.Height(34);
            return "<style>.TestCss .NodesFilterPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
