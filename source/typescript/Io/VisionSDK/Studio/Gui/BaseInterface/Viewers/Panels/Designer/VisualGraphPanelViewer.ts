/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import VisualGraphPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.VisualGraphPanelViewerArgs;
    import VisualGraphPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.VisualGraphPanel;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;

    export class VisualGraphPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : VisualGraphPanelViewerArgs {
            const args : VisualGraphPanelViewerArgs = new VisualGraphPanelViewerArgs();

            const visualGraph : KernelNode = new KernelNode(KernelNodeType.VISUAL_GRAPH, -1, -1);
            visualGraph.Type(KernelNodeType.VISUAL_GRAPH);
            visualGraph.UniqueIdPrefix(KernelNodeType.VISUAL_GRAPH);
            visualGraph.NamePrefix(KernelNodeType.VISUAL_GRAPH);

            const vxContext : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, 3, 3);
            vxContext.Type(KernelNodeType.VXCONTEXT);
            vxContext.UniqueIdPrefix(KernelNodeType.VXCONTEXT);
            vxContext.NamePrefix(KernelNodeType.VXCONTEXT);

            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            vxGraph.Type(KernelNodeType.VXGRAPH);
            vxGraph.UniqueIdPrefix(KernelNodeType.VXGRAPH);
            vxGraph.NamePrefix(KernelNodeType.VXGRAPH);

            args.GraphRoot(visualGraph);

            visualGraph.ChildNodes().Add(vxContext);
            vxContext.ParentNode(visualGraph);
            vxContext.ChildNodes().Add(vxGraph);
            vxGraph.ParentNode(vxContext);

            // const display : KernelNode = new KernelNode(KernelNodeType.DISPLAY, 6, 4);
            // vxGraph.ChildNodes().Add(display);
            // display.ParentNode(vxGraph);
            // display.Type(KernelNodeType.DISPLAY);
            // display.Name(KernelNode.GenerateUniqueName(display, "ioCom"));
            // display.setAttribute(<IKernelNodeAttribute>{
            //     name : "width",
            //     text : "Width",
            //     type : "Integer",
            //     value: 640
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     name : "height",
            //     text : "Height",
            //     type : "Integer",
            //     value: 480
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "color",
            //     text : "Color",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "color2",
            //     text : "Color2",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "color3",
            //     text : "Color3",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "color4",
            //     text : "Color4",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // display.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "color5",
            //     text : "Color5",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // display.Help(<IKernelNodeHelp>{
            //     inputs : ["(vx_image) inputImage"],
            //     tooltip: "Node test description."
            //
            // });
            // display.AddInput("vx_image");
            //
            // const node : KernelNode = new KernelNode(KernelNodeType.ACCUMULATE_SQUARED, 4, 7);
            // vxGraph.ChildNodes().Add(node);
            // node.ParentNode(vxGraph);
            // node.Type(KernelNodeType.ACCUMULATE_SQUARED);
            // node.Name(KernelNode.GenerateUniqueName(node, "accu_sq"));
            // node.setAttribute(<IKernelNodeAttribute>{
            //     name : "shift",
            //     text : "Shift",
            //     type : "Float",
            //     value: 0
            // });
            // node.Help(<IKernelNodeHelp>{
            //     outputs: ["(vx_image) outputImage"],
            //     tooltip: "Node test description."
            //
            // });
            // node.AddOutput("vx_image");
            //
            // const input : Input = display.getInput(0);
            // const output : Output = node.getOutput(0);
            // input.output = output;
            // output.inputs.Add(input);
            //
            // const node2 : KernelNode = new KernelNode(KernelNodeType.ACCUMULATE_WEIGHTED, 3, 7);
            // vxGraph.ChildNodes().Add(node2);
            // node2.ParentNode(vxGraph);
            // node2.Type(KernelNodeType.ACCUMULATE_WEIGHTED);
            // node2.Name(KernelNode.GenerateUniqueName(node2, "accu_wei"));
            // node2.setAttribute(<IKernelNodeAttribute>{
            //     format: {
            //         decimalPlaces: 1,
            //         max          : 1.0,
            //         min          : 0.0,
            //         step         : 0.1
            //     },
            //     name  : "alpha",
            //     text  : "Alpha",
            //     type  : "Float",
            //     value : 0.5
            // });
            // node2.Help(<IKernelNodeHelp>{
            //     inputs : ["(some_datatype) some input"],
            //     tooltip: "Node test description."
            // });
            // node2.AddInput("some_datatype");
            //
            // const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
            // vxGraph.ChildNodes().Add(file);
            // file.ParentNode(vxGraph);
            // file.Type(KernelNodeType.IMAGE_INPUT);
            // file.Name(KernelNode.GenerateUniqueName(file, "ioCom"));
            // file.setAttribute(<IKernelNodeAttribute>{
            //     format: {
            //         readonly: true
            //     },
            //     name  : "width",
            //     text  : "Width",
            //     type  : "Integer",
            //     value : 640
            // });
            // file.setAttribute(<IKernelNodeAttribute>{
            //     format: {
            //         readonly: true
            //     },
            //     name  : "height",
            //     text  : "Height",
            //     type  : "Integer",
            //     value : 480
            // });
            // file.setAttribute(<IKernelNodeAttribute>{
            //     name : "path",
            //     text : "Path",
            //     type : "String",
            //     value: "test/resource/graphics/Io/VisionSDK/Studio/Gui/VisionSDK.jpg"
            // });
            // file.setAttribute(<IKernelNodeAttribute>{
            //     items: [
            //         "VX_DF_IMAGE_U8",
            //         "VX_DF_IMAGE_S16"
            //     ],
            //     name : "outputType",
            //     text : "Output type",
            //     type : "Enum",
            //     value: "VX_DF_IMAGE_U8"
            // });
            // file.Help(<IKernelNodeHelp>{
            //     outputs: ["(some_datatype) some output"],
            //     tooltip: "Node test description."
            // });
            // file.AddOutput("some_datatype");

            args.NodeNotifications({
                no_compatible_node_connection_exists: "No compatible node connection exists",
                no_input_connections                : "No input connection available"
            });

            return args;
        }

        /* dev:end */

        constructor($args? : VisualGraphPanelViewerArgs) {
            super($args);
            this.setInstance(new VisualGraphPanel());
        }

        public getInstance() : VisualGraphPanel {
            return <VisualGraphPanel>super.getInstance();
        }

        public ViewerArgs($args? : VisualGraphPanelViewerArgs) : VisualGraphPanelViewerArgs {
            return <VisualGraphPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : VisualGraphPanel, $args : VisualGraphPanelViewerArgs) : void {
            $instance.GraphRoot($args.GraphRoot());
            KernelNode.Notifications($args.NodeNotifications());
        }

        /* dev:start */
        protected testImplementation($instance : VisualGraphPanel, $args : VisualGraphPanelViewerArgs) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(800);
            $instance.Height(790);
            return "<style>.TestCss .VisualGraphPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
