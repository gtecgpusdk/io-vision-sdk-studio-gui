/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";

    /**
     * EditorPageEventArgs class provides args connected with changes related to EditorPage class.
     */
    export class EditorPageEventArgs extends BuildSettingsEventArgs {

    }
}
