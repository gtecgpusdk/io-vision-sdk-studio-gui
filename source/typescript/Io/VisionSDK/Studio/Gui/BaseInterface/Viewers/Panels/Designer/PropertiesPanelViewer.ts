/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import PropertiesPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.PropertiesPanelViewerArgs;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;
    import BaseFormInputArgs = Com.Wui.Framework.UserControls.Structures.BaseFormInputArgs;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class PropertiesPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : PropertiesPanelViewerArgs {
            const args : PropertiesPanelViewerArgs = new PropertiesPanelViewerArgs();
            args.NameText("Name");
            args.NameValue("default");
            args.TypeText("Type");
            args.TypeValue("VisualGraph");
            return args;
        }

        /* dev:end */

        constructor($args? : PropertiesPanelViewerArgs) {
            super($args);
            this.setInstance(new PropertiesPanel());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.PropertiesPanelTest);
            /* dev:end */
        }

        public getInstance() : PropertiesPanel {
            return <PropertiesPanel>super.getInstance();
        }

        public ViewerArgs($args? : PropertiesPanelViewerArgs) : PropertiesPanelViewerArgs {
            return <PropertiesPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : PropertiesPanel, $args : PropertiesPanelViewerArgs) : void {
            if (!(ObjectValidator.IsSet($args.NameText) || ObjectValidator.IsSet($args.NameValue) ||
                ObjectValidator.IsSet($args.TypeText) || ObjectValidator.IsSet($args.TypeValue))) {
                return;
            }
            const setConfig : any = ($parameter : FormInput, $text : string, $value : string) : void => {
                const config : BaseFormInputArgs = $parameter.Configuration();
                config.Name($text);
                config.Value($value);
                config.ForceSetValue(true);
                $parameter.Configuration(config);
            };
            setConfig($instance.nameForm, $args.NameText(), $args.NameValue());
            setConfig($instance.typeForm, $args.TypeText(), $args.TypeValue());
        }
    }
}
