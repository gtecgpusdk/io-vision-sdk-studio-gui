/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EditorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPanelViewerArgs;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import KernelNodesPickerPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.KernelNodesPickerPanelViewer;
    import VisualGraphPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.VisualGraphPanelViewer;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import ToolbarPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.ToolbarPanelViewer;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import DetailsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.DetailsPanelViewer;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import HistoryManager = Io.VisionSDK.Studio.Gui.Utils.HistoryManager;
    import StateEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.StateEventArgs;
    import IEditorPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IEditorPanelEvents;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import KernelNodeUtils = Io.VisionSDK.UserControls.Utils.KernelNodeUtils;
    import KernelNodeRegistry = Io.VisionSDK.UserControls.Utils.KernelNodeRegistry;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import PropertyEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.PropertyEventArgs;
    import PropertiesPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.PropertiesPanelEventType;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import VisualGraphPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.VisualGraphPanelEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class EditorPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public toolbar : ToolbarPanel;
        public statusLabel : Label;
        public detailsPanel : DetailsPanel;
        public kernelNodePickerPanel : KernelNodesPickerPanel;
        public visualGraphPanel : VisualGraphPanel;
        public historyManager : HistoryManager;
        private progressStatus : string;

        constructor($id? : string) {
            super($id);
            this.statusLabel = new Label();
            this.progressStatus = "";

            this.addChildPanel(ToolbarPanelViewer, null,
                ($parent : EditorPanel, $child : ToolbarPanel) : void => {
                    $parent.toolbar = $child;
                });

            this.addChildPanel(DetailsPanelViewer, null,
                ($parent : EditorPanel, $child : DetailsPanel) : void => {
                    $parent.detailsPanel = $child;
                });

            this.addChildPanel(VisualGraphPanelViewer, null,
                ($parent : EditorPanel, $child : VisualGraphPanel) : void => {
                    $parent.visualGraphPanel = $child;

                    this.historyManager = new HistoryManager($child.gridManager, $child.connectionManager,
                        $child.nodeRegistry, ($rootName : string, $callback : any) : void => {
                            $child.RootNode($child.SearchRootNode($rootName), $callback);
                        });
                });

            this.addChildPanel(KernelNodesPickerPanelViewer, null,
                ($parent : EditorPanel, $child : KernelNodesPickerPanel) : void => {
                    $parent.kernelNodePickerPanel = $child;
                });

        }

        public Value($value? : EditorPanelViewerArgs) : EditorPanelViewerArgs {
            return <EditorPanelViewerArgs>super.Value($value);
        }

        public setProgressStatus($value : string) : void {
            this.progressStatus = $value;
            const handler : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.progressStatus)) {
                    this.toolbar.statusBar.Visible(true);
                }

                const handler : any = () : void => {
                    if (!ObjectValidator.IsEmptyOrNull(this.progressStatus)) {
                        this.toolbar.statusBar.Text(this.progressStatus);
                    } else {
                        this.toolbar.statusBar.Visible(false);
                    }
                };

                if (this.toolbar.statusBar.IsCompleted()) {
                    handler();
                } else {
                    this.toolbar.statusBar.getEvents().setOnComplete(handler);
                }
            };

            if (this.IsCompleted()) {
                handler();
            } else {
                this.getEvents().setOnComplete(() : void => {
                    handler();
                });
            }
        }

        public IsProgressVisible() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.progressStatus);
        }

        public getEvents() : IEditorPanelEvents {
            return <IEditorPanelEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            KernelNode.TypeToNodeMapper(($type : KernelNodeType) : IKernelNode => {
                return this.visualGraphPanel.nodeUtils.Copy(this.kernelNodePickerPanel.getNode($type), false,
                    false, false).getFirst();
            });

            let rootChangeHandle : number = null;
            this.detailsPanel.getEvents().setOnChange(() : void => {
                clearTimeout(rootChangeHandle);
                rootChangeHandle = null;
                const node : IKernelNode = this.detailsPanel.getSelectedNode();
                const root : IKernelNode = ObjectValidator.IsEmptyOrNull(node.ParentNode()) ? node : node.ParentNode();
                if (root !== this.visualGraphPanel.RootNode()) {
                    this.visualGraphPanel.RootNode(root, () : void => {
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            clearTimeout(rootChangeHandle);
                            rootChangeHandle = null;
                            this.visualGraphPanel.SelectNodeById(node.UniqueId());
                        }, 10);
                    });
                } else {
                    this.visualGraphPanel.DeselectAll(false);
                    this.visualGraphPanel.SelectNodeById(node.UniqueId());
                }
            });

            const propertiesPanel : PropertiesPanel = this.detailsPanel.accordion.getPanel(PropertiesPanel);

            const nodeUtils : KernelNodeUtils = this.visualGraphPanel.nodeUtils;
            const nodeRegistry : KernelNodeRegistry = this.visualGraphPanel.nodeRegistry;

            const updateNodeInfo : any = ($node : IKernelNode) : void => {
                if (!ObjectValidator.IsEmptyOrNull($node)) {
                    propertiesPanel.ShowProperties($node);
                    if ($node === this.visualGraphPanel.RootNode()) {
                        (<TextField>propertiesPanel.nameForm.getInput()).ReadOnly(true);
                    }
                }
            };

            let stateArgs : StateEventArgs = null;
            let node : IKernelNode = null;
            let attribute : string = null;
            const parseArgs : any = ($node : IKernelNode, $attribute : string) : void => {
                if ($node !== node || $attribute !== attribute) {
                    pushState();
                }
                if (ObjectValidator.IsEmptyOrNull(stateArgs)) {
                    stateArgs = StateEventArgs.getBeforeNodeAttributeUpdateArgs(this.visualGraphPanel.gridManager);
                    node = $node;
                    attribute = $attribute;
                }
            };

            const handlePush : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(stateArgs)) {
                    this.historyManager.Push(stateArgs);
                    stateArgs = null;
                    node = null;
                    attribute = null;
                }
            };

            const pushState : any = ($delay : number = 0) : void => {
                if ($delay === 0) {
                    handlePush();
                } else {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        handlePush();
                    }, true, $delay);
                }
            };

            propertiesPanel.getEvents().setOnPropertyChange(($eventArgs : PropertyEventArgs) : void => {
                const selectedNode : IKernelNode = this.visualGraphPanel.getSelectedNode();
                if (!ObjectValidator.IsEmptyOrNull(selectedNode)) {
                    if ($eventArgs.Name() === "name") {
                        if (selectedNode.Name() !== $eventArgs.Value()) {
                            if (selectedNode.Type() === "VisualGraph" || nodeRegistry.IsNameAvailable($eventArgs.Value())) {
                                parseArgs(selectedNode, $eventArgs.Name());
                                propertiesPanel.setNameValidity(true);
                                nodeRegistry.UnassignName(selectedNode.Name());
                                selectedNode.Name($eventArgs.Value());
                                this.getEventsManager().FireEvent(this, PropertiesPanelEventType.ON_PROPERTY_CHANGE, $eventArgs, false);
                                pushState(300);
                            } else {
                                propertiesPanel.setNameValidity(false);
                            }
                        }
                    } else {
                        const selectedAttribute : IKernelNodeAttribute = selectedNode.getAttributes().getItem($eventArgs.Name());
                        if (!ObjectValidator.IsEmptyOrNull(selectedAttribute) && selectedAttribute.value !== $eventArgs.Value()) {
                            parseArgs(selectedNode, $eventArgs.Name());
                            selectedAttribute.value = $eventArgs.Value();
                            if ($eventArgs.Name() === "path") {
                                updateNodeInfo(selectedNode);
                                if (selectedNode.Type() === "IMAGE_INPUT") {
                                    propertiesPanel.getInputFileIOSize(($size : Size) : void => {
                                        selectedNode.getAttributes().getItem("width").value = $size.Width();
                                        selectedNode.getAttributes().getItem("height").value = $size.Height();
                                        updateNodeInfo(selectedNode);
                                        this.getEventsManager().FireEvent(this.visualGraphPanel, EventType.ON_CHANGE);
                                    });
                                }
                            } else {
                                updateNodeInfo(selectedNode);
                            }
                            this.getEventsManager().FireEvent(this, PropertiesPanelEventType.ON_PROPERTY_CHANGE, $eventArgs, false);
                            pushState($eventArgs.PropertyType() === "Integer" || $eventArgs.PropertyType() === "Float" ? 300 : 0);
                        }
                    }
                }
                this.detailsPanel.setProjectStructure(this.visualGraphPanel.GraphRoot(), 300);
            });

            this.kernelNodePickerPanel.getEvents().setOnNodesPicked(($eventArgs : VisualGraphEventArgs) : void => {
                this.visualGraphPanel.IsAreaScrollEnabled(false);
                this.getEvents().FireAsynchronousMethod(() : void => {
                    this.visualGraphPanel.IsAreaScrollEnabled(true);
                }, 500);

                this.getEvents().FireAsynchronousMethod(() : void => {
                    this.visualGraphPanel.DragNodes(nodeUtils.Copy($eventArgs.SelectedNodes()), $eventArgs.ActiveNode().Type());
                }, 50);
            });

            this.visualGraphPanel.getEvents().setOnStateChange(($eventArgs : StateEventArgs) : void => {
                if (this.IsCompleted()) {
                    this.detailsPanel.setProjectStructure(this.visualGraphPanel.GraphRoot(), 150);
                } else {
                    this.getEvents().setOnComplete(() : void => {
                        this.detailsPanel.setProjectStructure(this.visualGraphPanel.GraphRoot());
                    });
                }
                this.historyManager.Push($eventArgs);
                this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_STATE_CHANGE, $eventArgs, false);
            });

            this.kernelNodePickerPanel.TypeFilter(
                {types: ArrayList.ToArrayList([KernelNodeType.VXCONTEXT, KernelNodeType.VXGRAPH]), exclude: true});
            this.kernelNodePickerPanel.Filter("");

            let prevRoot : IKernelNode = null;
            this.visualGraphPanel.getEvents().setOnRootChangeStart(() : void => {
                prevRoot = this.visualGraphPanel.RootNode();
                this.kernelNodePickerPanel.EnableDrag(false);
            });

            this.visualGraphPanel.getEvents().setOnRootChangeComplete(() : void => {
                switch (this.visualGraphPanel.RootNode().Type()) {
                case KernelNodeType.VISUAL_GRAPH:
                    this.kernelNodePickerPanel.TypeFilter(
                        {types: ArrayList.ToArrayList(KernelNodeType.VXCONTEXT), exclude: false});
                    break;
                case KernelNodeType.VXCONTEXT:
                    this.kernelNodePickerPanel.TypeFilter(
                        {types: ArrayList.ToArrayList(KernelNodeType.VXGRAPH), exclude: false});
                    break;
                case KernelNodeType.VXGRAPH:
                    this.kernelNodePickerPanel.TypeFilter(
                        {types: ArrayList.ToArrayList([KernelNodeType.VXCONTEXT, KernelNodeType.VXGRAPH]), exclude: true});
                    break;
                }
                if (!ObjectValidator.IsEmptyOrNull(prevRoot) || this.visualGraphPanel.RootNode().Type() !== KernelNodeType.VXGRAPH) {
                    this.kernelNodePickerPanel.Filter("");
                    this.kernelNodePickerPanel.nodesFilter.EnableSearch(false, true, () : void => {
                        this.kernelNodePickerPanel.nodesFilter.filterField.Value("");
                    });
                } else {
                    this.kernelNodePickerPanel.Filter(this.kernelNodePickerPanel.Filter());
                }

                this.detailsPanel.setProjectStructure(this.visualGraphPanel.GraphRoot(), 150);
                this.kernelNodePickerPanel.EnableDrag(true);
            });

            this.visualGraphPanel.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $manager.getActive(TextField).foreach(($element : TextField) : void => {
                    ElementManager.getElement($element.Id() + "_Input").blur();
                });
                if (ObjectValidator.IsEmptyOrNull(this.visualGraphPanel.getSelectedNodes())) {
                    this.detailsPanel.UpdateNodeInfo(this.visualGraphPanel.RootNode(), false);
                }
            });

            WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                if (!this.visualGraphPanel.gridManager.IsDragActive() && !this.visualGraphPanel.connectionManager.IsDragActive() &&
                    !$manager.IsActive(FormInput) && !$manager.IsActive(Dialog) && !$manager.IsActive(TextField) &&
                    ($eventArgs.getKeyCode() === KeyMap.DELETE || $eventArgs.getKeyCode() === KeyMap.ESC)) {
                    this.detailsPanel.UpdateNodeInfo(this.visualGraphPanel.RootNode(), false);
                }
            });

            this.visualGraphPanel.getEvents().setOnRootChangeComplete(() : void => {
                rootChangeHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull(propertiesPanel)) {
                        this.detailsPanel.SelectNode(this.visualGraphPanel.RootNode());
                    }
                }, true, 150);
            });

            this.visualGraphPanel.getEvents().setOnNodeSelect(() : void => {
                clearTimeout(rootChangeHandle);
                rootChangeHandle = null;
                const node : IKernelNode = ObjectValidator.IsEmptyOrNull(this.visualGraphPanel.getSelectedNode()) ?
                    this.visualGraphPanel.RootNode() : this.visualGraphPanel.getSelectedNode();
                if (!ObjectValidator.IsEmptyOrNull(node)) {
                    this.detailsPanel.UpdateNodeInfo(node, node !== this.visualGraphPanel.RootNode());
                    this.detailsPanel.SelectNode(node);
                }
            });

            this.visualGraphPanel.getEvents().setOnChange(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(propertiesPanel) && !this.visualGraphPanel.KernelNodes()
                    .Contains(propertiesPanel.getCurrentNode())) {
                    propertiesPanel.ShowProperties(this.visualGraphPanel.RootNode());
                }
            });

            this.visualGraphPanel.getEvents().setOnDragActionStart(() : void => {
                [this.toolbar, this.detailsPanel, this.visualGraphPanel.controlPanel, this.kernelNodePickerPanel]
                    .forEach(($panel : any) : void => {
                        $panel.EnableMouseEvents(false);
                    });
            });

            this.visualGraphPanel.getEvents().setOnDragActionComplete(() : void => {
                [this.toolbar, this.detailsPanel, this.visualGraphPanel.controlPanel, this.kernelNodePickerPanel]
                    .forEach(($panel : any) : void => {
                        $panel.EnableMouseEvents(true);
                    });
            });

            let onCompleteHandler : any = null;
            let onForcableHandler : any = null;
            const handleHistory : any = ($handler : any) : void => {
                this.visualGraphPanel.IsEventsEnabled(false);
                this.kernelNodePickerPanel.EnableDrag(false);
                if (this.historyManager.IsComplete()) {
                    $handler();
                } else {
                    if (this.historyManager.IsForceable()) {
                        onCompleteHandler = () : void => {
                            onCompleteHandler = null;
                            $handler();
                        };
                        this.historyManager.ForceChangeApply();

                    } else {
                        onForcableHandler = () : void => {
                            onForcableHandler = null;
                            onCompleteHandler = () : void => {
                                onCompleteHandler = null;
                                $handler();
                            };
                            this.historyManager.ForceChangeApply();
                        };
                    }
                }
            };

            this.toolbar.undoButton.getEvents().setOnClick(() : void => {
                handleHistory(() : void => {
                    this.historyManager.Undo();
                });
            });

            this.toolbar.redoButton.getEvents().setOnClick(() : void => {
                handleHistory(() : void => {
                    this.historyManager.Redo();
                });
            });

            this.visualGraphPanel.getEvents().setOnMouseOver(() : void => {
                if (!this.historyManager.IsComplete()) {
                    if (this.historyManager.IsForceable()) {
                        this.historyManager.ForceChangeApply();
                    } else {
                        onForcableHandler = () : void => {
                            onForcableHandler = null;
                            this.historyManager.ForceChangeApply();
                        };
                    }
                }
            });

            this.historyManager.setOnStateChange(() : void => {
                if (this.historyManager.IsComplete()) {
                    const selected : IKernelNode = this.visualGraphPanel.getSelectedNode();
                    updateNodeInfo(ObjectValidator.IsEmptyOrNull(selected) ? this.visualGraphPanel.RootNode() : selected);
                    this.detailsPanel.setProjectStructure(this.visualGraphPanel.GraphRoot());
                    this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_STATE_CHANGE, new EventArgs(), false);
                    this.visualGraphPanel.connectionManager.ValidateOutputs(this.visualGraphPanel.gridManager.getNodes());
                    this.visualGraphPanel.IsEventsEnabled(true);
                    this.kernelNodePickerPanel.EnableDrag(true);
                    if (!ObjectValidator.IsEmptyOrNull(onCompleteHandler)) {
                        onCompleteHandler();
                    }
                } else if (this.historyManager.IsForceable()) {
                    if (!ObjectValidator.IsEmptyOrNull(onForcableHandler)) {
                        onForcableHandler();
                    }
                }
            });

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            const getSidePanelWidth : any = () : string => {
                const width : number = this.Width();
                if (width > 1600) {
                    return "350px";
                } else if (width > 1400) {
                    return "300px";
                } else if (width > 1100) {
                    return "250px";
                } else {
                    return "200px";
                }
            };

            return this.addColumn()
                .WidthOfColumn(100, UnitType.PCT, false)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow()
                    .HeightOfRow(40, UnitType.PX)
                    .Add(this.addColumn()
                        .Add(this.toolbar)
                    )
                )
                .Add(this.addRow()
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            return new PropagableNumber(getSidePanelWidth());
                        })
                        .Add(this.detailsPanel)
                    )
                    .Add(this.addColumn()
                        .Add(this.visualGraphPanel)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            return new PropagableNumber(getSidePanelWidth());
                        })
                        .Add(this.kernelNodePickerPanel)
                    )
                )
                .Add(this.addRow()
                    .HeightOfRow(34, UnitType.PX)
                    .Alignment(Alignment.LEFT_PROPAGATED)
                    .Add(this.addColumn()
                        .StyleClassName("StatusEnvelop")
                        .Add(this.addElement(this.Id() + "_StatusLabel")
                            .StyleClassName("StatusLabel")
                            .Add(this.statusLabel)
                        )
                    )
                );
        }
    }
}
