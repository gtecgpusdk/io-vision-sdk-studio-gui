/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class ProjectPickerPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private loadButtonText : string;

        constructor() {
            super();
            this.headerText = "";
            this.loadButtonText = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public LoadButtonText($value? : string) : string {
            return this.loadButtonText = Property.String(this.loadButtonText, $value);
        }
    }
}
