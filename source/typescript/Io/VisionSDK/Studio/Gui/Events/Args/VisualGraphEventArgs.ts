/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    /**
     * VisualGraphEventArgs class provides args connected with modification of VisualGraph model.
     */
    export class VisualGraphEventArgs extends EventArgs {
        private selectedNodes : ArrayList<IKernelNode>;
        private selectedConnections : ArrayList<IConnection>;
        private activeNode : IKernelNode;

        /**
         * @param {IKernelNode} [$value] Set value representing active kernel node.
         * @return {IKernelNode} Returns value representing active kernel node.
         */
        public ActiveNode($value? : IKernelNode) : IKernelNode {
            if (ObjectValidator.IsSet($value)) {
                this.activeNode = $value;
            }
            return this.activeNode;
        }

        /**
         * @param {ArrayList<IKernelNode>} [$value] Set value representing selected kernel nodes.
         * @return {ArrayList<IKernelNode>} Returns value representing selected kernel nodes.
         */
        public SelectedNodes($value? : ArrayList<IKernelNode>) : ArrayList<IKernelNode> {
            if (ObjectValidator.IsSet($value)) {
                this.selectedNodes = $value;
            }
            return this.selectedNodes;
        }

        /**
         * @param {ArrayList<IKernelNode>} [$value] Set value representing selected kernel nodes.
         * @return {ArrayList<IKernelNode>} Returns value representing selected kernel nodes.
         */
        public SelectedConnections($value? : ArrayList<IConnection>) : ArrayList<IConnection> {
            if (ObjectValidator.IsSet($value)) {
                this.selectedConnections = $value;
            }
            return this.selectedConnections;
        }
    }
}
