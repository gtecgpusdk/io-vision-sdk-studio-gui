/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;

    export class BaseTextPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {
        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new BaseTextPanel());
        }

        public getInstance() : BaseTextPanel {
            return <BaseTextPanel>super.getInstance();
        }

        protected argsHandler($instance : BaseTextPanel, $args : BasePanelViewerArgs) : void {
            // todo : send text through custom args?
        }

        /* dev:start */
        protected testImplementation($instance : ProjectSettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .BaseTextPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
