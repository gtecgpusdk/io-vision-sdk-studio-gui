/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import AppWizardPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.AppWizardPanelViewerArgs;
    import AppWizardPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.AppWizardPanel;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class AppWizardPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : AppWizardPanelViewerArgs {
            const args : AppWizardPanelViewerArgs = new AppWizardPanelViewerArgs();

            args.HeaderText("WELCOME");
            args.DescriptionText("welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to " +
                "welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to welcome to");
            return args;
        }

        /* dev:end */

        constructor($args? : AppWizardPanelViewerArgs) {
            super($args);
            this.setInstance(new AppWizardPanel());
        }

        public getInstance() : AppWizardPanel {
            return <AppWizardPanel>super.getInstance();
        }

        public ViewerArgs($args? : AppWizardPanelViewerArgs) : AppWizardPanelViewerArgs {
            return <AppWizardPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : AppWizardPanel, $args : AppWizardPanelViewerArgs) : void {
            $instance.HeaderText($args.HeaderText());
            $instance.description.Text($args.DescriptionText());
        }

        /* dev:start */
        protected testImplementation($instance : AppWizardPanel) : string {
            $instance.StyleClassName("TestCss");

            const testWizardTexts : string[] = [
                "Some random text some random text some random text some random text some random text some random " +
                "text some random text some random text some random text some random text some random text ",
                "Another text that is random"
            ];

            let index : number = 0;
            $instance.getEvents().setOnWizardForward(() => {
                $instance.description.Text(testWizardTexts[index]);
                index++;
                index = index % testWizardTexts.length;
            });

            $instance.getEvents().setOnWizardBack(() => {
                $instance.description.Text(testWizardTexts[index]);
                index--;
                index = index < 0 ? testWizardTexts.length - 1 : index;
            });

            let mouseDown : boolean = false;
            const handlers : any = {
                onMouseDown($eventArgs : MouseEventArgs) {
                    $eventArgs.StopAllPropagation();
                    $instance.MouseClick($eventArgs.NativeEventArgs().clientX, $eventArgs.NativeEventArgs().clientY, "hello world");
                    mouseDown = true;
                },
                onMouseUp($eventArgs : MouseEventArgs) {
                    $eventArgs.StopAllPropagation();
                    $instance.MouseHide();
                    mouseDown = false;
                },
                onMouseMove($eventArgs : MouseEventArgs) {
                    if (mouseDown) {
                        $eventArgs.StopAllPropagation();
                        $instance.MouseDrag($eventArgs.NativeEventArgs().clientX, $eventArgs.NativeEventArgs().clientY);
                    }
                }
            };

            const registerDrag : any = () : void => {
                WindowManager.getEvents().setOnMouseDown(handlers.onMouseDown);
                WindowManager.getEvents().setOnMouseUp(handlers.onMouseUp);
                WindowManager.getEvents().setOnMove(handlers.onMouseMove);
            };

            const unregisterDrag : any = () : void => {
                WindowManager.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, handlers.onMouseDown);
                WindowManager.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, handlers.onMouseUp);
                WindowManager.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, handlers.onMouseMove);
                $instance.ReleaseEvents();
            };

            this.addTestButton("Click", () : void => {
                unregisterDrag();
                $instance.MouseClick(300, 300, "Click", () : void => {
                    LogIt.Debug("Click fired");
                });
            });

            this.addTestButton("DoubleClick", () : void => {
                unregisterDrag();
                $instance.MouseDoubleClick(300, 400, "DoubleClick", () : void => {
                    LogIt.Debug("DoubleClick fired");
                });
            });

            this.addTestButton("MoveTo", () : void => {
                unregisterDrag();
                $instance.MouseClick(100, 100, "", () : void => {
                    $instance.MouseMoveTo(350, 350, null, () : void => {
                        LogIt.Debug("MouseMoveTo fired");
                    });
                });
            });

            this.addTestButton("Hide", () : void => {
                unregisterDrag();
                $instance.MouseHide();
            });

            this.addTestButton("Drag", () : void => {
                registerDrag();
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .AppWizardPanel{border: 1px solid #E7E7E8; width: 640px; height: 468px; margin-top: 32px;}</style>";
        }

        /* dev:end */
    }
}
