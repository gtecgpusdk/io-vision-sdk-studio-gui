/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import SettingsAreaPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.SettingsAreaPanel;
    import SettingsAreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.SettingsAreaPanelViewerArgs;
    import ConnectionSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ConnectionSettingsPanelViewer;
    import ToolchainSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ToolchainSettingsPanelViewer;
    import ProjectSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ProjectSettingsPanelViewer;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;

    export class SettingsAreaPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : SettingsAreaPanelViewerArgs {
            const args : SettingsAreaPanelViewerArgs = new SettingsAreaPanelViewerArgs();
            args.ProjectSettingsPanelArgs((<any>ProjectSettingsPanelViewer).getTestViewerArgs());
            args.ToolchainSettingsPanelArgs((<any>ToolchainSettingsPanelViewer).getTestViewerArgs());
            args.ConnectionSettingsPanelArgs((<any>ConnectionSettingsPanelViewer).getTestViewerArgs());
            return args;
        }

        /* dev:end */

        constructor($args? : SettingsAreaPanelViewerArgs) {
            super($args);
            this.setInstance(new SettingsAreaPanel($args));
        }

        public getInstance() : SettingsAreaPanel {
            return <SettingsAreaPanel>super.getInstance();
        }

        public ViewerArgs($args? : SettingsAreaPanelViewerArgs) : SettingsAreaPanelViewerArgs {
            return <SettingsAreaPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : SettingsAreaPanel, $args : SettingsAreaPanelViewerArgs) : void {
            $instance.projectSettingsPanel.Value($args.ProjectSettingsPanelArgs());
            $instance.connectionSettingsPanel.Value($args.ConnectionSettingsPanelArgs());
            $instance.toolchainSettingsPanel.Value($args.ToolchainSettingsPanelArgs());
        }

        /* dev:start */
        protected testImplementation($instance : SettingsAreaPanel) : string {
            const sizeHandler : any = () : void => {
                $instance.Width(Math.min(1024, Math.max(640, WindowManager.getSize().Width() - 60)));
                $instance.Height(Math.min(768, Math.max(480, WindowManager.getSize().Height() - 160)));
            };

            WindowManager.getEvents().setOnResize(() => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            $instance.getEvents().setOnComplete(() : void => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(768);
            return "<style>.TestCss .SettingsAreaPanel {}</style>";
        }

        /* dev:end */
    }
}
