/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events {
    "use strict";
    import IPropertiesPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IPropertiesPanelEvents;
    import IPropertyEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IPropertyEventsHandler;
    import PropertiesPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.PropertiesPanelEventType;
    import IVisualGraphEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IVisualGraphEventsHandler;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IEditorPageEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IEditorPageEvents;
    import IEditorPageEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IEditorPageEventsHandler;
    import EditorPageEventType = Io.VisionSDK.Studio.Gui.Enums.Events.EditorPageEventType;
    import IBuildSettingsEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IBuildSettingsEventsHandler;
    import BuildSettingsEventType = Io.VisionSDK.Studio.Gui.Enums.Events.BuildSettingsEventType;
    import IProjectEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IProjectEvents;
    import IProjectEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IProjectEventsHandler;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import VisualGraphPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.VisualGraphPanelEventType;
    import IStateEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.IStateEventsHandler;
    import ISettingsPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEvents;
    import ISettingsPanelEventsHandler = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEventsHandler;
    import SettingsPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.SettingsPanelEventType;
    import IBuildSettingsEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IBuildSettingsEvents;
    import Parent = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import IVisualGraphPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IVisualGraphPanelEvents;
    import IKernelNodesPickerPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IKernelNodesPickerPanelEvents;
    import IControlPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IControlPanelEvents;
    import ControlPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ControlPanelEventType;

    export class ElementEventsManager extends Parent
        implements IPropertiesPanelEvents, IVisualGraphPanelEvents, IKernelNodesPickerPanelEvents, IEditorPageEvents,
                   IProjectEvents, ISettingsPanelEvents, IBuildSettingsEvents, IControlPanelEvents {

        public setOnProjectCreate($handler : IProjectEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ProjectEventType.ON_PROJECT_CREATE, $handler);
        }

        public setOnProjectLoad($handler : IProjectEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ProjectEventType.ON_PROJECT_LOAD, $handler);
        }

        public setOnProjectSettingsChange($handler : ISettingsPanelEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ProjectEventType.ON_PROJECT_SETTINGS_UPDATE, $handler);
        }

        public setOnPathRequest($handler : IEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ProjectEventType.ON_PATH_REQUEST, $handler);
        }

        public setOnWizardClose($handler : IEditorPageEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, EditorPageEventType.ON_WIZARD_CLOSE, $handler);
        }

        public setOnSettingsUpdate($handler : ISettingsPanelEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, SettingsPanelEventType.ON_SETTINGS_UPDATE, $handler);
        }

        public setOnWizardForward($handler : IEditorPageEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, EditorPageEventType.ON_WIZARD_FORWARD, $handler);
        }

        public setOnWizardBack($handler : IEditorPageEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, EditorPageEventType.ON_WIZARD_BACK, $handler);
        }

        public setOnNodeDrag($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_NODE_DRAG, $handler);
        }

        public setOnRootChangeStart($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_ROOT_CHANGE_START, $handler);
        }

        public setOnRootChangeComplete($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_ROOT_CHANGE_COMPLETE, $handler);
        }

        public setOnZoomChange($handler : IEventsHandler) : void;
        public setOnZoomChange($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_ZOOM_CHANGE, $handler);
        }

        public setOnDragActionComplete($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_DRAG_ACTION_COMPLETE, $handler);
        }

        public setOnDragActionStart($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_DRAG_ACTION_START, $handler);
        }

        public setOnBuildPlatformChange($handler : IBuildSettingsEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, BuildSettingsEventType.ON_BUILD_PLATFORM_CHANGE, $handler);
        }

        public setOnSettingsRequest($handler : IEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, BuildSettingsEventType.ON_SETTINGS_REQUEST, $handler);
        }

        public setOnStateChange($handler : IStateEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, VisualGraphPanelEventType.ON_STATE_CHANGE, $handler);
        }

        public setOnValidationRequest($handler : IEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, SettingsPanelEventType.ON_VALIDATION_REQUEST, $handler);
        }

        /**
         * @param {IPropertyEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnPropertyChange($handler : IPropertyEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, PropertiesPanelEventType.ON_PROPERTY_CHANGE, $handler);
        }

        /**
         * @param {IPropertyEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnPropertySelect($handler : IPropertyEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, PropertiesPanelEventType.ON_PROPERTY_SELECT, $handler);
        }

        /**
         * @param {IVisualGraphEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnNodeSelect($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, EventType.ON_SELECT, $handler);
        }

        /**
         * @param {IVisualGraphEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnNodesPicked($handler : IVisualGraphEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, EventType.ON_DRAG_CHANGE, $handler);
        }

        public setOnProjectExport($handler : IEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ProjectEventType.ON_PROJECT_EXPORT, $handler);
        }

        public setOnReset($handler : IEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ControlPanelEventType.ON_RESET, $handler);
        }
    }
}
