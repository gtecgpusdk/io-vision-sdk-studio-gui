/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import IDefaultPageDescriptor = Io.VisionSDK.Studio.Gui.Interfaces.IDefaultPageDescriptor;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IExampleSettings = Io.VisionSDK.Studio.Gui.Interfaces.IExampleSettings;

    export class ProjectDescriptionPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public projectNameLabel : Label;
        public descriptionTextLabel : Label;
        private defaultPage : IDefaultPageDescriptor;

        constructor($id? : string) {
            super($id);
            this.projectNameLabel = new Label();
            this.descriptionTextLabel = new Label();
            this.defaultPage = null;
        }

        public ApplyDescriptor($value : IExampleSettings) : void {
            this.projectNameLabel.Text($value.name);
            this.descriptionTextLabel.Text(StringUtils.Replace($value.description, "<img src=", "<img alt=\" \" src="));
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                BasePanel.scrollBarVisibilityHandler(this);
            }, 100);
        }

        public ApplyDefaultPage() : void {
            this.setDefaultPage(this.defaultPage);
        }

        public setDefaultPage($value : IDefaultPageDescriptor) : void {
            this.defaultPage = $value;
            this.projectNameLabel.Text($value.header);
            this.descriptionTextLabel.Text($value.description);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.projectNameLabel.StyleClassName("ProjectName");
            this.descriptionTextLabel.StyleClassName("DescriptionText");

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.projectNameLabel)
                .Add(this.descriptionTextLabel);
        }
    }
}
