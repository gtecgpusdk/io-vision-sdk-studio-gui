/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import VisualGraphControlPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.VisualGraphControlPanel;
    import ControlPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ControlPanelViewerArgs;

    export class VisualGraphControlPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ControlPanelViewerArgs {
            return (<any>ControlPanelViewer).getTestViewerArgs();
        }

        /* dev:end */

        constructor($args? : ControlPanelViewerArgs) {
            super($args);
            this.setInstance(new VisualGraphControlPanel());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer.VisualGraphControlsPanelTest);
            /* dev:end */
        }

        public getInstance() : VisualGraphControlPanel {
            return <VisualGraphControlPanel>super.getInstance();
        }

        public ViewerArgs($args? : ControlPanelViewerArgs) : ControlPanelViewerArgs {
            return <ControlPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : VisualGraphControlPanel, $args : ControlPanelViewerArgs) : void {
            // $instance.resetButton.Title().Text($args.ResetButtonTooltipText());
        }

        /* dev:start */
        protected testImplementation($instance : VisualGraphControlPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(300);
            return "<style>.TestCss .VisualGraphControlPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
