/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Pages.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import EditorPage = Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer.EditorPage;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import PropertiesPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.PropertiesPanel;
    import IBoundingRectangle = Io.VisionSDK.Studio.Gui.Interfaces.IBoundingRectangle;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import EditorPageViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPageViewerArgs;
    import AppWizardPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.AppWizardPanelViewerArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import DetailsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.DetailsPanel;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import VisualGraphPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.VisualGraphPanel;
    import DragNodeType = Io.VisionSDK.UserControls.Enums.DragNodeType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GridManager = Io.VisionSDK.UserControls.Utils.GridManager;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import HistoryManager = Io.VisionSDK.Studio.Gui.Utils.HistoryManager;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import StateEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.StateEventArgs;
    import ConnectionManager = Io.VisionSDK.UserControls.Utils.ConnectionManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import KernelNodeUtils = Io.VisionSDK.UserControls.Utils.KernelNodeUtils;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import ProjectEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.ProjectEventArgs;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import KernelNodesPickerPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.KernelNodesPickerPanel;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import VisualGraphPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.VisualGraphPanelEventType;

    export class EditorPageTest extends ViewerTestRunner<EditorPage> {

        constructor() {
            super();
            this.setMethodFilter("IgnoreAll");
        }

        public testConsoleShowHide() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                page.ShowConsole(100);
                this.assertEquals(page.consoleDialog.Visible(), true);
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    page.HideConsole(100, 300, () : void => {
                        this.assertEquals(page.consoleDialog.Visible(), false);
                        $done();
                    });
                }, 600);
            };
        }

        public testWizardOpen() : void {
            const page : EditorPage = this.getInstance();
            page.ShowWizard(page.editorPanel.visualGraphPanel);
            this.assertEquals(page.wizardDialog.Visible(), true);
            page.HideWizard();
            this.assertEquals(page.wizardDialog.Visible(), false);
        }

        public __IgnoretestsetOnRootChangeStart() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                const pickerPanel : KernelNodesPickerPanel = page.editorPanel.kernelNodePickerPanel;
                const connectionManager : ConnectionManager = visualGraphPanel.connectionManager;

                const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
                file.Type(KernelNodeType.IMAGE_INPUT);
                file.Name("TestFile");
                const filter : KernelNode = new KernelNode(KernelNodeType.BOX_FILTER, 6, 6);
                filter.Type(KernelNodeType.BOX_FILTER);
                filter.Name("TestFilter");
                const outputFile : KernelNode = new KernelNode(KernelNodeType.IMAGE_OUTPUT, 5, 6);
                outputFile.Type(KernelNodeType.IMAGE_OUTPUT);

                connectionManager.DragConnection(file, 0);
                connectionManager.ApplyDrag(filter.getInput(0));
                connectionManager.DragConnection(filter, 0);
                connectionManager.ApplyDrag(outputFile.getInput(0));

                // const args : VisualGraphEventArgs = {
                //     selectedNodes      : visualGraphPanel.RootNode(),
                //     selectedConnections: file.getOutput(0).inputs.getItem(0).node,
                //     activeNode         : visualGraphPanel.RootNode().ChildNodes().getLast()
                // };
                visualGraphPanel.getEvents().setOnRootChangeStart(($eventArgs : VisualGraphEventArgs) : void => {
                    this.assertEquals($eventArgs.ActiveNode(), file);
                    this.assertEquals($eventArgs.SelectedNodes(), visualGraphPanel.RootNode());
                    $done();
                });
                this.emulateEvent(visualGraphPanel, VisualGraphPanelEventType.ON_ROOT_CHANGE_START);
            };
        }

        public __IgnoretestOnLoad() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const projectValue : IProjectSettingsValue = {
                    description: "",
                    name       : "NewVisualProject",
                    path       : "C:/Users/nxf07848/AppData/Local/VisionSDK/Vision SDK Studio Nightly/designer_cache/OpenVX_1_1_0",
                    vxVersion  : "1.1.0"
                };

                const project : ProjectEventArgs = new ProjectEventArgs();
                project.Descriptor(projectValue);

                page.examplesDialog.getEvents().setOnLoad((project) : void => {
                    this.getEventsManager().FireEvent(page.examplesDialog, ProjectEventType.ON_PROJECT_LOAD, project);
                    this.assertEquals(project.Descriptor(), projectValue);
                    $done();
                });
                // this.getEventsManager().FireEvent(page.examplesDialog, ProjectEventType.ON_PROJECT_LOAD, project);
                this.emulateEvent(page.examplesDialog, ProjectEventType.ON_PROJECT_LOAD);
            };
        }

        public __IgnoretestExamplesDialog() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                page.editorPanel.toolbar.fileMenu.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                    // ElementManager.getElement((<any>page.editorPanel.toolbar.fileMenu).items.getItem(1).Id()).click();
                    ElementManager.getElement(page.editorPanel.toolbar.fileMenu.Id() + "_Item_3_Status").click();
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(page.examplesDialog.Visible(), true);
                        $done();
                    }, 300);
                });
                this.emulateEvent(page.editorPanel.toolbar.fileMenu, EventType.ON_CLICK);
            };
        }

        public testSettingsDialog() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                page.editorPanel.toolbar.fileMenu.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                    ElementManager.getElement(page.editorPanel.toolbar.fileMenu.Id() + "_Item_1_Status").click();
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(page.settingsPanel.Visible(), true);
                        $done();
                    }, 300);
                });
                this.emulateEvent(page.editorPanel.toolbar.fileMenu, EventType.ON_CLICK);
            };
        }

        public __IgnoretestExportDialog() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                page.editorPanel.toolbar.fileMenu.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                    ElementManager.getElement(page.editorPanel.toolbar.fileMenu.Id() + "_Item_2_Status").click();
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(page.exportDialog.Visible(), true);
                        $done();
                    }, 300);
                });
                this.emulateEvent(page.editorPanel.toolbar.fileMenu, EventType.ON_CLICK);
            };
        }

        public __IgnoretestNewProjectDialog() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                page.editorPanel.toolbar.fileMenu.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                    ElementManager.getElement(page.editorPanel.toolbar.fileMenu.Id() + "_Item_0_Status").click();
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.assertEquals(page.projectCreationDialog.Visible(), true);
                        $done();
                    }, 300);
                });
                this.emulateEvent(page.editorPanel.toolbar.fileMenu, EventType.ON_CLICK);
            };
        }

        public testHierarchyClick() : void {
            const page : EditorPage = this.getInstance();
            const detail : DetailsPanel = page.editorPanel.detailsPanel;
            const visualGraph : KernelNode = new KernelNode(KernelNodeType.VISUAL_GRAPH, 11, 11);
            const vxContext : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, 9, 9);
            const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
            const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
            vxGraph.ChildNodes().Add(file);
            file.ParentNode(vxGraph);
            visualGraph.ChildNodes().Add(vxContext);
            vxContext.ParentNode(visualGraph);
            vxContext.ChildNodes().Add(vxGraph);
            vxGraph.ParentNode(vxContext);
            detail.setProjectStructure(vxGraph);
            const hierarchy : DirectoryBrowser = detail.accordion.getPanel(DirectoryBrowser);
            this.assertEquals((<any>hierarchy).visibleItems.getItem(0).Text(), "VisualGraph");
            this.assertEquals((<any>hierarchy).visibleItems.getItem(1).Text(), "VxContext0");
            this.assertEquals((<any>hierarchy).visibleItems.getItem(2).Text(), "VxGraph0");
        }

        public AddNodes($nodes : IKernelNode[], $callback : any) : void {
            const page : EditorPage = this.getInstance();
            const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;
            const nodeUtils : KernelNodeUtils = visualGraphPanel.nodeUtils;
            visualGraphPanel.gridManager.DragNodes(nodeUtils.Copy(ArrayList.ToArrayList($nodes), true, false, true), $nodes[0],
                DragNodeType.PASTE, () : void => {
                    $callback();
                });
        }

        public testGridResize() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;

                const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
                file.Type(KernelNodeType.IMAGE_INPUT);
                file.Name("TestFile");
                const filter : KernelNode = new KernelNode(KernelNodeType.BOX_FILTER, 6, 6);
                filter.Type(KernelNodeType.BOX_FILTER);
                filter.Name("TestFilter");

                this.AddNodes([file, filter], () : void => {
                    // visualGraphPanel.gridManager.IndicateGridResize(new ElementOffset(100, 100), new ElementOffset(-1, -1));
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Column(), 2);
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Row(), 2);
                    // visualGraphPanel.gridManager.ApplyGridResize();
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Column(), 0);
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Row(), 0);
                    //
                    // visualGraphPanel.gridManager.IndicateGridResize(new ElementOffset(-100, -100), new ElementOffset(1, 1));
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Column(), 0);
                    // this.assertEquals(visualGraphPanel.nodeRegistry.getNodeByName(file.Name()).Row(), 0);
                    // visualGraphPanel.gridManager.ApplyGridResize();
                    // this.assertEquals(visualGraphPanel.gridManager.GridDims().Width(), 5);
                    // this.assertEquals(visualGraphPanel.gridManager.GridDims().Height(), 5);
                    $done();
                });
            };
        }

        public testGrid() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const nodes : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                // nodes.gridManager.IndicateGridResize(new ElementOffset(-100, -100), new ElementOffset(1, 1));
                // nodes.gridManager.ApplyGridResize();
                // this.assertEquals(nodes.gridManager.GridDims().Width(), 16);
                // this.assertEquals(nodes.gridManager.GridDims().Height(), 16);
                // nodes.gridManager.IndicateGridResize(new ElementOffset(100, 100), new ElementOffset(0, 1));
                // nodes.gridManager.ApplyGridResize();
                // this.assertEquals(nodes.gridManager.GridDims().Width(), 116);
                // this.assertEquals(nodes.gridManager.GridDims().Height(), 16);
                // nodes.gridManager.IndicateGridResize(new ElementOffset(-100, -100), new ElementOffset(-1, 0));
                // nodes.gridManager.ApplyGridResize();
                // this.assertEquals(nodes.gridManager.GridDims().Width(), 116);
                // this.assertEquals(nodes.gridManager.GridDims().Height(), 116);
                // nodes.gridManager.IndicateGridResize(new ElementOffset(100, 100), new ElementOffset(-1, -1));
                // nodes.gridManager.ApplyGridResize();
                // this.assertEquals(nodes.gridManager.GridDims().Width(), 16);
                // this.assertEquals(nodes.gridManager.GridDims().Height(), 16);
                $done();
            };
        }

        public testGridDeleteNode() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const nodes : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
                file.Type(KernelNodeType.IMAGE_INPUT);
                file.Name("TestFile");
                const filter : KernelNode = new KernelNode(KernelNodeType.BOX_FILTER, 6, 6);
                filter.Type(KernelNodeType.BOX_FILTER);
                filter.Name("TestFilter");

                this.AddNodes([file, filter], () : void => {
                    nodes.gridManager.RemoveNode(nodes.RootNode().ChildNodes().getLast());
                    this.assertEquals(nodes.KernelNodes().Length(), 1);
                    $done();
                });
            };
        }

        public testHistoryManager() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const nodes : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                $done();
            };
        }

        // GridManager, Copy, historyManager
        public testgetNodes() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const historyMan : HistoryManager = page.editorPanel.historyManager;
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;

                const file : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
                file.Type(KernelNodeType.IMAGE_INPUT);
                file.Name("TestNode");

                this.AddNodes([file], () : void => {
                    const nodeCopy : IKernelNode = visualGraphPanel.nodeRegistry.getNodeByName(file.Name());
                    (<any>nodeCopy).loaded = true; // hack -> connectors arent loaded, therefore node isnt also
                    visualGraphPanel.gridManager.DragNodes(nodeCopy, nodeCopy, DragNodeType.RETAIN_ON_CANCEL_RELATIVE, () : void => {
                        // const mousePos : IVector = visualGraphPanel.gridManager.getGridState().mousePos;
                        // const nodePosPx : IVector = file.getGridPosition();
                        // mousePos.x = nodePosPx.x + (KernelNode.envelopSize * 2);
                        // mousePos.y = nodePosPx.y + (KernelNode.envelopSize * 2);

                        const args : StateEventArgs = StateEventArgs
                            .getBeforeNodeUpdateArgs(visualGraphPanel.gridManager, visualGraphPanel.connectionManager);

                        historyMan.setOnStateChange(() : void => {
                            if (historyMan.IsComplete()) {
                                this.assertEquals(nodeCopy.Column(), 2);
                                this.assertEquals(nodeCopy.Row(), 2);
                                $done();
                            } else if (historyMan.IsForceable()) {
                                // not testing now
                            }
                        });

                        visualGraphPanel.gridManager.ApplyDrag(() : void => {
                            historyMan.Push(args);
                            this.assertEquals(historyMan.IsUndoable(), true);
                            this.assertEquals(historyMan.IsRedoable(), false);
                            historyMan.Undo();
                        });
                    });
                });
            };
        }

        public testConnectionValidate() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                const connectionManager : ConnectionManager = visualGraphPanel.connectionManager;

                const inputFile : KernelNode = new KernelNode(KernelNodeType.IMAGE_INPUT, 2, 2);
                inputFile.Type(KernelNodeType.IMAGE_INPUT);
                const filter : KernelNode = new KernelNode(KernelNodeType.CHANNEL_EXTRACT, 4, 5);
                filter.Type(KernelNodeType.CHANNEL_EXTRACT);
                const outputFile : KernelNode = new KernelNode(KernelNodeType.IMAGE_OUTPUT, 5, 6);
                outputFile.Type(KernelNodeType.IMAGE_OUTPUT);

                this.AddNodes([inputFile, filter, outputFile], () : void => {
                    // const nodeCopy : IKernelNode = visualGraphPanel.nodeRegistry.getNodeByName(inputFile.Name());
                    // (<any>nodeCopy).loaded = true; // hack -> connectors arent loaded, therefore node isnt also

                    inputFile.AddOutput("test_type");
                    filter.AddOutput("test_type");
                    filter.AddInput("test_type");
                    outputFile.AddInput("test_type");

                    connectionManager.DragConnection(inputFile, 0);
                    connectionManager.ApplyDrag(filter.getInput(0));
                    connectionManager.DragConnection(filter, 0);
                    connectionManager.ApplyDrag(outputFile.getInput(0));

                    this.assertEquals(inputFile.getOutput(0).inputs.getItem(0).node, filter);
                    this.assertEquals(filter.getOutput(0).inputs.getItem(0).node, outputFile);
                    connectionManager.RemoveConnection(outputFile.getInput(0).connection.id);
                    this.assertEquals(outputFile.IsConnectorsVisible(), false);
                    $done();
                });
            };
        }

        public testCopy() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;
                const historyMan : HistoryManager = page.editorPanel.historyManager;
                const connectionManager : ConnectionManager = visualGraphPanel.connectionManager;

                const args : StateEventArgs = StateEventArgs
                    .getBeforeNodeUpdateArgs(visualGraphPanel.gridManager, visualGraphPanel.connectionManager);
                visualGraphPanel.RootNode(visualGraphPanel.RootNode().ParentNode(), () : void => {

                    const graph : IKernelNode = visualGraphPanel.KernelNodes().getFirst();
                    const copiedNodes : ArrayList<IKernelNode> = visualGraphPanel.nodeUtils.Copy(graph);
                    copiedNodes.getFirst().Column(10);
                    copiedNodes.getFirst().Row(10);

                    visualGraphPanel.DragNodes(copiedNodes, copiedNodes.getFirst(), DragNodeType.PASTE, () : void => {
                        this.assertEquals(visualGraphPanel.KernelNodes().Length(), 2);
                        $done();
                    });
                });
            };
        }

        // Canvas bar
        public testResetPerspective() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        public testContextPanelClick() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        public testVisualGraphClick() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        public testHelpLink() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        public testSelectMultipleNodes() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        public testContextMenuOfNode() : IViewerTestPromise {
            return ($done : () => void) : void => {
                $done();
            };
        }

        protected setUp() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const page : EditorPage = this.getInstance();
                const visualGraphPanel : VisualGraphPanel = page.editorPanel.visualGraphPanel;

                const visualGraph : KernelNode = new KernelNode(KernelNodeType.VISUAL_GRAPH, -1, -1);
                visualGraph.Type(KernelNodeType.VISUAL_GRAPH);
                visualGraph.UniqueIdPrefix(KernelNodeType.VISUAL_GRAPH);
                visualGraph.NamePrefix(KernelNodeType.VISUAL_GRAPH);

                const vxContext : KernelNode = new KernelNode(KernelNodeType.VXCONTEXT, 3, 3);
                vxContext.Type(KernelNodeType.VXCONTEXT);
                vxContext.UniqueIdPrefix(KernelNodeType.VXCONTEXT);
                vxContext.NamePrefix(KernelNodeType.VXCONTEXT);

                const vxGraph : KernelNode = new KernelNode(KernelNodeType.VXGRAPH, 3, 3);
                vxGraph.Type(KernelNodeType.VXGRAPH);
                vxGraph.UniqueIdPrefix(KernelNodeType.VXGRAPH);
                vxGraph.NamePrefix(KernelNodeType.VXGRAPH);

                visualGraph.ChildNodes().Add(vxContext);
                vxContext.ParentNode(visualGraph);
                vxContext.ChildNodes().Add(vxGraph);
                vxGraph.ParentNode(vxContext);

                visualGraphPanel.GraphRoot(visualGraph);
                visualGraphPanel.RootNode(vxGraph, () : void => {
                    $done();
                });
            };

            const page : EditorPage = this.getInstance();
            Dialog.Close((<any>page).settingsPanel);
            //  Dialog.Close(page.exportDialog);
            // page.examplesDialog.Visible(false);
            // page.settingsPanel.Visible(false);
            // page.exportDialog.Visible(false);
            //   page.projectCreationDialog.Visible(false);
        }

        protected before() : string {
            const instance : EditorPage = this.getInstance();
            instance.StyleClassName("TestCss");

            const sizeHandler : any = () : void => {
                ElementManager.setCssProperty("Browser", "height", WindowManager.getSize().Height() - 60);
                ElementManager.setCssProperty("Browser", "width", WindowManager.getSize().Width() - 60);
                const eventArgs : ResizeEventArgs = new ResizeEventArgs();
                eventArgs.Owner("Browser");
                this.getEventsManager().FireEvent("Browser", EventType.ON_RESIZE_COMPLETE, eventArgs);
            };

            WindowManager.getEvents().setOnResize(() => {
                instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            const testWizardPanels : IGuiCommons[] = [
                instance.editorPanel.detailsPanel.accordion.getPanel(PropertiesPanel),
                instance.editorPanel.visualGraphPanel,
                instance.editorPanel.kernelNodePickerPanel
            ];

            const testWizardPanelsBoundingRectangles : IBoundingRectangle[] = [
                null,
                <IBoundingRectangle>{
                    offsetBottom: new PropagableNumber({number: 50, unitType: UnitType.PX}),
                    offsetLeft  : new PropagableNumber({number: 62, unitType: UnitType.PCT}),
                    offsetRight : new PropagableNumber({number: 25, unitType: UnitType.PX}),
                    offsetTop   : new PropagableNumber({number: 50, unitType: UnitType.PCT})
                },
                null
            ];

            const testWizardTexts : string[] = [
                "Some random text some random text some random text some random text some random text some random " +
                "text some random text some random text some random text some random text some random text ",
                "Another text that is random"
            ];

            let panelIndex = 0;
            let textIndex = 0;

            WindowManager.getEvents().setOnKeyPress(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                if ($eventArgs.getKeyCode() === KeyMap.X) {
                    instance.ShowWizard(null,
                        <IBoundingRectangle>{
                            offsetBottom: new PropagableNumber({number: 68, unitType: UnitType.PX}),
                            offsetLeft  : new PropagableNumber({number: 15, unitType: UnitType.PCT}),
                            offsetRight : new PropagableNumber({number: 15, unitType: UnitType.PCT}),
                            offsetTop   : new PropagableNumber({number: 10, unitType: UnitType.PCT})
                        });
                } else if ($eventArgs.getKeyCode() === KeyMap.C) {
                    instance.ToggleConsole();
                } else if ($eventArgs.getKeyCode() === KeyMap.A) {
                    if (Math.random() > 0.5) {
                        Echo.Println(testWizardTexts[0]);
                    } else {
                        Echo.Println(testWizardTexts[1]);
                    }
                } else if ($eventArgs.getKeyCode() === KeyMap.V) {
                    instance.notificationPanel.AddSuccess("SUCCESS !!");
                }
            });

            const applyWizardArgs : any = () : void => {
                const args : EditorPageViewerArgs = (<any>this).owner.ViewerArgs();
                const appWizardArgs : AppWizardPanelViewerArgs = args.AppWizardPanelViewerArgs();
                appWizardArgs.HeaderText(testWizardPanels[panelIndex].Id());
                appWizardArgs.DescriptionText(testWizardTexts[textIndex]);
                (<any>this).owner.argsHandler(instance, args);
            };

            instance.getEvents().setOnWizardForward(() : void => {
                textIndex++;
                if (textIndex > testWizardTexts.length - 1) {
                    textIndex = 0;
                    panelIndex++;
                    if (panelIndex > testWizardPanels.length - 1) {
                        panelIndex = 0;
                    }
                    instance.ShowWizard(testWizardPanels[panelIndex], testWizardPanelsBoundingRectangles[panelIndex]);
                }
                applyWizardArgs();
            });

            instance.getEvents().setOnWizardBack(() : void => {
                textIndex--;
                if (textIndex < 0) {
                    textIndex = 0;
                    panelIndex--;
                    if (panelIndex < 0) {
                        panelIndex = 0;
                    }
                    instance.ShowWizard(testWizardPanels[panelIndex], testWizardPanelsBoundingRectangles[panelIndex]);
                }
                applyWizardArgs();
            });

            instance.getEvents().setOnWizardClose(() : void => {
                panelIndex = 0;
                textIndex = 0;
            });

            instance.getEvents().FireAsynchronousMethod(sizeHandler);

            instance.editorPanel.toolbar.consoleButton.getEvents().setOnClick(() : void => {
                instance.ShowConsole();
            });

            instance.editorPanel.toolbar.AddFileMenuItem("New", () : void => {
                instance.ShowCreateProjectDialog(instance.Value().ProjectCreationDialogArgs().SettingsArgs().Value());
            });
            instance.editorPanel.toolbar.AddFileMenuItem("Settings", () : void => {
                instance.ShowSettings(true);
            });
            instance.editorPanel.toolbar.AddFileMenuItem("Export", () : void => {
                instance.ShowExportDialog();
            }, true);
            instance.editorPanel.toolbar.AddFileMenuItem("Examples", () : void => {
                instance.ShowExamplesDialog();
            });

            instance.editorPanel.visualGraphPanel.getEvents().setOnComplete(() : void => {
                instance.editorPanel.visualGraphPanel.RootNode(instance.editorPanel.visualGraphPanel.SearchRootNode());
                sizeHandler();
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .EditorPanel {border: 1px solid #E7E7E8;}</style>";
        }

        protected after() : void {
            this.setUp();
        }
    }
}
/* dev:end */
