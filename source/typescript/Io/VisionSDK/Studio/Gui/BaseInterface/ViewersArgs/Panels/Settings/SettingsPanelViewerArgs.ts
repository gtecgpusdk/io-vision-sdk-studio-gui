/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ToolbarPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ToolbarPanelViewerArgs;

    export class SettingsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private toolbarArgs : ToolbarPanelViewerArgs;
        private backText : string;
        private projectText : string;
        private connectionsText : string;
        private toolchainText : string;
        private aboutText : string;
        private saveText : string;
        private cancelText : string;
        private projectSettingsPanelArgs : ProjectSettingsPanelViewerArgs;
        private connectionSettingsPanelArgs : ConnectionSettingsPanelViewerArgs;
        private toolchainSettingsPanelArgs : ToolchainSettingsPanelViewerArgs;
        private aboutPanelArgs : AboutSettingsPanelViewerArgs;
        private menuHeaderText : string;
        private descriptionHeaderText : string;

        constructor() {
            super();
            this.toolbarArgs = new ToolbarPanelViewerArgs();
            this.menuHeaderText = "";

            this.projectText = "";
            this.connectionsText = "";
            this.toolchainText = "";
            this.aboutText = "";

            this.saveText = "";
            this.cancelText = "";

            this.projectSettingsPanelArgs = new ProjectSettingsPanelViewerArgs();
            this.connectionSettingsPanelArgs = new ConnectionSettingsPanelViewerArgs();
            this.toolchainSettingsPanelArgs = new ToolchainSettingsPanelViewerArgs();
            this.aboutPanelArgs = new AboutSettingsPanelViewerArgs();
        }

        public ToolbarArgs($value? : ToolbarPanelViewerArgs) : ToolbarPanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsTypeOf(ToolbarPanelViewerArgs)) {
                this.toolbarArgs = $value;
            }
            return this.toolbarArgs;
        }

        public BackText($value? : string) : string {
            return this.backText = Property.String(this.backText, $value);
        }

        public MenuHeaderText($value? : string) : string {
            return this.menuHeaderText = Property.String(this.menuHeaderText, $value);
        }

        public DescriptionHeaderText($value? : string) : string {
            return this.descriptionHeaderText = Property.String(this.descriptionHeaderText, $value);
        }

        public ProjectText($value? : string) : string {
            return this.projectText = Property.String(this.projectText, $value);
        }

        public ConnectionText($value? : string) : string {
            return this.connectionsText = Property.String(this.connectionsText, $value);
        }

        public ToolchainText($value? : string) : string {
            return this.toolchainText = Property.String(this.toolchainText, $value);
        }

        public AboutText($value? : string) : string {
            return this.aboutText = Property.String(this.aboutText, $value);
        }

        public SaveText($value? : string) : string {
            return this.saveText = Property.String(this.saveText, $value);
        }

        public CancelText($value? : string) : string {
            return this.cancelText = Property.String(this.cancelText, $value);
        }

        public ProjectSettingsPanelArgs($value? : ProjectSettingsPanelViewerArgs) : ProjectSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ProjectSettingsPanelViewerArgs)) {
                this.projectSettingsPanelArgs = $value;
            }
            return this.projectSettingsPanelArgs;
        }

        public ConnectionSettingsPanelArgs($value? : ConnectionSettingsPanelViewerArgs) : ConnectionSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ConnectionSettingsPanelViewerArgs)) {
                this.connectionSettingsPanelArgs = $value;
            }
            return this.connectionSettingsPanelArgs;
        }

        public ToolchainSettingsPanelArgs($value? : ToolchainSettingsPanelViewerArgs) : ToolchainSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(ToolchainSettingsPanelViewerArgs)) {
                this.toolchainSettingsPanelArgs = $value;
            }
            return this.toolchainSettingsPanelArgs;
        }

        public AboutPanelArgs($value? : AboutSettingsPanelViewerArgs) : AboutSettingsPanelViewerArgs {
            if (ObjectValidator.IsSet($value) && $value.IsTypeOf(AboutSettingsPanelViewerArgs)) {
                this.aboutPanelArgs = $value;
            }
            return this.aboutPanelArgs;
        }
    }
}
