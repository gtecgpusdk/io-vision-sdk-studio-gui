/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class ControlPanelEventType extends EventType {
        public static readonly ON_ZOOM_CHANGE : string = "OnZoomChange";
        public static readonly ON_RESET : string = "OnReset";
    }
}
