/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import InteractiveMessage = Io.VisionSDK.UserControls.Utils.InteractiveMessage;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export class ConsolePanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        private lastStamp : number;
        private stampStep : number;
        private isActive : boolean;

        constructor($id? : string) {
            super($id);
            this.stampStep = 3;
            this.lastStamp = 0;
        }

        public setStampStep($value : number) {
            this.stampStep = $value;
        }

        public Clear() : void {
            ElementManager.getElement(this.Id() + "_ConsoleText").innerHTML = "";
            this.lastStamp = 0;
        }

        public IsActive() : boolean {
            return this.isActive;
        }

        public PrintMessage($message : string | InteractiveMessage, $forceTimeStamp? : boolean) : void {
            ElementManager.getElement(this.Id() + "_ConsoleText").appendChild(this.getMessageElement($message,
                $forceTimeStamp));
            BasePanel.scrollBarVisibilityHandler(this);
            BasePanel.scrollTop(this, 100);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            let handle : number;
            this.getEvents().setOnMouseOver(() : void => {
                clearTimeout(handle);
                this.isActive = true;
            });

            this.getEvents().setOnMouseOut(() : void => {
                handle = this.getEvents().FireAsynchronousMethod(() : void => {
                    this.isActive = false;
                }, true, 10);
            });
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement().Id(this.Id() + "_ConsoleText").StyleClassName("ConsoleText");
        }

        private getMessageElement($message : string | InteractiveMessage, $forceStamp : boolean = false, $date? : Date) : HTMLElement {
            const date : Date = ObjectValidator.IsSet($date) ? $date : new Date();
            const showStamp : boolean = $forceStamp || ((date.getTime() - this.lastStamp) / 1000) > this.stampStep;
            if (showStamp) {
                this.lastStamp = date.getTime();
            }
            const envelopHtml : HTMLElement = document.createElement("span");
            envelopHtml.className = "ConsoleMessageEnvelop";

            if (showStamp) {
                const timeStampHtml : HTMLElement = document.createElement("span");
                timeStampHtml.className = "TimeStamp";
                timeStampHtml.innerText = date.toLocaleTimeString();
                envelopHtml.appendChild(timeStampHtml);
            }
            const messageHtml : HTMLElement = document.createElement("span");
            messageHtml.className = "ConsoleMessage";
            envelopHtml.appendChild(messageHtml);

            const message : InteractiveMessage = <InteractiveMessage>$message;
            if (Reflection.getInstance().IsInstanceOf(message, InteractiveMessage)) {
                messageHtml.appendChild(InteractiveMessage.ToHtmlElement(message));
            } else {
                messageHtml.innerText = <string>$message;
            }
            return envelopHtml;
        }
    }
}
