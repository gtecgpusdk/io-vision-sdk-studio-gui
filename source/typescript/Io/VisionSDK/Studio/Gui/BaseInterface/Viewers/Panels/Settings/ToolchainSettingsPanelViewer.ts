/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings {
    "use strict";
    import ToolchainSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ToolchainSettingsPanelViewerArgs;
    import ToolchainSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ToolchainSettingsPanel;

    export class ToolchainSettingsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ToolchainSettingsPanelViewerArgs {
            const args : ToolchainSettingsPanelViewerArgs = new ToolchainSettingsPanelViewerArgs();
            args.HeaderText("Toolchain Settings");
            args.NameText("Toolchain: ");
            args.ToolchainHint("Select toolchain option...");
            args.CmakeText("CMAKE path: ");
            args.CppText("C++ compiler path: ");
            args.LinkerText("Linker info: ");
            args.ValidateButtontext("Validate");
            args.PanelDescription("Toolchain settings test description");

            /* tslint:disable: object-literal-sort-keys */
            args.Value({
                options: [
                    {
                        name    : "MinGW",
                        platform: "WinGCC",
                        compiler: "compiler 1",
                        cmake   : "cmake 1",
                        linker  : "linker 1"
                    }, {
                        name    : "MSVC",
                        platform: "MSVC",
                        compiler: "compiler 2",
                        cmake   : "cmake 2",
                        linker  : "linker 2"
                    }
                ]
            });
            /* tslint:enable */

            return args;
        }

        /* dev:end */

        constructor($args? : ToolchainSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new ToolchainSettingsPanel());
        }

        public getInstance() : ToolchainSettingsPanel {
            return <ToolchainSettingsPanel>super.getInstance();
        }

        public ViewerArgs($args? : ToolchainSettingsPanelViewerArgs) : ToolchainSettingsPanelViewerArgs {
            return <ToolchainSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ToolchainSettingsPanel, $args : ToolchainSettingsPanelViewerArgs) : void {
            $instance.headerText = $args.HeaderText();
            $instance.nameLabel.Text($args.NameText());
            $instance.cmakeLabel.Text($args.CmakeText());
            $instance.cppLabel.Text($args.CppText());
            $instance.linkerLabel.Text($args.LinkerText());
            $instance.panelDescription = $args.PanelDescription();
            $instance.ApplySettings($args.Value());
            $instance.validateButton.Text($args.ValidateButtontext());
        }

        /* dev:start */
        protected testImplementation($instance : ToolchainSettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(550);
            $instance.Height(550);
            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ToolchainnSettingsPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
