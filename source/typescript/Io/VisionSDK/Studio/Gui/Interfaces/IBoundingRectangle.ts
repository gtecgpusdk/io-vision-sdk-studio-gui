/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces {
    "use strict";
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    export class IBoundingRectangle {
        public offsetTop? : PropagableNumber;
        public offsetBottom? : PropagableNumber;
        public offsetLeft? : PropagableNumber;
        public offsetRight? : PropagableNumber;
        public width? : PropagableNumber;
        public height? : PropagableNumber;
    }
}
