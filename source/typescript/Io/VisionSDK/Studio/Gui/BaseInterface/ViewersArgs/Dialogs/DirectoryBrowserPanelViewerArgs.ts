/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class DirectoryBrowserPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private saveButtonText : string;
        private closeButtonText : string;
        private pathValue : string;

        constructor() {
            super();
            this.saveButtonText = "";
            this.closeButtonText = "";
            this.pathValue = "";
        }

        public SaveButtonText($value? : string) : string {
            return this.saveButtonText = Property.String(this.saveButtonText, $value);
        }

        public CloseButtonText($value? : string) : string {
            return this.closeButtonText = Property.String(this.closeButtonText, $value);
        }

        public PathValue($value? : string) : string {
            return this.pathValue = Property.String(this.pathValue, $value);
        }
    }
}
