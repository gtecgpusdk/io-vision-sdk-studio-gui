/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import ProjectPickerPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectPickerPanelViewerArgs;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import InformativeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.InformativeButton;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import IProjectEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IProjectEvents;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import ProjectEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.ProjectEventArgs;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import IExampleSettings = Io.VisionSDK.Studio.Gui.Interfaces.IExampleSettings;

    export class ProjectPickerPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public header : Label;
        public descriptionPanel : ProjectDescriptionPanel;
        public selectionPanel : BasePanel;
        public loadButton : Button;
        private currentProject : IProjectSettingsValue;

        constructor($id? : string) {
            super($id);
            this.header = new Label();
            this.selectionPanel = new BasePanel();
            this.descriptionPanel = new ProjectDescriptionPanel();
            this.loadButton = new Button(ButtonType.GREEN_MEDIUM);
            this.currentProject = null;
        }

        public Value($value? : ProjectPickerPanelViewerArgs) : ProjectPickerPanelViewerArgs {
            return <ProjectPickerPanelViewerArgs>super.Value($value);
        }

        public AddProject($descriptor : IExampleSettings) : void {
            const selectorButton : InformativeButton = new InformativeButton();
            selectorButton.Text($descriptor.name);
            selectorButton.Description($descriptor.vxVersionPrefix + $descriptor.vxVersion);
            selectorButton.getEvents().setOnClick(() : void => {
                this.selectionPanel.getChildElements().foreach(($element : InformativeButton) : void => {
                    if ($element.IsTypeOf(InformativeButton)) {
                        $element.Selected(false);
                    }
                });
                selectorButton.Selected(true);
                this.loadButton.Visible(true);
                this.descriptionPanel.ApplyDescriptor($descriptor);
                this.currentProject = $descriptor;
            });
            this.selectionPanel.AddChild(selectorButton);
        }

        public getEvents() : IProjectEvents {
            return <IProjectEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.header.StyleClassName("Header");
            this.selectionPanel.Scrollable(true);
            this.selectionPanel.StyleClassName("SelectionPanel");

            this.loadButton.Visible(false);
            this.loadButton.Width(150);
            this.loadButton.getEvents().setOnClick(() : void => {
                const eventArgs : ProjectEventArgs = new ProjectEventArgs();
                eventArgs.Owner(this);
                eventArgs.Descriptor(this.currentProject);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PROJECT_LOAD, eventArgs);
            });

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Add(this.addRow().HeightOfRow(60, UnitType.PX).Alignment(Alignment.CENTER)
                    .Add(this.header)
                )
                .Add(this.addRow().FitToParent(FitToParent.FULL)
                    .Add(this.addColumn().WidthOfColumn(30, UnitType.PCT)
                        .Add(this.selectionPanel)
                    )
                    .Add(this.addColumn().WidthOfColumn(70, UnitType.PCT)
                        .Add(this.addRow()
                            .Add(this.descriptionPanel)
                        )
                        .Add(this.addRow()
                            .FitToParent(FitToParent.FULL)
                            .HeightOfRow(60, UnitType.PX)
                            .WidthOfColumn(150, UnitType.PX, true)
                            .Alignment(Alignment.RIGHT_PROPAGATED)
                            .Add(this.addColumn())
                            .Add(this.addColumn().WidthOfColumn(150, UnitType.PX)
                                .Add(this.loadButton))
                        )
                    )
                );
        }
    }
}
