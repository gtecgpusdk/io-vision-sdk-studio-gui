/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import SettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.SettingsPanelViewerArgs;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ToolchainSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ToolchainSettingsPanelViewer;
    import AboutSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.AboutSettingsPanelViewer;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import ToolbarPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.ToolbarPanel;
    import ToolbarPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.ToolbarPanelViewer;
    import ISettingsPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEvents;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import SettingsPanelEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.SettingsPanelEventArgs;
    import SettingsPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.SettingsPanelEventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ProjectSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ProjectSettingsPanelViewer;
    import ConnectionSettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.ConnectionSettingsPanelViewer;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;

    export class SettingsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public toolbar : ToolbarPanel;
        public menuHeader : Label;
        public descriptionHeader : Label;
        public cancelButton : Button;
        public saveButton : Button;
        public menuButtons : IButtons;
        public projectSettingsPanel : ProjectSettingsPanel;
        public connectionSettingsPanel : ConnectionSettingsPanel;
        public toolchainSettingsPanel : ToolchainSettingsPanel;
        public aboutSettingsPanel : AboutSettingsPanel;
        private currentPanel : BasePanel;
        private descriptionPanel : BaseTextPanel;
        private prevSettings : ISettings;
        private menuPanel : BasePanel;
        private headerLabel : Label;

        constructor($id? : string) {
            super($id);

            this.addChildPanel(ToolbarPanelViewer, null,
                ($parent : SettingsPanel, $child : ToolbarPanel) => {
                    $parent.toolbar = $child;
                });
            this.addChildPanel(ProjectSettingsPanelViewer, null,
                ($parent : SettingsPanel, $child : ProjectSettingsPanel) => {
                    $parent.projectSettingsPanel = $child;
                });
            this.addChildPanel(ConnectionSettingsPanelViewer, null,
                ($parent : SettingsPanel, $child : ConnectionSettingsPanel) => {
                    $parent.connectionSettingsPanel = $child;
                });
            this.addChildPanel(ToolchainSettingsPanelViewer, null,
                ($parent : SettingsPanel, $child : ToolchainSettingsPanel) => {
                    $parent.toolchainSettingsPanel = $child;
                });
            this.addChildPanel(AboutSettingsPanelViewer, null,
                ($parent : SettingsPanel, $child : AboutSettingsPanel) => {
                    $parent.aboutSettingsPanel = $child;
                });
            this.addChildPanel(BaseTextPanelViewer, null,
                ($parent : SettingsPanel, $child : BaseTextPanel) => {
                    $parent.descriptionPanel = $child;
                });

            this.saveButton = new Button(ButtonType.GREEN_MEDIUM);
            this.cancelButton = new Button(ButtonType.GRAY_MEDIUM);

            this.headerLabel = new Label();
            this.menuPanel = new BasePanel();
            this.menuButtons = {
                /* tslint:disable: object-literal-sort-keys */
                projectButton   : new Button(ButtonType.TRANSPARENT),
                connectionButton: new Button(ButtonType.TRANSPARENT),
                toolchainButton : new Button(ButtonType.TRANSPARENT),
                aboutButton     : new Button(ButtonType.TRANSPARENT)
                /* tslint:enable: object-literal-sort-keys */
            };
            this.menuPanel.AddChild(this.menuButtons.projectButton);
            this.menuPanel.AddChild(this.menuButtons.connectionButton);
            this.menuPanel.AddChild(this.menuButtons.toolchainButton);
            this.menuPanel.AddChild(this.menuButtons.aboutButton);

            this.menuHeader = new Label();
            this.descriptionHeader = new Label();
        }

        public Value($value? : SettingsPanelViewerArgs) : SettingsPanelViewerArgs {
            return <SettingsPanelViewerArgs>super.Value($value);
        }

        public setDefaultSettings($value : ISettings) : void {
            this.prevSettings = $value;
            this.HandleButtonVisibility();
        }

        public EnableBuildSettings($value : boolean) : void {
            // todo : buttons dont disable post complete?
            // this.connection.Enabled($value);
            // this.toolchain.Enabled($value);

            this.connectionSettingsPanel.Enabled($value);
            this.toolchainSettingsPanel.Enabled($value);
        }

        public HandleButtonVisibility() : void {
            const replacer : any = (key : any, val : any) : void => {
                if (key !== "selected") {
                    return val;
                }
            };
            if (JSON.stringify(this.prevSettings, replacer) !== JSON.stringify(this.getSettings(), replacer)) {
                this.saveButton.Visible(true);
                this.cancelButton.Visible(true);
                (<any>Button).resize(this.saveButton);
                (<any>Button).resize(this.cancelButton);
            } else {
                this.saveButton.Visible(false);
                this.cancelButton.Visible(false);
            }
        }

        public getSettings() : ISettings {
            /* tslint:disable: object-literal-sort-keys */
            return {
                projectSettings   : this.projectSettingsPanel.getSettings(),
                connectionSettings: this.connectionSettingsPanel.getSettings(),
                toolchainSettings : this.toolchainSettingsPanel.getSettings()
            };
            /* tslint:enable */
        }

        public ApplySettings($value? : ISettings) : void {
            const apply : any = ($settings : ISettings) : void => {
                this.projectSettingsPanel.ApplySettings($settings.projectSettings);
                this.connectionSettingsPanel.ApplySettings($settings.connectionSettings);
                this.toolchainSettingsPanel.ApplySettings($settings.toolchainSettings);
                this.prevSettings = this.getSettings();
            };
            if (ObjectValidator.IsSet($value)) {
                apply($value);
            } else {
                apply(this.getSettings());
            }
            this.HandleButtonVisibility();
        }

        public getEvents() : ISettingsPanelEvents {
            return <ISettingsPanelEvents>super.getEvents();
        }

        public ShowPanel($value : ProjectSettingsPanel | ToolchainSettingsPanel | ConnectionSettingsPanel | AboutSettingsPanel) : void {
            if ($value !== this.currentPanel) {
                this.ApplySettings(this.prevSettings);
                [this.projectSettingsPanel, this.connectionSettingsPanel, this.toolchainSettingsPanel, this.aboutSettingsPanel]
                    .forEach(this.HidePanel);
                $value.Visible(true);
                this.headerLabel.Text($value.headerText);
                this.descriptionPanel.Text($value.panelDescription);
                if (!ObjectValidator.IsEmptyOrNull(this.currentPanel)) {
                    $value.Width(this.currentPanel.Width());
                    $value.Height(this.currentPanel.Height());
                }
                this.currentPanel = $value;
            }
        }

        public HidePanel($panel : BasePanel) : void {
            if ($panel.Visible()) {
                $panel.Visible(false);
            }
        }

        protected innerCode() : IGuiElement {
            const getFieldSize : any = ($panel : BasePanel) : string => {
                if ($panel.Width() > 1000) {
                    return "550px";
                } else if ($panel.Width() > 700) {
                    return "400px";
                } else {
                    return undefined;
                }
            };

            [this.projectSettingsPanel, this.connectionSettingsPanel, this.toolchainSettingsPanel]
                .forEach(($element : any) : void => {
                    $element.setFieldSizeHandler(() : string => {
                        return getFieldSize($element);
                    });
                });

            [
                this.toolbar.fileMenu, this.toolbar.saveButton, this.toolbar.redoButton, this.toolbar.openProjectFolderButton,
                this.toolbar.consoleButton, this.toolbar.platformList, this.toolbar.runButton, this.toolbar.helpButton,
                this.projectSettingsPanel, this.connectionSettingsPanel, this.toolchainSettingsPanel, this.aboutSettingsPanel
            ].forEach(($element : IGuiCommons) : void => {
                $element.Visible(false);
            });

            [this.projectSettingsPanel, this.toolchainSettingsPanel]
                .forEach(($panel : any) : void => {
                    $panel.getEvents().setOnPathRequest(($eventArgs : EventArgs) : void => {
                        this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, $eventArgs);
                    });
                });

            [this.projectSettingsPanel, this.toolchainSettingsPanel, this.connectionSettingsPanel]
                .forEach(($panel : any) : void => {
                    $panel.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                        if ($eventArgs.Owner() !== this.connectionSettingsPanel ||
                            $eventArgs.Owner() === this.connectionSettingsPanel && this.connectionSettingsPanel.Enabled()) {
                            this.HandleButtonVisibility();
                        }
                    });
                });

            this.saveButton.getEvents().setOnClick(() : void => {
                this.ApplySettings();
                const args : SettingsPanelEventArgs = new SettingsPanelEventArgs();
                args.Owner(this);
                args.Settings(this.getSettings());
                this.getEventsManager().FireEvent(this, SettingsPanelEventType.ON_SETTINGS_UPDATE, args);
            });

            this.cancelButton.getEvents().setOnClick(() : void => {
                this.ApplySettings(this.prevSettings);
            });

            this.menuButtons.projectButton.getEvents().setOnClick(() : void => {
                this.ShowPanel(this.projectSettingsPanel);
            });

            this.menuButtons.toolchainButton.getEvents().setOnClick(() : void => {
                this.ShowPanel(this.toolchainSettingsPanel);
            });

            this.menuButtons.connectionButton.getEvents().setOnClick(() : void => {
                this.ShowPanel(this.connectionSettingsPanel);
            });

            this.menuButtons.aboutButton.getEvents().setOnClick(() : void => {
                this.ShowPanel(this.aboutSettingsPanel);
            });

            this.getEvents().setOnComplete(() : void => {
                [this.projectSettingsPanel, this.toolchainSettingsPanel, this.connectionSettingsPanel, this.aboutSettingsPanel]
                    .forEach(($panel : any) : void => {
                        $panel.Visible(true);
                    });
                this.saveButton.Visible(false);
                this.cancelButton.Visible(false);
            });

            this.toolchainSettingsPanel.validateButton.getEvents().setOnClick(() : void => {
                this.getEventsManager().FireEvent(this, SettingsPanelEventType.ON_VALIDATION_REQUEST);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow().HeightOfRow(40, UnitType.PX)
                    .Add(this.toolbar))
                .Add(this.addRow()
                    .Add(this.addColumn().WidthOfColumn(20, UnitType.PCT).StyleClassName("MenuColumn")
                        .Add(this.addRow().HeightOfRow(34, UnitType.PX)
                            .Add(this.menuHeader)
                        )
                        .Add(this.menuPanel)
                    )
                    .Add(this.addColumn()
                        .Add(this.addRow().FitToParent(FitToParent.NONE).Alignment(Alignment.LEFT_PROPAGATED)
                            .HeightOfRow(34, UnitType.PX)
                            .Add(this.addColumn().WidthOfColumn(100, UnitType.PCT, true)
                                .StyleClassName("Header")
                                .Add(this.headerLabel)
                            )
                        )
                        .Add(this.addRow()
                            .Add(this.addColumn().HeightOfRow("100%", true)
                                .StyleClassName("MultiPanelColumn")
                                .Add(this.projectSettingsPanel)
                                .Add(this.connectionSettingsPanel)
                                .Add(this.toolchainSettingsPanel)
                                .Add(this.aboutSettingsPanel)
                            )
                        )
                        .Add(this.addRow()
                            .HeightOfRow(40, UnitType.PX)
                            .StyleClassName("ButtonRow")
                            .Add(this.saveButton)
                            .Add(this.cancelButton)
                        )
                    )
                    .Add(this.addColumn().WidthOfColumn(20, UnitType.PCT).StyleClassName("DescriptionColumn")
                        .Add(this.addRow().HeightOfRow(34, UnitType.PX)
                            .Add(this.descriptionHeader)
                        )
                        .Add(this.descriptionPanel)
                    )
                )
                .Add(this.addRow().HeightOfRow(34, UnitType.PX).StyleClassName("StatusEnvelop"));
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }

    export abstract class IButtons {
        public projectButton : Button;
        public connectionButton : Button;
        public toolchainButton : Button;
        public aboutButton : Button;
    }
}
