/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class ExportPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private exportButtonText : string;
        private closeButtonText : string;
        private platformText : string;
        private readonly platforms : ArrayList<string>;
        private exportPathText : string;
        private exportPathValue : string;

        constructor() {
            super();
            this.headerText = "";
            this.exportButtonText = "";
            this.closeButtonText = "";
            this.platformText = "";
            this.platforms = new ArrayList<string>();
            this.exportPathText = "";
            this.exportPathValue = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public ExportButtonText($value? : string) : string {
            return this.exportButtonText = Property.String(this.exportButtonText, $value);
        }

        public CloseButtonText($value? : string) : string {
            return this.closeButtonText = Property.String(this.closeButtonText, $value);
        }

        public PlatformText($value? : string) : string {
            return this.platformText = Property.String(this.platformText, $value);
        }

        public AddPlatform($text : string, $value : string) : void {
            if (!this.platforms.KeyExists($value)) {
                this.platforms.Add($text, $value);
            }
        }

        public getPlatforms() : ArrayList<string> {
            return this.platforms;
        }

        public ExportPathText($value? : string) : string {
            return this.exportPathText = Property.String(this.exportPathText, $value);
        }

        public ExportPathValue($value? : string) : string {
            return this.exportPathValue = Property.String(this.exportPathValue, $value);
        }
    }
}
