/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces {
    "use strict";

    export abstract class ISettings {
        public selectedPlatform? : string;
        public projectSettings : IProjectSettingsValue;
        public connectionSettings : IConnectionSettings;
        public toolchainSettings : IToolchainSettings;
    }

    export abstract class IConnectionSettings {
        public options : IConnectionSettingsValue[];
    }

    export abstract class IToolchainSettings {
        public options : IToolchainSettingsValue[];
    }

    export abstract class IToolchainSettingsValue {
        public name : string;
        public enabled? : boolean;
        public platform : string;
        public compiler? : string;
        public linker? : string;
        public cmake? : string;
    }

    export abstract class IConnectionSettingsValue extends IToolchainSettingsValue {
        public address? : string;
        public username? : string;
        public password? : string;
        public agentName? : string;
    }

    export abstract class IProjectSettingsValue {
        public name : string;
        public description : string;
        public path : string;
        public vxVersion : string;
    }

    export abstract class IExampleSettings extends IProjectSettingsValue {
        public readonly vxVersionPrefix? : string;
    }
}
