/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import EditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.EditorPanel;
    import EditorPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.EditorPanelViewer;
    import EditorPageViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPageViewerArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import DialogType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import IEditorPageEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IEditorPageEvents;
    import EditorPageEventType = Io.VisionSDK.Studio.Gui.Enums.Events.EditorPageEventType;
    import AppWizardPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.AppWizardPanelViewer;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import DirectoryBrowserPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.DirectoryBrowserPanelViewer;
    import KernelNodeHelpPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.KernelNodeHelpPanelViewerArgs;
    import KernelNodeHelpPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.KernelNodeHelpPanelViewer;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import ConsolePanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ConsolePanelViewer;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import GeneralNotificationsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.GeneralNotificationsPanel;
    import GeneralNotificationsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.GeneralNotificationsPanelViewer;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KernelNodeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNodeButton;
    import ProjectPickerPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ProjectPickerPanelViewer;
    import ProjectEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.ProjectEventArgs;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import ProjectPickerPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ProjectPickerPanel;
    import IDefaultPageDescriptor = Io.VisionSDK.Studio.Gui.Interfaces.IDefaultPageDescriptor;
    import ConsolePanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ConsolePanel;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ExportPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ExportPanelViewer;
    import SettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.SettingsPanelViewer;
    import SettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.SettingsPanel;
    import ResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeEventArgs;
    import ProjectCreationPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ProjectCreationPanelViewer;
    import ProjectCreationPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ProjectCreationPanel;
    import SettingsPanelEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.SettingsPanelEventArgs;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import IExampleSettings = Io.VisionSDK.Studio.Gui.Interfaces.IExampleSettings;
    import BuildSettingsEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.BuildSettingsEventArgs;
    import BuildSettingsEventType = Io.VisionSDK.Studio.Gui.Enums.Events.BuildSettingsEventType;
    import IBoundingRectangle = Io.VisionSDK.Studio.Gui.Interfaces.IBoundingRectangle;
    import IContextMenuItem = Io.VisionSDK.Studio.Gui.Interfaces.IContextMenuItem;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import ExportPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ExportPanel;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;

    export class EditorPage extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public wizardDialog : Dialog;
        public consoleDialog : Dialog;
        public editorPanel : EditorPanel;
        public directoryBrowserDialog : Dialog;
        public kernelNodeHelpDialog : Dialog;
        public examplesDialog : Dialog;
        public projectCreationDialog : Dialog;
        public settingsPanel : SettingsPanel;
        public exportDialog : Dialog;
        public notificationPanel : GeneralNotificationsPanel;
        private selectedHelpElement : IGuiCommons;
        private selectedHelpElementId : string;
        private wizardBoundingRectangle : IBoundingRectangle;
        private consoleBoundingRectangle : IBoundingRectangle;
        private helpHovered : boolean;
        private readonly cssFilterName : string;
        private lastSelectedNode : KernelNodeButton;
        private consoleHideHandle : number;
        private helpHideHandle : number;
        private readonly contextMenu : DropDownList;
        private contextMenuItems : ArrayList<IContextMenuItem>;

        constructor($id? : string) {
            super($id);
            this.wizardDialog = new Dialog(DialogType.TRANSPARENT);
            this.wizardDialog.PanelViewer(new AppWizardPanelViewer());

            this.consoleDialog = new Dialog(DialogType.TRANSPARENT);
            this.consoleDialog.PanelViewer(new ConsolePanelViewer());

            this.selectedHelpElement = null;
            this.selectedHelpElementId = null;
            this.wizardBoundingRectangle = null;
            this.consoleBoundingRectangle = null;

            this.directoryBrowserDialog = new Dialog(DialogType.GREEN);
            this.directoryBrowserDialog.PanelViewer(new DirectoryBrowserPanelViewer());

            this.examplesDialog = new Dialog(DialogType.GREEN);
            this.examplesDialog.PanelViewer(new ProjectPickerPanelViewer());

            this.projectCreationDialog = new Dialog(DialogType.GREEN);
            this.projectCreationDialog.PanelViewer(new ProjectCreationPanelViewer());

            this.exportDialog = new Dialog(DialogType.GREEN);
            this.exportDialog.PanelViewer(new ExportPanelViewer());

            this.helpHovered = false;
            this.kernelNodeHelpDialog = new Dialog(DialogType.BLUE);
            this.kernelNodeHelpDialog.PanelViewer(new KernelNodeHelpPanelViewer());

            this.contextMenu = new DropDownList();
            this.contextMenuItems = new ArrayList<IContextMenuItem>();

            this.addChildPanel(EditorPanelViewer, null,
                ($parent : EditorPage, $child : EditorPanel) : void => {
                    $parent.editorPanel = $child;
                });

            this.addChildPanel(GeneralNotificationsPanelViewer, null,
                ($parent : EditorPage, $child : GeneralNotificationsPanel) : void => {
                    $parent.notificationPanel = $child;
                });

            this.addChildPanel(SettingsPanelViewer, null,
                ($parent : EditorPage, $child : SettingsPanel) : void => {
                    $parent.settingsPanel = $child;
                });

            this.cssFilterName = "filter";
            if (this.getHttpManager().getRequest().IsWuiJre()) {
                this.cssFilterName = "-webkit-filter";
            }
        }

        public ShowContextMenu($items : ArrayList<IContextMenuItem>, $position : ElementOffset) : void {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.contextMenu.Clear();
                this.contextMenuItems.Clear();
                $items.foreach(($item : IContextMenuItem) : void => {
                    this.contextMenu.Add($item.text);
                    this.contextMenuItems.Add($item, $item.text);
                });
                this.contextMenu.Height(-1);
                ElementManager.setPosition(this.contextMenu, new ElementOffset($position.Top(), $position.Left()));
                DropDownList.Focus(this.contextMenu);
            }, 200);
        }

        public ShowConsole($fadeOutMs? : number) : void {
            if (!this.wizardDialog.Visible()) {
                this.showDialog(this.consoleDialog);
                BasePanel.scrollTop(this.consoleDialog.PanelViewer().getInstance(), 100);
                this.snapDialog(this.consoleDialog, this.consoleBoundingRectangle);
                clearInterval(this.consoleHideHandle);
                if (ObjectValidator.IsSet($fadeOutMs)) {
                    this.HideConsole($fadeOutMs);
                }
            }
        }

        public HideConsole($delayMs : number = 0, $lengthMs : number = 500, $callback? : () => void) : void {
            this.consoleHideHandle = this.hideDialog(this.consoleDialog, $delayMs, $lengthMs, () : void => {
                if (ObjectValidator.IsSet($callback)) {
                    $callback();
                }
            });
        }

        public ClearConsole() : void {
            (<ConsolePanel>this.consoleDialog.PanelViewer().getInstance()).Clear();
        }

        public ToggleConsole() : void {
            this.consoleDialog.Visible(!ElementManager.IsVisible(this.consoleDialog));
            if (this.consoleDialog.Visible()) {
                this.ShowConsole();
            }
        }

        public ShowWizard($element : IGuiCommons, $boundingRectangle? : IBoundingRectangle) : void {
            if (!this.wizardDialog.Visible()) {
                this.HideConsole(0, 0);
            }
            if (!ObjectValidator.IsEmptyOrNull(this.selectedHelpElement)) {
                this.HideWizard();
            }
            ElementManager.Show(this.Id() + "_WizardUIShield");

            this.wizardBoundingRectangle = $boundingRectangle;
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                this.selectedHelpElement = $element;
                this.selectedHelpElementId = this.getSelectedElementId();

                const targetDiv : Node = ElementManager.getElement(this.Id() + "_WizardDropArea");
                const temp : Node = document.createElement("div");
                const divPlaceholder : HTMLElement = document.createElement("div");
                divPlaceholder.id = this.Id() + "_DivPlaceholder";
                divPlaceholder.style.width = "100%";
                divPlaceholder.style.height = "100%";

                const element : Node = ElementManager.getElement(this.getSelectedElementId()).parentNode.parentNode;

                element.parentNode.insertBefore(divPlaceholder, element);
                targetDiv.appendChild(temp);
                temp.parentNode.insertBefore(element, temp);
                temp.parentNode.removeChild(temp);
                ElementManager.setCssProperty(this.selectedHelpElementId, "position", "absolute");
                ElementManager.setCssProperty(this.selectedHelpElementId + "_GuiWrapper", "float", "left");
                ElementManager.setCssProperty(this.editorPanel, this.cssFilterName, "blur(1.4px) brightness(0.75)");
                ElementManager.setCssProperty(this.notificationPanel, this.cssFilterName, "blur(1.4px) brightness(0.75)");
                this.snapHelpElement();
            }
            if (!ObjectValidator.IsEmptyOrNull($element) || ObjectValidator.IsSet($boundingRectangle)) {
                this.wizardDialog.Visible(true);
                this.snapDialog(this.wizardDialog, $boundingRectangle);
                ElementManager.setCssProperty(this.editorPanel, this.cssFilterName, "blur(1.4px) brightness(0.75)");
                ElementManager.setCssProperty(this.notificationPanel, this.cssFilterName, "blur(1.4px) brightness(0.75)");
                ElementManager.setCssProperty(this.wizardDialog, "pointer-events", "none");
            }
        }

        public HideWizard() {
            if (!ObjectValidator.IsEmptyOrNull(this.selectedHelpElement)) {
                if (this.selectedHelpElement.Implements(IGuiCommons)) {
                    ElementManager.ClearCssProperty(this.getSelectedElementId() + "_GuiWrapper", "float");
                    ElementManager.ClearCssProperty(this.selectedHelpElementId, "top");
                    ElementManager.ClearCssProperty(this.selectedHelpElementId, "left");
                    ElementManager.ClearCssProperty(this.selectedHelpElementId, "position");
                    const element : Node = ElementManager.getElement(this.selectedHelpElementId).parentNode.parentNode;
                    const targetDiv : Node = ElementManager.getElement(this.Id() + "_DivPlaceholder");
                    targetDiv.parentNode.insertBefore(element, targetDiv);
                    targetDiv.parentNode.removeChild(targetDiv);
                }
                this.selectedHelpElementId = null;
                this.selectedHelpElement = null;
            }
            ElementManager.ClearCssProperty(this.editorPanel, this.cssFilterName);
            ElementManager.ClearCssProperty(this.notificationPanel, this.cssFilterName);
            ElementManager.Hide(this.Id() + "_WizardUIShield");
            this.wizardBoundingRectangle = null;
            this.wizardDialog.Visible(false);
        }

        public ShowKernelNodeHelp($value : boolean = true, $fadeOutMs : number = 0) : void {
            const hideHelp : any = () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.helpHideHandle) || $fadeOutMs === 0) {
                    this.helpHideHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.hideDialog(this.kernelNodeHelpDialog, 0, $fadeOutMs, () : void => {
                            if (!ObjectValidator.IsEmptyOrNull(this.helpHideHandle)) {
                                const args : KernelNodeHelpPanelViewerArgs =
                                    <KernelNodeHelpPanelViewerArgs>this.kernelNodeHelpDialog.PanelViewerArgs();
                                args.ResetView(true);
                                this.kernelNodeHelpDialog.PanelViewerArgs(args);
                                this.kernelNodeHelpDialog.Visible(false);
                                if (!ObjectValidator.IsEmptyOrNull(this.lastSelectedNode)) {
                                    KernelNodeButton.TurnOff(this.lastSelectedNode, GuiObjectManager.getInstanceSingleton());
                                }
                                this.helpHideHandle = null;
                            }
                        });
                    });
                }
            };

            if ($value) {
                if (!this.kernelNodeHelpDialog.IsCompleted()) {
                    this.kernelNodeHelpDialog.PanelViewer().getInstance().getEvents().setOnComplete(() : void => {
                        this.resizeHelpDialog();
                    });
                }
                if (!this.kernelNodeHelpDialog.Visible()) {
                    const args : KernelNodeHelpPanelViewerArgs =
                        <KernelNodeHelpPanelViewerArgs>this.kernelNodeHelpDialog.PanelViewerArgs();
                    args.ForceUpdate();
                }
                if (!this.editorPanel.kernelNodePickerPanel.IsNodePicked()) {
                    this.helpHideHandle = null;
                    this.showDialog(this.kernelNodeHelpDialog);
                    this.resizeHelpDialog();
                } else {
                    hideHelp();
                }
            } else {
                hideHelp();
            }
        }

        public setExamples($descriptors : IExampleSettings[], $defaultPage : IDefaultPageDescriptor) : void {
            const panel : ProjectPickerPanel = <ProjectPickerPanel>this.examplesDialog.PanelViewer().getInstance();
            $descriptors.forEach(($descriptor : IProjectSettingsValue) : void => {
                panel.AddProject($descriptor);
            });
            if (!panel.IsCompleted()) {
                panel.descriptionPanel.setDefaultPage($defaultPage);
            }
        }

        public ShowExamplesDialog() : void {
            this.showDialog(this.examplesDialog);
            this.centerDialog(this.examplesDialog);
        }

        public ShowCreateProjectDialog($descriptor : IProjectSettingsValue) : void {
            const panel : ProjectCreationPanel = <ProjectCreationPanel>this.projectCreationDialog.PanelViewer().getInstance();
            panel.ApplySettings($descriptor);
            this.showDialog(this.projectCreationDialog);
            this.centerDialog(this.projectCreationDialog);
        }

        public ShowExportDialog() : void {
            this.showDialog(this.exportDialog);
            this.centerDialog(this.exportDialog);
        }

        public ShowSettings($value : boolean, $innerPanel? : any) : void {
            if ($value) {
                this.HideConsole(0, 0);
            }
            this.editorPanel.Visible(!$value);
            this.settingsPanel.Visible($value);
            if ($value) {
                const innerPanel : any = ObjectValidator.IsSet($innerPanel) ?
                    $innerPanel : this.settingsPanel.projectSettingsPanel;
                this.settingsPanel.ShowPanel(innerPanel);
                this.settingsPanel.Width(this.Width());
                this.settingsPanel.Height(this.Height());
            } else {
                this.editorPanel.Width(this.Width());
                this.editorPanel.Height(this.Height());
            }
        }

        public Value($value? : EditorPageViewerArgs) : EditorPageViewerArgs {
            return <EditorPageViewerArgs>super.Value($value);
        }

        public getEvents() : IEditorPageEvents {
            return <IEditorPageEvents>super.getEvents();
        }

        protected getSelectedElementId() : string {
            return this.selectedHelpElement.Id();
        }

        protected snapDialog($dialog : Dialog, $boundingRectangle? : IBoundingRectangle) : void {
            if ($dialog.Visible()) {
                const editorPos : ElementOffset = ElementManager.getAbsoluteOffset(this);
                const navTopOffset = 40;
                const totalHeightOffset : number = navTopOffset +
                    ElementManager.getCssIntegerValue($dialog.Id() + "_Header", "height")
                    + ElementManager.getCssIntegerValue($dialog.Id() + "_BottomCenter", "height");
                const snapTo : ElementOffset = new ElementOffset();
                if (!ObjectValidator.IsEmptyOrNull($boundingRectangle)) {
                    const availableSize : Size = this.getAvailableSize($boundingRectangle, totalHeightOffset);
                    this.resizeDialog($dialog, availableSize, true);
                    if (ObjectValidator.IsSet($boundingRectangle.offsetLeft)) {
                        snapTo.Left($boundingRectangle.offsetLeft.Normalize(this.Width(), UnitType.PX));
                    } else if (ObjectValidator.IsSet($boundingRectangle.offsetRight)) {
                        snapTo.Left(this.Width() - $boundingRectangle.offsetRight.Normalize(this.Width(), UnitType.PX)
                            - (ObjectValidator.IsSet($boundingRectangle.width) ?
                                Math.min(availableSize.Width(), $boundingRectangle.width.Normalize(this.Width(), UnitType.PX))
                                : availableSize.Width()));
                    }
                    if (ObjectValidator.IsSet($boundingRectangle.offsetTop)) {
                        snapTo.Top($boundingRectangle.offsetTop.Normalize(this.Height(), UnitType.PX) + navTopOffset);
                    } else if (ObjectValidator.IsSet($boundingRectangle.offsetBottom)) {
                        snapTo.Top(this.Height() - $boundingRectangle.offsetBottom.Normalize(this.Height(), UnitType.PX)
                            - (ObjectValidator.IsSet($boundingRectangle.height) ?
                                Math.min(availableSize.Height(), $boundingRectangle.height.Normalize(this.Height(), UnitType.PX))
                                : availableSize.Height()));
                    }
                } else if ($dialog === this.wizardDialog) {
                    const sizeLimit : Size = new Size();
                    const wizardSnapOffset : number = 40;
                    const helpElePos : ElementOffset = ElementManager.getAbsoluteOffset(this.Id() + "_DivPlaceholder");
                    helpElePos.Left(helpElePos.Left() - editorPos.Left());
                    helpElePos.Top(helpElePos.Top() - editorPos.Top());
                    const helpEleSize : Size = new Size(this.selectedHelpElementId, true);

                    const spaceLeft : number = helpElePos.Left();
                    const spaceTop : number = helpElePos.Top();
                    const spaceRight : number = this.Width() - (helpElePos.Left() + helpEleSize.Width());
                    const spaceBottom : number = this.Height() - (helpElePos.Top() + helpEleSize.Height());
                    const maxSpace : number = Math.max(spaceLeft, spaceTop, spaceRight, spaceBottom);
                    if (spaceRight === maxSpace) {
                        sizeLimit.Height(this.Height() - spaceTop - wizardSnapOffset - totalHeightOffset);
                        sizeLimit.Width(spaceRight - wizardSnapOffset * 2);
                        this.resizeDialog($dialog, sizeLimit);

                        snapTo.Left(helpEleSize.Width() + spaceLeft + wizardSnapOffset);
                        snapTo.Top(helpElePos.Top() + navTopOffset);
                    } else if (spaceBottom === maxSpace) {
                        sizeLimit.Height(spaceBottom - wizardSnapOffset * 2 - totalHeightOffset);
                        sizeLimit.Width(this.Width() - spaceRight - helpEleSize.Width() - wizardSnapOffset);
                        this.resizeDialog($dialog, sizeLimit);

                        snapTo.Left(spaceLeft);
                        snapTo.Top(helpElePos.Top() + helpEleSize.Height() + wizardSnapOffset * 2 + navTopOffset);
                    } else if (spaceLeft === maxSpace) {
                        sizeLimit.Height(this.Height() - spaceTop - wizardSnapOffset - totalHeightOffset);
                        sizeLimit.Width(spaceLeft - wizardSnapOffset * 2);
                        this.resizeDialog($dialog, sizeLimit);

                        snapTo.Left(helpElePos.Left() - $dialog.Width() - wizardSnapOffset);
                        snapTo.Top(helpElePos.Top() + navTopOffset);
                    } else {
                        sizeLimit.Height(spaceTop - wizardSnapOffset * 2 - totalHeightOffset);
                        sizeLimit.Width(this.Width() - spaceRight - helpEleSize.Width() - wizardSnapOffset);
                        this.resizeDialog($dialog, sizeLimit);

                        snapTo.Left(spaceLeft);
                        snapTo.Top(helpElePos.Top() - $dialog.Height() - wizardSnapOffset - navTopOffset);
                    }
                }
                ElementManager.setPosition($dialog.Id() + "_Envelop", snapTo);
            }
        }

        protected getAvailableSize($boundingRectangle, $heightOffset : number = 0) : Size {
            const sizeLimit : Size = new Size();

            const boundedLeft : number = (ObjectValidator.IsSet($boundingRectangle.offsetLeft) ?
                $boundingRectangle.offsetLeft.Normalize(this.Width(), UnitType.PX) : 40);
            const boundedRight : number = (ObjectValidator.IsSet($boundingRectangle.offsetRight) ?
                $boundingRectangle.offsetRight.Normalize(this.Width(), UnitType.PX) : 40);
            const boundedTop : number = (ObjectValidator.IsSet($boundingRectangle.offsetTop) ?
                $boundingRectangle.offsetTop.Normalize(this.Height(), UnitType.PX) : 40);
            const boundedBottom : number = (ObjectValidator.IsSet($boundingRectangle.offsetBottom) ?
                $boundingRectangle.offsetBottom.Normalize(this.Height(), UnitType.PX) : 40);

            const boundedWidth : PropagableNumber = ObjectValidator.IsSet($boundingRectangle.width) ?
                $boundingRectangle.width : new PropagableNumber({
                    number  : this.Width() - boundedLeft - boundedRight,
                    unitType: UnitType.PX
                });

            const boundedHeight : PropagableNumber = ObjectValidator.IsSet($boundingRectangle.height) ?
                $boundingRectangle.height : new PropagableNumber({
                    number  : this.Height() - boundedTop - boundedBottom,
                    unitType: UnitType.PX
                });

            const widthOverflow : number = this.Width() - boundedWidth.Normalize(this.Width(), UnitType.PX) - boundedLeft - boundedRight;
            if (widthOverflow >= 0) {
                sizeLimit.Width(boundedWidth.Normalize(this.Width(), UnitType.PX));
            } else {
                sizeLimit.Width(boundedWidth.Normalize(this.Width(), UnitType.PX) + widthOverflow);
            }

            const heightOverflow : number = this.Height() - $heightOffset - boundedHeight.Normalize(this.Height(), UnitType.PX) - boundedTop
                - boundedBottom;
            if (heightOverflow >= 0) {
                sizeLimit.Height(boundedHeight.Normalize(this.Height(), UnitType.PX));
            } else {
                sizeLimit.Height(boundedHeight.Normalize(this.Height(), UnitType.PX) + heightOverflow);
            }
            return sizeLimit;
        }

        protected resizeDialog($dialog : Dialog, $size : Size, $expand : boolean = false) {
            if ($dialog.Visible()) {
                const defaultWidth : number = 500;
                const defaultHeight : number = 250;

                ElementManager.setCssProperty($dialog.Id() + "_Envelop", "top", 0);
                ElementManager.setCssProperty($dialog.Id() + "_Envelop", "left", 0);

                if (defaultWidth > $size.Width()) {
                    $dialog.Width(defaultWidth - (defaultWidth - $size.Width()));
                } else if ($expand) {
                    $dialog.Width($size.Width());
                } else {
                    $dialog.Width(defaultWidth);
                }

                if (defaultHeight > $size.Height()) {
                    $dialog.Height(defaultHeight - (defaultHeight - $size.Height()));
                } else if ($expand) {
                    $dialog.Height($size.Height());
                } else {
                    $dialog.Height(defaultHeight);
                }
                (<any>Dialog).resize($dialog, $dialog.Width(), $dialog.Height());
            }
        }

        protected resizeHelpDialog() : void {
            if (this.kernelNodeHelpDialog.Visible()) {
                const args : KernelNodeHelpPanelViewerArgs =
                    <KernelNodeHelpPanelViewerArgs>this.kernelNodeHelpDialog.PanelViewerArgs();
                const sizeLimit : Size = new Size();
                sizeLimit.Height(this.editorPanel.kernelNodePickerPanel.Height() - 106);
                sizeLimit.Width(this.editorPanel.visualGraphPanel.Width() + this.editorPanel.kernelNodePickerPanel.Width() - 53);
                args.SizeLimit(sizeLimit);
                this.kernelNodeHelpDialog.PanelViewerArgs(args);
            }
        }

        protected snapHelpDialog() : void {
            if (this.kernelNodeHelpDialog.Visible()) {
                const snapTo : ElementOffset = ElementManager.getAbsoluteOffset(this.editorPanel.kernelNodePickerPanel);
                snapTo.Top(snapTo.Top() + 60);
                snapTo.Left(snapTo.Left() - this.kernelNodeHelpDialog.Width() - 25);
                ElementManager.setPosition(this.kernelNodeHelpDialog.Id() + "_Envelop", snapTo);
            }
        }

        protected snapHelpElement() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.selectedHelpElement) && this.selectedHelpElement.Implements(IGuiCommons)) {
                const thisOffset : ElementOffset = ElementManager.getAbsoluteOffset(this);
                const elementSize : Size = new Size(this.Id() + "_DivPlaceholder", true);
                const elementOffset : ElementOffset = ElementManager.getAbsoluteOffset(this.Id() + "_DivPlaceholder");
                this.selectedHelpElement.Width(elementSize.Width());
                this.selectedHelpElement.Height(elementSize.Height());
                ElementManager.setCssProperty(this.getSelectedElementId(), "top", elementOffset.Top() - thisOffset.Top());
                ElementManager.setCssProperty(this.getSelectedElementId(), "left", elementOffset.Left() - thisOffset.Left());
            }
        }

        protected centerDialog($dialog : Dialog) : void {
            if ($dialog.Visible()) {
                const editorOffset : ElementOffset = ElementManager.getAbsoluteOffset(this);
                const size : Size = new Size();
                size.Width(this.Width() > 800 ? 800 : 400);
                size.Height(this.Height() > 600 ? 600 : 300);
                this.resizeDialog($dialog, size, true);
                ElementManager.setCssProperty($dialog.Id() + "_Envelop",
                    "left", editorOffset.Left() + this.Width() / 2 - size.Width() / 2);
                ElementManager.setCssProperty($dialog.Id() + "_Envelop",
                    "top", editorOffset.Top() + 101);
            }
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setBeforeResize(($eventArgs : ResizeEventArgs) : void => {
                const page : EditorPage = $eventArgs.Owner();
                page.resizeHelpDialog();
                page.snapHelpElement();
                page.snapDialog(page.wizardDialog, page.wizardBoundingRectangle);
                page.snapDialog(page.consoleDialog, page.consoleBoundingRectangle);
                page.centerDialog(page.examplesDialog);
                page.centerDialog(page.projectCreationDialog);
                page.centerDialog(page.exportDialog);
                page.settingsPanel.Width($eventArgs.Width());
                page.settingsPanel.Height($eventArgs.Height());
                ElementManager.setCssProperty(page.settingsPanel, "left", $eventArgs.Width() / 2);
                ElementManager.setCssProperty(page.settingsPanel, "top", $eventArgs.Height() / 2);
                ElementManager.setCssProperty(page.notificationPanel, "left",
                    $eventArgs.Width() / 2 - page.notificationPanel.Width() / 2);
            });

            // todo : modals cannot be preloaded - oncomplete is never fired if modals are set to visible
            const preloadedElements : GuiCommons[] = [
                this.kernelNodeHelpDialog, this.wizardDialog, this.settingsPanel,
                this.consoleDialog /*this.projectPickerDialog*/
            ];
            preloadedElements.forEach(($element : GuiCommons) : void => {
                $element.Visible(true);
            });
            this.getEvents().setOnComplete(() : void => {
                preloadedElements.forEach(($element : GuiCommons) : void => {
                    $element.Visible(false);
                });
                ElementManager.Hide(this.Id() + "_WizardUIShield");
            });

            const eventRestrictingElements : GuiCommons[] = [
                this.settingsPanel, this.wizardDialog, this.directoryBrowserDialog, this.exportDialog, this.examplesDialog,
                this.projectCreationDialog
            ];
            eventRestrictingElements.forEach(($element : GuiCommons) : void => {
                $element.getEvents().setOnShow(() : void => {
                    this.handleVisualGraphEvents(eventRestrictingElements);
                });
                $element.getEvents().setOnHide(() : void => {
                    this.handleVisualGraphEvents(eventRestrictingElements);
                });
                if ($element instanceof Dialog) {
                    $element.getEvents().setOnClose(() : void => {
                        if ($element.Modal() && $element.Visible()) {
                            $element.Visible(false);
                        }
                    });
                }
            });

            const textSelectableElements : GuiCommons[] = [
                this.settingsPanel, this.wizardDialog, this.directoryBrowserDialog, this.exportDialog, this.examplesDialog,
                this.projectCreationDialog, this.consoleDialog, this.kernelNodeHelpDialog
            ];
            textSelectableElements.forEach(($element : GuiCommons) : void => {
                $element.getEvents().setOnHide(this.deselectAllText);
            });

            this.subscribeKernelNodeHelp();
            this.subscribeWizard();
            this.subscribeConsole();

            const setModalDialog : any = ($instance : Dialog) : void => {
                $instance.headerIcon.Visible(false);
                $instance.AutoCenter(false);
                $instance.AutoResize(false);
                $instance.Draggable(false);
                $instance.ResizeableType(ResizeableType.NONE);
                $instance.getEvents().setOnLoad(() : void => {
                    ElementManager.setOpacity($instance.Id() + "_Type", 0);
                });
                $instance.PanelViewer().getInstance().getEvents().setOnComplete(() : void => {
                    this.centerDialog($instance);
                    ElementManager.setOpacity($instance.Id() + "_Type", 100);
                });
            };

            setModalDialog(this.examplesDialog);
            setModalDialog(this.exportDialog);
            setModalDialog(this.projectCreationDialog);

            [
                this.settingsPanel, this.projectCreationDialog.PanelViewer().getInstance(), this.exportDialog.PanelViewer().getInstance()
            ].forEach(($panel : any) : void => {
                $panel.getEvents().setOnPathRequest(($eventArgs : EventArgs) : void => {
                    this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, $eventArgs);
                });
            });

            const projectCreationPanel : ProjectCreationPanel
                = <ProjectCreationPanel>this.projectCreationDialog.PanelViewer().getInstance();
            projectCreationPanel.getEvents().setOnSettingsUpdate(($eventArgs : SettingsPanelEventArgs) : void => {
                this.hideDialog(this.projectCreationDialog);
                const eventArgs : ProjectEventArgs = new ProjectEventArgs();
                eventArgs.Owner(this);
                eventArgs.Descriptor($eventArgs.Settings().projectSettings);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PROJECT_CREATE, eventArgs);
            });

            const examplesPanel : ProjectPickerPanel = <ProjectPickerPanel>this.examplesDialog.PanelViewer().getInstance();
            examplesPanel.getEvents().setOnProjectLoad(($eventArgs : ProjectEventArgs) : void => {
                this.hideDialog(this.examplesDialog);
                const eventArgs : ProjectEventArgs = new ProjectEventArgs();
                eventArgs.Owner(this);
                eventArgs.Descriptor($eventArgs.Descriptor());
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PROJECT_LOAD, eventArgs);
            });

            const exportPanel : ExportPanel = <ExportPanel>this.exportDialog.PanelViewer().getInstance();
            exportPanel.exportButton.getEvents().setOnClick(() : void => {
                this.hideDialog(this.exportDialog);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PROJECT_EXPORT, eventArgs);
            });
            exportPanel.closeButton.getEvents().setOnClick(() : void => {
                this.hideDialog(this.exportDialog);
            });

            this.settingsPanel.toolbar.undoButton.getEvents().setOnClick(() : void => {
                this.ShowSettings(false);
            });

            this.settingsPanel.getEvents().setOnSettingsUpdate(($eventArgs : SettingsPanelEventArgs) : void => {
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PROJECT_SETTINGS_UPDATE, $eventArgs);
            });

            this.editorPanel.toolbar.getEvents().setOnBuildPlatformChange(($eventArgs : BuildSettingsEventArgs) : void => {
                this.getEventsManager().FireEvent(this, BuildSettingsEventType.ON_BUILD_PLATFORM_CHANGE, $eventArgs);
            });

            this.editorPanel.getEvents().setOnClick(() : void => {
                if (this.getGuiManager().getHovered() !== this.editorPanel.kernelNodePickerPanel && this.kernelNodeHelpDialog.Visible()) {
                    this.ShowKernelNodeHelp(false, 50);
                }
            });
            this.editorPanel.visualGraphPanel.getEvents().setOnDragActionStart(() : void => {
                this.ShowKernelNodeHelp(false, 50);
            });

            this.contextMenu.ShowField(false);
            this.contextMenu.ShowButton(false);
            this.contextMenu.Width(200);
            this.contextMenu.getEvents().setOnSelect(() : void => {
                this.contextMenuItems.getItem(this.contextMenu.Value()).callback();
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.editorPanel)
                .Add(this.settingsPanel)
                .Add(this.notificationPanel)
                .Add(this.addElement().Id(this.Id() + "_WizardDropArea").StyleClassName("WizardDropArea"))
                .Add(this.addElement().Id(this.Id() + "_WizardUIShield").StyleClassName("WizardUIShield"))
                .Add(this.wizardDialog)
                .Add(this.directoryBrowserDialog)
                .Add(this.kernelNodeHelpDialog)
                .Add(this.consoleDialog)
                .Add(this.examplesDialog)
                .Add(this.projectCreationDialog)
                .Add(this.exportDialog)
                .Add(ToolTip.getInstanceSingleton())
                .Add(this.addElement().Id(this.Id() + "_ContextMenu").StyleClassName("ContextMenu")
                    .Add(this.contextMenu));
        }

        private subscribeKernelNodeHelp() : void {
            this.kernelNodeHelpDialog.StyleClassName("KernelNodeHelpDialog");
            this.kernelNodeHelpDialog.headerIcon.Visible(true);
            this.kernelNodeHelpDialog.AutoCenter(false);
            this.kernelNodeHelpDialog.AutoResize(false);
            this.kernelNodeHelpDialog.Modal(false);
            this.kernelNodeHelpDialog.Draggable(false);
            this.kernelNodeHelpDialog.ResizeableType(ResizeableType.NONE);

            this.editorPanel.kernelNodePickerPanel.getEvents().setOnMouseOut(() : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if (!this.helpHovered && !this.editorPanel.kernelNodePickerPanel.IsContentHovered()) {
                        this.ShowKernelNodeHelp(false, 200);
                    }
                }, 5000);
            });

            const showKernelNodeHelp : any = ($timeout : number) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if (this.editorPanel.kernelNodePickerPanel.IsContentHovered()
                        && (!this.kernelNodeHelpDialog.Visible() || !this.helpHovered)) {
                        this.ShowKernelNodeHelp();
                    }
                }, $timeout);
            };

            this.editorPanel.kernelNodePickerPanel.getEvents()
                .setOnNodeSelect(($eventArgs : VisualGraphEventArgs, $manager : GuiObjectManager) : void => {
                    if (!ObjectValidator.IsEmptyOrNull(this.lastSelectedNode) && this.lastSelectedNode !== $eventArgs.Owner()) {
                        KernelNodeButton.TurnOff(this.lastSelectedNode, $manager);
                    }
                    const args : KernelNodeHelpPanelViewerArgs =
                        <KernelNodeHelpPanelViewerArgs>this.kernelNodeHelpDialog.PanelViewerArgs();
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if (args.KernelNode() !== $eventArgs.ActiveNode() && this.editorPanel.kernelNodePickerPanel.IsContentHovered()) {
                            args.KernelNode($eventArgs.ActiveNode());
                            args.ResetView(true);
                            this.kernelNodeHelpDialog.headerIcon.IconType($eventArgs.ActiveNode().IconType());
                            this.kernelNodeHelpDialog.PanelViewerArgs(args);
                            this.lastSelectedNode = $eventArgs.Owner();
                        }
                    }, true, 100);
                    showKernelNodeHelp(500);
                });

            this.editorPanel.kernelNodePickerPanel.getEvents().setOnFocus(() : void => {
                showKernelNodeHelp(250);
            });

            let prevRoot : IKernelNode = null;
            this.editorPanel.visualGraphPanel.getEvents().setOnRootChangeStart(() : void => {
                prevRoot = this.editorPanel.visualGraphPanel.RootNode();
            });

            this.editorPanel.visualGraphPanel.getEvents().setOnRootChangeComplete(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(prevRoot)
                    || this.editorPanel.visualGraphPanel.RootNode().Type() !== KernelNodeType.VXGRAPH) {
                    this.hideDialog(this.kernelNodeHelpDialog);
                }
            });

            this.kernelNodeHelpDialog.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                this.helpHovered = true;
                if (!ObjectValidator.IsEmptyOrNull(this.lastSelectedNode)) {
                    KernelNodeButton.TurnOn(this.lastSelectedNode, $manager);
                }
            });
            this.kernelNodeHelpDialog.PanelViewer().getInstance().getEvents()
                .setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    $eventArgs.StopAllPropagation();
                    this.helpHovered = true;
                    if (!ObjectValidator.IsEmptyOrNull(this.lastSelectedNode)) {
                        KernelNodeButton.TurnOn(this.lastSelectedNode, $manager);
                    }
                });

            this.kernelNodeHelpDialog.getEvents().setOnMouseOut(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                this.helpHovered = false;
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if (!this.helpHovered && !this.editorPanel.kernelNodePickerPanel.IsContentHovered()
                        && !$manager.IsHovered(this.kernelNodeHelpDialog.PanelViewer().getInstance())) {
                        this.ShowKernelNodeHelp(false);
                    }
                }, 5000);
            });

            this.kernelNodeHelpDialog.getEvents().setOnResize(() : void => {
                this.snapHelpDialog();
            });

            this.kernelNodeHelpDialog.getEvents().Subscribe(true);
        }

        private subscribeWizard() : void {
            this.wizardDialog.StyleClassName("AppWizardDialog");
            this.wizardDialog.headerIcon.Visible(false);
            this.wizardDialog.headerText.Visible(true);
            this.wizardDialog.closeButton.Visible(true);
            this.wizardDialog.AutoCenter(false);
            this.wizardDialog.AutoResize(false);
            this.wizardDialog.Modal(false);
            this.wizardDialog.Draggable(false);
            this.wizardDialog.ResizeableType(ResizeableType.NONE);

            this.wizardDialog.getEvents().setOnComplete(() : void => {
                const events : ElementEventsManager = new ElementEventsManager(this.Id() + "_WizardUIShield");
                events.setOnClick(() : void => {
                    this.getEventsManager()
                        .FireEvent(this.wizardDialog.PanelViewer().getInstance(), EditorPageEventType.ON_WIZARD_FORWARD);
                });
                events.setEvent("oncontextmenu", () : void => {
                    this.getEventsManager()
                        .FireEvent(this.wizardDialog.PanelViewer().getInstance(), EditorPageEventType.ON_WIZARD_BACK);
                });
                events.Subscribe();
            });

            (<IEditorPageEvents>this.wizardDialog.PanelViewer().getInstance().getEvents()).setOnWizardForward(() : void => {
                this.getEventsManager().FireEvent(this, EditorPageEventType.ON_WIZARD_FORWARD);
            });

            (<IEditorPageEvents>this.wizardDialog.PanelViewer().getInstance().getEvents()).setOnWizardBack(() : void => {
                this.getEventsManager().FireEvent(this, EditorPageEventType.ON_WIZARD_BACK);
            });

            this.wizardDialog.closeButton.getEvents().setOnClick(() : void => {
                this.HideWizard();
                this.getEventsManager().FireEvent(this, EditorPageEventType.ON_WIZARD_CLOSE);
            });

            this.wizardDialog.getEvents().Subscribe(true);

            let mouseDownPos : IVector;
            WindowManager.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                if (this.wizardDialog.Visible()) {
                    const args : MouseEvent = $eventArgs.NativeEventArgs();
                    mouseDownPos = {x: args.clientX, y: args.clientY};
                } else {
                    mouseDownPos = undefined;
                }
            });

            WindowManager.getEvents().setOnMove(($eventArgs : MoveEventArgs) : void => {
                if (this.wizardDialog.Visible() && ObjectValidator.IsSet(mouseDownPos)) {
                    const args : MouseEvent = $eventArgs.NativeEventArgs();
                    const curPos : IVector = {x: args.clientX, y: args.clientY};
                    if (Math.pow(mouseDownPos.x - curPos.x, 2) + Math.pow(mouseDownPos.y - curPos.y, 2) > 300) {
                        ElementManager.ClearCssProperty(this.wizardDialog, "pointer-events");
                    }
                }
            });
        }

        private subscribeConsole() : void {
            this.consoleDialog.StyleClassName("ConsoleDialog");
            this.consoleDialog.headerIcon.Visible(false);
            this.consoleDialog.headerText.Visible(false);
            this.consoleDialog.closeButton.Visible(true);
            this.consoleDialog.AutoCenter(false);
            this.consoleDialog.AutoResize(false);
            this.consoleDialog.Modal(false);
            this.consoleDialog.Draggable(false);
            this.consoleDialog.ResizeableType(ResizeableType.NONE);

            this.consoleBoundingRectangle = <IBoundingRectangle>{
                height      : new PropagableNumber({
                    number: 50, unitType: UnitType.PCT
                }),
                offsetBottom: new PropagableNumber({
                    number: 60, unitType: UnitType.PX
                }),
                offsetLeft  : new PropagableNumber([
                    {
                        number: 20, unitType: UnitType.PCT
                    },
                    {
                        number: 27, unitType: UnitType.PX
                    }
                ]),
                offsetRight : new PropagableNumber([
                    {
                        number: 20, unitType: UnitType.PCT
                    },
                    {
                        number: 25, unitType: UnitType.PX
                    }
                ]),
                offsetTop   : new PropagableNumber({
                    number: 50, unitType: UnitType.PCT
                })
            };
        }

        private showDialog($dialog : Dialog) : void {
            ElementManager.StopOpacityChange($dialog);
            ElementManager.setOpacity($dialog, 100);
            this.HideConsole();
            $dialog.Visible(true);
            ElementManager.ClearCssProperty($dialog, "pointer-events");
        }

        private hideDialog($dialog : Dialog, $delayMs : number = 0, $lengthMs : number = 50, $callback? : () => void) : number {
            return this.getEvents().FireAsynchronousMethod(() : void => {
                ElementManager.setCssProperty($dialog, "pointer-events", "none");
                if ($lengthMs === 0) {
                    ElementManager.setOpacity($dialog, 0);
                    Dialog.Close($dialog);
                    if (ObjectValidator.IsSet($callback)) {
                        $callback();
                    }
                } else {
                    ElementManager.ChangeOpacity($dialog, DirectionType.DOWN, $lengthMs / 5, () : void => {
                        Dialog.Close($dialog);
                        if (ObjectValidator.IsSet($callback)) {
                            $callback();
                        }
                    });
                }
            }, false, $delayMs);
        }

        private handleVisualGraphEvents($restrictedElements : GuiCommons[]) : void {
            let isEventsEnabled : boolean = true;
            ArrayList.ToArrayList($restrictedElements).foreach(($element : GuiCommons) : boolean => {
                return isEventsEnabled = !$element.Visible();
            });
            this.editorPanel.visualGraphPanel.IsEventsEnabled(isEventsEnabled);
        }

        private deselectAllText() : void {
            if (ObjectValidator.IsSet(window.getSelection)) {
                if (ObjectValidator.IsSet(window.getSelection().empty)) {
                    window.getSelection().empty();
                } else if (ObjectValidator.IsSet(window.getSelection().removeAllRanges)) {
                    window.getSelection().removeAllRanges();
                }
            }
        }
    }
}
