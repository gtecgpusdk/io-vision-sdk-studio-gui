/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IKernelNodeNotifications = Io.VisionSDK.UserControls.Interfaces.IKernelNodeNotifications;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    export class VisualGraphPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private graphRoot : IKernelNode;
        private nodeNotifications : IKernelNodeNotifications;

        constructor() {
            super();

            this.graphRoot = null;
            this.nodeNotifications = new IKernelNodeNotifications();
        }

        public GraphRoot($value? : IKernelNode) : IKernelNode {
            return this.graphRoot = ObjectValidator.IsSet($value) ? $value : this.graphRoot;
        }

        public NodeNotifications($value? : IKernelNodeNotifications) : IKernelNodeNotifications {
            return this.nodeNotifications = ObjectValidator.IsSet($value) ? $value : this.nodeNotifications;
        }
    }
}
