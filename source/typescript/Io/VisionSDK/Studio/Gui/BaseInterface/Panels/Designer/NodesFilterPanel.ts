/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import NodesFilterPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.NodesFilterPanelViewerArgs;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class NodesFilterPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public filterField : TextField;
        private searchEnabled : boolean;

        constructor($id? : string) {
            super($id);
            this.filterField = new TextField(TextFieldType.PROPERTY);
            this.searchEnabled = false;
        }

        public Value($value? : NodesFilterPanelViewerArgs) : NodesFilterPanelViewerArgs {
            return <NodesFilterPanelViewerArgs>super.Value($value);
        }

        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value) && $value !== super.Width()) {
                super.Width($value);
                ValueProgressManager.Remove(this.Id() + "_Search");
                this.setOffsetPercentage(this.searchEnabled ? 0 : 100);
            }
            return super.Width();
        }

        public EnableSearch($value : boolean, $animate : boolean = true, $callback? : () => void) : void {
            const managerId : string = this.Id() + "_Search";
            if (this.searchEnabled !== $value) {
                ValueProgressManager.Remove(managerId);

                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(managerId);
                manipulatorArgs.Owner(managerId);
                manipulatorArgs.DirectionType($value ? DirectionType.DOWN : DirectionType.UP);
                if (manipulatorArgs.DirectionType() === DirectionType.UP) {
                    manipulatorArgs.ProgressType(ProgressType.FAST_START);
                } else {
                    manipulatorArgs.ProgressType(ProgressType.FAST_END);
                }

                manipulatorArgs.Step($animate ? (manipulatorArgs.DirectionType() === DirectionType.UP ? 30 : 15) : 1);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.RangeEnd(100);
                manipulatorArgs.CurrentValue($value ? 100 : 0);
                manipulatorArgs.StartEventType(managerId + "Start");
                manipulatorArgs.ChangeEventType(managerId + "Change");
                manipulatorArgs.CompleteEventType(managerId + "Complete");

                EventsManager.getInstanceSingleton().setEvent(managerId, manipulatorArgs.ChangeEventType(),
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        this.setOffsetPercentage($eventArgs.CurrentValue());
                    });

                EventsManager.getInstanceSingleton().setEvent(managerId, manipulatorArgs.CompleteEventType(), () : void => {
                    if (ObjectValidator.IsFunction($callback)) {
                        $callback();
                    }
                });
                ValueProgressManager.Execute(manipulatorArgs);
            } else {
                if (ObjectValidator.IsFunction($callback)) {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        $callback();
                    });
                }
            }
            this.searchEnabled = $value;
        }

        protected setOffsetPercentage($value : number) : void {
            ElementManager.setCssProperty(this.filterField.Id() + "_GuiWrapper", "left", Math.max(2, $value / 100 * this.Width() - 32));
        }

        protected innerCode() : IGuiElement {
            this.filterField.IconName(IconType.SEARCH);

            let isMouseOver : boolean = false;
            let mouseOutHandle : number = null;
            this.getEvents().setOnMouseOver(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(mouseOutHandle)) {
                    this.getEventsManager().Clear(mouseOutHandle);
                    mouseOutHandle = null;
                }
                isMouseOver = true;
                this.EnableSearch(true);
            });

            this.getEvents().setOnMouseOut(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                isMouseOver = false;
                mouseOutHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    if (!$manager.IsActive(this.filterField) && !isMouseOver && ObjectValidator.IsEmptyOrNull(this.filterField.Value())) {
                        this.EnableSearch(false);
                    }
                }, true, 1000);
            });

            this.getEvents().setOnComplete(() : void => {
                this.setOffsetPercentage(100);
            });

            WindowManager.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                if (!$manager.IsActive(this.filterField) && ObjectValidator.IsEmptyOrNull(this.filterField.Value())) {
                    this.EnableSearch(false);
                }
            });

            WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                if ($eventArgs.getKeyCode() === KeyMap.F && $eventArgs.NativeEventArgs().ctrlKey) {
                    $eventArgs.PreventDefault();
                    $eventArgs.StopAllPropagation();
                    this.EnableSearch(true, false, () : void => {
                        TextField.Focus(this.filterField);
                        (<HTMLInputElement>ElementManager.getElement(this.filterField.Id() + "_Input")).select();
                    });
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .WidthOfColumn(100, UnitType.PCT)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow()
                    .Add(this.addColumn().Add(this.filterField)));
        }
    }
}
