/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodesPickerPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.KernelNodesPickerPanel;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import KernelNodeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNodeButton;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class KernelNodesPickerPanelTest extends ViewerTestRunner<KernelNodesPickerPanel> {

        public testSearchString() : IViewerTestPromise {
            return ($done : () => void) : void => {
                const pickerPanel : KernelNodesPickerPanel = this.getInstance();

                const testFilter : any = ($filter : string, $expectedNumber : number) : void => {
                    pickerPanel.Filter($filter);
                    const visibleButtons : ArrayList<KernelNodeButton> = new ArrayList<KernelNodeButton>();
                    (<any>pickerPanel).buttons.foreach(($button : KernelNodeButton) : void => {
                        if (ObjectValidator.IsEmptyOrNull($filter) || StringUtils.Contains($button.Text(), $filter)) {
                            visibleButtons.Add($button);
                        }
                    });
                    this.assertEquals(visibleButtons.Length(), $expectedNumber);
                };

                testFilter("", 54);
                testFilter("Image", 7);
                testFilter("Accu", 3);
                $done();
            };
        }

        protected before() : string {
            const instance : KernelNodesPickerPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.Width(400);
            instance.Height(790);

            // this.addTestButton("ClearNodes", () : void => {
            //     instance.ClearNodes();
            // });
            //
            // this.addTestButton("setNodes", () : void => {
            //     instance.setNodes(KernelNodesPickerPanelViewer.getTestViewerArgs().getKernelNodes());
            // });

            return "<style>.TestCss .KernelNodesPickerPanel {border: 1px solid #E7E7E8;}</style>";
        }
    }
}
/* dev:end */
