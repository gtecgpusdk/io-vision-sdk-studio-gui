/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    export class KernelNodeHelpPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private descriptionText : string;
        private linkText : string;
        private node : IKernelNode;
        private sizeLimit : Size;
        private forceUpdateMs : number;
        private resetView : boolean;

        constructor() {
            super();
            this.headerText = "";
            this.descriptionText = "";
            this.linkText = "";
            this.forceUpdateMs = 0;
            this.sizeLimit = new Size();
            this.resetView = false;
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }

        public LinkText($value? : string) : string {
            return this.linkText = Property.String(this.linkText, $value);
        }

        public KernelNode($value? : IKernelNode) : IKernelNode {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.node = $value;
            }
            return this.node;
        }

        public SizeLimit($value? : Size) : Size {
            return this.sizeLimit = ObjectValidator.IsSet($value) ? $value : this.sizeLimit;
        }

        public ForceUpdate() : void {
            this.forceUpdateMs = new Date().getTime();
        }

        public ResetView($value? : boolean) : boolean {
            return this.resetView = Property.Boolean(this.resetView, $value);
        }
    }
}
