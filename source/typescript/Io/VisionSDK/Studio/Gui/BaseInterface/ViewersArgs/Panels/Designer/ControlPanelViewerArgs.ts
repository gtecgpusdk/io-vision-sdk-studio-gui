/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class ControlPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private resetPerspectiveText : string;

        constructor() {
            super();
            this.resetPerspectiveText = "";
        }

        public ResetButtonTooltipText($value? : string) : string {
            return this.resetPerspectiveText = Property.String(this.resetPerspectiveText, $value);
        }
    }
}
