/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import DirectoryBrowserPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.DirectoryBrowserPanelViewerArgs;

    export class DirectoryBrowserPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public pathValue : TextField;
        public browser : DirectoryBrowser;
        public saveButton : Button;
        public closeButton : Button;

        constructor($id? : string) {
            super($id);
            this.pathValue = new TextField();
            this.browser = new DirectoryBrowser();
            this.saveButton = new Button(ButtonType.GREEN_MEDIUM);
            this.closeButton = new Button(ButtonType.GRAY_MEDIUM);
        }

        public Value($value? : DirectoryBrowserPanelViewerArgs) : DirectoryBrowserPanelViewerArgs {
            return <DirectoryBrowserPanelViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Width(800);
            this.Height(500);

            this.closeButton.getEvents().setOnClick(() : void => {
                this.Parent().Visible(false);
            });
            this.saveButton.getEvents().setOnClick(() : void => {
                this.Parent().Visible(false);
            });
            this.pathValue.IsPersistent(false);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .WidthOfColumn(90, UnitType.PCT, true)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow()
                    .HeightOfRow(45, UnitType.PX)
                    .Add(this.pathValue)
                )
                .Add(this.addRow()
                    .Add(this.browser)
                )
                .Add(this.addRow()
                    .HeightOfRow(60, UnitType.PX)
                    .Add(this.addColumn()
                        .WidthOfColumn(90, UnitType.PCT, false)
                        .Add(this.addRow()
                            .Add(this.closeButton)
                            .Add(this.addColumn().WidthOfColumn(25, UnitType.PX))
                            .Add(this.saveButton)
                        )
                    )
                );
        }
    }
}
