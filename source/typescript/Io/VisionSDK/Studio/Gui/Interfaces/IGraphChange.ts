/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces {
    "use strict";
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import GraphChangeType = Io.VisionSDK.Studio.Gui.Enums.GraphChangeType;

    export class IGraphChange {
        public nodeChanges : INodeChange[];
        public selectedNodesIds : string[];
        public connectionChanges? : IConnectionChange[];
        public positionChange? : IPointChange;
        public sizeChange? : ISizeChange;
        public root : string;
        public type : GraphChangeType;
        public rootChange : IIndexChange;
    }

    export class INodeChange {
        public id : string;
        public type : string;
        public column? : number;
        public row? : number;
        public nameChange? : IAttributeChange;
        public subnodeChanges? : INodeChange[];
        public attributeChanges? : IAttributeChange[];
    }

    export class IConnectionChange {
        public inputNodeId : string;
        public outputNodeId : string;
        public inputNodeIndex : number;
        public outputNodeIndex : number;
    }

    export class IAttributeChange {
        public name? : string;
        public from? : any;
        public to : any;
    }

    export class IPointChange {
        public offsetLeft : number;
        public offsetTop : number;
    }

    export class ISizeChange {
        public anchorHorizontal : number;
        public anchorVertical : number;
        public directionHorizontal : number;
        public directionVertical : number;
    }

    export class IIndexChange {
        public fromIndex : number;
        public toIndex : number;
        public interfaceType : KernelNodeInterfaceType;
        public connectionChanges? : IConnectionChange[];
    }
}
