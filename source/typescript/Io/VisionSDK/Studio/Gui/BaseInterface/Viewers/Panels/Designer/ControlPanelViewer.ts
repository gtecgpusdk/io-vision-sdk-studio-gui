/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ControlPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.ControlPanel;
    import ControlPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ControlPanelViewerArgs;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;

    export class ControlPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ControlPanelViewerArgs {
            const args : ControlPanelViewerArgs = new ControlPanelViewerArgs();
            args.ResetButtonTooltipText("Reset perspective");
            return args;
        }

        /* dev:end */

        constructor($args? : ControlPanelViewerArgs) {
            super($args);
            this.setInstance(new ControlPanel());
        }

        public getInstance() : ControlPanel {
            return <ControlPanel>super.getInstance();
        }

        public ViewerArgs($args? : ControlPanelViewerArgs) : ControlPanelViewerArgs {
            return <ControlPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ControlPanel, $args : ControlPanelViewerArgs) : void {
            $instance.setLocalization($args);
        }

        /* dev:start */
        protected testImplementation($instance : ControlPanel) : string {
            this.addTestButton("resizeX + ", () : void => {
                $instance.Width($instance.Width() + 20);
            });
            this.addTestButton("resizeX - ", () : void => {
                $instance.Width($instance.Width() - 20);
            });

            this.addTestButton("resizeY + ", () : void => {
                $instance.Height($instance.Height() + 2);
            });
            this.addTestButton("resizeY - ", () : void => {
                $instance.Height($instance.Height() - 2);
            });

            let resizeConfIndex : number = 0;
            const resizeConfigurations : any[] = [
                {
                    getBreadcrumbButtonWidth: ($index : number) : string | string[] => {
                        if ($instance.Width() > 800) {
                            const controlMenuWidth : number
                                = new PropagableNumber($instance.ResizeConfiguration().getControlMenuWidth())
                                .Normalize($instance.Width(), UnitType.PX);
                            return (800 - controlMenuWidth) * .334 + "px";
                        } else {
                            return "33.4%";
                        }
                    },
                    getControlMenuWidth     : () : string | string[] => {
                        if ($instance.Width() > 700) {
                            return "250px";
                        } else {
                            return "150px";
                        }
                    },
                    isControlButtonsVisible : () : boolean => {
                        return $instance.Width() > 700;
                    }
                },
                {
                    getBreadcrumbButtonWidth: () : string | string[] => {
                        return undefined;
                    },
                    getControlMenuWidth     : () : string | string[] => {
                        return "150px";
                    },
                    isControlButtonsVisible : () : boolean => {
                        return false;
                    }
                }
            ];
            $instance.ResizeConfiguration(resizeConfigurations[resizeConfIndex]);

            let zoomConfIndex : number = 0;
            const zoomConfigurations : any[] = [
                {
                    defaultValue: 100,
                    maxValue    : 125,
                    minValue    : 75,
                    valueStep   : 5
                }
                ,
                {
                    defaultValue: 100,
                    maxValue    : 200,
                    minValue    : 50,
                    valueStep   : 10
                }

            ];
            $instance.ZoomConfiguration(zoomConfigurations[zoomConfIndex]);

            this.addTestButton("resize conf", () : void => {
                resizeConfIndex++;
                if (resizeConfIndex >= resizeConfigurations.length) {
                    resizeConfIndex = 0;
                }
                $instance.ResizeConfiguration(resizeConfigurations[resizeConfIndex]);
            });

            this.addTestButton("zoom conf", () : void => {
                zoomConfIndex++;
                if (zoomConfIndex >= zoomConfigurations.length) {
                    zoomConfIndex = 0;
                }
                $instance.ZoomConfiguration(zoomConfigurations[zoomConfIndex]);
            });

            let buttonNum : number = 3;
            this.addTestButton("button num", () : void => {
                if (buttonNum === 3) {
                    buttonNum = 1;
                } else {
                    buttonNum++;
                }
                const newArgs : IDropDownButtonArgs[] = [];
                for (let i = 0; i < buttonNum; i++) {
                    newArgs.push(args[i]);
                }
                $instance.setBreadcrumbButtons(newArgs);
            });

            const args : IDropDownButtonArgs[] = [
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.ORANGE,
                    values     : [
                        {
                            callback: () : void => {
                                Echo.Println("Simple button pressed");
                            },
                            text    : "Simple button"
                        }
                    ]
                },
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.SKY_BLUE,
                    values     : [
                        {
                            callback: () : void => {
                                Echo.Println("Simple menu menu button pressed");
                            },
                            text    : "Simple menu"
                        },
                        {
                            callback: () : void => {
                                Echo.Println("Simple menu Item 1 button pressed");
                            },
                            text    : "Simple menu Item 1"
                        },
                        {
                            callback: () : void => {
                                Echo.Println("Simple menu Item 2 button pressed");
                            },
                            text    : "Simple menu Item 2"
                        }
                    ]
                },
                {
                    activeIndex: 0,
                    type       : DropDownButtonType.GREEN,
                    values     : [
                        {
                            callback: () : void => {
                                Echo.Println("Dynamic menu Item 1 button pressed");
                                args[2].activeIndex = 0;
                                $instance.setBreadcrumbButtons(args);
                            },
                            text    : "Dynamic menu Item 1"
                        },
                        {
                            callback: () : void => {
                                Echo.Println("Dynamic menu Item 2 button pressed");
                                args[2].activeIndex = 1;
                                $instance.setBreadcrumbButtons(args);
                            },
                            text    : "Dynamic menu Item 2"
                        },
                        {
                            callback: () : void => {
                                Echo.Println("Dynamic menu Item 3 button pressed");
                                args[2].activeIndex = 2;
                                $instance.setBreadcrumbButtons(args);
                            },
                            text    : "Dynamic menu Item 3"
                        }
                    ]
                }
            ];

            $instance.setBreadcrumbButtons(args);

            $instance.StyleClassName("testCssClass");
            $instance.Width(750);
            $instance.Height(40);
            return "<style>.testCssClass {padding: 75px}</style>";
        }

        /* dev:end */
    }
}
