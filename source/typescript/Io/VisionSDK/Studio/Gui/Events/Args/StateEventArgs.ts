/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IIndexChange = Io.VisionSDK.Studio.Gui.Interfaces.IIndexChange;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GridManager = Io.VisionSDK.UserControls.Utils.GridManager;
    import ConnectionManager = Io.VisionSDK.UserControls.Utils.ConnectionManager;
    import INodeChange = Io.VisionSDK.Studio.Gui.Interfaces.INodeChange;
    import StateUtils = Io.VisionSDK.UserControls.Utils.StateUtils;
    import IConnectionChange = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionChange;
    import IPointChange = Io.VisionSDK.Studio.Gui.Interfaces.IPointChange;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import GraphChangeType = Io.VisionSDK.Studio.Gui.Enums.GraphChangeType;
    import ISizeChange = Io.VisionSDK.Studio.Gui.Interfaces.ISizeChange;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;

    /**
     * StateEventArgs class provides args connected with VisualGraph model modification.
     */
    export class StateEventArgs extends EventArgs {
        private changeType : GraphChangeType;
        private indexChange : IIndexChange;
        private nodeChanges : ArrayList<INodeChange>;
        private connectionChanges : ArrayList<IConnectionChange>;
        private sizeChange : ISizeChange;
        private positionChange : IPointChange;
        private selectedNodesIds : string[];

        public static getBeforeNodeUpdateArgs($gridManager : GridManager, $connectionManager : ConnectionManager) : StateEventArgs {
            const eventArgs : StateEventArgs = new StateEventArgs();
            const parsedNodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();
            parsedNodes.Copy($gridManager.getSelectedNodes());
            let parseSubnodes : boolean = false;

            if ($gridManager.IsDragActive()) {
                if ($gridManager.DragAnchorNode().Column() < 0) {
                    eventArgs.changeType = GraphChangeType.CREATE;
                    parseSubnodes = true;
                } else {
                    eventArgs.changeType = GraphChangeType.MOVE;
                    const move : ElementOffset = $gridManager.getMove();
                    eventArgs.positionChange = {offsetLeft: move.Left(), offsetTop: move.Top()};
                }
            } else if ($connectionManager.IsDragActive()) {
                const inputNode : IKernelNode = $connectionManager.getDragTargetInput().node;
                const reconnectedInput : Input = $connectionManager.getDefaultDragInput();
                if (!ObjectValidator.IsEmptyOrNull(reconnectedInput)) {
                    const removal : IConnectionChange
                        = StateUtils.getConnectionChange(reconnectedInput, $connectionManager.getDragSourceOutput());
                    eventArgs.connectionChanges.Add(removal, JSON.stringify(removal));
                    const creation : IConnectionChange
                        = StateUtils.getConnectionChange($connectionManager.getDragTargetInput(), $connectionManager.getDragSourceOutput());
                    eventArgs.connectionChanges.Add(creation, JSON.stringify(creation));
                    parsedNodes.Add(reconnectedInput.node, reconnectedInput.node.UniqueId());
                    parsedNodes.Add(inputNode, inputNode.UniqueId());
                    eventArgs.changeType = GraphChangeType.CONNECTION_MOVE;
                } else {
                    eventArgs.changeType = GraphChangeType.CONNECTION_CREATE;
                    parsedNodes.Add(inputNode, inputNode.UniqueId());
                }
            } else if ($gridManager.IsResizeActive()) {
                eventArgs.changeType = GraphChangeType.RESIZE;
            } else {
                eventArgs.changeType = GraphChangeType.REMOVE;
                eventArgs.selectedNodesIds = $gridManager.getSelectedNodesIds();
                parseSubnodes = true;
            }

            if ($gridManager.IsResizeActive()) {
                const resizeDirection : ElementOffset = $gridManager.getAreaResizeDirection();
                if (!(resizeDirection.Left() === 0 && resizeDirection.Top() === 0)) {
                    const resizeAnchor : ElementOffset = $gridManager.getResizeAnchor();
                    eventArgs.sizeChange = {
                        anchorHorizontal   : resizeAnchor.Left(), anchorVertical: resizeAnchor.Top(),
                        directionHorizontal: resizeDirection.Left(), directionVertical: resizeDirection.Top()
                    };
                }
            }
            if (ObjectValidator.IsEmptyOrNull($connectionManager.getDefaultDragInput())) {
                eventArgs.connectionChanges.Copy(StateUtils.getNodeConnectionChanges($gridManager.getSelectedNodes(), parseSubnodes));
                eventArgs.connectionChanges.Copy(StateUtils.getConnectionChanges($connectionManager.getSelectedConnections()));
            }
            eventArgs.nodeChanges = StateUtils.getNodeChanges(parsedNodes, parseSubnodes);

            return eventArgs;
        }

        public static getBeforeNodeAttributeUpdateArgs($gridManager : GridManager) : StateEventArgs {
            const eventArgs : StateEventArgs = new StateEventArgs();
            eventArgs.changeType = GraphChangeType.ATTRIBUTE_UPDATE;
            eventArgs.nodeChanges = StateUtils.getNodeChanges($gridManager.getSelectedNodes(), false, true);
            return eventArgs;
        }

        public static getOnRootInterfaceUpdateArgs($change : IIndexChange) : StateEventArgs {
            const eventArgs : StateEventArgs = new StateEventArgs();
            if (ObjectValidator.IsEmptyOrNull($change.fromIndex) && !ObjectValidator.IsEmptyOrNull($change.toIndex)) {
                eventArgs.changeType = GraphChangeType.ROOT_INTERFACE_CREATE;
            } else if (!ObjectValidator.IsEmptyOrNull($change.fromIndex) && ObjectValidator.IsEmptyOrNull($change.toIndex)) {
                eventArgs.changeType = GraphChangeType.ROOT_INTERFACE_REMOVE;
            } else if (!ObjectValidator.IsEmptyOrNull($change.fromIndex) && !ObjectValidator.IsEmptyOrNull($change.toIndex)) {
                eventArgs.changeType = GraphChangeType.ROOT_INTERFACE_MOVE;
            }
            eventArgs.indexChange = $change;
            return eventArgs;
        }

        private constructor() {
            super();
            this.nodeChanges = new ArrayList<INodeChange>();
            this.connectionChanges = new ArrayList<IConnectionChange>();
            this.indexChange = null;
            this.positionChange = null;
            this.selectedNodesIds = [];
            this.sizeChange = null;
        }

        public getChangeType() : GraphChangeType {
            return this.changeType;
        }

        public getIndexChange() : IIndexChange {
            return this.indexChange;
        }

        public getSizeChange() : ISizeChange {
            return this.sizeChange;
        }

        public getPositionChange() : IPointChange {
            return this.positionChange;
        }

        public getNodeChanges() : ArrayList<INodeChange> {
            return this.nodeChanges;
        }

        public getConnectionChanges() : ArrayList<IConnectionChange> {
            return this.connectionChanges;
        }

        public getSelectedNodesIds() : string[] {
            return this.selectedNodesIds;
        }
    }
}
