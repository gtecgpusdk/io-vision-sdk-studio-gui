/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class NodesFilterPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private filterFieldHint : string;

        constructor() {
            super();
            this.filterFieldHint = "";
        }

        public FilterFieldHint($value? : string) : string {
            return this.filterFieldHint = Property.String(this.filterFieldHint, $value);
        }
    }
}
