/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Pages.Designer {
    "use strict";
    import EditorPage = Io.VisionSDK.Studio.Gui.BaseInterface.Pages.Designer.EditorPage;
    import EditorPageViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPageViewerArgs;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import AppWizardPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.AppWizardPanelViewer;
    import EditorPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.EditorPanelViewer;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import DirectoryBrowserPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.DirectoryBrowserPanelViewer;
    import KernelNodeHelpPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.KernelNodeHelpPanelViewer;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProjectPickerPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ProjectPickerPanelViewer;
    import ExportPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ExportPanelViewer;
    import SettingsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings.SettingsPanelViewer;
    import ProjectCreationPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs.ProjectCreationPanelViewer;

    export class EditorPageViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : EditorPageViewerArgs {
            const args : EditorPageViewerArgs = new EditorPageViewerArgs();
            args.EditorPanelViewerArgs((<any>EditorPanelViewer).getTestViewerArgs());
            args.AppWizardPanelViewerArgs((<any>AppWizardPanelViewer).getTestViewerArgs());
            args.DirectoryBrowserDialogArgs((<any>DirectoryBrowserPanelViewer).getTestViewerArgs());
            args.KernelNodeHelpDialogArgs((<any>KernelNodeHelpPanelViewer).getTestViewerArgs());
            args.ProjectCreationDialogArgs((<any>ProjectCreationPanelViewer).getTestViewerArgs());
            args.ProjectPickerDialogArgs((<any>ProjectPickerPanelViewer).getTestViewerArgs());
            args.SettingsPanelArgs((<any>SettingsPanelViewer).getTestViewerArgs());
            args.ExportDialogArgs((<any>ExportPanelViewer).getTestViewerArgs());
            return args;
        }

        /* dev:end */

        constructor($args? : EditorPageViewerArgs) {
            super($args);
            this.setInstance(new EditorPage());
            /* dev:start */
            this.setTestSubscriber(Io.VisionSDK.Studio.Gui.RuntimeTests.Pages.Designer.EditorPageTest);
            /* dev:end */
        }

        public getInstance() : EditorPage {
            return <EditorPage>super.getInstance();
        }

        public ViewerArgs($args? : EditorPageViewerArgs) : EditorPageViewerArgs {
            return <EditorPageViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : EditorPage, $args : EditorPageViewerArgs) : void {
            $instance.editorPanel.Value($args.EditorPanelViewerArgs());
            $instance.directoryBrowserDialog.PanelViewerArgs($args.DirectoryBrowserDialogArgs());
            $instance.wizardDialog.PanelViewer().ViewerArgs($args.AppWizardPanelViewerArgs());
            $instance.kernelNodeHelpDialog.PanelViewerArgs($args.KernelNodeHelpDialogArgs());
            $instance.examplesDialog.PanelViewerArgs($args.ProjectPickerDialogArgs());
            $instance.projectCreationDialog.PanelViewerArgs($args.ProjectCreationDialogArgs());
            $instance.settingsPanel.Value($args.SettingsPanelArgs());
            $instance.exportDialog.PanelViewerArgs($args.ExportDialogArgs());
        }

        protected beforeLoad($instance : EditorPage) : void {
            WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                if (!$manager.IsActive(FormInput) && !$manager.IsActive(Dialog) &&
                    $eventArgs.getKeyCode() === KeyMap.DELETE || $eventArgs.getKeyCode() === KeyMap.ESC) {
                    if ($manager.IsActive(Dialog)) {
                        $instance.directoryBrowserDialog.Visible(false);
                    }
                }
            });
        }
    }
}
