/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.KernelGenerator {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelEditorPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.KernelGenerator.KernelEditorPanelViewerArgs;
    import KernelEditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.KernelGenerator.KernelEditorPanel;

    export class KernelEditorPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : KernelEditorPanelViewerArgs {
            const args : KernelEditorPanelViewerArgs = new KernelEditorPanelViewerArgs();
            args.HeaderText("OpenVX Kernel generator");

            args.ProjectPathText("Target path");
            args.ProjectPathValue("");

            args.ProjectNameText("Target project");
            args.AddProject("io-vision-sdk-kernels");
            args.AddProject("com-nxp-openvx-kernels");

            args.ProjectNamespaceText("Project namespace");
            args.ProjectNamespaceValue("Io.VisionSDK.Kernels");

            args.KernelNamespaceText("Kernel namespace");
            args.KernelNamespaceValue("Example");

            args.KernelNameText("Kernel name");
            args.KernelNameValue("Example");

            args.DescriptionText("Description");
            args.DescriptionValue("");

            args.ParametersText("Parameters");
            args.ParameterDirectionText("Direction");
            args.ParameterNameText("Name");
            args.ParameterTypeText("Type");
            args.ParameterDescriptionText("Description");
            args.AddParameterDirection("VX_INPUT");
            args.AddParameterDirection("VX_OUTPUT");
            args.AddParameterType("VX_IMAGE");
            args.AddParameterType("VX_SCALAR");
            args.AddParameterType("VX_UINT8");

            args.GenerateButtonText("Generate");
            return args;
        }

        /* dev:end */

        constructor($args? : KernelEditorPanelViewerArgs) {
            super($args);
            this.setInstance(new KernelEditorPanel());
        }

        public getInstance() : KernelEditorPanel {
            return <KernelEditorPanel>super.getInstance();
        }

        public ViewerArgs($args? : KernelEditorPanelViewerArgs) : KernelEditorPanelViewerArgs {
            return <KernelEditorPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : KernelEditorPanel, $args : KernelEditorPanelViewerArgs) : void {
            $instance.headerLabel.Text($args.HeaderText());

            $instance.projectPathLabel.Text($args.ProjectPathText());
            $instance.projectPath.Value($args.ProjectPathValue());

            $instance.projectNameLabel.Text($args.ProjectNameText());
            $instance.projectName.Clear();
            $instance.projectName.Height(-1);
            $args.getProjects().foreach(($value : string) : void => {
                $instance.projectName.Add($value);
            });
            $instance.projectName.Select(0);

            $instance.projectNamespaceLabel.Text($args.ProjectNamespaceText());
            $instance.projectNamespace.Value($args.ProjectNamespaceValue());

            $instance.kernelNamespaceLabel.Text($args.KernelNamespaceText());
            $instance.kernelNamespace.Value($args.KernelNamespaceValue());

            $instance.kernelNameLabel.Text($args.KernelNameText());
            $instance.kernelName.Value($args.KernelNameValue());

            $instance.descriptionLabel.Text($args.DescriptionText());
            $instance.description.Value($args.DescriptionValue());

            $instance.parametersLabel.Text($args.ParametersText());
            $instance.parameterDirectionLabel.Text($args.ParameterDirectionText());
            $instance.parameterNameLabel.Text($args.ParameterNameText());
            $instance.parameterTypeLabel.Text($args.ParameterTypeText());
            $instance.parameterDescriptionLabel.Text($args.ParameterDescriptionText());

            $instance.parameterDirection.Clear();
            $args.getParameterDirections().foreach(($value : string) : void => {
                $instance.parameterDirection.Add($value);
            });
            $instance.parameterDirection.Select(0);

            $instance.parameterType.Clear();
            $args.getParameterTypes().foreach(($value : string) : void => {
                $instance.parameterType.Add($value);
            });
            $instance.parameterType.Select(0);

            $instance.generateButton.Text($args.GenerateButtonText());
        }

        /* dev:start */
        protected testImplementation($instance : KernelEditorPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(900);

            const params : any[] = [
                {
                    description: "Description text",
                    direction  : "VX_INPUT",
                    name       : "input",
                    type       : "VX_IMAGE"
                },
                {
                    description: "Description text",
                    direction  : "VX_INPUT",
                    name       : "shift",
                    type       : "VX_SCALAR"
                },
                {
                    description: "Description text",
                    direction  : "VX_OUTPUT",
                    name       : "output",
                    type       : "VX_IMAGE"
                }
            ];
            let index : number = 0;
            params.forEach(($param : any) : void => {
                if (index < $instance.parameters.length) {
                    $instance.parameters[index][0].Text($param.direction);
                    $instance.parameters[index][1].Text($param.name);
                    $instance.parameters[index][2].Text($param.type);
                    $instance.parameters[index][3].Text($param.description);
                }
                index++;
            });

            this.addTestButton("notify", () : void => {
                $instance.notificationPanel.AddInfo("Test notify");
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".DrawGuiObject {margin: 0; padding: 0} " +
                ".TestCss .KernelEditorPanel{}</style>";
        }

        /* dev:end */
    }
}
