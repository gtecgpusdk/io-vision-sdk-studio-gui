/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import IVisualGraphPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IVisualGraphPanelEvents;
    import VisualGraphPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.VisualGraphPanelEventType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IDropDownButtonItem = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonItem;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;

    export class VisualGraphControlPanel extends ControlPanel {
        constructor($id? : string) {
            super($id);
        }

        public setBreadcrumb($rootNode : IKernelNode) : void {
            const args : ArrayList<IDropDownButtonArgs> = new ArrayList<IDropDownButtonArgs>();
            let currRoot : IKernelNode = $rootNode;
            while (!ObjectValidator.IsEmptyOrNull(currRoot)) {
                const items : ArrayList<IDropDownButtonItem> = new ArrayList<IDropDownButtonItem>();
                if (!ObjectValidator.IsEmptyOrNull(currRoot.ParentNode())) {
                    currRoot.ParentNode().ChildNodes().foreach(($node : IKernelNode) : void => {
                        if ($node.Type() === KernelNodeType.VXCONTEXT || $node.Type() === KernelNodeType.VXGRAPH
                            || $node.Type() === KernelNodeType.VXGROUP) {
                            items.Add(<IDropDownButtonItem>{
                                callback: () : void => {
                                    this.fireRootChangeEvent($node);
                                },
                                text    : $node.Name()
                            }, $node.Name());
                        }
                    });
                } else {
                    const rootInstance : IKernelNode = currRoot;
                    items.Add(<IDropDownButtonItem>{
                        callback: () : void => {
                            this.fireRootChangeEvent(rootInstance);
                        },
                        text    : rootInstance.Name()
                    }, rootInstance.Name());
                }
                args.Add({
                    activeIndex: items.IndexOf(items.getItem(currRoot.Name())),
                    type       : this.GuiType().type,
                    values     : items.ToArray()
                });
                currRoot = currRoot.ParentNode();
            }
            args.SortByKeyDown();
            super.setBreadcrumbButtons(args.ToArray());
        }

        public getEvents() : IVisualGraphPanelEvents {
            return <IVisualGraphPanelEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.ResizeConfiguration({
                getBreadcrumbButtonWidth: ($index : number) : string | string[] => {
                    if (this.Width() > 800) {
                        const controlMenuWidth : number
                            = new PropagableNumber(this.ResizeConfiguration().getControlMenuWidth()).Normalize(this.Width(), UnitType.PX);
                        return (800 - controlMenuWidth) * .334 + "px";
                        // todo: use when grouping is enabled
                        // if ($index === 3) {
                        //     return (1000 - controlMenuWidth) * .19 + "px";
                        // } else {
                        //     return (1000 - controlMenuWidth) * .27 + "px";
                        // }
                    } else {
                        return "33.4%";
                        // if ($index === 3) {
                        //     return "19%";
                        // } else {
                        //     return "27%";
                        // }
                    }
                },
                getControlMenuWidth     : () : string | string[] => {
                    if (this.Width() > 700) {
                        return "250px";
                    } else {
                        return "150px";
                    }
                },
                isControlButtonsVisible : () : boolean => {
                    return this.Width() > 700;
                }
            });

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected cssContainerName() : string {
            return VisualGraphControlPanel.ClassNameWithoutNamespace() + " " + ControlPanel.ClassNameWithoutNamespace();
        }

        private fireRootChangeEvent($node : IKernelNode) : void {
            const eventArgs : VisualGraphEventArgs = new VisualGraphEventArgs();
            eventArgs.Owner(this);
            eventArgs.ActiveNode($node);
            this.getEventsManager().FireEvent(this, VisualGraphPanelEventType.ON_ROOT_CHANGE_COMPLETE, eventArgs);
        }
    }
}
