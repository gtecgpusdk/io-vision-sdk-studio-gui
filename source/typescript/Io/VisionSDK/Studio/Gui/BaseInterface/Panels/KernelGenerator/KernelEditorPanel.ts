/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.KernelGenerator {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ToolbarPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ToolbarPanelViewerArgs;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import IProjectEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IProjectEvents;
    import ProjectEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ProjectEventType;
    import TextArea = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextArea;
    import TextAreaType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextAreaType;
    import InputLabel = Io.VisionSDK.UserControls.BaseInterface.UserControls.InputLabel;
    import InputLabelType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.InputLabelType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralNotificationsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.GeneralNotificationsPanel;
    import GeneralNotificationsPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.GeneralNotificationsPanelViewer;

    export class KernelEditorPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerLabel : Label;
        public projectPathLabel : Label;
        public projectPath : TextField;
        public projectNameLabel : Label;
        public projectName : DropDownList;
        public projectNamespaceLabel : Label;
        public projectNamespace : TextField;
        public kernelNamespaceLabel : Label;
        public kernelNamespace : TextField;
        public kernelNameLabel : Label;
        public kernelName : TextField;
        public descriptionLabel : Label;
        public description : TextArea;
        public generateButton : Button;
        public parametersLabel : Label;
        public parameterDirectionLabel : InputLabel;
        public parameterDirection : DropDownList;
        public parameterNameLabel : InputLabel;
        public parameterName : TextField;
        public parameterTypeLabel : InputLabel;
        public parameterType : DropDownList;
        public parameterDescriptionLabel : InputLabel;
        public parameterDescription : TextField;
        public parameters : InputLabel[][];
        public notificationPanel : GeneralNotificationsPanel;

        constructor($id? : string) {
            super($id);
            this.headerLabel = new Label();

            this.projectPathLabel = new Label();
            this.projectPath = new TextField(TextFieldType.SETTING);

            this.projectNameLabel = new Label();
            this.projectName = new DropDownList(DropDownListType.SETTING);

            this.projectNamespaceLabel = new Label();
            this.projectNamespace = new TextField(TextFieldType.SETTING);

            this.kernelNamespaceLabel = new Label();
            this.kernelNamespace = new TextField(TextFieldType.SETTING);

            this.kernelNameLabel = new Label();
            this.kernelName = new TextField(TextFieldType.SETTING);

            this.descriptionLabel = new Label();
            this.description = new TextArea(TextAreaType.SETTING);

            this.parametersLabel = new Label();
            this.parameterDirectionLabel = new InputLabel(InputLabelType.PROPERTY);
            this.parameterDirection = new DropDownList(DropDownListType.SETTING);
            this.parameterNameLabel = new InputLabel(InputLabelType.PROPERTY);
            this.parameterName = new TextField(TextFieldType.SETTING);
            this.parameterTypeLabel = new InputLabel(InputLabelType.PROPERTY);
            this.parameterType = new DropDownList(DropDownListType.SETTING);
            this.parameterDescriptionLabel = new InputLabel(InputLabelType.PROPERTY);
            this.parameterDescription = new TextField(TextFieldType.SETTING);

            this.parameters = [];
            for (let rowIndex : number = 0; rowIndex < 10; rowIndex++) {
                const row : InputLabel[] = [];
                for (let colIndex : number = 0; colIndex < 4; colIndex++) {
                    const newParam : InputLabel = new InputLabel(InputLabelType.PROPERTY);
                    newParam.Text(" ");
                    this["parameter_" + rowIndex + "_" + colIndex] = newParam;
                    row.push(newParam);
                }
                this.parameters.push(row);
            }

            this.generateButton = new Button(ButtonType.GREEN_MEDIUM);

            this.addChildPanel(GeneralNotificationsPanelViewer, null,
                ($parent : KernelEditorPanel, $child : GeneralNotificationsPanel) : void => {
                    $parent.notificationPanel = $child;
                });
        }

        public Value($value? : ToolbarPanelViewerArgs) : ToolbarPanelViewerArgs {
            return <ToolbarPanelViewerArgs>super.Value($value);
        }

        public getEvents() : IProjectEvents {
            return <IProjectEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.projectPath.ReadOnly(true);
            this.projectPath.IsPersistent(false);
            this.projectPath.getEvents().setOnClick(() : void => {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this.projectPath);
                this.getEventsManager().FireEvent(this, ProjectEventType.ON_PATH_REQUEST, eventArgs);
            });
            this.projectNamespace.ReadOnly(true);

            this.parameterDirection.MaxVisibleItemsCount(2);
            this.parameterType.MaxVisibleItemsCount(5);

            let currentColumns : InputLabel[] = null;
            this.parameterDirection.getEvents().setOnSelect(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(currentColumns)) {
                    currentColumns[0].Text(this.parameterDirection.Value());
                }
            });
            this.parameterName.getEvents().setOnChange(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(currentColumns)) {
                    currentColumns[1].Text(this.parameterName.Value());
                }
            });
            this.parameterType.getEvents().setOnSelect(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(currentColumns)) {
                    currentColumns[2].Text(this.parameterType.Value());
                }
            });
            this.parameterDescription.getEvents().setOnChange(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(currentColumns)) {
                    currentColumns[3].Text(this.parameterDescription.Value());
                }
            });
            this.parameters.forEach(($columns : any[]) : void => {
                $columns.forEach(($column : InputLabel) : void => {
                    $column.getEvents().setOnMouseOver(() : void => {
                        currentColumns = null;
                        this.parameterDirection.Select($columns[0].Text());
                        this.parameterName.Value($columns[1].Text());
                        this.parameterType.Select($columns[2].Text());
                        this.parameterDescription.Value($columns[3].Text());
                        this.getGuiManager().setActive(this.parameterDirection, false);
                        this.getGuiManager().setActive(this.parameterType, false);
                        ElementManager.setCssProperty(this.Id() + "_ParameterEditor", "top",
                            ElementManager.getAbsoluteOffset($column).Top() - ElementManager.getAbsoluteOffset(this).Top());
                        currentColumns = $columns;
                    });
                });
            });

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            const innerHtml : IGuiElement = this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .Add(this.addRow().FitToParent(FitToParent.NONE)
                    .HeightOfRow(80, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT, true)
                        .StyleClassName("Header")
                        .Add(this.headerLabel)
                    )
                )
                .Add(this.addRow()
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                    .Add(this.addColumn().WidthOfColumn(150, UnitType.PX).HeightOfRow(50, UnitType.PX, true)
                        .Add(this.projectPathLabel)
                        .Add(this.projectNameLabel)
                        .Add(this.projectNamespaceLabel)
                        .Add(this.kernelNamespaceLabel)
                        .Add(this.kernelNameLabel)
                        .Add(this.descriptionLabel)
                    )
                    .Add(this.addColumn().HeightOfRow(50, UnitType.PX, true)
                        .Add(this.projectPath)
                        .Add(this.projectName)
                        .Add(this.projectNamespace)
                        .Add(this.kernelNamespace)
                        .Add(this.kernelName)
                        .Add(this.description)
                    )
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                )
                .Add(this.addRow().FitToParent(FitToParent.NONE)
                    .HeightOfRow(40, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT, true)
                        .Add(this.parametersLabel)
                    )
                );

            const rowHeight : number = 30;
            const addSplitter : any = () : any => {
                return this.addColumn().WidthOfColumn(5, UnitType.PX);
            };
            innerHtml.Add(this.addRow().FitToParent(FitToParent.FULL)
                .HeightOfRow(rowHeight, UnitType.PX)
                .StyleClassName("ParametersTableHeader")
                .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                .Add(this.addColumn().Add(this.parameterDirectionLabel))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterNameLabel))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterTypeLabel))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterDescriptionLabel))
                .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
            );

            innerHtml.Add(this.addRow(this.Id() + "_ParameterEditor").FitToParent(FitToParent.FULL)
                .HeightOfRow(rowHeight, UnitType.PX)
                .StyleClassName("ParameterEditor")
                .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
                .Add(this.addColumn().Add(this.parameterDirection))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterName))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterType))
                .Add(addSplitter())
                .Add(this.addColumn().Add(this.parameterDescription))
                .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT))
            );

            this.parameters.forEach(($columns : any[]) : void => {
                const row : IGuiElement = this.addRow().FitToParent(FitToParent.FULL)
                    .HeightOfRow(rowHeight, UnitType.PX)
                    .Add(this.addColumn().WidthOfColumn(10, UnitType.PCT));
                let colIndex : number = 0;
                $columns.forEach(($column : InputLabel) : void => {
                    row.Add(this.addColumn().Add($column));
                    if (colIndex + 1 < $columns.length) {
                        row.Add(addSplitter());
                    }
                    colIndex++;
                });
                innerHtml.Add(row.Add(this.addColumn().WidthOfColumn(10, UnitType.PCT)));
            });

            innerHtml.Add(this.addRow().HeightOfRow(10, UnitType.PX)
                .Add(this.addColumn().WidthOfColumn(450, UnitType.PX).StyleClassName("NotificationPanel")
                    .Add(this.notificationPanel))
            );

            innerHtml.Add(this.addRow()
                .HeightOfRow(90, UnitType.PX)
                .Add(this.addColumn().WidthOfColumn(90, UnitType.PCT, true)
                    .Add(this.addRow()
                        .Add(this.generateButton)
                    )
                )
            );

            return innerHtml;
        }
    }
}
