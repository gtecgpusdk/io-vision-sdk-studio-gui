/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;

    export class ProjectSettingsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private nameText : string;
        private descriptionText : string;
        private vxVersionText : string;
        private readonly vxVersions : ArrayList<string>;
        private pathText : string;
        private panelDescription : string;
        private value : IProjectSettingsValue;

        constructor() {
            super();
            this.headerText = "";
            this.nameText = "";
            this.descriptionText = "";
            this.vxVersionText = "";
            this.vxVersions = new ArrayList<string>();
            this.pathText = "";
            this.panelDescription = "";
            this.value = null;
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public DescriptionText($value? : string) : string {
            return this.descriptionText = Property.String(this.descriptionText, $value);
        }

        public VxVersionText($value? : string) : string {
            return this.vxVersionText = Property.String(this.vxVersionText, $value);
        }

        public AddVxVersion($value : string) : void {
            if (!this.vxVersions.Contains($value)) {
                this.vxVersions.Add($value);
            }
        }

        public getVxVersions() : ArrayList<string> {
            return this.vxVersions;
        }

        public PathText($value? : string) : string {
            return this.pathText = Property.String(this.pathText, $value);
        }

        public PanelDescription($value? : string) : string {
            return this.panelDescription = ObjectValidator.IsSet($value) ? $value : this.panelDescription;
        }

        public Value($value? : IProjectSettingsValue) : IProjectSettingsValue {
            return this.value = ObjectValidator.IsSet($value) ? $value : this.value;
        }
    }
}
