/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ExportPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ExportPanelViewerArgs;
    import ExportPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ExportPanel;

    export class ExportPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ExportPanelViewerArgs {
            const args : ExportPanelViewerArgs = new ExportPanelViewerArgs();
            args.HeaderText("Project export");
            args.ExportButtonText("Export");
            args.CloseButtonText("Cancel");
            args.PlatformText("Target platform");
            args.AddPlatform("Windows GCC", "WinGCC");
            args.AddPlatform("Linux", "Linux");
            args.ExportPathText("Export path");
            args.ExportPathValue("C:/VisionSDK/ExportPackage.zip");
            return args;
        }

        /* dev:end */

        constructor($args? : ExportPanelViewerArgs) {
            super($args);
            this.setInstance(new ExportPanel());
        }

        public getInstance() : ExportPanel {
            return <ExportPanel>super.getInstance();
        }

        public ViewerArgs($args? : ExportPanelViewerArgs) : ExportPanelViewerArgs {
            return <ExportPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ExportPanel, $args : ExportPanelViewerArgs) : void {
            $instance.headerLabel.Text($args.HeaderText());
            $instance.exportButton.Text($args.ExportButtonText());
            $instance.closeButton.Text($args.CloseButtonText());
            $instance.platformLabel.Text($args.PlatformText());
            $instance.platformType.Clear();
            $instance.platformType.Height(-1);
            $args.getPlatforms().foreach(($text : string, $value : string) : void => {
                $instance.platformType.Add($text, $value);
            });
            $instance.platformType.Select(0);
            $instance.exportPathLabel.Text($args.ExportPathText());
            $instance.exportPath.Value($args.ExportPathValue());
        }

        /* dev:start */
        protected testImplementation($instance : ExportPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ExportPanel{border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
