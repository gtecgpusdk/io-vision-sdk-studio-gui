/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class PropertiesPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private nameText : string;
        private nameValue : string;
        private typeText : string;
        private typeValue : string;

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public NameValue($value? : string) : string {
            return this.nameValue = Property.String(this.nameValue, $value);
        }

        public TypeText($value? : string) : string {
            return this.typeText = Property.String(this.typeText, $value);
        }

        public TypeValue($value? : string) : string {
            return this.typeValue = Property.String(this.typeValue, $value);
        }
    }
}
