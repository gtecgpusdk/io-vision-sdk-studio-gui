/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IBuildPlatform = Io.VisionSDK.Studio.Gui.Interfaces.IBuildPlatform;

    export class ToolbarPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private projectMenuText : string;
        private saveButtonText : string;
        private loadButtonText : string;
        private undoButtonText : string;
        private redoButtonText : string;
        private runButtonText : string;
        private openButtonText : string;
        private consoleButtonText : string;
        private helpButtonText : string;
        private configureBuildText : string;
        private updateButtonText : string;
        private updateButtonHint : string;
        private buildPlatforms : IBuildPlatform[];

        constructor() {
            super();
            this.projectMenuText = "";
            this.saveButtonText = "";
            this.loadButtonText = "";
            this.undoButtonText = "";
            this.redoButtonText = "";
            this.runButtonText = "";
            this.openButtonText = "";
            this.consoleButtonText = "";
            this.helpButtonText = "";
            this.configureBuildText = "";
            this.updateButtonText = "";
            this.updateButtonHint = "";
            this.buildPlatforms = [];
        }

        public ProjectMenuText($value? : string) : string {
            return this.projectMenuText = Property.String(this.projectMenuText, $value);
        }

        public SaveButtonText($value? : string) : string {
            return this.saveButtonText = Property.String(this.saveButtonText, $value);
        }

        public UndoButtonText($value? : string) : string {
            return this.undoButtonText = Property.String(this.undoButtonText, $value);
        }

        public RedoButtonText($value? : string) : string {
            return this.redoButtonText = Property.String(this.redoButtonText, $value);
        }

        public RunButtonText($value? : string) : string {
            return this.runButtonText = Property.String(this.runButtonText, $value);
        }

        public OpenButtonText($value? : string) : string {
            return this.openButtonText = Property.String(this.openButtonText, $value);
        }

        public ConsoleButtonText($value? : string) : string {
            return this.consoleButtonText = Property.String(this.consoleButtonText, $value);
        }

        public HelpButtonText($value? : string) : string {
            return this.helpButtonText = Property.String(this.helpButtonText, $value);
        }

        public ConfigureBuildText($value? : string) : string {
            return this.configureBuildText = Property.String(this.configureBuildText, $value);
        }

        public UpdateButtonText($value? : string) : string {
            return this.updateButtonText = Property.String(this.updateButtonText, $value);
        }

        public UpdateButtonHint($value? : string) : string {
            return this.updateButtonHint = Property.String(this.updateButtonHint, $value);
        }

        public BuildPlatforms($value? : IBuildPlatform[]) : IBuildPlatform[] {
            if (ObjectValidator.IsSet($value)) {
                this.buildPlatforms = $value;
            }
            return this.buildPlatforms;
        }
    }
}
