/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IToolchainSettings = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettings;

    export class ToolchainSettingsPanelViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private nameText : string;
        private toolchainHint : string;
        private cmakeText : string;
        private cppText : string;
        private linkerText : string;
        private panelDescription : string;
        private value : IToolchainSettings;
        private validateButtonText : string;

        constructor() {
            super();

            this.headerText = "";
            this.nameText = "";
            this.toolchainHint = "";
            this.cmakeText = "";
            this.cppText = "";
            this.panelDescription = "";
            this.value = null;
            this.validateButtonText = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public NameText($value? : string) : string {
            return this.nameText = Property.String(this.nameText, $value);
        }

        public ToolchainHint($value? : string) : string {
            return this.toolchainHint = Property.String(this.toolchainHint, $value);
        }

        public CmakeText($value? : string) : string {
            return this.cmakeText = Property.String(this.cmakeText, $value);
        }

        public CppText($value? : string) : string {
            return this.cppText = Property.String(this.cppText, $value);
        }

        public LinkerText($value? : string) : string {
            return this.linkerText = Property.String(this.linkerText, $value);
        }

        public PanelDescription($value? : string) : string {
            return this.panelDescription = ObjectValidator.IsSet($value) ? $value : this.panelDescription;
        }

        public Value($value? : IToolchainSettings) : IToolchainSettings {
            return this.value = ObjectValidator.IsSet($value) ? $value : this.value;
        }

        public ValidateButtontext($value? : string) : string {
            return this.validateButtonText = Property.String(this.validateButtonText, $value);
        }
    }
}
