/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class VisualGraphPanelEventType extends EventType {
        public static readonly ON_NODE_DRAG : string = "OnNodeDrag";
        public static readonly ON_ROOT_CHANGE_START : string = "OnRootChangeStart";
        public static readonly ON_ROOT_CHANGE_COMPLETE : string = "OnRootChangeComplete";
        public static readonly ON_ZOOM_CHANGE : string = "OnZoomChange";
        public static readonly ON_STATE_CHANGE : string = "OnStateChange";
        public static readonly ON_DRAG_ACTION_START : string = "OnDragActionStart";
        public static readonly ON_DRAG_ACTION_COMPLETE : string = "OnDragActionComplete";
    }
}
