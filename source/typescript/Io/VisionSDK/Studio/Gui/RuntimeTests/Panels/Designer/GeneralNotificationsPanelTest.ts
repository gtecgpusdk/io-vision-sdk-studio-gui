/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import GeneralNotificationsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.GeneralNotificationsPanel;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;

    export class GeneralNotificationsPanelTest extends ViewerTestRunner<GeneralNotificationsPanel> {
        public testAddInfo() : void {
            const notificationPanel : GeneralNotificationsPanel = this.getInstance();
            notificationPanel.AddInfo("info");
            this.assertDeepEqual((<any>notificationPanel).notifications.getLast().toString(),
                "Io.VisionSDK.UserControls.BaseInterface.UserControls.NotificationMessage " +
                "(GeneralNotificationsPanel1548318158140181_Message1)");
        }

        public testAddNotification() : void {
            const notificationPanel : GeneralNotificationsPanel = this.getInstance();

            notificationPanel.AddNotification("Success", NotificationMessageType.GENERAL_SUCCESS);
            this.assertEquals(notificationPanel.Visible(), true);
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                // empty function
            }, 5000);
            notificationPanel.CloseAll();
            this.assertEquals(notificationPanel.Visible(), false);
        }

        protected before() : string {
            const instance : GeneralNotificationsPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.AddInfo("start-up info message");
            instance.AddWarning("start-up warning message");
            instance.AddSuccess("start-up success message");

            // this.addTestButton("Show", () : void => {
            //     $instance.Visible(true);
            // });
            // this.addTestButton("Hide", () : void => {
            //     $instance.Visible(false);
            // });
            //
            // let messagesCounter : number = 0;
            // this.addTestButton("Add info", () : void => {
            //     messagesCounter++;
            //     $instance.AddInfo("added info message " + messagesCounter);
            // });
            // this.addTestButton("Add warning", () : void => {
            //     messagesCounter++;
            //     $instance.AddWarning("added warning message " + messagesCounter, "WARNING:");
            // });
            // this.addTestButton("Add error", () : void => {
            //     messagesCounter++;
            //     $instance.AddError("added error message " + messagesCounter, "ERROR:");
            // });
            // this.addTestButton("Add success", () : void => {
            //     messagesCounter++;
            //     $instance.AddSuccess("added success message " + messagesCounter);
            // });
            // this.addTestButton("Close all", () : void => {
            //     $instance.CloseAll();
            // });

            return "<style>.TestCss {position: absolute; top: 100px; left: 200px;}</style>";
        }
    }
}
/* dev:end */
