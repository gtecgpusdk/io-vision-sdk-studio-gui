/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import KernelNodeHelpPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.KernelNodeHelpPanelViewerArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class KernelNodeHelpPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public header : Label;
        public nodeName : Label;
        public description : Label;
        public nodeInfo : Label;
        public link : Label;
        private specsLink : string;
        private specsEnabled : boolean;
        private specs : HTMLIFrameElement;
        private readonly initWidth : number;
        private sizeLimit : Size;
        private readonly specsWidth : number;
        private readonly infoTotalPadding : number;

        constructor($id? : string) {
            super($id);
            this.header = new Label();
            this.nodeName = new Label();
            this.description = new Label();
            this.nodeInfo = new Label();
            this.link = new Label();
            this.specsEnabled = false;
            this.specsLink = "";
            this.initWidth = 400;
            this.specsWidth = 1000;
            this.infoTotalPadding = 60;
            this.sizeLimit = new Size();
        }

        public Value($value? : KernelNodeHelpPanelViewerArgs) : KernelNodeHelpPanelViewerArgs {
            return <KernelNodeHelpPanelViewerArgs>super.Value($value);
        }

        public NavigateTo($value? : string) : void {
            if (this.IsCompleted() && this.specsEnabled && this.specsLink !== $value) {
                this.ResetView();
            }
            this.specsLink = Property.String(this.specsLink, $value);
        }

        public SizeLimit($value? : Size) : Size {
            this.sizeLimit = ObjectValidator.IsSet($value) ? $value : this.sizeLimit;
            if (ObjectValidator.IsSet($value)) {
                this.recomputeWidth();
                this.recomputeHeight();
                const parent : Dialog = <Dialog>this.Parent();
                if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsMemberOf(Dialog)) {
                    ElementManager.setCssProperty(parent.Id() + "_Envelop", "top", 0);
                    ElementManager.setCssProperty(parent.Id() + "_Envelop", "left", 0);
                    parent.Width(this.Width());
                    parent.Height(this.Height());
                    (<any>Dialog).resize(parent, this.Width(), this.Height());
                }
            }
            return this.sizeLimit;
        }

        public ResetView() : void {
            this.specsEnabled = false;
            ElementManager.Hide(this.Id() + "_SpecsEnvelop");
            ElementManager.Show(this.Id() + "_InfoEnvelop");
            this.recomputeWidth();
            this.recomputeHeight();
            const parent : Dialog = <Dialog>this.Parent();
            (<any>Dialog).resize(parent, this.Width(), this.Height());
        }

        protected recomputeWidth() : void {
            if (this.specsEnabled) {
                const widthMult : number = this.sizeLimit.Width() / this.specsWidth;
                if (widthMult < 1) {
                    ElementManager.setWidth(this.Id() + "_Specs", this.specsWidth * widthMult);
                    this.Width(this.specsWidth * widthMult);
                } else {
                    ElementManager.setWidth(this.Id() + "_Specs", this.specsWidth);
                    this.Width(this.specsWidth);
                }
            } else {
                if (this.initWidth > this.sizeLimit.Width()) {
                    this.Width(this.initWidth - (this.initWidth - this.sizeLimit.Width()));
                } else {
                    this.Width(this.initWidth);
                }
                ElementManager.setWidth(this.Id() + "_InfoEnvelop", this.Width() - this.infoTotalPadding);
            }
        }

        protected recomputeHeight() : void {
            this.Height(this.sizeLimit.Height());
            if (!ObjectValidator.IsEmptyOrNull(this.specs)) {
                ElementManager.setHeight(this.specs.id, this.Height());
            }
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);
            this.Width(this.initWidth);
            this.Height(600);

            this.sizeLimit.Width(this.Width());
            this.sizeLimit.Height(this.Height());

            this.specs = document.createElement("iframe");
            this.specs.width = this.specsWidth + "px";
            this.specs.height = this.Height() + "px";
            this.specs.id = this.Id() + "_Specs";

            this.nodeName.StyleClassName("Name");
            this.nodeInfo.StyleClassName("Info");
            this.link.StyleClassName("SpecsLink");
            this.link.getEvents().setOnClick(() : void => {
                if (!this.specsEnabled) {
                    this.specsEnabled = true;
                    const specs : HTMLIFrameElement = (<HTMLIFrameElement>ElementManager.getElement(this.specs.id));
                    specs.src = this.specsLink;
                    ElementManager.Hide(this.Id() + "_InfoEnvelop");
                    ElementManager.Show(this.Id() + "_SpecsEnvelop");
                    this.recomputeWidth();
                    const parent : Dialog = <Dialog>this.Parent();
                    if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsMemberOf(Dialog)) {
                        ElementManager.setCssProperty(parent.Id() + "_Envelop", "top", 0);
                        ElementManager.setCssProperty(parent.Id() + "_Envelop", "left", 0);
                        parent.Width(this.Width());
                        parent.Height(this.Height());
                        (<any>Dialog).resize(parent, this.Width(), this.Height());
                    }
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement(this.Id() + "_SpecsEnvelop")
                    .Visible(this.specsEnabled)
                    .StyleClassName("SpecsEnvelop")
                    .Add(this.specs))
                .Add(this.addElement(this.Id() + "_InfoEnvelop")
                    .StyleClassName("InfoEnvelop").Width(340)
                    .Add(this.header)
                    .Add(this.nodeName)
                    .Add(this.description)
                    .Add(this.nodeInfo)
                    .Add(this.link)
                );
        }
    }
}
