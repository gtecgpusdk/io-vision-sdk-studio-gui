/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Dialogs {
    "use strict";
    import ProjectPickerPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Dialogs.ProjectPickerPanelViewerArgs;
    import ProjectPickerPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs.ProjectPickerPanel;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class ProjectPickerPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ProjectPickerPanelViewerArgs {
            const args : ProjectPickerPanelViewerArgs = new ProjectPickerPanelViewerArgs();
            args.HeaderText("Out of box examples and templates");
            args.LoadButtonText("Load");
            return args;
        }

        /* dev:end */

        constructor($args? : ProjectPickerPanelViewerArgs) {
            super($args);
            this.setInstance(new ProjectPickerPanel());
        }

        public getInstance() : ProjectPickerPanel {
            return <ProjectPickerPanel>super.getInstance();
        }

        public ViewerArgs($args? : ProjectPickerPanelViewerArgs) : ProjectPickerPanelViewerArgs {
            return <ProjectPickerPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ProjectPickerPanel, $args : ProjectPickerPanelViewerArgs) : void {
            $instance.header.Text($args.HeaderText());
            $instance.loadButton.Text($args.LoadButtonText());
        }

        /* dev:start */
        protected testImplementation($instance : ProjectPickerPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(700);
            $instance.Height(500);
            $instance.selectionPanel.Width(210);
            $instance.selectionPanel.Height(430);
            $instance.descriptionPanel.Width(490);

            for (let index : number = 1; index <= 10; index++) {
                $instance.AddProject({
                    description    : "description",
                    name           : "Test " + index,
                    path           : "...",
                    vxVersion      : "1.0.1",
                    vxVersionPrefix: "OVX "
                });
            }

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ProjectPickerPanel {border: 1px solid #E7E7E8; background-color: #F8F8F8;}</style>";
        }

        /* dev:end */
    }
}
