/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Gui.RuntimeTests.Panels.Designer {
    "use strict";
    import ViewerTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.ViewerTestRunner;
    import ToolbarPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.ToolbarPanel;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IViewerTestPromise = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.IViewerTestPromise;
    import BuildSettingsEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.BuildSettingsEventArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class ToolbarPanelTest extends ViewerTestRunner<ToolbarPanel> {

        public testsetPlatforms() : void {
            const toolbar : ToolbarPanel = this.getInstance();
            toolbar.setPlatforms([{value: "android", text: "Android"}]);
            this.assertEquals(JSON.stringify(toolbar.getPlatforms()).toString(),
                JSON.stringify([{value: "android", text: "Android"}]).toString());
        }

        public __IgnoretestBuildPlatformChange() : IViewerTestPromise {
            // const page : EditorPage = this.getInstance();
            const instance : ToolbarPanel = this.getInstance();
            const eventArgs : BuildSettingsEventArgs = new BuildSettingsEventArgs();
            return ($done : () => void) : void => {
                (<any>toolbar).platformList.list.getEvents().setOnFocus(($eventArgs : MouseEventArgs) : void => {
                    //   page.getEvents().FireAsynchronousMethod(() => void{
                    (<any>toolbar).getEvents().setOnBuildPlatformChange(($eventArgs : BuildSettingsEventArgs) : void => {

                        //                     //$eventArgs.BuildPlatform()
                        this.assertEquals($eventArgs.Owner(), toolbar);
                        $done();
                    });
                    ElementManager.getElement((<any>toolbar).platformList.list.Id() + "_Item_2_Status").click();
                }, 200);
                // });
                this.emulateEvent((<any>toolbar).platformList.list, EventType.ON_CLICK, <MouseEvent>{clientX: 0, clientY: 0});
            };
        }

        protected before() : string {
            const instance : ToolbarPanel = this.getInstance();
            instance.StyleClassName("TestCss");
            instance.Width(1024);
            instance.Height(34);

            // this.addTestButton("Show/Hide status", () : void => {
            //     if (!instance.statusBar.Visible()) {
            //         instance.statusBar.Visible(true);
            //         instance.statusBar.Text("Test status ...");
            //     } else {
            //         instance.statusBar.Visible(false);
            //     }
            // });
            //
            // instance.platformList.getGuiOptions().Add(GuiOptionType.DISABLE);
            // this.addTestButton("Disable/Enable platform", () : void => {
            //     instance.platformList.Enabled(!instance.platformList.Enabled());
            // });
            //
            // this.addTestButton("Update", () : void => {
            //     instance.updateButton.Visible(true);
            // });

            return "<style>.TestCss .ToolbarPanel {border: 1px solid #E7E7E8;}</style>";
        }
    }
}
/* dev:end */
