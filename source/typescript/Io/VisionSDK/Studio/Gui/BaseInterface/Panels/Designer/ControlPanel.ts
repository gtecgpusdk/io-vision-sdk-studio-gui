/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DropDownButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownButton;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import NumberPicker = Io.VisionSDK.UserControls.BaseInterface.UserControls.NumberPicker;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IDropDownButton = Io.VisionSDK.UserControls.Interfaces.IDropDownButton;
    import IImageButton = Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton;
    import INumberPicker = Com.Wui.Framework.Gui.Interfaces.UserControls.INumberPicker;
    import ControlPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.ControlPanelViewerArgs;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import IResponsiveElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IResponsiveElement;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import ControlPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.ControlPanelEventType;
    import IControlPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IControlPanelEvents;
    import NumberPickerType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import ControlPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.ControlPanelType;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import IControlPanelZoomConfiguration = Io.VisionSDK.Studio.Gui.Interfaces.IControlPanelZoomConfiguration;
    import IControlPanelResizeConfiguration = Io.VisionSDK.Studio.Gui.Interfaces.IControlPanelResizeConfiguration;
    import IControlPanelType = Io.VisionSDK.Studio.Gui.Interfaces.IControlPanelType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class ControlPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        private static readonly buttonNum : number = 6;
        private readonly buttons : ArrayList<IDropDownButton>;
        private readonly picker : INumberPicker;
        private readonly minusButton : IImageButton;
        private readonly plusButton : IImageButton;
        private readonly resetButton : IImageButton;
        private buttonArgs : ArrayList<IDropDownButtonArgs>;
        private resizeConfiguration : IControlPanelResizeConfiguration;
        private zoomConfiguration : IControlPanelZoomConfiguration;
        private guiType : IControlPanelType;

        constructor($id? : string) {
            super($id);
            const dropDownButtonClass : any = this.getDropDownButtonClass();
            const numberPickerClass : any = this.getNumberPickerClass();
            const imageButtonClass : any = this.getImageButtonClass();

            this.buttons = new ArrayList<IDropDownButton>();
            this.picker = new numberPickerClass();
            this.minusButton = new imageButtonClass();
            this.plusButton = new imageButtonClass();
            this.resetButton = new imageButtonClass();

            for (let i = 0; i < ControlPanel.buttonNum; i++) {
                const button : IDropDownButton = new dropDownButtonClass();
                this.buttons.Add(button);
                button.Visible(false);
                this["button" + i] = button;
            }
            this.buttonArgs = new ArrayList<IDropDownButtonArgs>();

            this.ZoomConfiguration({
                defaultValue: 100,
                maxValue    : 125,
                minValue    : 75,
                valueStep   : 5
            });

            this.GuiType({
                childTypes: {
                    minusButtonType: ImageButtonType.ARROW_LEFT_WHITE,
                    pickerType     : NumberPickerType.ZOOM_WHITE,
                    plusButtonType : ImageButtonType.ARROW_RIGHT_WHITE,
                    resetButtonType: ImageButtonType.COMPASS
                },
                type      : ControlPanelType.ORANGE
            });

            this.ResizeConfiguration({
                getBreadcrumbButtonWidth: ($index : number) : string | string[] => {
                    return undefined;
                },
                getControlMenuWidth     : () : string | string[] => {
                    if (this.Width() > 1000) {
                        return "250px";
                    } else {
                        return 250 - this.Height() * 2 + "px";
                    }
                },
                isControlButtonsVisible : () : boolean => {
                    return this.Width() > 1000;
                }
            });
        }

        public Value($value? : ControlPanelViewerArgs) : ControlPanelViewerArgs {
            return <ControlPanelViewerArgs>super.Value($value);
        }

        public setLocalization($value : ControlPanelViewerArgs) : void {
            this.resetButton.Title().Text($value.ResetButtonTooltipText());
        }

        public setBreadcrumbButtons($args : IDropDownButtonArgs[]) : void {
            const apply : any = () : void => {
                this.hideButtons();
                this.buttonArgs = ArrayList.ToArrayList($args);
                $args.forEach(($buttonArgs : IDropDownButtonArgs, $index : number) : void => {
                    if ($index < ControlPanel.buttonNum) {
                        const button : IDropDownButton = this.buttons.getItem($index);
                        button.Value($buttonArgs);
                        button.Visible(true);
                    } else {
                        LogIt.Warning("Button " + $buttonArgs.activeIndex + " omitted, limit reached");
                    }
                });
            };

            if (this.IsCompleted()) {
                apply();
            } else {
                this.getEvents().setOnComplete(() : void => {
                    apply();
                });
            }
        }

        public Zoom($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                const normalizedZoom : number = Math.max(Math.min($value, this.picker.RangeEnd()), this.picker.RangeStart());
                if (normalizedZoom !== this.picker.Value()) {
                    this.picker.Value(normalizedZoom);
                    if (this.picker.IsCompleted()) {
                        const numberPickerClass : any = this.getNumberPickerClass();
                        numberPickerClass.NotificationShow(this.picker);
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            numberPickerClass.NotificationHide();
                        }, true, 2000);
                    }
                }
            }
            return this.picker.Value();
        }

        public ZoomConfiguration($value? : IControlPanelZoomConfiguration) : IControlPanelZoomConfiguration {
            if (ObjectValidator.IsSet($value)) {
                this.zoomConfiguration = $value;
                this.picker.RangeStart(this.zoomConfiguration.minValue);
                this.picker.RangeEnd(this.zoomConfiguration.maxValue);
                this.picker.ValueStep(this.zoomConfiguration.valueStep);
                this.Zoom(this.zoomConfiguration.defaultValue);
            }
            return this.zoomConfiguration;
        }

        public ResizeConfiguration($value? : IControlPanelResizeConfiguration) : IControlPanelResizeConfiguration {
            if (ObjectValidator.IsSet($value)) {
                this.resizeConfiguration = $value;
                if (this.IsCompleted()) {
                    ControlPanel.resize(this, this.Width(), this.Height());
                }
            }
            return this.resizeConfiguration;
        }

        public GuiType($controlPanelType? : IControlPanelType) : IControlPanelType {
            if (ObjectValidator.IsSet($controlPanelType)) {
                this.guiType = $controlPanelType;
                this.minusButton.GuiType(this.guiType.childTypes.minusButtonType);
                this.plusButton.GuiType(this.guiType.childTypes.plusButtonType);
                this.picker.GuiType(this.guiType.childTypes.pickerType);
                this.resetButton.GuiType(this.guiType.childTypes.resetButtonType);
                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.type);
                }
            }
            return this.guiType;
        }

        public EnableMouseEvents($value : boolean) {
            if ($value) {
                ElementManager.ClearCssProperty(this, "pointer-events");
            } else {
                ElementManager.setCssProperty(this, "pointer-events", "none");
            }
        }

        public getEvents() : IControlPanelEvents {
            return <IControlPanelEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnComplete(() : void => {
                ElementManager.setOpacity(this.Id(), 100);
            });

            this.getEvents().setBeforeResize(() : void => {
                const buttonsVisible : boolean = this.resizeConfiguration.isControlButtonsVisible();
                if (this.minusButton.Visible() !== buttonsVisible) {
                    this.minusButton.Visible(buttonsVisible);
                    this.plusButton.Visible(buttonsVisible);
                }
            });

            this.picker.getEvents().setOnChange(() : void => {
                this.getEventsManager().FireEvent(this, ControlPanelEventType.ON_ZOOM_CHANGE);
            });

            this.resetButton.getEvents().setOnClick(() : void => {
                this.getEventsManager().FireEvent(this, ControlPanelEventType.ON_RESET);
            });

            let moveHandle : number = null;
            const moveZoom : any = ($movePerTick : number, $repeat? : boolean) : void => {
                const handler : any = () : void => {
                    const prevZoom = this.Zoom();
                    this.Zoom(this.Zoom() + $movePerTick);
                    if ($repeat && prevZoom !== this.Zoom()) {
                        moveZoom($movePerTick, $repeat);
                    }
                };
                if ($repeat) {
                    moveHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                        handler();
                    }, 100);
                } else {
                    handler();
                }
            };

            this.plusButton.getEvents().setOnMouseDown(() : void => {
                moveHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    moveZoom(this.picker.ValueStep(), true);
                }, 200);
            });

            this.minusButton.getEvents().setOnMouseDown(() : void => {
                moveHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    moveZoom(-this.picker.ValueStep(), true);
                }, 200);
            });

            this.plusButton.getEvents().setOnClick(() : void => {
                moveZoom(+this.picker.ValueStep());
            });

            this.minusButton.getEvents().setOnClick(() : void => {
                moveZoom(-this.picker.ValueStep());
            });

            WindowManager.getEvents().setOnMouseUp(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(ControlPanel).foreach(($panel : ControlPanel) : void => {
                    if ($panel.IsCompleted() && $panel.Visible()) {
                        clearTimeout(moveHandle);
                    }
                });
            });

            return super.innerCode();
        }

        protected getNumberPickerClass() : any {
            return NumberPicker;
        }

        protected getImageButtonClass() : any {
            return ImageButton;
        }

        protected getDropDownButtonClass() : any {
            return DropDownButton;
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            const buttons : IResponsiveElement =
                this.addRow(this.Id() + "_ButtonEnvelop")
                    .FitToParent(FitToParent.FULL)
                    .Alignment(Alignment.LEFT_PROPAGATED)
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED);
            this.buttons.foreach(($button : IDropDownButton, $index : number) : void => {
                buttons.Add(this.addColumn()
                    .WidthOfColumn(() : PropagableNumber => {
                        return new PropagableNumber(this.resizeConfiguration.getBreadcrumbButtonWidth($index));
                    })
                    .Add($button));
            });

            return this.addRow(this.Id() + "_Type")
                .StyleClassName(this.GuiType().type)
                .Add(buttons)
                .Add(this.addColumn(this.Id() + "_ControlMenu")
                    .WidthOfColumn(() : PropagableNumber => {
                        return new PropagableNumber(this.resizeConfiguration.getControlMenuWidth());
                    })
                    .StyleClassName("ControlMenu")
                    .Add(this.addRow()
                        .Alignment(Alignment.CENTER_PROPAGATED)
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                        .Add(this.addRow()
                            .StyleClassName("ZoomMenu")
                            .Add(this.addColumn()
                                .WidthOfColumn(() : PropagableNumber => {
                                    return new PropagableNumber(this.Height() * .5 + "px");
                                }))
                            .Add(this.addColumn()
                                .WidthOfColumn(this.Height(), UnitType.PX)
                                .StyleClassName("ZoomButton")
                                .Add(this.minusButton))
                            .Add(this.addColumn()
                                .FitToParent(FitToParent.FULL)
                                .StyleClassName("ZoomPicker")
                                .Add(this.picker))
                            .Add(this.addColumn()
                                .WidthOfColumn(() : PropagableNumber => {
                                    return new PropagableNumber(this.Height() + "px");
                                })
                                .StyleClassName("ZoomButton")
                                .Add(this.plusButton)))
                        .Add(this.addColumn()
                            .WidthOfColumn(() : PropagableNumber => {
                                return new PropagableNumber(this.Height() * .25 + "px");
                            }))
                        .Add(this.addColumn()
                            .WidthOfColumn(() : PropagableNumber => {
                                return new PropagableNumber(this.Height() * 1.05 + "px");
                            })
                            .Add(this.resetButton))));
        }

        private hideButtons() : void {
            this.buttons.foreach(($button : IDropDownButton) : void => {
                $button.Visible(false);
            });
        }
    }
}
