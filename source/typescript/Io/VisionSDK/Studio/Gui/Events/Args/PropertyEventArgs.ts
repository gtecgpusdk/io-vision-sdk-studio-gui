/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * PropertyEventArgs class provides args connected with modification of KernelNode property.
     */
    export class PropertyEventArgs extends EventArgs {
        private name : string;
        private propertyType : string;
        private value : any;

        /**
         * @param {string} [$value] Set property name.
         * @return {string} Returns property name.
         */
        public Name($value? : string) : string {
            return this.name = Property.String(this.name, $value);
        }

        /**
         * @param {string} [$value] Set property type.
         * @return {string} Returns property type.
         */
        public PropertyType($value? : string) : string {
            return this.propertyType = Property.String(this.propertyType, $value);
        }

        /**
         * @param {any} [$value] Set value represented by current property name.
         * @return {any} Returns value represented by current property name.
         */
        public Value($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.value = $value;
            }
            return this.value;
        }
    }
}
