/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import IBuildSettingsEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IBuildSettingsEvents;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ConnectionSettingsPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.ConnectionSettingsPanelViewerArgs;
    import IConnectionSettings = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettings;
    import IConnectionSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettingsValue;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;

    export class ConnectionSettingsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public headerText : string;
        public nameLabel : Label;
        public addressLabel : Label;
        public usernameLabel : Label;
        public passwordLabel : Label;
        public platformLabel : Label;
        public agentNameLabel : Label;
        public connectionOption : DropDownList;
        public address : TextField;
        public username : TextField;
        public password : TextField;
        public platform : TextField;
        public agentName : TextField;
        public panelDescription : string;
        public values : ArrayList<IConnectionSettingsValue>;
        private selected : number;
        private isEnabled : boolean;
        private fieldSizeHandler : () => (string | string[]);

        constructor($id? : string) {
            super($id);
            this.headerText = "";
            this.nameLabel = new Label();
            this.addressLabel = new Label();
            this.usernameLabel = new Label();
            this.passwordLabel = new Label();
            this.platformLabel = new Label();
            this.agentNameLabel = new Label();
            this.connectionOption = new DropDownList(DropDownListType.SETTING);
            this.address = new TextField(TextFieldType.SETTING);
            this.username = new TextField(TextFieldType.SETTING);
            this.password = new TextField(TextFieldType.SETTING);
            this.platform = new TextField(TextFieldType.SETTING);
            this.agentName = new TextField(TextFieldType.SETTING);
            this.panelDescription = "";
            this.values = new ArrayList<IConnectionSettingsValue>();
            this.selected = 0;
            this.isEnabled = true;
            this.fieldSizeHandler = () : string => {
                return undefined;
            };
        }

        public Enabled($value? : boolean) : boolean {
            this.isEnabled = Property.Boolean(this.isEnabled, $value);
            [
                this.address, this.username, this.password, this.platform, this.agentName
            ].forEach(($commons : GuiCommons) : void => {
                $commons.Enabled(this.isEnabled);
            });
            return this.isEnabled;
        }

        public Value($value? : ConnectionSettingsPanelViewerArgs) : ConnectionSettingsPanelViewerArgs {
            return <ConnectionSettingsPanelViewerArgs>super.Value($value);
        }

        public getSettings() : IConnectionSettings {
            const settings : IConnectionSettings = {
                options: JSON.parse(JSON.stringify(this.values.ToArray()))
            };

            const selected : IConnectionSettingsValue = settings.options[this.selected];
            if (!ObjectValidator.IsEmptyOrNull(selected)) {
                selected.name = this.connectionOption.Value();
                selected.address = this.address.Value();
                selected.username = this.username.Value();
                selected.password = this.password.Value();
                selected.platform = this.platform.Value();
                selected.agentName = this.agentName.Value();
            }

            return settings;
        }

        public ApplySettings($value? : IConnectionSettings) : void {
            const replacer : any = (key : any, val : any) : void => {
                if (key !== "selected") {
                    return val;
                }
            };
            const apply : any = ($settings : IConnectionSettings) : void => {
                const value : IConnectionSettingsValue = $settings.options[this.selected];
                this.address.Value(value.address);
                this.username.Value("");
                this.password.Value("");
                this.platform.Value("");
                this.agentName.Value("");
                if (ObjectValidator.IsEmptyOrNull(value.agentName)) {
                    this.username.Value(value.username);
                    this.password.Value(value.password);
                } else {
                    this.platform.Value(value.platform);
                    this.agentName.Value(value.agentName);
                }

                this.values.Clear();
                this.connectionOption.Clear();
                $settings.options.forEach(($option : IConnectionSettingsValue) : void => {
                    this.values.Add($option, $option.name);
                    this.connectionOption.Add($option.name);
                });
                this.connectionOption.Select(value.name);

                const args : ConnectionSettingsPanelViewerArgs = this.Value();
                if (!ObjectValidator.IsEmptyOrNull(args)) {
                    args.Value($settings);
                }
            };
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (JSON.stringify(this.getSettings(), replacer) !== JSON.stringify($value, replacer)) {
                    apply($value);
                }
            } else {
                apply(this.getSettings());
            }
        }

        public setFieldSizeHandler($value : () => string | string[]) : void {
            this.fieldSizeHandler = $value;
        }

        public getEvents() : IBuildSettingsEvents {
            return <IBuildSettingsEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            [
                this.connectionOption, this.address, this.username, this.password, this.platform, this.agentName
            ].forEach(($element : any) : void => {
                $element.getEvents().setOnChange(() : void => {
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                });
            });

            this.connectionOption.getEvents().setOnSelect(() : void => {
                const selected : IConnectionSettingsValue = this.values.getItem(this.connectionOption.Value());
                this.address.Value(selected.address);
                this.username.Value("");
                this.password.Value("");
                this.platform.Value("");
                this.agentName.Value("");
                if (ObjectValidator.IsEmptyOrNull(selected.agentName)) {
                    this.username.Value(selected.username);
                    this.password.Value(selected.password);
                } else {
                    this.platform.Value(selected.platform);
                    this.agentName.Value(selected.agentName);
                }
                this.selected = this.values.IndexOf(selected);
            });

            this.password.setPasswordEnabled();
            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .FitToParent(FitToParent.FULL)
                .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                .Add(this.addRow()
                    .Add(this.addColumn()
                        .WidthOfColumn(10, UnitType.PCT))
                    .Add(this.addColumn()
                        .WidthOfColumn(180, UnitType.PX)
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.nameLabel)
                        .Add(this.addressLabel)
                        .Add(this.platformLabel)
                        .Add(this.agentNameLabel)
                        .Add(this.usernameLabel)
                        .Add(this.passwordLabel)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            return new PropagableNumber(this.fieldSizeHandler());
                        })
                        .HeightOfRow(70, UnitType.PX, true)
                        .Add(this.connectionOption)
                        .Add(this.address)
                        .Add(this.platform)
                        .Add(this.agentName)
                        .Add(this.username)
                        .Add(this.password)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(10, UnitType.PCT))
                );
        }
    }
}
