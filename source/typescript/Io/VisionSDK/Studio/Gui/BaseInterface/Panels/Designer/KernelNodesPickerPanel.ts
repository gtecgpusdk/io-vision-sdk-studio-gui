/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelNodesPickerPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.KernelNodesPickerPanelViewerArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import VisualGraphEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.VisualGraphEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IKernelNodesPickerPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.IKernelNodesPickerPanelEvents;
    import KernelNodeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNodeButton;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import Accordion = Io.VisionSDK.UserControls.BaseInterface.UserControls.Accordion;
    import AccordionViewer = Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls.AccordionViewer;
    import AccordionViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.AccordionViewerArgs;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import BasePanelHolder = Com.Wui.Framework.UserControls.Primitives.BasePanelHolder;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import NodesFilterPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.NodesFilterPanelViewer;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import ITypeFilter = Io.VisionSDK.Studio.Gui.Interfaces.ITypeFilter;

    export class KernelNodesPickerPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public accordion : Accordion;
        public nodesFilter : NodesFilterPanel;
        private types : ArrayList<IKernelNode>;
        private buttons : ArrayList<KernelNodeButton>;
        private readonly selectedButtons : ArrayList<KernelNodeButton>;
        private isSimpleView : boolean;
        private pickEnabled : boolean;
        private nodePicked : boolean;
        private lastSelectedButton : KernelNodeButton;
        private typeFilter : ITypeFilter;
        private isDragEnabled : boolean;
        private readonly groupNum : number = 10;
        private groupMap : ArrayList<number>;
        private nameFilter : string;
        private asyncLoadStatus : number;
        private contentHovered : boolean;
        private isMouseDown : boolean;
        private userOpenedHolders : ArrayList<number>;
        private anchorNodeButton : KernelNodeButton;
        private ctrlStatus : boolean;

        constructor($id? : string) {
            super($id);
            this.isSimpleView = false;
            this.pickEnabled = false;
            this.nodePicked = false;
            this.contentHovered = false;
            this.types = new ArrayList<IKernelNode>();
            this.buttons = new ArrayList<KernelNodeButton>();
            this.selectedButtons = new ArrayList<KernelNodeButton>();
            this.groupMap = new ArrayList<number>();
            this.typeFilter = null;
            this.nameFilter = null;
            this.isDragEnabled = true;
            this.asyncLoadStatus = 0;
            this.lastSelectedButton = null;
            this.isMouseDown = false;
            this.userOpenedHolders = new ArrayList<number>();
            this.anchorNodeButton = null;
            this.ctrlStatus = false;

            this.addChildPanel(NodesFilterPanelViewer, null,
                ($parent : KernelNodesPickerPanel, $child : NodesFilterPanel) : void => {
                    $parent.nodesFilter = $child;
                });

            const accordionArgs : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.VERTICAL);
            accordionArgs.ResizeType(AccordionResizeType.DEFAULT);
            for (let i = 0; i < this.groupNum; i++) {
                const holderArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
                holderArgs.HeaderText("");
                holderArgs.DescriptionText("");
                holderArgs.IsOpened(false);
                accordionArgs.AddPanelHoldersArgs(holderArgs);
            }

            this.addChildPanel(AccordionViewer, accordionArgs,
                ($parent : KernelNodesPickerPanel, $child : Accordion) : void => {
                    $parent.accordion = $child;
                });
        }

        public DeselectAll($applyToVisible : boolean = true) : void {
            this.selectedButtons.ToArray().forEach(($button : KernelNodeButton) : void => {
                const holder : BasePanelHolder = this.accordion
                    .getPanel(this.groupMap.getItem(this.types.getItem($button.Id()).getGroupId())).Parent();
                if ($applyToVisible ||
                    !(ElementManager.IsVisible(holder.Id() + "_PanelEnvelop") && holder.IsOpened() && $button.Visible())) {
                    $button.Selected(false);
                    if ($button === this.lastSelectedButton) {
                        this.lastSelectedButton = null;
                    }
                    this.selectedButtons.RemoveAt(this.selectedButtons.IndexOf($button));
                }
            });
        }

        public TypeFilter($value : ITypeFilter) : void {
            if ($value !== this.typeFilter) {
                this.DeselectAll(true);
            }
            this.typeFilter = $value;
        }

        public Filter($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.nameFilter = $value;
                if (this.asyncLoadStatus <= 0) {
                    this.buttons.foreach(($button : KernelNodeButton) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($button)) {
                            const type : IKernelNode = this.types.getItem($button.Id());
                            const name : string = ObjectValidator.IsEmptyOrNull(type.Help().name) ? type.Type() : type.Help().name;
                            $button.Visible(this.filterByType($button) &&
                                (ObjectValidator.IsEmptyOrNull($value) || (StringUtils.ContainsIgnoreCase(name, $value) ||
                                    StringUtils.ContainsIgnoreCase(type.Help().methodName, $value))));
                        }
                    });
                    this.computeGroupVisibility();
                    this.DeselectAll(false);
                }
            }
            return this.nameFilter;
        }

        public UserOpenedHolders($value? : ArrayList<number>, $computeGroupVisibility? : boolean) : ArrayList<number> {
            if (ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsEmptyOrNull($value)) {
                    this.userOpenedHolders = new ArrayList<number>();
                } else {
                    this.userOpenedHolders = $value;
                }
                if ($computeGroupVisibility) {
                    this.computeGroupVisibility();
                }
            }
            return this.userOpenedHolders;
        }

        public EnableDrag($value : boolean) {
            this.isDragEnabled = $value;
        }

        public EnableMouseEvents($value : boolean) {
            if ($value) {
                ElementManager.ClearCssProperty(this, "pointer-events");
            } else {
                ElementManager.setCssProperty(this, "pointer-events", "none");
            }
        }

        public getEvents() : IKernelNodesPickerPanelEvents {
            return <IKernelNodesPickerPanelEvents>super.getEvents();
        }

        public Value($value? : KernelNodesPickerPanelViewerArgs) : KernelNodesPickerPanelViewerArgs {
            return <KernelNodesPickerPanelViewerArgs>super.Value($value);
        }

        public setNodes($value : ArrayList<IKernelNode>) : void {
            this.ClearNodes();
            $value.foreach(($node : IKernelNode) => {
                this.AddNode($node);
            });
        }

        public ClearNodes() : void {
            this.buttons.Clear();
            this.types.Clear();
            this.groupMap.Clear();
            this.asyncLoadStatus = 0;

            if (this.IsCompleted()) {
                for (let i : number = 0; i < this.groupNum; i++) {
                    const groupPanel : BasePanel = this.accordion.getPanel(i);
                    groupPanel.getChildElements().Clear();
                    const content : HTMLElement = ElementManager.getElement(groupPanel.Id() + "_PanelContent");
                    if (!ObjectValidator.IsEmptyOrNull(content)) {
                        content.innerHTML = "";
                    }
                }
                this.computeGroupVisibility();
            }
        }

        public AddNode($value : IKernelNode) : void {
            if (!this.types.Contains($value)) {
                const child : KernelNodeButton = new KernelNodeButton($value.IconType());
                const name : string = ObjectValidator.IsEmptyOrNull($value.Help().name) ? $value.Type() : $value.Help().name;
                child.Text(name);
                child.getEvents().setOnMouseOver(() : void => {
                    this.contentHovered = true;
                    const eventArgs : VisualGraphEventArgs = new VisualGraphEventArgs();
                    eventArgs.Owner(child);
                    eventArgs.ActiveNode($value);
                    this.getEventsManager().FireEvent(this, EventType.ON_SELECT, eventArgs);
                });

                child.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                    this.ctrlStatus = !child.Selected();
                    this.isMouseDown = true;
                    this.pickEnabled = false;
                    this.anchorNodeButton = child;
                    this.focusButton($eventArgs, child);
                });

                child.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs) : void => {
                    if (!$eventArgs.NativeEventArgs().ctrlKey && !$eventArgs.NativeEventArgs().shiftKey) {
                        this.DeselectAll();
                        this.selectButton(child);
                    } else if ($eventArgs.NativeEventArgs().ctrlKey) {
                        this.selectButton(child, this.ctrlStatus);
                    }
                });

                if (!this.groupMap.KeyExists($value.getGroupId()) && this.groupMap.Length() < this.groupNum) {
                    this.groupMap.Add(this.groupMap.Length(), $value.getGroupId());
                }

                const panel : BasePanel = this.accordion.getPanel(this.groupMap.getItem($value.getGroupId()));
                if (this.groupMap.KeyExists($value.getGroupId())) {
                    panel.AddChild(child);
                }

                this.asyncLoadStatus++;
                child.getEvents().setOnComplete(() : void => {
                    this.asyncLoadStatus--;
                    if (this.asyncLoadStatus === 0) {
                        ElementManager.ClearCssProperty(this, "pointer-events");
                        for (let i : number = 0; i < this.groupNum; i++) {
                            const groupPanel : BasePanel = this.accordion.getPanel(i);
                            const holder : BasePanelHolder = <BasePanelHolder>groupPanel.Parent();
                            holder.headerLabel.Text(<string>this.groupMap.getKey(i));
                        }
                        this.Filter(this.nameFilter);
                        Accordion.recomputeContentSizes(this.accordion);
                        ElementManager.setOpacity(this.Id() + "_PanelContentEnvelop", 100);
                    }
                });

                this.buttons.Add(child);
                this.types.Add($value, child.Id());
            }
        }

        public IsNodePicked() : boolean {
            return this.nodePicked;
        }

        public IsContentHovered() : boolean {
            return this.contentHovered;
        }

        public getType($node : KernelNodeButton) : IKernelNode {
            return this.types.getItem($node.Id());
        }

        public getNode($type : KernelNodeType) : IKernelNode {
            let node : IKernelNode = null;
            this.types.foreach(($node : IKernelNode) : boolean => {
                if ($node.Type() === $type) {
                    node = $node;
                    return false;
                }
            });
            return node;
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(false);
            this.nodesFilter.filterField.getEvents().setEvent(EventType.ON_KEY_UP, () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.Filter(this.nodesFilter.filterField.Value());
                }, true, 150);
            });

            const parseOpenedHolders : any = () : void => {
                if (this.nameFilter === "") {
                    this.userOpenedHolders.Clear();
                    for (let i : number = 0; i < this.groupNum; i++) {
                        const innerPanel : BasePanel = this.accordion.getPanel(i);
                        const holder : BasePanelHolder = <BasePanelHolder>innerPanel.Parent();

                        if (holder.IsOpened()) {
                            this.userOpenedHolders.Add(i);
                        }
                    }
                }
            };

            for (let i : number = 0; i < this.groupNum; i++) {
                this.userOpenedHolders.Add(i);
            }

            this.accordion.getEvents().setOnChange(() : void => {
                this.DeselectAll(false);
                parseOpenedHolders();
            });

            this.accordion.AnimIterationNum(3);

            this.getEvents().setOnMouseOut(() : void => {
                if (this.isMouseDown && !this.nodePicked) {
                    this.pickEnabled = true;
                }
                this.contentHovered = false;
            });

            WindowManager.getEvents().setOnMouseUp(() : void => {
                this.nodePicked = false;
                this.pickEnabled = false;
                this.isMouseDown = false;
            });

            WindowManager.getEvents().setOnMove(() : void => {
                if (this.isMouseDown && !ObjectValidator.IsEmptyOrNull(this.selectedButtons) && !this.nodePicked
                    && this.isDragEnabled && this.pickEnabled) {
                    const eventArgs : VisualGraphEventArgs = new VisualGraphEventArgs();
                    eventArgs.Owner(this);
                    const selectedNodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();

                    for (let i : number = 0; i < this.groupNum; i++) {
                        const innerPanel : BasePanel = this.accordion.getPanel(i);
                        innerPanel.getChildElements().foreach(($button : KernelNodeButton) : void => {
                            if (Reflection.getInstance().IsInstanceOf($button, KernelNodeButton) && $button.Selected()) {
                                selectedNodes.Add(this.types.getItem($button.Id()));
                            }
                        });
                    }
                    eventArgs.ActiveNode(this.types.getItem(this.anchorNodeButton.Id()));
                    eventArgs.SelectedNodes(selectedNodes);
                    this.getEventsManager().FireEvent(this, EventType.ON_DRAG_CHANGE, eventArgs);
                    this.nodePicked = true;
                } else if (this.isMouseDown && !this.isDragEnabled) {
                    document.body.style.cursor = "not-allowed";
                }
            });
            ElementManager.setCssProperty(this, "pointer-events", "none");
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addRow().FitToParent(FitToParent.FULL)
                .Add(this.addColumn()
                    .Add(this.addRow().HeightOfRow(30, UnitType.PX).StyleClassName("NodesFilterRow")
                        .Add(this.nodesFilter))
                    .Add(this.addRow().HeightOfRow(100, UnitType.PCT)
                        .Add(this.addColumn().Add(this.accordion))));
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        private filterByType($button : KernelNodeButton) : boolean {
            return ObjectValidator.IsEmptyOrNull(this.typeFilter) ||
                (!this.typeFilter.exclude && this.typeFilter.types.Contains(this.types.getItem($button.Id()).Type())
                    || this.typeFilter.exclude && (this.typeFilter.types.IsEmpty()
                        || !this.typeFilter.types.Contains(this.types.getItem($button.Id()).Type())));
        }

        private computeGroupVisibility() : void {
            let isFirstSeparator : boolean = true;
            for (let i : number = 0; i < this.groupNum; i++) {
                const innerPanel : BasePanel = this.accordion.getPanel(i);
                let groupVisible : boolean = false;
                innerPanel.getChildElements().foreach(($button : KernelNodeButton) : boolean => {
                    if (Reflection.getInstance().IsInstanceOf($button, KernelNodeButton) && $button.Visible()) {
                        groupVisible = true;
                        return false;
                    }
                });
                const holder : BasePanelHolder = <BasePanelHolder>innerPanel.Parent();
                if (groupVisible) {
                    ElementManager.Show(holder.Id() + "_PanelEnvelop");
                    if (isFirstSeparator) {
                        ElementManager.Hide(holder.Id() + "_Separator");
                        isFirstSeparator = false;
                    } else {
                        ElementManager.Show(holder.Id() + "_Separator");
                    }
                    const isOpened : boolean = !ObjectValidator.IsEmptyOrNull(this.nameFilter)
                        || ObjectValidator.IsEmptyOrNull(this.userOpenedHolders) || this.userOpenedHolders.Contains(i);
                    if (holder.IsOpened() !== isOpened) {
                        holder.IsOpened(isOpened, true);
                    }
                } else {
                    ElementManager.Hide(holder.Id() + "_PanelEnvelop");
                    ElementManager.Hide(holder.Id() + "_Separator");
                }
            }
            if (this.asyncLoadStatus !== 0) {
                Accordion.recomputeContentSizes(this.accordion);
            } else {
                this.asyncLoadStatus = -1;
            }
            BasePanel.scrollBarVisibilityHandler(this);
        }

        private focusButton($eventArgs : MouseEventArgs, $button : KernelNodeButton) : void {
            if ($eventArgs.NativeEventArgs().shiftKey && !ObjectValidator.IsEmptyOrNull(this.lastSelectedButton)) {
                this.selectedButtons.Clear();
                let currStatus = false;

                for (let i : number = 0; i < this.groupNum; i++) {
                    const groupPanel : BasePanel = this.accordion.getPanel(i);
                    const holder : BasePanelHolder = <BasePanelHolder>groupPanel.Parent();
                    groupPanel.getChildElements().foreach(($element : IGuiCommons) : boolean => {
                        const element : KernelNodeButton = <KernelNodeButton>$element;
                        if (Reflection.getInstance().IsInstanceOf(element, KernelNodeButton)) {
                            let status : boolean = currStatus;
                            if ($button === this.lastSelectedButton) {
                                this.selectedButtons.Add(element, element.Id());
                                return false;
                            }
                            if (element === this.lastSelectedButton || element === $button) {
                                currStatus = !currStatus;
                                if (currStatus) {
                                    status = currStatus;
                                }
                            }

                            if (holder.IsOpened() && element.Visible() && ElementManager.IsVisible(holder.Id() + "_PanelEnvelop")) {
                                element.Selected(status);
                                if (status) {
                                    this.selectedButtons.Add(element, element.Id());
                                }
                            }
                        }
                    });
                }
            } else {
                if (!$eventArgs.NativeEventArgs().ctrlKey && !$button.Selected()) {
                    this.DeselectAll();
                }
                this.selectButton($button);
            }
            this.getEventsManager().FireEvent(this, EventType.ON_FOCUS);
        }

        private selectButton($button : KernelNodeButton, $value : boolean = true) : void {
            $button.Selected($value);
            if ($value) {
                this.selectedButtons.Add($button, $button.Id());
                this.lastSelectedButton = $button;
            } else {
                this.selectedButtons.RemoveAt(this.selectedButtons.IndexOf($button));
                this.lastSelectedButton = null;
            }

        }
    }
}
