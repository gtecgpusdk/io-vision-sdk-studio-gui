/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import CropBox = Io.VisionSDK.UserControls.BaseInterface.Components.CropBox;
    import CropBoxEventArgs = Com.Wui.Framework.Gui.Events.Args.CropBoxEventArgs;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ScrollEventArgs = Com.Wui.Framework.Gui.Events.Args.ScrollEventArgs;
    import Borders = Com.Wui.Framework.Gui.Structures.Borders;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import CursorType = Com.Wui.Framework.Gui.Enums.CursorType;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IResizeBar = Com.Wui.Framework.Gui.Interfaces.Components.IResizeBar;
    import AreaManager = Io.VisionSDK.UserControls.Utils.AreaManager;
    import IAreaStrategy = Io.VisionSDK.UserControls.Interfaces.IAreaStrategy;
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import NumberPicker = Io.VisionSDK.UserControls.BaseInterface.UserControls.NumberPicker;
    import ControlPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.ControlPanelViewer;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IBounds = Io.VisionSDK.UserControls.Interfaces.IBounds;
    import CropBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.CropBoxType;
    import AreaPanelType = Io.VisionSDK.Studio.Gui.BaseInterface.Enums.AreaPanelType;
    import IAreaPanelType = Io.VisionSDK.Studio.Gui.Interfaces.IAreaPanelType;

    export class AreaPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public controlPanel : ControlPanel;
        private defaultStrategy : IAreaStrategy;
        private readonly selectorCropBox : CropBox;
        private isEventsEnabled : boolean;
        private isAreaScrollEnabled : boolean;
        private readonly topResizeBar : ResizeBar;
        private readonly leftResizeBar : ResizeBar;
        private readonly topLeftResizeBar : ResizeBar;
        private readonly topRightResizeBar : ResizeBar;
        private readonly rightResizeBar : ResizeBar;
        private readonly bottomResizeBar : ResizeBar;
        private readonly bottomLeftResizeBar : ResizeBar;
        private readonly bottomRightResizeBar : ResizeBar;
        private isResizeBarActive : boolean;
        private dimensionsIndicator : Label;
        private startCropOffset : ElementOffset;
        private isCtrlDown : boolean;
        private isMouseDown : boolean;
        private isAreaDragActive : boolean;
        private guiType : IAreaPanelType;
        private selectionBounds : IBounds;
        private borders : Borders;
        private isAreaSelectEnabled : boolean;

        constructor($args : AreaPanelViewerArgs, $id? : string) {
            super($id);
            this.controlPanel = <ControlPanel>this.addChildPanel(this.getControlPanelViewerClass(), null);
            this.controlPanel.Height(34);

            const labelClass : any = this.getLabelClass();
            const cropBoxClass : any = this.getCropBoxClass();
            const resizeBarClass : any = this.getResizeBarClass();

            this.dimensionsIndicator = new labelClass();
            this.selectorCropBox = new cropBoxClass();

            this.topLeftResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL, CursorType.NWSE_RESIZE);
            this.topLeftResizeBar.DisableAsynchronousDraw();
            this.topResizeBar = new resizeBarClass(ResizeableType.VERTICAL);
            this.topResizeBar.DisableAsynchronousDraw();
            this.topRightResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL, CursorType.NESW_RESIZE);
            this.topRightResizeBar.DisableAsynchronousDraw();

            this.leftResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL);
            this.leftResizeBar.DisableAsynchronousDraw();
            this.rightResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL);
            this.rightResizeBar.DisableAsynchronousDraw();

            this.bottomLeftResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL, CursorType.NESW_RESIZE);
            this.bottomLeftResizeBar.DisableAsynchronousDraw();
            this.bottomResizeBar = new resizeBarClass(ResizeableType.VERTICAL);
            this.bottomResizeBar.DisableAsynchronousDraw();
            this.bottomRightResizeBar = new resizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL, CursorType.NWSE_RESIZE);
            this.bottomRightResizeBar.DisableAsynchronousDraw();

            this.defaultStrategy = new AreaManager({
                backgroundId       : this.Id() + "_AreaBackground",
                collapseAreaId     : this.Id() + "_CollapseArea",
                dimensionsIndicator: this.dimensionsIndicator,
                envelopId          : this.Id() + "_AreaEnvelop",
                expandAreaId       : this.Id() + "_ExpandArea"
            });

            this.isEventsEnabled = true;
            this.isAreaScrollEnabled = true;
            this.isAreaSelectEnabled = true;
            this.isResizeBarActive = false;
            this.isCtrlDown = false;
            this.isMouseDown = false;
            this.isAreaDragActive = false;
            this.startCropOffset = new ElementOffset();
            this.selectionBounds = null;

            this.GuiType({
                childTypes: {
                    controlPanelType: this.controlPanel.GuiType(),
                    cropBoxType     : CropBoxType.ORANGE
                },
                type      : AreaPanelType.ORANGE
            });
        }

        public IsMouseDown() : boolean {
            return this.isMouseDown;
        }

        public IsCtrlDown() : boolean {
            return this.isCtrlDown;
        }

        public getSelectionBounds() : IBounds {
            return this.selectionBounds;
        }

        public EnableMouseEvents($value : boolean) {
            if ($value) {
                ElementManager.ClearCssProperty(this, "pointer-events");
            } else {
                ElementManager.setCssProperty(this, "pointer-events", "none");
            }
        }

        public IsEventsEnabled($value? : boolean) : boolean {
            return this.isEventsEnabled = Property.Boolean(this.isEventsEnabled, $value);
        }

        public IsAreaScrollEnabled($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.isAreaScrollEnabled = $value;
                if (!this.isAreaScrollEnabled) {
                    const strategy : IAreaStrategy = this.getAreaStrategy();
                    strategy.ToggleScroll();
                }
            }
            return this.isAreaScrollEnabled;
        }

        public IsAreaSelectEnabled($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.isAreaSelectEnabled = $value;
                if (!this.isAreaSelectEnabled) {
                    this.selectorCropBox.Visible(false);
                }
            }
            return this.isAreaSelectEnabled;
        }

        public IsSelectorActive() : boolean {
            return this.selectorCropBox.Visible();
        }

        public GuiType($areaPanelType? : IAreaPanelType) : any {
            if (ObjectValidator.IsSet($areaPanelType)) {
                this.guiType = $areaPanelType;
                this.controlPanel.GuiType(this.guiType.childTypes.controlPanelType);

                this.selectorCropBox.GuiType(this.guiType.childTypes.cropBoxType);

                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_ViewEnvelop", this.guiType.type);
                }
            }
            return this.guiType;
        }

        protected innerCode() : IGuiElement {
            this.subscribeResizeEvents();
            this.dimensionsIndicator.Visible(true);
            this.selectorCropBox.Visible(false);
            this.selectorCropBox.Enabled(false);
            this.selectorCropBox.EnvelopOwner(this);

            const updateView : any = () : void => {
                if (this.IsCompleted()) {
                    this.controlPanel.Width(this.Width());
                    const size : Size = new Size();
                    size.Width(this.Width() - this.borders.getWidth());
                    size.Height(this.Height() - this.borders.getHeight() - this.controlPanel.Height());
                    this.getAreaStrategy().setViewSize(size);
                }
            };

            this.getEvents().setBeforeResize(() : void => {
                updateView();
            });
            this.getEvents().setOnComplete(() : void => {
                this.borders = new Borders(this.Id() + "_ViewEnvelop");
                updateView();
                this.getAreaStrategy().ResetAreaPosition();
            });

            this.controlPanel.getEvents().setOnReset(() => {
                const strategy : IAreaStrategy = this.getAreaStrategy();
                this.controlPanel.Zoom(this.controlPanel.ZoomConfiguration().defaultValue);
                strategy.setZoomPercentage(this.controlPanel.ZoomConfiguration().defaultValue, false);
                strategy.ResetAreaPosition();
            });

            this.controlPanel.getEvents().setOnZoomChange(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                const strategy : IAreaStrategy = this.getAreaStrategy();
                strategy.setZoomPercentage(this.controlPanel.Zoom(), !$manager.IsActive(ImageButton) &&
                    !$manager.IsActive(NumberPicker) && strategy.IsViewHovered());
                this.onZoomChangeHandler(this.controlPanel.Zoom());
            });

            WindowManager.getEvents().setOnMove(($eventArgs : MoveEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    if ($panel.IsCompleted() && $panel.Visible() && $panel.isEventsEnabled) {
                        const strategy : IAreaStrategy = $panel.getAreaStrategy();
                        const panelOffset : ElementOffset = $panel.getScreenPosition();
                        strategy.setMouseViewPosition({
                            x: $eventArgs.getPositionX() - panelOffset.Left() - this.borders.Left(),
                            y: $eventArgs.getPositionY() - panelOffset.Top() - $panel.controlPanel.Height() - this.borders.Top()
                        });
                        const areaMove : IVector = strategy.getMouseAreaMove();
                        if (areaMove.x !== 0 || areaMove.y !== 0) {
                            const viewMove : IVector = strategy.getMouseLastViewMove();
                            $panel.onMouseMoveCompleteHandler(viewMove);
                        }
                    }
                });
            });

            WindowManager.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    const strategy : IAreaStrategy = $panel.getAreaStrategy();
                    if ($panel.IsCompleted() && $panel.IsHovered() && strategy.IsViewHovered() && $panel.isEventsEnabled) {
                        $panel.onMouseDownHandler($eventArgs);
                    }
                });
            });

            WindowManager.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    if ($panel.IsCompleted()) {
                        this.enableResizeBars(true);
                        $panel.isMouseDown = false;
                        $panel.isAreaDragActive = false;
                        if (document.body.style.cursor === "move") {
                            document.body.style.cursor = "default";
                        }
                        if ($panel.Visible() && $panel.isEventsEnabled) {
                            $panel.onMouseUpHandler($eventArgs);
                        }
                    }
                });
            });

            WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    if ($panel.IsCompleted() && $panel.Visible() && $panel.isEventsEnabled &&
                        !$manager.IsActive(FormInput) && !$manager.IsActive(TextField)) {
                        $panel.onKeyDownHandler($eventArgs);
                    }
                });
            });

            WindowManager.getEvents().setOnKeyUp(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    if ($panel.IsCompleted() && $panel.Visible() && $panel.isEventsEnabled) {
                        $panel.onKeyUpHandler($eventArgs);
                    }
                });
            });

            WindowManager.getEvents().setOnScroll(($eventArgs : ScrollEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    if ($panel.IsCompleted() && $panel.Visible() && $panel.IsHovered() && $panel.isEventsEnabled) {
                        const strategy : IAreaStrategy = $panel.getAreaStrategy();
                        $panel.getEventsManager().FireAsynchronousMethod(() : void => {
                            if (strategy.IsViewHovered()) {
                                $panel.controlPanel
                                    .Zoom($panel.controlPanel.Zoom() + ($eventArgs.DirectionType() === DirectionType.UP ?
                                        1 : -1) * this.controlPanel.ZoomConfiguration().valueStep);
                            }
                        });
                    }
                });
            });

            this.selectorCropBox.getEvents().setOnResize(($eventArgs : CropBoxEventArgs) : void => {
                if (this.selectorCropBox.Visible()) {
                    const offsetLeft : number = Math.max($eventArgs.OffsetLeft(), 0);
                    const offsetTop : number = Math.max($eventArgs.OffsetTop(), 0);
                    const areaPos : IVector = this.getAreaStrategy().getAreaPosition();
                    this.selectionBounds = {
                        maxX: offsetLeft + $eventArgs.Width() - areaPos.x,
                        maxY: offsetTop - this.controlPanel.Height() + $eventArgs.Height() - areaPos.y,
                        minX: offsetLeft - areaPos.x,
                        minY: offsetTop - this.controlPanel.Height() - areaPos.y
                    };
                    this.onSelectChangeHandler(this.selectionBounds);
                }
            });

            this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    $manager.setHovered(this, true);
                }, 10);
            });

            this.getEvents().setOnMouseUp(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    $manager.setHovered(this, true);
                }, 10);
            });

            return super.innerCode();
        }

        protected onMouseMoveCompleteHandler($move : IVector) : void {
            const strategy : IAreaStrategy = this.getAreaStrategy();
            const areaPosition : IVector = strategy.getMouseViewPosition();
            if (this.isMouseDown && !this.getGuiManager().IsActive(ResizeBar) && this.isAreaSelectEnabled) {
                const topOffset : number = this.controlPanel.Height();
                if (!this.selectorCropBox.Visible()) {
                    this.startCropOffset.Top(areaPosition.y + topOffset);
                    this.startCropOffset.Left(areaPosition.x);
                    this.selectorCropBox.Visible(true);
                    this.selectorCropBox.setDimensions(this.startCropOffset.Left(), this.startCropOffset.Top(),
                        this.startCropOffset.Left(), this.startCropOffset.Top());
                } else {
                    const left : number = Math.min(areaPosition.x, this.startCropOffset.Left());
                    const top : number = Math.min(areaPosition.y + topOffset, this.startCropOffset.Top());
                    const right : number = Math.max(areaPosition.x, this.startCropOffset.Left());
                    const bottom : number = Math.max(areaPosition.y + topOffset, this.startCropOffset.Top());
                    this.selectorCropBox.setDimensions(left, top, Math.max(left, right), Math.max(top, bottom));
                }
            } else if (this.isAreaDragActive) {
                if (document.body.style.cursor !== "move") {
                    document.body.style.cursor = "move";
                }
                const areaPos : IVector = strategy.getAreaPosition();
                strategy.setAreaPosition({
                    x: areaPos.x + $move.x,
                    y: areaPos.y + $move.y
                });
            }
        }

        protected onMouseDownHandler($eventArgs : MouseEventArgs) : void {
            const strategy : IAreaStrategy = this.getAreaStrategy();
            switch ($eventArgs.NativeEventArgs().which) {
            case 1:
                this.isMouseDown = true;
                break;
            case 3:
                strategy.setMouseViewPosition(strategy.getMouseViewPosition(), true);
                this.isAreaDragActive = true;
                this.enableResizeBars(false);
                break;
            }
        }

        protected onMouseUpHandler($eventArgs : MouseEventArgs) : void {
            if (this.isResizeBarActive) {
                this.onResizeCompleteHandler();
            } else if (this.selectorCropBox.Visible() && this.isAreaSelectEnabled) {
                this.selectorCropBox.Visible(false);
                this.onSelectCompleteHandler(this.selectionBounds);
                this.selectionBounds = null;
            }
        }

        protected onKeyDownHandler($eventArgs : KeyEventArgs) : void {
            const strategy : IAreaStrategy = this.getAreaStrategy();
            this.isCtrlDown = $eventArgs.NativeEventArgs().ctrlKey;
            switch ($eventArgs.getKeyCode()) {
            case KeyMap.SPACE:
                this.isAreaDragActive = true;
                document.body.style.cursor = "move";
                break;
            case KeyMap.ESC:
                this.isMouseDown = false;
                break;
            case KeyMap.ZERO:
            case 96: // numpad zero
                if (this.isCtrlDown) {
                    $eventArgs.PreventDefault();
                    strategy.setAreaPosition({x: 0, y: 0});
                    this.controlPanel.Zoom(this.controlPanel.ZoomConfiguration().defaultValue);
                }
                break;
            case KeyMap.PLUS:
            case 107: // numpad plus
                if (this.isCtrlDown) {
                    $eventArgs.PreventDefault();
                    this.controlPanel.Zoom(this.controlPanel.Zoom() + this.controlPanel.ZoomConfiguration().valueStep);
                }
                break;
            case KeyMap.MINUS:
            case 109: // numpad minus
                if (this.isCtrlDown) {
                    $eventArgs.PreventDefault();
                    this.controlPanel.Zoom(this.controlPanel.Zoom() - this.controlPanel.ZoomConfiguration().valueStep);
                }
                break;
            }
        }

        protected onKeyUpHandler($eventArgs : KeyEventArgs) : void {
            this.isCtrlDown = $eventArgs.NativeEventArgs().ctrlKey;
            switch ($eventArgs.getKeyCode()) {
            case KeyMap.SPACE:
                this.isAreaDragActive = false;
                document.body.style.cursor = "default";
                break;
            }
        }

        protected onSelectChangeHandler($bounds : IBounds) : void {
            // overwrite to handle
        }

        protected onSelectCompleteHandler($bounds : IBounds) : void {
            // overwrite to handle
        }

        protected onResizeStartHandler() : void {
            // overwrite to handle
        }

        protected onResizeCompleteHandler() : void {
            const strategy : IAreaStrategy = this.getAreaStrategy();
            strategy.ApplyAreaResize();
            strategy.ToggleScroll();
        }

        protected onZoomChangeHandler($zoom : number) : void {
            // overwrite to handle
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_ViewEnvelop")
                .GuiTypeTag("ViewEnvelop")
                .StyleClassName(this.GuiType().type)
                .Add(this.controlPanel)
                .Add(this.addElement(this.Id() + "_AreaEnvelop")
                    .GuiTypeTag("AreaEnvelop")
                    .StyleClassName(GeneralCssNames.OFF)
                    .Add(this.addElement(this.Id() + "_AreaBackground")
                        .StyleClassName("AreaBackground")
                    )
                    .Add(this.addElement(this.Id() + "_ExpandArea")
                        .StyleClassName("ExpandArea")
                        .Add(this.addElement(this.Id() + "_DimensionsIndicator")
                            .StyleClassName("DimensionsIndicator")
                            .Add(this.dimensionsIndicator)
                        )
                    )
                    .Add(this.addElement(this.Id() + "_CollapseArea")
                        .StyleClassName("CollapseArea"))
                    .Add(this.getAreaElement())
                    .Add(this.addElement(this.Id() + "_ResizeBars")
                        .StyleClassName("ResizeBars")
                        .Add(this.topLeftResizeBar)
                        .Add(this.topResizeBar)
                        .Add(this.topRightResizeBar)
                        .Add(this.rightResizeBar)
                        .Add(this.bottomRightResizeBar)
                        .Add(this.bottomResizeBar)
                        .Add(this.bottomLeftResizeBar)
                        .Add(this.leftResizeBar)
                    )
                )
                .Add(this.selectorCropBox);
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected getAreaStrategy() : IAreaStrategy {
            return this.defaultStrategy;
        }

        protected getControlPanelViewerClass() : any {
            return ControlPanelViewer;
        }

        protected getCropBoxClass() : any {
            return CropBox;
        }

        protected getLabelClass() : any {
            return Label;
        }

        protected getResizeBarClass() : any {
            return ResizeBar;
        }

        protected getAreaElement() : IGuiElement {
            return undefined;
        }

        protected IsHovered() : boolean {
            return true;
        }

        private enableResizeBars($value : boolean) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.getResizeBarClass())) {
                [
                    this.topLeftResizeBar, this.topResizeBar, this.topRightResizeBar, this.rightResizeBar,
                    this.bottomRightResizeBar, this.bottomResizeBar, this.bottomLeftResizeBar, this.leftResizeBar
                ].forEach(($resizeBar : ResizeBar) : void => {
                    $resizeBar.Enabled($value);
                });
            }
        }

        private subscribeResizeEvents() : void {
            let mousePos : IVector = null;
            const indicateResize : any = ($eventArgs : ResizeBarEventArgs, $anchor : ElementOffset) : void => {
                const strategy : IAreaStrategy = this.getAreaStrategy();
                if (this.isMouseDown) {
                    if (ObjectValidator.IsEmptyOrNull(mousePos) || !this.isResizeBarActive || !strategy.IsResizeActive()) {
                        mousePos = this.getAreaStrategy().getMouseAreaPosition();
                        this.isResizeBarActive = true;
                    }

                    const handler : any = () : void => {
                        if (this.isResizeBarActive) {
                            const currPos : IVector = this.getAreaStrategy().getMouseAreaPosition();
                            strategy
                                .IndicateAreaResize(new ElementOffset(currPos.y - mousePos.y, currPos.x - mousePos.x), $anchor);
                            strategy.UpdateDimensionsIndicator($anchor);
                        } else {
                            mousePos = null;
                        }
                    };
                    if (this.isAreaScrollEnabled) {
                        const scrollVector : IVector = strategy.getPointScrollVector();
                        strategy.ToggleScroll(() : IVector => {
                            return scrollVector;
                        }, () : void => {
                            handler();
                        }, true);
                    } else {
                        handler();
                    }
                }
            };

            [
                this.topLeftResizeBar, this.topResizeBar, this.topRightResizeBar, this.rightResizeBar,
                this.bottomRightResizeBar, this.bottomResizeBar, this.bottomLeftResizeBar, this.leftResizeBar
            ].forEach(($resizeBar : IResizeBar) : void => {
                $resizeBar.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                    if ($eventArgs.NativeEventArgs().which === 1) {
                        this.onResizeStartHandler();
                    }
                });
            });

            WindowManager.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
                $manager.getType(AreaPanel).foreach(($panel : AreaPanel) : void => {
                    $panel.getEvents().FireAsynchronousMethod(() : void => {
                        $panel.isResizeBarActive = false;
                    }, 10);
                });
            });

            this.topLeftResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(-1, -1));
            });
            this.topResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(-1, 0));
            });
            this.topRightResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(-1, 1));
            });
            this.rightResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(0, 1));
            });
            this.bottomRightResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(1, 1));
            });
            this.bottomResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(1, 0));
            });
            this.bottomLeftResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(1, -1));
            });
            this.leftResizeBar.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
                indicateResize($eventArgs, new ElementOffset(0, -1));
            });
        }
    }
}
