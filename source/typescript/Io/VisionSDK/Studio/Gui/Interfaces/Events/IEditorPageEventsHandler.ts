/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Interfaces.Events {
    "use strict";
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EditorPageEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.EditorPageEventArgs;

    export interface IEditorPageEventsHandler extends Com.Wui.Framework.Commons.Interfaces.IEventsHandler {
        /* tslint:disable: no-duplicate-variable */
        ($eventArgs : EditorPageEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

        ($eventArgs : EditorPageEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;

        /* tslint:enable */
    }
}
