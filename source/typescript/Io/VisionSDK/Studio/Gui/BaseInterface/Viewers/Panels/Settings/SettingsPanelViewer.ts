/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings {
    "use strict";
    import SettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.SettingsPanel;
    import SettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.SettingsPanelViewerArgs;
    import ToolbarPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.ToolbarPanelViewer;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;

    export class SettingsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : SettingsPanelViewerArgs {
            const args : SettingsPanelViewerArgs = new SettingsPanelViewerArgs();
            args.ToolbarArgs((<any>ToolbarPanelViewer).getTestViewerArgs());
            args.ToolbarArgs().UndoButtonText("Back");
            args.ProjectText("Project");
            args.ConnectionText("Connection");
            args.ToolchainText("Toolchain");
            args.AboutText("About");
            args.SaveText("Save");
            args.CancelText("Cancel");
            args.MenuHeaderText("Menu");
            args.DescriptionHeaderText("Description");
            args.ProjectSettingsPanelArgs((<any>ProjectSettingsPanelViewer).getTestViewerArgs());
            args.ConnectionSettingsPanelArgs((<any>ConnectionSettingsPanelViewer).getTestViewerArgs());
            args.ToolchainSettingsPanelArgs((<any>ToolchainSettingsPanelViewer).getTestViewerArgs());
            args.AboutPanelArgs((<any>AboutSettingsPanelViewer).getTestViewerArgs());
            return args;
        }

        /* dev:end */

        constructor($args? : SettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new SettingsPanel());
        }

        public getInstance() : SettingsPanel {
            return <SettingsPanel>super.getInstance();
        }

        public ViewerArgs($args : SettingsPanelViewerArgs) : SettingsPanelViewerArgs {
            return <SettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : SettingsPanel, $args : SettingsPanelViewerArgs) : void {
            $instance.toolbar.Value($args.ToolbarArgs());
            $instance.toolbar.undoButton.Title().Text($args.BackText());
            $instance.menuHeader.Text($args.MenuHeaderText());
            $instance.descriptionHeader.Text($args.DescriptionHeaderText());
            $instance.menuButtons.aboutButton.Text($args.AboutText());
            $instance.menuButtons.toolchainButton.Text($args.ToolchainText());
            $instance.menuButtons.connectionButton.Text($args.ConnectionText());
            $instance.menuButtons.projectButton.Text($args.ProjectText());
            $instance.projectSettingsPanel.Value($args.ProjectSettingsPanelArgs());
            $instance.connectionSettingsPanel.Value($args.ConnectionSettingsPanelArgs());
            $instance.toolchainSettingsPanel.Value($args.ToolchainSettingsPanelArgs());
            $instance.aboutSettingsPanel.Value($args.AboutPanelArgs());
            $instance.saveButton.Text($args.SaveText());
            $instance.cancelButton.Text($args.CancelText());
        }

        /* dev:start */
        protected testImplementation($instance : SettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(768);
            $instance.EnableBuildSettings(false);

            const sizeHandler : any = () : void => {
                $instance.Width(Math.max(640, WindowManager.getSize().Width() - 60));
                $instance.Height(Math.max(480, WindowManager.getSize().Height() - 160));
            };

            WindowManager.getEvents().setOnResize(() => {
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    sizeHandler();
                });
            });

            $instance.getEvents().setOnComplete(() : void => {
                sizeHandler();
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    $instance.ShowPanel($instance.projectSettingsPanel);
                });
            });
            return "<style>.TestCss .SettingsPanel {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
