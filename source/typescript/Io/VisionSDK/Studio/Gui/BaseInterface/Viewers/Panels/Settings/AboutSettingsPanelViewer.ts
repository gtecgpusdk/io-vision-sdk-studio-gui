/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Settings {
    "use strict";
    import AboutSettingsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Settings.AboutSettingsPanelViewerArgs;
    import AboutSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.AboutSettingsPanel;

    export class AboutSettingsPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : AboutSettingsPanelViewerArgs {
            const args : AboutSettingsPanelViewerArgs = new AboutSettingsPanelViewerArgs();
            args.HeaderText("About");
            args.VersionText("Version: ");
            args.VersionContentsText("6.6.6");
            args.PanelDescription("About panel test description");
            args.TextContentsText("Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu. " +
                "Lorem Ipsum je považováno za standard v této oblasti už od začátku 16. století, kdy dnes neznámý tiskař vzal kusy textu " +
                "a na jejich základě vytvořil speciální vzorovou knihu. Jeho odkaz nevydržel pouze pět století, on přežil " +
                "i nástup elektronické sazby v podstatě beze změny. Nejvíce popularizováno bylo " +
                "Lorem Ipsum v šedesátých letech 20. století, kdy byly vydávány speciální vzorníky s jeho pasážemi a později pak díky " +
                "počítačovým DTP programům jako Aldus PageMaker.");
            return args;
        }

        /* dev:end */

        constructor($args? : AboutSettingsPanelViewerArgs) {
            super($args);
            this.setInstance(new AboutSettingsPanel());
        }

        public getInstance() : AboutSettingsPanel {
            return <AboutSettingsPanel>super.getInstance();
        }

        public ViewerArgs($args : AboutSettingsPanelViewerArgs) : AboutSettingsPanelViewerArgs {
            return <AboutSettingsPanelViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : AboutSettingsPanel, $args : AboutSettingsPanelViewerArgs) : void {
            $instance.headerText = $args.HeaderText();
            $instance.versionLabel.Text($args.VersionText());
            $instance.versionContents.Text($args.VersionContentsText());
            $instance.textContents.Text($args.TextContentsText());
            $instance.panelDescription = $args.PanelDescription();
        }

        /* dev:start */
        protected testImplementation($instance : AboutSettingsPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(500);
            $instance.Height(500);
            return "<style>.TestCss .AboutSettingsPanel {border: 1px solid #E9E0E9;}</style>";
        }

        /* dev:end */
    }
}
