/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import LayerSelectorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.LayerSelectorPanelViewerArgs;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class LayerSelectorPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public propertyViewSelector : Button;
        public hierarchyViewSelector : Button;
        public mixedViewSelector : Button;
        private selectedType : IconType;

        constructor($id? : string) {
            super($id);

            this.propertyViewSelector = new Button(ButtonType.TRANSPARENT);
            this.hierarchyViewSelector = new Button(ButtonType.TRANSPARENT);
            this.mixedViewSelector = new Button(ButtonType.TRANSPARENT);
        }

        public Value($value? : LayerSelectorPanelViewerArgs) : LayerSelectorPanelViewerArgs {
            return <LayerSelectorPanelViewerArgs>super.Value($value);
        }

        public getSelectedType() : IconType {
            return this.selectedType;
        }

        protected innerCode() : IGuiElement {
            const handleSelector : any = ($selector : Button) : void => {
                $selector.getEvents().setOnClick(() : void => {
                    this.propertyViewSelector.Selected(false);
                    this.hierarchyViewSelector.Selected(false);
                    this.mixedViewSelector.Selected(false);
                    $selector.Selected(true);
                    this.selectedType = $selector.IconName();
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                });
                $selector.Text(null);
            };
            this.propertyViewSelector.IconName(IconType.PROPERTIES_VIEW);
            this.hierarchyViewSelector.IconName(IconType.HIERARCHY_VIEW);
            this.mixedViewSelector.IconName(IconType.MIXED_VIEW);
            handleSelector(this.propertyViewSelector);
            handleSelector(this.hierarchyViewSelector);
            handleSelector(this.mixedViewSelector);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Add(this.addRow()
                    .FitToParent(FitToParent.FULL)
                    .Add(this.mixedViewSelector)
                    .Add(this.propertyViewSelector)
                    .Add(this.hierarchyViewSelector)
                );
        }
    }
}
