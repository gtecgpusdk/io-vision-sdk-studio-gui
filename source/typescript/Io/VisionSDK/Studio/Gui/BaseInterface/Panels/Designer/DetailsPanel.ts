/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import DetailsPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.DetailsPanelViewerArgs;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import PropertiesPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer.PropertiesPanelViewer;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DirectoryBrowserEventArgs = Com.Wui.Framework.Gui.Events.Args.DirectoryBrowserEventArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Accordion = Io.VisionSDK.UserControls.BaseInterface.UserControls.Accordion;
    import AccordionViewer = Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls.AccordionViewer;
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import DirectoryBrowserViewer = Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls.DirectoryBrowserViewer;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import AccordionViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.AccordionViewerArgs;
    import AccordionResizeType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionResizeType;
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class DetailsPanel extends Io.VisionSDK.UserControls.Primitives.BasePanel {
        public accordion : Accordion;
        private readonly panelSizeOffset : number;
        private nodesByName : ArrayList<IKernelNode>;
        private selectedNode : IKernelNode;
        private isRenameEnabled : boolean;
        private graphRoot : IKernelNode;
        private setStructureHandle : number;
        private lastStructureHash : number;

        constructor($id? : string) {
            super($id);

            this.panelSizeOffset = -10;
            this.nodesByName = new ArrayList<IKernelNode>();

            // todo : localization
            const propertiesHolderArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            propertiesHolderArgs.HeaderText("Properties");
            propertiesHolderArgs.DescriptionText("");
            propertiesHolderArgs.BodyViewerClass(PropertiesPanelViewer);
            propertiesHolderArgs.PrioritySize(new PropagableNumber({number: 200, unitType: UnitType.PCT}));
            propertiesHolderArgs.IsOpened(true);

            const hierarchyHolderArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            hierarchyHolderArgs.HeaderText("Hierarchy");
            hierarchyHolderArgs.DescriptionText("");
            hierarchyHolderArgs.BodyViewerClass(DirectoryBrowserViewer);
            hierarchyHolderArgs.PrioritySize(new PropagableNumber({number: 150, unitType: UnitType.PCT}));
            hierarchyHolderArgs.IsOpened(false);

            const graphInfoPanelArgs : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.VERTICAL);
            graphInfoPanelArgs.ResizeType(AccordionResizeType.RESPONSIVE);
            graphInfoPanelArgs.AddPanelHoldersArgs(propertiesHolderArgs);
            graphInfoPanelArgs.AddPanelHoldersArgs(hierarchyHolderArgs);

            this.addChildPanel(AccordionViewer, graphInfoPanelArgs,
                ($parent : DetailsPanel, $child : Accordion) : void => {
                    $parent.accordion = $child;
                });

            this.selectedNode = null;
            this.isRenameEnabled = true;
            this.setStructureHandle = null;
        }

        public Value($value? : DetailsPanelViewerArgs) : DetailsPanelViewerArgs {
            return <DetailsPanelViewerArgs>super.Value($value);
        }

        public EnableMouseEvents($value : boolean) {
            if ($value) {
                ElementManager.ClearCssProperty(this, "pointer-events");
            } else {
                ElementManager.setCssProperty(this, "pointer-events", "none");
            }
        }

        public UpdateNodeInfo($node : IKernelNode, $isRenameEnabled : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($node)) {
                this.selectedNode = $node;
                this.isRenameEnabled = $isRenameEnabled;
                this.accordion.getPanel(PropertiesPanel).ShowProperties($node, $isRenameEnabled);
            }
        }

        public getSelectedNode() : IKernelNode {
            return this.selectedNode;
        }

        public SelectNode($value : IKernelNode) : void {
            clearTimeout(this.setStructureHandle);
            let path = "/" + $value.Name();
            let parent : IKernelNode = $value.ParentNode();
            while (!ObjectValidator.IsEmptyOrNull(parent)) {
                path = "/" + parent.Name() + path;
                parent = parent.ParentNode();
            }
            this.accordion.getPanel(DirectoryBrowser).Path(path);
        }

        public setProjectStructure($graphRoot : IKernelNode, $delay : number = 0) : void {
            this.graphRoot = $graphRoot;
            const hierarchy : DirectoryBrowser = this.accordion.getPanel(DirectoryBrowser);
            if (hierarchy.Visible()) {
                this.setStructureHandle = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.nodesByName.Clear();
                    const protocol : IFileSystemItemProtocol = this.createFileProtocolFromNode($graphRoot);
                    const hash : number = StringUtils.getCrc(JSON.stringify(protocol));
                    if (this.lastStructureHash !== hash) {
                        hierarchy.setStructure([protocol]);
                        this.lastStructureHash = hash;
                    }
                }, true, $delay);
            }
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(false);
            this.accordion.Scrollable(false);

            this.accordion.AnimIterationNum(6);

            const hierarchy : DirectoryBrowser = this.accordion.getPanel(DirectoryBrowser);

            hierarchy.getEvents().setOnChange(($eventArgs : DirectoryBrowserEventArgs) : void => {
                this.selectedNode = this.nodesByName.getItem($eventArgs.Value().replace(/(.*)\//, ""));
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
            });

            hierarchy.getEvents().setOnShow(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.graphRoot)) {
                    this.setProjectStructure(this.graphRoot);
                }
            });

            hierarchy.getEvents().setOnMouseOver(() : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.setStructureHandle)) {
                    clearInterval(this.setStructureHandle);
                    this.setStructureHandle = null;
                    this.nodesByName.Clear();
                    hierarchy.setStructure([this.createFileProtocolFromNode(this.graphRoot)]);
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addRow().FitToParent(FitToParent.FULL)
                .Add(this.accordion);
        }

        private createFileProtocolFromNode($node : IKernelNode) : IFileSystemItemProtocol {
            const childNodes : IFileSystemItemProtocol[] = [];
            $node.ChildNodes().foreach(($node : IKernelNode) : void => {
                this.nodesByName.Add($node, $node.Name());
                childNodes.push(this.createFileProtocolFromNode($node));
            });
            this.nodesByName.Add($node, $node.Name());
            return <IFileSystemItemProtocol>{
                map : childNodes,
                name: $node.Name(),
                type: FileSystemItemType.READONLY
            };
        }
    }
}
