/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.Designer {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import AreaPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Designer.AreaPanel;
    import AreaPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.AreaPanelViewerArgs;

    export class AreaPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : AreaPanelViewerArgs {
            return new AreaPanelViewerArgs();
        }

        /* dev:end */

        constructor($args? : AreaPanelViewerArgs) {
            super($args);
            this.setInstance(new AreaPanel($args));
        }

        public getInstance() : AreaPanel {
            return <AreaPanel>super.getInstance();
        }

        public ViewerArgs($args? : AreaPanelViewerArgs) : AreaPanelViewerArgs {
            return <AreaPanelViewerArgs>super.ViewerArgs($args);
        }

        /* dev:start */
        protected testImplementation($instance : AreaPanel) : string {
            $instance.StyleClassName("TestCss");
            $instance.Width(1024);
            $instance.Height(768);
            return "<style>.TestCss .AreaPanel {}</style>";
        }

        /* dev:end */
    }
}
