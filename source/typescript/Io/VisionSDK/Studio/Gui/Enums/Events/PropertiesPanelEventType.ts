/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class PropertiesPanelEventType extends EventType {
        public static readonly ON_PROPERTY_CHANGE : string = "OnPropertyChange";
        public static readonly ON_PROPERTY_SELECT : string = "OnPropertySelect";
    }
}
