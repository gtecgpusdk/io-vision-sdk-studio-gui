/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.BaseInterface.Dialogs {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ProjectSettingsPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.Settings.ProjectSettingsPanel;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import ElementEventsManager = Io.VisionSDK.Studio.Gui.Events.ElementEventsManager;
    import ISettingsPanelEvents = Io.VisionSDK.Studio.Gui.Interfaces.Events.ISettingsPanelEvents;
    import SettingsPanelEventArgs = Io.VisionSDK.Studio.Gui.Events.Args.SettingsPanelEventArgs;
    import SettingsPanelEventType = Io.VisionSDK.Studio.Gui.Enums.Events.SettingsPanelEventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;

    export class ProjectCreationPanel extends ProjectSettingsPanel {
        public headerLabel : Label;
        public createButton : Button;

        constructor($id? : string) {
            super($id);
            this.headerLabel = new Label();
            this.createButton = new Button(ButtonType.GREEN_GRAY_MEDIUM);
        }

        public Value($value? : any) : any {
            return <any>super.Value($value);
        }

        public getEvents() : ISettingsPanelEvents {
            return <ISettingsPanelEvents>super.getEvents();
        }

        public HandleButtonVisibility() : void {
            const settings : IProjectSettingsValue = this.getSettings();
            this.createButton.Visible(!(ObjectValidator.IsEmptyOrNull(settings.name) || ObjectValidator.IsEmptyOrNull(settings.vxVersion)
                || ObjectValidator.IsEmptyOrNull(settings.path)));
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.createButton.getEvents().setOnClick(() : void => {
                const args : SettingsPanelEventArgs = new SettingsPanelEventArgs();
                args.Owner(this);
                args.Settings(<ISettings>{projectSettings: this.getSettings()});
                this.getEventsManager().FireEvent(this, SettingsPanelEventType.ON_SETTINGS_UPDATE, args);
            });

            this.getEvents().setOnChange(() : void => {
                this.HandleButtonVisibility();
            });

            const parent : IGuiElement = super.innerCode();
            this.vxVersionOption.Enabled(true);
            return parent;
        }

        protected innerHtml() : IGuiElement {
            return this.addColumn()
                .Add(this.addRow().HeightOfRow(160, UnitType.PX).Alignment(Alignment.LEFT_PROPAGATED)
                    .StyleClassName("Header")
                    .Add(this.addColumn().WidthOfColumn("10%"))
                    .Add(this.headerLabel)
                    .Add(this.addColumn().WidthOfColumn("10%"))
                )
                .Add(this.addRow()
                    .Add(super.innerHtml())
                )
                .Add(this.addRow().HeightOfRow(80, UnitType.PX).FitToParent(FitToParent.FULL)
                    .Add(this.addColumn().WidthOfColumn("10%"))
                    .Add(this.createButton)
                    .Add(this.addColumn().WidthOfColumn("10%"))
                );
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }
    }
}
