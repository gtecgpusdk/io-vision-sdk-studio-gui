/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Gui.HttpProcessor {
    "use strict";

    export class HttpResolver extends Io.VisionSDK.UserControls.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.VisionSDK.Studio.Gui.Index;
            resolvers["/index"] = Io.VisionSDK.Studio.Gui.Index;
            resolvers["/web/"] = Io.VisionSDK.Studio.Gui.Index;
            return resolvers;
        }
    }
}
